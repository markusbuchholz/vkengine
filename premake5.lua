-- Globals
separator = "===================================================================="
PROJECT_PATH = path.getabsolute("./") .. "/"

vulkansdk_path = "libraries/vulkansdk-linux/1.2.154.0/x86_64/"
shader_dir = "Src/GLSL-Shaders/"

-- Find and return a list of all files under the current directory
function dirtree(dir)
    shaders = {}
    local handle = io.popen("find " .. dir .. " -type f -print")
    local shader_string = handle:read("*a")
    for line in shader_string:gmatch("([^\n]*)\n?") do
        table.insert(shaders, line)
    end
    handle:close()
    return shaders
end

-- Split a string
function Split (inputstr, sep)
    local t={}
    for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
            table.insert(t, str)
    end
    return t
end


function IncludeLibraries(prefix)
    -- TODO: change inclusions to not include the libraries directory
    includedirs {prefix .. "Src/Engine/"}
    includedirs {prefix .. "libraries/stb/"}
    includedirs {prefix .. "libraries/glm/"}
    includedirs {prefix .. "libraries/Eigen/"}
    includedirs {prefix .. "libraries/tinygltf/"}
    includedirs {prefix .. "libraries/backward-cpp/"}
    includedirs {prefix .. "libraries/libigl/include/"}
    includedirs {prefix .. "libraries/benchmark/include/"}
    includedirs {prefix .. "libraries/magic_get/include/"}
    includedirs {prefix .. "libraries/VulkanMemoryAllocator/src/"}
    includedirs {prefix .. "libraries/shaderc/libshaderc/include/"}
    includedirs {prefix .. "libraries/googletest/googletest/include/"}
    includedirs {prefix .. "libraries/vulkansdk-linux/1.2.154.0/source/Vulkan-Headers/include/"}
end

function AddStaticLibraries(prefix)
    -- GLFW
	libdirs {prefix .. "libraries/glfw-3.2.1/bin/"}
    links "glfw"
    -- Libunwind
    libdirs {"/usr/local/opt/libunwind-headers/include"}
    links("unwind")
    -- Vulkan SDK
    libdirs {prefix .. vulkansdk_path .. "lib/"}
    links "vulkan"
    -- shaderc
    libdirs {prefix .. "libraries/shaderc/build/libshaderc/"}
    links("shaderc_combined")
    -- Multi threading
    links("pthread")
    -- FreeType
    includedirs {prefix .. "libraries/freetype2/include/"}
    links("freetype")
    -- Harfbuzz
    includedirs {prefix .. "libraries/harfbuzz/"}
    links("harfbuzz")
    -- Cairo
    includedirs {prefix .. "libraries/cairo/"}
    links("cairo")
    -- GoogleTest
    libdirs {prefix .. "libraries/googletest/build/lib/"}
    links("gtest")
    -- GoogleBenchmark
    libdirs {prefix .. "libraries/benchmark/build/src"}
    links("benchmark")
    -- Main one
    libdirs {"build/"}
    links("CoreVulkanEngine")
end

newoption {
    trigger     = "build-extra",
    value       = "path to directory",
    description = "Specifies which example to build",
    allowed = {
        { "tests",  "Build tests" },
        { "benchmarks",  "Build benchmarks" },
        { "all", "Build everything"}
    }
}

newoption {
    trigger     = "path",
    value       = "targets",
    description = "Determine whether additional executables will be built",
}

-- Setup workspace
workspace "VulkanEngine"
configurations { "Debug", "DebugVerbose", "Release"}
-- Output directory for building files
location "Generated"

configuration { "linux", "gmake" }
optimize "Debug"
defines {"VULKAN_HPP_NO_EXCEPTIONS", "VULKAN_HPP_TYPESAFE_CONVERSION"}
---ftime-report
buildoptions {"-std=c++17"}
linkoptions {"-lstdc++fs -O0 -ldl -export-dynamic"}

-- Setup the directory structure
print("\n" .. separator)
print("Creating directories")
print(separator)

print("Transfer subdirectories")
os.execute("mkdir build 2> /dev/null")
os.execute("mkdir build/shaders 2> /dev/null")
os.execute("cp -a Src/GLSL-Shaders/. build/shaders")
os.execute("mkdir build/Assets 2> /dev/null")
os.execute("cp -a Src/Assets/. build/Assets")

IncludeLibraries(PROJECT_PATH)
AddStaticLibraries(PROJECT_PATH)

include("Src/Engine/Engine.lua")

if(_OPTIONS["path"] ~= nil)
then
    tokens = Split(_OPTIONS["path"], "/")
    name = tokens[#tokens]

    include(_OPTIONS["path"] .. name .. ".lua")
    print("cp -a " .. _OPTIONS["path"] .. "shaders" .. ". " .. "build/shaders/" .. name .. "/")
    os.execute("cp -a " .. _OPTIONS["path"] .. "Shaders/" .. ". " .. "build/shaders/" .. name .. "/")
end

if(_OPTIONS["build-extra"] == "tests" or _OPTIONS["build-extra"] == "all")
then
    include("Src/tests/tests.lua")
end

if(_OPTIONS["build-extra"] == "benchmarks" or _OPTIONS["build-extra"] == "all")
then
    include("Src/benchmarks/benchmarks.lua")
end
