import glob
import os
import re
import argparse
from datetime import datetime
from pyparsing import nestedExpr
import shlex
import subprocess
from termcolor import colored
import itertools
import git
from functools import cmp_to_key
from datetime import datetime, timedelta
import dateparser
from prompt_toolkit import prompt
import urwid
import documentation_menu
import shutil
import tempfile

header_text = \
"//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" \
"/**                                                                                     *\n" \
" * @file {}\n" \
" * @author {}\n" \
" * @brief\n" \
" * @version {}\n" \
" * @date {}\n" \
" *\n" \
" * @copyright Copyright (c) 2019\n" \
" *\n" \
" *                                                                                      */\n" \
"//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n" \

author = "Camilo Talero"
version = "0.1"


def MakeParser():
    parser = argparse.ArgumentParser(description='Parse C++ source files and find missing'
        'documentation')
    parser.add_argument('--editor', default='code',
                        help='Editor used to open the files, default is code')
    parser.add_argument('--edit', action='store_true',
                        help="If present, the editor will be opened in each file."
                        " Otherwise a list of undocuemnted files is printed to the"
                        " terminal.")
    parser.add_argument('--dir', default='./Src',
                        help='Path to directory where the files are')

    return parser.parse_args()


def AssertFileHeaders(args):
    # Get all files
    files = []
    for r, d, f in os.walk(args.dir):
        for file in f:
            if '.cpp' in file \
                or '.hpp' in file \
                or '.h' in file:
                files.append(os.path.join(r, file))

    # Fabricate a regular expression that matches the file header
    file_header_regex = r"\/\/\++\n\/\*\*\s+\*\n+"
    keywords = ["file", "author", "brief", "version", "date", "copyright"]
    for keyword in keywords:
        file_header_regex += "(:? \*)*\s*\*\s\@{}\s*.*".format(keyword) + r"\n+"
    file_header_regex += r"\s\*\s*\*.*\n\/\/\++"

    # Assert headers in all cpp files
    for name in files:
        with open(name, 'r') as f:
            content = f.read()
        matches = [x.group() for x in re.finditer(file_header_regex, content)]
        # Modify the file only if there's no header already
        if len(matches) == 0:
            print("\nChanged the following file:\n{}".format(name))
            header = header_text.format(os.path.basename(name), author, version,
                datetime.date(datetime.now()))
            new_content = header + content
            with open(name, 'w') as f:
                f.write(new_content)

    header_files = [file for file in files if '.cpp' not in file]


def GetFilesInDirectory(dir):
    files = []
    for r, d, f in os.walk(dir):
        for file in f:
            if '.cpp' in file or '.hpp' in file or '.h' in file:
                files.append(os.path.join(r,file))

    return files


def OrganizeIncludes(args):
    files = []
    for r, d, f in os.walk(args.dir):
        for file in f:
            if '.cpp' in file or '.hpp' in file or '.h' in file:
                files.append(os.path.join(r, file))

    # Sort headers in all files
    directory_files = GetFilesInDirectory(args.dir)
    for file_path in files:
        with open(file_path, 'r') as f:
            content = f.read()
        # Find includes
        include_regex = r'#include\s+\n*[<,"].*'
        matches = [x.group() for x in re.finditer(include_regex, content)]
        include_list = []
        for match in matches:
            include_list.append(match)

        # Sort includes into 4 categories
        file_header_include = ""
        system_includes = []
        third_party_includes = []
        internal_includes = []
        # Loop over all include files and sort them based on heuristics
        for include in include_list:
            path = include[len('#include'):].replace('<', '').replace('>', '')
            path = path.replace('"', '').strip()
            if os.path.basename(path).split('.')[0] in file_path:
                file_header_include = include
            elif '<' in include:
                system_includes += [include]
            else:
                for check_file_path in directory_files:
                    if path in check_file_path:
                        internal_includes += [include]
                        break

        if file_header_include in include_list:
            include_list.remove(file_header_include)
        for val in system_includes:
            if val in include_list:
                include_list.remove(val)
        for val in internal_includes:
            if val in include_list:
                include_list.remove(val)

        third_party_includes = include_list

        # Find all defines
        define_regex = r'#define\s+(.*\\.*\n)*.*'
        matches = [x.group() for x in re.finditer(define_regex, content)]
        define_list = []
        for match in matches:
            define_list.append(match)

        content = re.sub(define_regex, '', content)

        def CheckLine(line):
            test = '#include' in line
            test = test or  '#define' in line
            test = test or '/** @cond */' in line
            test = test or '/** @endcond */' in line

            if test:
                return True
            return False
        try:
            lines = content.split('\n')
            first_include_line = next(i for i, val in enumerate(lines) if CheckLine(val))
            last_include_line = len(lines) \
                - next(i for i, val in enumerate(reversed(lines)) if CheckLine(val))
        except: pass

        victims = []
        for line in itertools.islice(lines, first_include_line, last_include_line):
            if not CheckLine(line):
                victims += [line]

        new_lines = lines[:first_include_line]
        new_lines += lines[last_include_line:]
        lines = new_lines

        # Sort all include groups
        system_includes.sort()
        third_party_includes.sort()
        internal_includes.sort()

        # Generate sorted inclusions
        new_inclusion_lines = []
        # Defines
        for define in define_list:
            new_inclusion_lines += [define]
        # File header if present
        if file_header_include != '':
            new_inclusion_lines += []
            new_inclusion_lines += [file_header_include]
            new_inclusion_lines += ['']
        # Prevent doxygen from using these headers for graphs
        if system_includes or third_party_includes:
            new_inclusion_lines += ['\n/** @cond */']
        # System libraries
        for include in system_includes:
            new_inclusion_lines += [include]
        if third_party_includes and system_includes:
            new_inclusion_lines += ['']
        # Third party libraries
        for include in third_party_includes:
            new_inclusion_lines += [include]
        # Close condition
        if system_includes or third_party_includes:
            new_inclusion_lines += ['/** @endcond */']
        if internal_includes:
            new_inclusion_lines += ['']
        for include in internal_includes:
            new_inclusion_lines += [include]

        # Add any line that would have been deleted by erasing the lines between headers
        for victim in victims:
            new_inclusion_lines += [victim]

        new_lines = lines[:first_include_line] + new_inclusion_lines
        new_lines += lines[first_include_line:]
        lines = new_lines

        new_content = ""
        for line in lines:
            new_content += line + '\n'

        # Collapse any repeated new lines
        new_content = re.sub(r'\n\n\n+', '\n\n', new_content)
        new_content.replace('\n\n', '\n')

        # Only overwrite the file if there are changes
        if new_content != content:
            with open(file_path, 'w') as f:
                f.write(new_content)


def RecursiveSearchAndReplaceMD(dir_start, pattern, replacement):
    for dname, dirs, files in os.walk(dir_start):
        for fname in files:
            if "md" in fname:
                fpath = os.path.join(dname, fname)
                with open(fpath) as f:
                    s = f.read()
                s = s.replace(pattern, replacement)
                with open(fpath, "w") as f:
                    f.write(s)


def RunDoxygen(args):
    temp_dir = tempfile.gettempdir()
    temp_dir_path = os.path.join(temp_dir, "Docs")
    if(os.path.isdir(temp_dir_path)):
        shutil.rmtree(temp_dir_path)

    shutil.copytree("./Docs", temp_dir_path)
    RecursiveSearchAndReplaceMD("./Docs/", "$", r"\f$")

    command = "doxygen Docs/config.dox"
    process = subprocess.run(shlex.split(command),shell=False, capture_output=True)

    # Restore Docs and remove temporary directory
    shutil.rmtree("./Docs")
    shutil.copytree(temp_dir_path, "./Docs")
    shutil.rmtree(temp_dir_path)

    if process.returncode is None or process.returncode == 2:
        print("Error generating doxygen docs")
        return

    # Dictionary specifying all the undocumented lines at each file
    file_line_dict = dict()
    errors = process.stderr.decode().split('\n')
    for line in errors:
        if '.hpp' in line or '.cpp' in line and ':' in line:
            # Prevent spliting namespaces since they are ususally appended as namespace::
            line = line.replace('::', '!!')
            tokens  = line.split(':')
            file_path = tokens[0]
            line_num = tokens[1]
            error_msg = tokens[-1].replace('!!', '::')
            if file_path in file_line_dict:
                file_line_dict[file_path] += [(line_num, error_msg)]
            else:
                file_line_dict[file_path] = [(line_num, error_msg)]

    # Just print the files
    if not args.edit:
        for file_path in file_line_dict:
            print(colored(f"{file_path}", 'green'))
            undocumented_list = file_line_dict[file_path]
            for data_pair in undocumented_list:
                line, msg = data_pair
                print(colored(f"\t{line}:{msg}", 'blue'))
            print()
        print(f"Found {len(file_line_dict)} files with undocumented code")
        return

    gp = git.Git(args.dir)

    date_regex = r'Date:.*'
    file_line_list = []
    temp = []
    for file_path in file_line_dict:
        loginfo = gp.log('--follow','--',file_path)

        date = [x.group() for x in re.finditer(date_regex, loginfo)]
        date = date[0].split()[1:-1]
        file_line_list.append((' '.join(date), file_path, file_line_dict[file_path]))
        temp.append(' '.join(date))

    def Key(t1):
        return dateparser.parse(t1[0])

    file_line_list.sort(key=Key)

    documentation_menu.Start(args, file_line_list, EditFile)

    return


def EditFile(args, path_line_tuple, button):
    file_path, line = path_line_tuple
    # Open the selected editor at the given line
    editor_command = f"{args.editor} -g {file_path}:{line} -w -n"
    subprocess.call(shlex.split(editor_command))


def RecursiveSearchAndReplaceSvg():
    for dname, dirs, files in os.walk('./Documentation/'):
        for fname in files:
            if "svg" in fname:
                fpath = os.path.join(dname, fname)
                with open(fpath) as f:
                    s = f.read()
                s = re.sub(r'stroke="#0*?"', 'stroke="#ff4301"', s)
                s = re.sub(r'stroke="#191970"', 'stroke="#ff4301"', s)
                s = re.sub(r'stroke="#bfbfbf"', 'stroke="#fa7d09"', s)
                s = re.sub(r'fill="#ddddee"', 'fill="#4a3f35"', s)
                s = re.sub(r'fill="#eeeeff"', 'fill="#4a3f35"', s)
                s = re.sub(r'fill="#0*?"', 'fill="#fa7d09"', s)
                s = re.sub(r'fill="#191970"', 'fill="#ff4301"', s)

                with open(fpath, "w") as f:
                    f.write(s)


def main():
    args = MakeParser()

    AssertFileHeaders(args)
    OrganizeIncludes(args)
    RunDoxygen(args)
    #RecursiveSearchAndReplaceSvg()

    '''src_files = os.listdir('./Docs/images/')
    for file_name in src_files:
        full_file_name = os.path.join('./Docs/images/', file_name)
        if os.path.isfile(full_file_name):
            shutil.copy(full_file_name, './Documentation/html/')'''


if __name__ == "__main__":
    main()