import glob
import os
import re
import argparse
from pyparsing import nestedExpr

cpp_body = \
'#include "{}"\n\n'\
'using namespace std;\n\n'\

hpp_body = \
'#pragma once\n\n'\

def main():
    parser = argparse.ArgumentParser(description='Automatically create a '
        'C++ file and header')

    parser.add_argument('--dir', default='./',
                        help='Path to directory where the files are')

    parser.add_argument('--name', default='example',
                        help='name of the file before extension. e.g "example" '
                        'will create example.hpp and example.cpp')
    args = parser.parse_args()

    prefix = os.path.join(args.dir, args.name)
    cpp_file_str = cpp_body.format(args.name + ".hpp")
    hpp_file_str = hpp_body

    with open(prefix + ".cpp", "x") as f:
        f.write(cpp_file_str)

    with open(prefix + ".hpp", "x") as f:
        f.write(hpp_file_str)


if __name__ == "__main__":
    main()