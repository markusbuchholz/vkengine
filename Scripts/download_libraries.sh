git pull --recurse-submodules

# Unpack the vulkan SDK
wget https://sdk.lunarg.com/sdk/download/1.2.154.0/linux/vulkansdk-linux-x86_64-1.2.154.0.tar.gz?Human=true -O libraries/vulkansdk-linux-x86_64-1.2.154.0.tar.gz
mkdir libraries/vulkansdk-linux
tar -C libraries/vulkansdk-linux -xvf libraries/vulkansdk-linux-x86_64-1.2.154.0.tar.gz
rm libraries/vulkansdk-linux-x86_64-1.2.154.0.tar.gz

# Build google test
mkdir libraries/googletest/build
cd libraries/googletest/build
cmake ..
cd ../../../
make -C libraries/googletest/build/

# Build Google Benchmark
mkdir libraries/benchmark/build/
cd libraries/benchmark/build/
cmake .. -DBENCHMARK_ENABLE_GTEST_TESTS=OFF
cd ../../../
make -C libraries/benchmark/build/
