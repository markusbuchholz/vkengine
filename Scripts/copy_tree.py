import glob
import os
import re
import argparse
from datetime import datetime
from pyparsing import nestedExpr
import shlex
import subprocess
from termcolor import colored
import itertools
import git
from functools import cmp_to_key
from datetime import datetime, timedelta
import dateparser
from prompt_toolkit import prompt
import urwid
import documentation_menu
import shutil
import tempfile


def MakeParser():
    parser = argparse.ArgumentParser(description='Create a copy of a directory tree.')
    parser.add_argument('--source', default='Src/Engine',
                        help='Directory to copy')
    parser.add_argument('--output', default='Src/tests/Engine',
                        help="Destination directory.")
    parser.add_argument('--suffix', default='_test',
                        help='string to append to each generated file')

    return parser.parse_args()


def main():
    args = MakeParser()

    if not args.source[-1] == "/":
        args.source += "/"

    if not args.output[-1] == "/":
        args.output += "/"

    for dname, dirs, files in os.walk(args.source):
        for fname in files:

            base_name, extension = fname.split('.')
            if extension == 'cpp':
                new_base_name = "".join([base_name, args.suffix, ".", extension])

                new_dir_name = dname.replace(args.source, args.output, 1)
                new_file_path = os.path.join(new_dir_name, new_base_name)

                original_header_path = os.path.join(dname, ".".join([base_name, "hpp"]))

                if not os.path.exists(original_header_path):
                    raise Exception("Header couldn't be found")

                if os.path.exists(new_file_path):
                    continue

                try:
                    os.makedirs(new_dir_name)
                except:
                    if not os.path.exists(new_dir_name):
                        raise Exception("Can't create file and it does not exist")

                with open(new_file_path,"w+") as f:
                    f.write(f"#include \"{original_header_path}\"")



if __name__ == "__main__":
    main()