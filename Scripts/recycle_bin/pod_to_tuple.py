
template = \
    'template<class C>\n' \
    'constexpr auto PODToTuple(const Num<{}>&)\n' \
    '{{\n' \
    '    C c;\n' \
    '    auto& [{}] = c;\n' \
    '    return std::make_tuple({});\n' \
    '}}\n' \

final = ''
for num in range(64):
    parameters = [f'a_{i}' for i in range(1, num)]
    string = ','.join(parameters)
    final += template.format(num, string, string) + '\n'

print(final)