import glob
import os
import re
import argparse
import git
import clang.cindex
import inspect
from clang.cindex import *
from termcolor import colored
import subprocess

OUT_FILE = "./tmp/changed_functions"


def MakeParser():
    parser = argparse.ArgumentParser(description='Use git to assert that documentation is updated')
    parser.add_argument('add_flag',
                        help='Flag for git add')

    return parser.parse_args()

#libclang not working
'''def FindComment(path, function_name):
    idx = clang.cindex.Index.create()
    tu = idx.parse(path, args='--std=c++17'.split())
    print(function_name + " !!! ")

    location = None
    for f in tu.cursor.walk_preorder():
        #if f.spelling == function_name:
            location = f.location
            print(f.spelling)

    #print(function_name)
    #assert(location is not None)'''


def CheckAllUpdated(path):
    valid = True
    with open(OUT_FILE, "r") as f:
        for line in f:
            if(line[0:3] != "[O]"):
                print(f"function: {line[4:-1]} has not had its documentation updated.\n"
                        f"Check {os.path.abspath(os.getcwd())}/tmp/.")
                valid = False
    return valid


def CheckDocumentationUpdates(args):
    repo = git.Repo("./")
    t = repo.head.commit.tree
    diffs = repo.git.diff(t, name_only=True).split('\n')

    changed_files_dictionary = dict()

    for file in diffs:
        if not "hpp" in file and not "cpp" in file:
            continue
        if not os.path.exists(file):
            print(f"Warning: {file} seems to have been deleted. Make sure this is intentional.")
            continue
        function_name_regex = r"@@.*@@(.*)\((|{|\n)"
        # Search in the diff of the file
        functions_search = re.findall(function_name_regex, repo.git.diff(file))

        function_names = [x[0].split(' ')[-1] for x in functions_search]
        changed_files_dictionary[file] = function_names

    if(not os.path.exists("./tmp/")):
        os.mkdir("./tmp/")
    elif os.path.exists(OUT_FILE):
        if not CheckAllUpdated(OUT_FILE):
            raise Exception("File documentation has not been updated")

    with open(OUT_FILE, "w") as f:
        function_documentation_status = dict()
        for file in changed_files_dictionary:
            for function in changed_files_dictionary[file]:
                function_documentation_status[function] = False
                if not os.path.isfile(file):
                    continue
                print("".join(["Function ", colored(f"{function}", "green"), " was changed in file ", colored(f"{file}.", "blue")]))
                file = file.replace('.cpp', '.hpp')
                print("".join(["Check the declaration at: ", colored(f"{file} ", "blue"), "and update the documentation."]))

                f.write("".join(f"[x] {function}\n"))


def main():
    args = MakeParser()

    g = git.cmd.Git('./')

    pattern = r'(?:modified:|new file:).*\.(?:cpp|hpp|h)'
    matches = re.findall(pattern, g.status())
    files = []
    for match in matches:
        tokens = match.split(' ')
        files.append(tokens[-1])

    for file in files:
        subprocess.run(["clang-format", file, "-i", "-style=file"])

    #CheckDocumentationUpdates(args)

    g.add(f"-{args.add_flag}")
    #os.remove(OUT_FILE)


if __name__ == "__main__":
    main()