import urwid

# TODO(low): Edit and generalize this file to handle arbitrary menus
class MenuButton(urwid.Button):
    def __init__(self, caption, callback, args=[]):
        super(MenuButton, self).__init__("")
        urwid.connect_signal(self, 'click', callback, user_args=args)
        self._w = urwid.AttrMap(urwid.SelectableIcon(
            [u'  \N{BULLET} ', caption], 2), None, 'selected')

class SubMenu(urwid.WidgetWrap):
    def __init__(self, caption, choices):
        super(SubMenu, self).__init__(MenuButton(
            [caption, u"\N{HORIZONTAL ELLIPSIS}"], self.open_menu))
        line = urwid.Divider(u'\N{LOWER ONE QUARTER BLOCK}')
        listbox = urwid.ListBox(urwid.SimpleFocusListWalker([
            urwid.AttrMap(urwid.Text([u"\n  ", caption]), 'heading'),
            urwid.AttrMap(line, 'line'),
            urwid.Divider()] + choices + [urwid.Divider()]))
        self.menu = urwid.AttrMap(listbox, 'options')

    def open_menu(self, button):
        top.open_box(self.menu)

class Choice(urwid.WidgetWrap):
    def __init__(self, caption, callback, args=[]):
        super(Choice, self).__init__(
            MenuButton(caption, self.item_chosen))
        self.caption = caption
        self.callback = callback
        self.args = args


    def SetParent(self, parent):
        self.parent = parent 

    # TODO(low): Only change the color of the parent when all detected lines have been edited
    def item_chosen(self, button):
        def ChangeColor(*args, **kwargs):
            self._w = urwid.AttrMap(self._w, 'visited')
            if self.parent:
                self.parent._w = urwid.AttrMap(self.parent._w, 'visited')
            self.callback(*args)

        response = urwid.Text([u'Open Editor here?\n', self.caption, u'\n'])
        done = MenuButton(u'Edit', ChangeColor, self.args)
        response_box = urwid.Filler(urwid.Pile([response, done]))
        top.open_box(urwid.AttrMap(response_box, 'options'))  


def exit_program(key):
    raise urwid.ExitMainLoop()

def exit_on_q(key):
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()

palette = [
    (None,  'light gray', 'black'),
    ('heading', 'black', 'light gray'),
    ('line', 'black', 'light gray'),
    ('options', 'dark red', 'black'),
    ('focus heading', 'white', 'dark red'),
    ('focus line', 'black', 'dark red'),
    ('focus options', 'black', 'light gray'),
    ('selected', 'white', 'dark blue'),
    ('visited', 'white', 'dark green')]
focus_map = {
    'heading': 'focus heading',
    'options': 'focus options',
    'line': 'focus line'}


class HorizontalBoxes(urwid.Columns):
    def __init__(self):
        super(HorizontalBoxes, self).__init__([], dividechars=1)

    def open_box(self, box):
        if self.contents:
            del self.contents[self.focus_position + 1:]
        self.contents.append((urwid.AttrMap(box, 'options', focus_map),
            self.options('given', 54)))
        self.focus_position = len(self.contents) - 1

global top
def Start(args, file_map, function):
    file_menus = []
    for date, file_path, line_message_pair_list in file_map:
        title = f'{file_path} ({date})'
        line_menus = []
        for line, message in line_message_pair_list:
            heading = f'{line}: {message}'
            line_menus.append(Choice(heading, function, args=[args, (file_path, line)]))

        sub_menu = SubMenu(title, line_menus)
        for menu in line_menus:
            menu.SetParent(sub_menu)

        file_menus.append(sub_menu)
    
    menu_top = SubMenu(u'Files:', file_menus)

    global top
    top = HorizontalBoxes()
    top.open_box(menu_top.menu)
    urwid.MainLoop(urwid.Filler(top, 'middle', 10), palette, 
        unhandled_input=exit_on_q).run()
