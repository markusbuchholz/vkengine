git submodule add -b master https://github.com/nothings/stb.git libraries/stb/
git submodule add -b master https://github.com/harfbuzz/harfbuzz.git libraries/harfbuzz/
git submodule add -b master https://github.com/google/shaderc.git libraries/shaderc/
git submodule add -b master https://github.com/aseprite/freetype2.git libraries/freetype2/
git submodule add -b master https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator.git libraries/VulkanMemoryAllocator/
git submodule add -b master https://github.com/google/googletest.git libraries/googletest/
git submodule add -b master https://github.com/g-truc/glm.git libraries/glm
git submodule add -b master https://gitlab.com/libeigen/eigen.git libraries/Eigen