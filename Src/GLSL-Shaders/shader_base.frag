#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 modelPosition;
layout(location = 1) in vec2 fragTexCoord;
layout(location = 2) in vec3 modelNormal;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;
layout(binding = 2) uniform sampler2D texSampler2;

layout(binding = 0) uniform UniformBufferObject {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

void main() {
    vec3 screen_position = (ubo.view * vec4(modelPosition, 1.0)).xyz;
    outColor = texture(texSampler2, fragTexCoord);
    //outColor = vec4(vec3(-screen_position.z / (ubo.proj[3][3] - ubo.proj[2][3])), 1.0);
}

