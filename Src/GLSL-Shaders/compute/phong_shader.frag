#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;

layout(binding = 1) uniform CameraInfo {
    vec3 camera_position;
};

vec4 BlinnPhong(vec3 pos, vec3 normal)
{
	vec4 color = vec4(0);
	vec3 l = vec3(1);

    vec3 c = vec3(0,0,0.7);
	vec3 n = normalize(normal);
	vec3 e = camera_position - pos;
	e = normalize(e);
	vec3 h = normalize(e+l);

	color = vec4(c * (vec3(0.1) + 0.9 * max(0, dot(n,l))) +
		vec3(0.9) * max(0,pow(dot(h, n), 100)), 1);

    return color;
}

void main()
{
    color_out = BlinnPhong(position, normal);
}