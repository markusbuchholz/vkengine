#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 1) uniform sampler2D texSampler;

layout(binding = 2) uniform GuiVisualProperties {
    vec4 color;
    int is_transparent;
} ubo;

void main()
{
    if(bool(ubo.is_transparent)) {
        outColor = texture(texSampler, fragTexCoord.xy);
        return;
    }

    outColor = vec4(ubo.color.xyz, 1);
}

