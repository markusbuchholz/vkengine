#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 tex_coord;
layout(location = 2) in vec3 normal;

float ASG(vec3 position, vec3 x, vec3 y, vec3 z, float lambda, float mu)
{
    position = normalize(position);
    float v1 = -lambda * dot(position, x) * dot(position, x);
    float v2 = -mu * dot(position, y) * dot(position, y);
    float exponent = v1 + v2;
    float coeff = max(0, dot(position, z));
    return coeff * exp(exponent);
}

vec3 GetOrthogonal(vec3 v)
{
    float coeff = dot(v, vec3(1,0,0));
    int toggle = int(coeff < 0.3);
    return  (1.f - toggle) * normalize(cross(v, vec3(1,0,0)))
           + (1.f) * normalize(cross(v, vec3(0,0,1)));
}

void main()
{
    float asg1 = ASG(position, vec3(1,0,0), vec3(0,1,0), vec3(0,0,1), 10, 40);

    vec3 lobe2 = normalize(vec3(1));
    vec3 x = GetOrthogonal(lobe2);
    vec3 y = normalize(cross(lobe2, x));
    float asg2 = ASG(position, x, y, lobe2, 40, 10);

    float final = asg1 + asg2;
    color_out = vec4(vec3(final) + 0.01, 1);
}