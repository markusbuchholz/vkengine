/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: line strip
 * @line_width: 10
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(location = 0) out vec3 position;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

void main() {
    gl_PointSize = 10.0;
    vec3 modelPosition = (ubo.model * vec4(inPosition, 1.0)).xyz;
    gl_Position = ubo.proj * ubo.view * vec4(modelPosition, 1.0);
    position = abs(inPosition);
}