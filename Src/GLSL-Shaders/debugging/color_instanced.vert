/**
 * Input Description:
 *
 * @depth_test: false
 * @topology: line list
 * @line_width: 5
*/
#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(location = 1) in vec3 instance_position;
layout(location = 2) in vec4 instance_rotation;
layout(location = 3) in float instance_scale;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
} ubo;

vec4 QuaternionProduct(vec4 q1, vec4 q2)
{
    vec4 q;
    q.x = (q1[0] * q2[1]) + (q1[1] * q2[0]) + (q1[2] * q2[3]) - (q1[3] * q2[2]);
    q.y = (q1[0] * q2[2]) - (q1[1] * q2[3]) + (q1[2] * q2[0]) + (q1[3] * q2[1]);
    q.z = (q1[0] * q2[3]) + (q1[1] * q2[2]) - (q1[2] * q2[1]) + (q1[3] * q2[0]);
    q.w = (q1[0] * q2[0]) - (q1[1] * q2[1]) - (q1[2] * q2[2]) - (q1[3] * q2[3]);

    return q;
}

vec3 QuaterionPointProduct(vec4 q, vec3 p)
{
    vec4 qp = vec4(0, p.x, p.y, p.z);
    return QuaternionProduct(q, qp).xyz;
}

void main()
{
    vec3 r_position = QuaterionPointProduct(instance_rotation, inPosition);
    gl_PointSize = 10.0;
    vec3 modelPosition =
        (ubo.model * vec4(instance_position + r_position, 1.0)).xyz * instance_scale;
    gl_Position = ubo.proj * ubo.view * vec4(modelPosition, 1.0);
}