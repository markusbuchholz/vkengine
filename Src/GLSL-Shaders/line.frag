#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 p1;
layout(location = 1) in vec2 p2;
layout(location = 2) in vec2 frag_pos;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform LineProperties {
    vec4 offset;
    vec4 color;
    float aspect_ratio;
    float line_width;
};

void main() {

    vec2 line_dir = normalize(p2 - p1);
    vec2 proj = dot(frag_pos - p1, line_dir) * line_dir;
    float distance = length((frag_pos - p1) - proj);
    float coeff = distance / line_width;
    float intensity = 1 - smoothstep(0.2, 0.9, coeff);
    outColor = vec4(intensity * color.xyz, intensity);
}