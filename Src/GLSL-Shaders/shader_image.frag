#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 2) uniform PostProcessingEffects {
    uint use_vertical_guassian;
    uint use_blur;
} ubo;

layout(binding = 1) uniform sampler2D texSampler;


float Guassian(int x, int sigma)
{
    return 1.f / (sqrt(2.f * PI) * sigma) * exp(-(x * x) / (2.f * sigma * sigma));
}

void main() {
    vec4 color = vec4(0);
    ivec2 texture_size = textureSize(texSampler, 0).xy;
    ivec2 texel_coord = ivec2(fragTexCoord.x * texture_size.x, fragTexCoord.y * texture_size.y);

    if(bool(ubo.use_blur)) {
        int radius = 5;
        uint orientation = ubo.use_vertical_guassian;
        for(int i = -radius; i <= radius; i++)
        {
            vec2 new_coord = texel_coord + ivec2(i * (1 - orientation), i * orientation);
            new_coord.x /= texture_size.x;
            new_coord.y /= texture_size.y;
            color += texture(texSampler, new_coord) * Guassian(i, radius);
        }
        color.w = 1;
    } else {
        vec2 coord = texel_coord;
        coord.x /= texture_size.x;
        coord.y /= texture_size.y;
        color = texture(texSampler, coord);
    }
    outColor = color;
}