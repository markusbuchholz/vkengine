#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

void main() {
    gl_PointSize = 10.0;
    vec3 modelPosition = (vec4(inPosition, 1.0)).xyz;
    gl_Position = vec4(inPosition.xy, 0.1, 1.0);
}