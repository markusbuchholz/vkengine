#version 450
#extension GL_ARB_separate_shader_objects : enable
#extension GL_EXT_scalar_block_layout : enable

#define PI 3.14159265359

layout(location = 0) out vec4 color_out;

layout(location = 0) in vec2 fragTexCoord;

layout(std430, binding = 0) uniform GaussianVisualization {
    vec2 mu1;
    vec2 mu2;
    mat2 sigma1;
    mat2 sigma2;
};

float Gaussian2D(vec2 x, vec2 mu, mat2 sigma)
{
    float norm = sqrt(
        sigma[0][0] * sigma[0][0] + sigma[0][1] * sigma[0][1] +
        sigma[1][0] * sigma[1][0] + sigma[1][1] * sigma[1][1]);
    vec2 dir = x - mu;
    float exponent = dot(dir, inverse(sigma) * dir);
    return (1.f / sqrt(2.0 * PI * norm)) * exp((-1.f / 2.f) * (exponent));
}

void main() 
{
    float distance = Gaussian2D(fragTexCoord, mu1, sigma1);
    distance = max(distance, Gaussian2D(fragTexCoord, mu2, sigma2));
    
    color_out = vec4(0);//vec4(distance * distance * distance * distance * 100, 0,0,1);
}
