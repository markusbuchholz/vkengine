#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform PointInfo {
    vec4 offset;
    vec4 color;
    int size;
};

void main() {
    vec2 position = gl_PointCoord;
    float distance = length(position - vec2(0.5));
    float delta = fwidth(distance);
    float alpha = 1.0 - smoothstep(0.5 - delta, 0.5 + delta, distance);
    outColor = vec4(color.xyz * alpha,alpha);
}