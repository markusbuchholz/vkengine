#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(binding = 0) uniform PointInfo {
    vec4 offset;
    vec4 color;
    int size;
};

void main() {
    gl_PointSize = size;
    gl_Position = vec4(inPosition.xy + offset.xy, 0, 1.0);
}