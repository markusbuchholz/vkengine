#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(lines) in;
layout(triangle_strip, max_vertices = 6) out;

layout(location = 0) out vec2 fp1;
layout(location = 1) out vec2 fp2;
layout(location = 2) out vec2 frag_pos;

layout(binding = 0) uniform LineProperties {
    vec4 offset;
    vec4 color;
    float aspect_ratio;
    float line_width;
} ubo;

void main() {
    vec2 p1 = gl_in[0].gl_Position.xy;
    vec2 p2 = gl_in[1].gl_Position.xy;
    vec2 tangent = normalize(p2 - p1);
    vec2 normal = vec2(tangent.y, -tangent.x) * ubo.line_width;

    vec2 quad[4] = vec2[](
        p1 + normal, p1 - normal,
        p2 + normal, p2 - normal);

    // Create first triangle
    for(int i=0; i < 3; i++)
    {
        fp1 = p1;
        fp2 = p2;
        gl_Position = vec4(quad[i], 0, 1);
        frag_pos = gl_Position.xy;
        EmitVertex();
    }
    EndPrimitive();

    // Create second triangle
    for(int i=1; i < 4; i++)
    {
        fp1 = p1;
        fp2 = p2;
        gl_Position = vec4(quad[i], 0, 1);
        frag_pos = gl_Position.xy;
        EmitVertex();
    }
    EndPrimitive();
    EndPrimitive();
}