#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;

layout(binding = 0) uniform LineProperties {
    vec4 offset;
    vec4 color;
    float aspect_ratio;
    float line_width;
};

void main() {
    gl_Position = vec4(inPosition.xy + offset.xy, 0, 1.0);
}