#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform sampler2D blur1;
layout(binding = 1) uniform sampler2D blur2;

void main()
{
    outColor = (abs(texture(blur1, fragTexCoord)) - abs(texture(blur2, fragTexCoord)));
    outColor.x = max(0, outColor.x);
    outColor.y = max(0, outColor.y);
    outColor.z = max(0, outColor.z);

    outColor = vec4(vec3(
        abs(outColor.x) + abs(outColor.y) + abs(outColor.z)) * 20, 1
    );
}
