#version 450
#extension GL_ARB_separate_shader_objects : enable

#define PI 3.14159265359

layout(location = 0) in vec2 fragTexCoord;

layout(location = 0) out vec4 outColor;

layout(binding = 0) uniform GaussianProperties {
    int radius;
    uint vertical;
} ubo;

layout(binding = 1) uniform sampler2D texSampler;


float Gaussian(int x, int sigma)
{
    return 1.f / (sqrt(2.f * PI) * sigma) * exp(-(x * x) / (2.f * sigma * sigma));
}

void main() {
    vec4 color = vec4(0);
    ivec2 texture_size = textureSize(texSampler, 0).xy;
    ivec2 texel_coord = ivec2(fragTexCoord.x * texture_size.x, fragTexCoord.y * texture_size.y);

    int radius = ubo.radius;
    uint orientation = ubo.vertical;
    for(int i = -radius; i <= radius; i++)
    {
        vec2 new_coord = texel_coord + ivec2(i * (1 - orientation), i * orientation);
        new_coord.x /= texture_size.x;
        new_coord.y /= texture_size.y;
        color += texture(texSampler, new_coord) * Gaussian(i, radius);
    }
    color.w = 1;

    outColor = color;
}