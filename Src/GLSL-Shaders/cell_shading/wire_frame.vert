#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec2 inTexCoord;
layout(location = 2) in vec3 inNormal;

layout(binding = 0) uniform CellShadingInfo {
    mat4 model;
    mat4 view;
    mat4 proj;
    vec4 view_dir;
    vec3 camera_position;
    float near_plane;
    float far_plane;
} ubo;

void main()
{
    vec3 modelPosition = (ubo.model * vec4(inPosition + inNormal * 0.001, 1.0)).xyz;
    gl_Position = ubo.proj * ubo.view * vec4(modelPosition, 1.0);
}