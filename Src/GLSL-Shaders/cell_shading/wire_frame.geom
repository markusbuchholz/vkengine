#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(triangles) in;
layout(line_strip, max_vertices = 6) out;

void main() {
    vec4 p1 = gl_in[0].gl_Position;
    vec4 p2 = gl_in[1].gl_Position;
    vec4 p3 = gl_in[2].gl_Position;

    gl_Position = p1;
    EmitVertex();
    gl_Position = p2;
    EmitVertex();
    EndPrimitive();

    gl_Position = p2;
    EmitVertex();
    gl_Position = p3;
    EmitVertex();
    EndPrimitive();

    gl_Position = p3;
    EmitVertex();
    gl_Position = p1;
    EmitVertex();
    EndPrimitive();
}