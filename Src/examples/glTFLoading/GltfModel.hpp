#pragma once

#include "GltfLib.hpp"

namespace GltfModel
{

class Model
{
    tinygltf::Model model;
public:
    Model(const std::string& path);
};

} // gltf