#pragma once

#include "tiny_gltf.h"
#include "Eigen/Dense"
#include "Helpers/log.hpp"

namespace Animation {
struct Animation
{
    std::vector<float> time;
    std::vector<Eigen::Quaternionf> rotations;
    uint target_mesh_id = -1;

    Eigen::Quaternionf SampleRotation(float t) const
    {
        t = fmod(t, time.back());
        assert(time.size() > 1);
        assert(time.size() == rotations.size());
        uint index = 0;
        while(!(t >= time[index] && t <= time[index + 1]))
        {
            index++;
        }

        const float v = (t - time[index]) / (time[index + 1] - time[index]);

        return rotations[index].slerp(v, rotations[index + 1]);
    }
};

class AnimationStudio
{
    std::vector<Animation> animations;

public:
    uint AddAnimation(const Animation& animation)
    { animations.push_back(animation); return animations.size() - 1;}

    const std::vector<Animation>& Animations() { return animations; }
};
} // Animation

namespace GltfLib
{
struct Model
{
    tinygltf::Model model;
};
using AttributeMap = std::map<std::string, std::vector<float>>;
using MeshInfo = std::pair<AttributeMap, std::vector<uint>>;
using MeshMap = std::map<uint, MeshInfo>;

GltfLib::Model LoadModel(const std::string& path);

MeshMap ExtractMesh(const tinygltf::Model& model, uint id);

Animation::Animation ExtractAnimation(const tinygltf::Model& model, uint id);
} // GltfLib
