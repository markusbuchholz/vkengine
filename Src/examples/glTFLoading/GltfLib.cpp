#include "GltfLib.hpp"

#define TINYGLTF_IMPLEMENTATION
#define TINYGLTF_NOEXCEPTION
#include "tiny_gltf.h"

using namespace std;

namespace GltfLib
{

namespace
{
template<typename T>
int ComponentCode(){ Log::RecordLogError("Component not recongized"); return -1; }
template<>
constexpr int ComponentCode<float>() { return TINYGLTF_COMPONENT_TYPE_FLOAT; }
template<>
constexpr int ComponentCode<double>() { return TINYGLTF_COMPONENT_TYPE_DOUBLE; }
template<>
constexpr int ComponentCode<char>() { return TINYGLTF_COMPONENT_TYPE_BYTE; }
template<>
constexpr int ComponentCode<unsigned char>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_BYTE; }
template<>
constexpr int ComponentCode<short>() { return TINYGLTF_COMPONENT_TYPE_SHORT; }
template<>
constexpr int ComponentCode<unsigned short>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_SHORT; }
template<>
constexpr int ComponentCode<int>() { return TINYGLTF_COMPONENT_TYPE_INT; }
template<>
constexpr int ComponentCode<unsigned int>() { return TINYGLTF_COMPONENT_TYPE_UNSIGNED_INT; }

//TODO(low): add to this list as cases are encountered
template<typename T>
int TypeCode(){ Log::RecordLogError("Type not recongized"); return -1; }
template<>
constexpr int TypeCode<short>() { return TINYGLTF_TYPE_SCALAR; }
template<>
constexpr int TypeCode<float>() { return TINYGLTF_TYPE_SCALAR; }
template<>
constexpr int TypeCode<Eigen::Vector2f>() { return TINYGLTF_TYPE_VEC2; }
template<>
constexpr int TypeCode<Eigen::Vector3f>() { return TINYGLTF_TYPE_VEC3; }
template<>
constexpr int TypeCode<Eigen::Vector4f>() { return TINYGLTF_TYPE_VEC4; }
template<>
constexpr int TypeCode<Eigen::Matrix2f>() { return TINYGLTF_TYPE_MAT2; }
template<>
constexpr int TypeCode<Eigen::Matrix3f>() { return TINYGLTF_TYPE_MAT3; }
template<>
constexpr int TypeCode<Eigen::Matrix4f>() { return TINYGLTF_TYPE_MAT4; }
template<>
constexpr int TypeCode<std::vector<float>>() { return TINYGLTF_TYPE_VECTOR; }

int TotalElementsInType(const int type)
{
    if(type % 64 == 1) return 1;
    assert(type != TINYGLTF_TYPE_VECTOR && "No idea how to handle these cases.");
    assert(type != TINYGLTF_TYPE_MATRIX && "No idea how to handle these cases.");

    return (type % 32) * (type / 32 + 1);
}

void CopySparseBuffer(
    void *dest,
    const void *src,
    const size_t element_count,
    const size_t stride,
    const size_t type_size)
{
    assert(stride >= type_size);
   // Typecast src and dest addresses to (char *)
   char *csrc = (char *)src;
   char *cdest = (char *)dest;

    // Iterate over the total number of elements to copy
    for (int i=0; i<element_count; i++)
        // Copy each byte of the element. Since the stride could be different from the
        // type size (in the case of padding bytes for example) the left access
        // should skip over any interleaved data, that's why we use the stride.
        for(int j=0; j < type_size; j++)
            *(cdest + i * type_size + j) = *(csrc + i * stride + j);
}

template<typename T>
std::vector<T> ExtractDataFromAccessor(
    const tinygltf::Model& model,
    const int accessor_index)
{
    const int buffer_view_index = model.accessors[accessor_index].bufferView;
    const int array_type = model.accessors[accessor_index].type;
    const int component_type = model.accessors[accessor_index].componentType;
    const int accessor_offset = model.accessors[accessor_index].byteOffset;
    const int element_num = model.accessors[accessor_index].count;

    const int buffer_index = model.bufferViews[buffer_view_index].buffer;
    const int buffer_length = model.bufferViews[buffer_view_index].byteLength;
    const int buffer_offset = model.bufferViews[buffer_view_index].byteOffset;
    const int buffer_stride = model.bufferViews[buffer_view_index].byteStride;

    const std::vector<unsigned char> data = model.buffers[buffer_index].data;

    assert(component_type == ComponentCode<T>() &&
        "Te component type foun here should match that of the type (e.g. float and float).");

    // Size in bytes of a single elemt (e.g. 12 for a vec3 of floats)
    const int type_size = TotalElementsInType(array_type) * sizeof(T);
    assert(buffer_stride == 0 || buffer_stride > sizeof(T) &&
        "It doesn't make sense for a negative buffer stride to be less than the type size");
    const size_t stride = std::max(buffer_stride, type_size);

    std::vector<T> holder(element_num * type_size / sizeof(T));
    CopySparseBuffer(
        holder.data(),
        data.data() + buffer_offset + accessor_offset,
        element_num,
        stride,
        type_size);

    return holder;
}

} // namespace

GltfLib::Model LoadModel(const std::string& path)
{
    tinygltf::Model model;
    tinygltf::TinyGLTF loader;
    std::string err;
    std::string warn;

    bool ret = loader.LoadASCIIFromFile(&model, &err, &warn, path);

    if(!warn.empty()) { Log::RecordLogError(warn); }

    if(!err.empty()) { Log::RecordLogError(err); }

    if(!ret) { Log::RecordLogError("Failed to parse glTF"); }

    return {model};
}

MeshMap ExtractMesh(const tinygltf::Model& model, uint id)
{
    assert(model.meshes[id].primitives.size() == 1);
    auto& mesh = model.meshes[id];
    MeshMap mesh_map;
    for(auto& [attribute, index]: mesh.primitives[0].attributes)
    {
        AttributeMap attribute_map;
        cout << attribute << endl;

        attribute_map[attribute] = ExtractDataFromAccessor<float>(model, index);

        vector<uint> indices;
        const int indices_index = mesh.primitives[0].indices;
        if(indices_index >= 0)
        {
            vector<unsigned short> shorts =
                ExtractDataFromAccessor<unsigned short>(model, indices_index);

            for(unsigned short s: shorts)
                indices.push_back(uint(s));
        }
        mesh_map[id] = {attribute_map, indices};
    }
    return mesh_map;
}

Animation::Animation ExtractAnimation(
    const tinygltf::Model& model, const uint id)
{
    const tinygltf::Animation& raw_animation = model.animations[id];
    const int input = raw_animation.samplers[0].input;
    const int output = raw_animation.samplers[0].output;
    const int node = raw_animation.channels[0].target_node;

    vector<float> time = ExtractDataFromAccessor<float>(model, input);
    vector<float> rot = ExtractDataFromAccessor<float>(model, output);
    Animation::Animation animation = {};
    animation.time = time;
    animation.rotations.resize(rot.size() / 4); // num of elements in a quaternion
    for(uint i = 0; i < animation.rotations.size(); i++)
    {
        // Flip the coordinates here, Eigen likes being a special little boy
        // so it puts the w coordinate at the wrong spot.
        animation.rotations[i] = Eigen::Quaternionf(
            rot[i * 4 + 3],
            rot[i * 4 + 0],
            rot[i * 4 + 1],
            rot[i * 4 + 2]
        );
        assert(i * 4 + 3 < rot.size());
    }
    return animation;
}

} // GltfLib