#include "gltf.hpp"

/** @cond */
#include <array>
#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "MasterEngine/MasterEngine.hpp"
#include "Helpers/TextManipulation.hpp"

#include "GltfModel.hpp"

using namespace tinygltf;

using namespace std;
using namespace Animation;

using namespace GltfLib;

namespace
{
const std::string MODEL_PATH = "Assets/bunny/low_poly_bunny.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";

int global_mesh_index = 0;

}  // namespace

MeshMap LoadGltf(const std::string& path, AnimationStudio& studio)
{
    auto model = GltfLib::LoadModel(path).model;
    MeshMap mesh_map;
    uint id = 0;
    assert(model.meshes.size() == 1);

    // TODO(low): implement this functionality as needed
    mesh_map = ExtractMesh(model, id++);

    if(model.animations.size()) { studio.AddAnimation(ExtractAnimation(model, 0)); }

    return mesh_map;
}

std::pair<std::vector<float>, std::vector<uint>> LoadGLTFMesh(std::any& container)
{
    auto& mesh_map = std::any_cast<MeshMap&>(container);
    assert(mesh_map.size() == 1);
    for(auto& [mesh_name, mesh_info]: mesh_map)
    {
        auto& [attributes, indices] = mesh_info;

        auto& positions = attributes.at("POSITION");
        const uint n = attributes.at("POSITION").size();
        assert(n % 3 == 0 && n > 0);

        std::vector<float> buffer;
        const bool has_uvs = attributes.count("UV") != 0;
        const bool has_normals = attributes.count("NORMAL") != 0;
        assert(!has_uvs);
        assert(!has_normals);

        for(uint i = 0; i < n; i += 3)
        {
            buffer.push_back(positions[i + 0]);
            buffer.push_back(positions[i + 1]);
            buffer.push_back(positions[i + 2]);

            buffer.push_back(0);
            buffer.push_back(0);

            buffer.push_back(0);
            buffer.push_back(1);
            buffer.push_back(0);
        }

        if(indices.empty())
        {
            indices.resize(attributes.at("POSITION").size() / 3);
            std::iota(indices.begin(), indices.end(), 0);
        }

        return {buffer, indices};
    }
    return {};
}

vector<std::string> model_names = {};
uint selected_model = 0;

float time_step = 0.01;
float total_time = 0;
void DrawLoop(MasterEngine* engine, void* ptr)
{
    auto studio = reinterpret_cast<AnimationStudio*>(ptr);

    total_time += time_step;
    Eigen::Quaternionf rotation =
        studio->Animations()[0].SampleRotation(total_time * (selected_model == 2));

    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();

    Eigen::Matrix3f mat3 = rotation.normalized().toRotationMatrix();
    Eigen::Matrix4f mat4 = Eigen::Matrix4f::Identity();
    mat4.block(0, 0, 3, 3) = mat3;
    MVPOnlyUbo mvp = {};
    mvp.model = mat4;
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    mvp.model = mvp.model * 0.2f;

    CameraInfo camera_info = {};
    camera_info.camera_position = camera.GetPosition();

    Gallery& asset_map = rendering_engine.GetGallery();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer(model_names[selected_model]);

    auto& target = rendering_engine.AtomicEffect(
        gpu_buffer, "shaders/debugging/phong_shader", {}, mvp, 0, camera_info, 1);

    rendering_engine.PresentRender(*target.GetColorImage(0));
}

void KeyEvent(void* ptr, int key, KeyActionState action)
{
    if(action == KeyActionState::PRESS && key == GLFW_KEY_LEFT)
    { selected_model = (selected_model + model_names.size() - 1) % model_names.size(); }
    if(action == KeyActionState::PRESS && key == GLFW_KEY_RIGHT)
    { selected_model = (selected_model + 1) % model_names.size(); }
}

const std::vector<std::string> paths = {
    "/home/makogan/glTF-Sample-Models/2.0/TriangleWithoutIndices/glTF/"
    "TriangleWithoutIndices.gltf",
    "/home/makogan/glTF-Sample-Models/2.0/Triangle/glTF/Triangle.gltf",
    "/home/makogan/glTF-Sample-Models/2.0/AnimatedTriangle/glTF/AnimatedTriangle.gltf",
    "/home/makogan/glTF-Sample-Models/2.0/SimpleMorph/glTF/SimpleMorph.gltf"};

int main()
{
    MasterEngine engine;
    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();
    camera.SetRadius(6);

    input_handler.AddEvent(
        MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(KeyEvent, &engine);

    auto& gallery = engine.GetRenderingEngine().GetGallery();

    CpuImage image(TEXTURE_PATH);
    gallery.StoreImageData(image, "statue");

    AnimationStudio studio = {};

    for(const string& path: paths)
    {
        // Get the basename without the extension
        auto path_name = TM::Split(TM::Split(path, '/').back(), '.')[0];
        auto mesh = LoadGltf(path, studio);
        gallery.StoreGeometryData(mesh, path_name, LoadGLTFMesh);
        model_names.push_back(path_name);
    }

    engine.Loop(&studio, DrawLoop);

    return EXIT_SUCCESS;
}
