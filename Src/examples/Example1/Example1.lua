-- Setup project
project "Example1"
    -- Compilation variables
    kind "WindowedApp"
    targetdir ("../../../build/")
    language "C++"

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
    filter "configurations:DebugVerbose"
        defines { "DEBUG", "VERBOSITY_1" }
        symbols "On"
    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

    configuration { "linux", "gmake" }
        optimize "Debug"
        --buildoptions {"-std=c++2a -g -fno-omit-frame-pointer"}
        --linkoptions {"-lstdc++fs -ldl -export-dynamic -lunwind -fsanitize=address"}
        buildoptions {"-std=c++2a"}
        -- -pg Creates the performance file gmon.out
        linkoptions {"-lstdc++fs -O0 -ldl -pg -export-dynamic"}

    files {"**.cpp"}

    includedirs {PROJECT_PATH .. "/Src/Engine/"}
    IncludeLibraries(PROJECT_PATH)
    AddStaticLibraries(PROJECT_PATH)
