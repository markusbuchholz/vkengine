//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file example1.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-21
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "MasterEngine/MasterEngine.hpp"

void Loopy(MasterEngine* engine, void* d);

int example1();

void AddMeshes(VulkanEngine& rendering_engine);

void KeyTemp(void* ptr, KeyActionState action);

void KeyETemp(void* ptr, KeyActionState action);

void KeyState(void *ptr, KeyActionState action);

