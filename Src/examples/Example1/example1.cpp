//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "example1.hpp"

/** @cond */
#include <algorithm>
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <random>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Helpers/EigenHelpers.hpp"
#include "MasterEngine/Geometry/CpuImage.hpp"
#include "MasterEngine/Geometry/HMesh.hpp"
#include "MasterEngine/Geometry/Parametrics.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace
{
const std::string MODEL_PATH = "Assets/bunny/low_poly_bunny.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";

VulkanImage image;
vk::Buffer buffer;
}  // namespace

struct HyperVertex
{
    Eigen::VectorXf position = Eigen::Vector3f(0, 0, 0);
    Eigen::Vector2f uv;
    Eigen::Vector3f normal;
};

// Example -------------------------------------------------------------------------------
std::pair<std::vector<float>, std::vector<uint>> GetFloatsFromFloatVector(std::any& input)
{
    return std::any_cast<std::pair<vector<float>, vector<uint>>&>(input);
}

void SetDebugMesh(Gallery& asset_map, float size = 1.f)
{
    auto& mesh = asset_map.GetCpuGeometry<HMesh<HyperVertex>>("loaded_model_tmp");
    const auto& faces = mesh.Faces();
    vector<float> m_i_d;
    for(const auto& face: faces)
    {
        auto vertices = face.Vertices();
        for(uint i = 0; i < 3; i++)
        {
            m_i_d.push_back(vertices[i]->Data().position[0]);
            m_i_d.push_back(vertices[i]->Data().position[1]);
            m_i_d.push_back(vertices[i]->Data().position[2]);

            m_i_d.push_back(vertices[i]->Data().normal[0]);
            m_i_d.push_back(vertices[i]->Data().normal[1]);
            m_i_d.push_back(vertices[i]->Data().normal[2]);

            m_i_d.push_back(0.3);
            m_i_d.push_back(0);
            m_i_d.push_back(0.3);
        }
    }
    auto model_instance_data = m_i_d;

    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.04 * size, 0.2 * size, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            arrow_vertices.push_back(v.x());
            arrow_vertices.push_back(v.y());
            arrow_vertices.push_back(v.z());
            arrow_vertices.push_back(0);
            arrow_vertices.push_back(0);
            auto normal = normals[i++];
            arrow_vertices.push_back(normal.x());
            arrow_vertices.push_back(normal.y());
            arrow_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "arrow", GetFloatsFromFloatVector);

        asset_map.SetGeometryInstanceData(id, model_instance_data);
    }

    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.02 * size, 15, 15);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> sphere_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            sphere_vertices.push_back(v.x());
            sphere_vertices.push_back(v.y());
            sphere_vertices.push_back(v.z());
            sphere_vertices.push_back(0);
            sphere_vertices.push_back(0);
            auto normal = normals[i++];
            sphere_vertices.push_back(normal.x());
            sphere_vertices.push_back(normal.y());
            sphere_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {
            sphere_vertices, indices};
        uint id =
            asset_map.StoreGeometryData(data, "debug_sphere", GetFloatsFromFloatVector);
        asset_map.SetGeometryInstanceData(id, model_instance_data);
    }

    {
        // cylinder
        auto edges = asset_map.GetCpuGeometry<HMesh<HyperVertex>>("loaded_model_tmp")
                         .EdgeVertices();

        auto [vertices, indices] = GenerateCylinder(0.01 * size, 1.f, 15, 15);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> cylinder_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            cylinder_vertices.push_back(v.x());
            cylinder_vertices.push_back(v.y());
            cylinder_vertices.push_back(v.z());
            cylinder_vertices.push_back(0);
            cylinder_vertices.push_back(0);
            auto normal = normals[i++];
            cylinder_vertices.push_back(normal.x());
            cylinder_vertices.push_back(normal.y());
            cylinder_vertices.push_back(normal.z());
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {
            cylinder_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "cylinder", GetFloatsFromFloatVector);

        vector<Eigen::Vector3f> edge_instance_data;
        edge_instance_data.reserve(model_instance_data.size());
        for(auto& [n1, n2]: edges)
        {
            auto& p1 = n1->Data().position;
            auto& p2 = n2->Data().position;
            const auto v1 = Eigen::Vector3d{p1[0], p1[1], p1[2]};
            const auto v2 = Eigen::Vector3d{p2[0], p2[1], p2[2]};
            edge_instance_data.push_back(v1.cast<float>());
            edge_instance_data.push_back(v2.cast<float>());
            edge_instance_data.push_back(Eigen::Vector3f(0, 0.2, 0.6));
        }
        asset_map.SetGeometryInstanceData(id, edge_instance_data);
    }
}

void InitGaussianSigmas(HMesh<HyperVertex>& mesh)
{
    auto& vertices = mesh.VertexDataD();
    for(auto& vertex: vertices)
    {
        Eigen::VectorXf& position = vertex.position;
        position.conservativeResize(9, 1);
        MatrixXf sigma_inv = MatrixXf::Identity(3, 3);

        position << position[0], position[1], position[2], sigma_inv(0, 0),
            sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
            sigma_inv(2, 2);
    }
}

void SetCovarianceOfVertex(HMesh<HyperVertex>& mesh, uint i, const Eigen::MatrixXf& cov)
{
    auto& vertex = mesh.VertexDataD()[i].position;
    MatrixXf position(1, 3);
    position << vertex[0], vertex[1], vertex[2];
    MatrixXf sigma_inv = cov.inverse();

    vertex << position(0, 0), position(0, 1), position(0, 2), sigma_inv(0, 0),
        sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
        sigma_inv(2, 2);
}

void AddMeshes(VulkanEngine& rendering_engine)
{
    Gallery& asset_map = rendering_engine.GetAssetMap();

    CpuImage image(TEXTURE_PATH);
    asset_map.StoreImageData(image, "statue");

    // Model
    HMesh<HyperVertex> mesh_tmp(MODEL_PATH);
    InitGaussianSigmas(mesh_tmp);
    std::default_random_engine generator;
    std::uniform_int_distribution<int> distribution(0, mesh_tmp.VertexData().size() - 1);
    auto index_selector = std::bind(distribution, generator);

    std::normal_distribution<double> fdistribution(10, 1005.2);
    auto size_selector = std::bind(fdistribution, generator);
    for(uint i = 0; i < 100; i++)
    {
        uint selected = index_selector();
        double size = abs(size_selector());

        MatrixXf covariance = MatrixXf::Identity(3, 3);
        covariance *= size;
        SetCovarianceOfVertex(mesh_tmp, selected, covariance);
    }
    asset_map.StoreGeometryData(mesh_tmp, "loaded_model_tmp");

    // Sphere
    auto [vertices, indices] = GenerateSphere(0.98, 50, 50);
    auto normals = GenerateNormals(vertices, indices);
    vector<float> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
    {
        sphere_vertices.push_back(v.x());
        sphere_vertices.push_back(v.y());
        sphere_vertices.push_back(v.z());
        sphere_vertices.push_back(0);
        sphere_vertices.push_back(0);
        auto normal = normals[i++];
        sphere_vertices.push_back(normal.x());
        sphere_vertices.push_back(normal.y());
        sphere_vertices.push_back(normal.z());
    }

    std::pair<std::vector<float>, std::vector<uint>> data = {sphere_vertices, indices};

    asset_map.StoreGeometryData(data, "sphere", GetFloatsFromFloatVector);

    SetDebugMesh(asset_map);
}

void GaussianSubdivideMesh(HMesh<HyperVertex>& mesh)
{
    for(auto& vertex: mesh.VertexDataD())
    {
        if(vertex.position.cols() == 3)
        {
            VectorXf position = vertex.position;
            MatrixXf sigma_inv = MatrixXf::Identity(3, 3);
            position.conservativeResize(9, 1);
            vertex.position << position(0), position(1), position(2), sigma_inv(0, 0),
                sigma_inv(1, 0), sigma_inv(1, 1), sigma_inv(2, 0), sigma_inv(2, 1),
                sigma_inv(2, 2);
        }
        else
        {
            Eigen::VectorXf& position = vertex.position;
            Vector3f dual_position;
            dual_position << position[0], position[1], position[2];
            MatrixXf sigma_inv(3, 3);
            for(uint k = 0; k < 6; k++)
            {
                uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
                uint y = k - x * (x + 1) / 2;

                sigma_inv(x, y) = position(3 + k);
                sigma_inv(y, x) = sigma_inv(x, y);
            }
            dual_position = sigma_inv * dual_position;
            vertex.position[0] = dual_position[0];
            vertex.position[1] = dual_position[1];
            vertex.position[2] = dual_position[2];
        }
    }
    HMesh<HyperVertex>::LoopSubdivision(mesh);
    for(auto& vertex: mesh.VertexDataD())
    {
        Eigen::VectorXf& position = vertex.position;
        Vector3f dual_position;
        dual_position << position(0), position(1), position(2);
        MatrixXf sigma_inv(3, 3);
        for(uint k = 0; k < 6; k++)
        {
            uint x = (sqrt(1.0 + 8.0 * double(k)) - 1.0) / 2.0;
            uint y = k - x * (x + 1) / 2;

            sigma_inv(x, y) = position(3 + k);
            sigma_inv(y, x) = sigma_inv(x, y);
        }
        dual_position = sigma_inv.inverse() * dual_position;
        vertex.position[0] = dual_position(0);
        vertex.position[1] = dual_position(1);
        vertex.position[2] = dual_position(2);
    }
}

float d_scale = 1.f;
void KeyTemp(void* ptr, int key, KeyActionState action)
{
    if(action == KeyActionState::PRESS && key == GLFW_KEY_SPACE)
    {
        auto engine = reinterpret_cast<MasterEngine*>(ptr);
        auto& asset_map = engine->GetRenderingEngine().GetGallery();
        auto& mesh = asset_map.GetCpuGeometry<HMesh<HyperVertex>>("loaded_model_tmp");
        uint time;
        GaussianSubdivideMesh(mesh);

        asset_map.UpdateGeometryData("loaded_model_tmp");
    }
}

bool toggle = false;
void KeyETemp(void* ptr, int key, KeyActionState action)
{
    if(action == KeyActionState::PRESS && key == GLFW_KEY_E) { toggle = !toggle; }
}

void Loopy(MasterEngine* engine, void* d)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();
    uint width, height;
    rendering_engine.GetWindow()->GetDimensions(&width, &height);
    CellShadingInfo ubo = {};
    ubo.model = Rotate(0.f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    ubo.view = camera.GetViewMatrix();
    ubo.proj = camera.GetProjectionMatrix();

    ubo.camera_position = camera.GetPosition();
    ubo.view_dir = Append(camera.GetLookDirection(), 0.f);
    ubo.far_plane = 5;
    ubo.near_plane = 0.1;

    Gallery& asset_map = rendering_engine.GetGallery();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("loaded_model_tmp");
    GeometryInfo normal_buffer = asset_map.GetGpuGeometryBuffer("arrow");
    GeometryInfo vertex_buffer = asset_map.GetGpuGeometryBuffer("debug_sphere");
    GeometryInfo edge_buffer = asset_map.GetGpuGeometryBuffer("cylinder");

    RenderTarget& target = EffectFramework::CellShadingRender(
        asset_map, effect_framework, gpu_buffer, ubo, *asset_map.GetGpuImage("statue"));

    MVPOnlyUbo mvp = {};
    mvp.model = ubo.model;
    mvp.proj = ubo.proj;
    mvp.view = ubo.view;
    WireframeDebugInfo wire_info = {};
    wire_info.camera_position = ubo.camera_position;
    wire_info.treat_normal_as_point = false;

    EffectFramework::AtomicEffect(
        effect_framework,
        normal_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);
    EffectFramework::AtomicEffect(
        effect_framework,
        vertex_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);
    wire_info.treat_normal_as_point = true;
    EffectFramework::AtomicEffect(
        effect_framework,
        edge_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);

    vk::Result result;
    EffectFramework::PresentRender(
        asset_map, effect_framework, *target.GetColorImage(0), result);
}

int example1()
{
    MasterEngine engine;

    auto extent =
        engine.GetRenderingEngine().GetEffectFramework().GetEffectiveDisplayExtent();
    image = VulkanImage(extent.width, extent.height, 4);

    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
    input_handler.AddEvent(
        MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(KeyTemp, &engine);
    input_handler.AddEvent(KeyETemp, &engine);

    AddMeshes(rendering_engine);
    engine.Loop(nullptr, Loopy);

    return EXIT_SUCCESS;
}
