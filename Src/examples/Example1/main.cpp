//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-21
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "example1.hpp"

int main()
{
    return example1();
}

