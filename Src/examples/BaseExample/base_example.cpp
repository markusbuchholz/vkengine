//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "base_example.hpp"

/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "MasterEngine/Geometry/CpuImage.hpp"
#include "MasterEngine/Geometry/GeometryUtils.hpp"
#include "MasterEngine/Geometry/HMesh.hpp"
#include "MasterEngine/Geometry/HMeshUtils.hpp"
#include "MasterEngine/Geometry/MeshGradient.hpp"
#include "MasterEngine/Geometry/Parametrics.hpp"
#include "MasterEngine/MasterEngine.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace
{
const std::string MODEL_PATH = "Assets/dragon.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";
}  // namespace

// Master Engine implementation ----------------------------------------------------------
void AddMeshes(VulkanEngine& rendering_engine)
{
    Gallery& asset_map = rendering_engine.GetGallery();

    CpuImage image(TEXTURE_PATH);
    asset_map.StoreImageData(image, "statue");

    // Model
    HMesh<VertexData> mesh(MODEL_PATH);
    uint id = asset_map.StoreGeometryData(mesh, "loaded_model");
}

void Loopy(MasterEngine* engine, void* d)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();

    MVPOnlyUbo mvp = {};
    mvp.model = Rotate(Radians(180.0f), Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    mvp.model = mvp.model * 0.2f;

    CameraInfo camera_info = {};
    camera_info.camera_position = camera.GetPosition();
    ;

    Gallery& asset_map = rendering_engine.GetGallery();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("loaded_model");

    auto& target = rendering_engine.AtomicEffect(
        gpu_buffer, "shaders/debugging/phong_shader", {}, mvp, 0, camera_info, 1);

    rendering_engine.PresentRender(*target.GetColorImage(0));
}

int example()
{
    MasterEngine engine;

    auto extent =
        engine.GetRenderingEngine().GetEffectFramework().GetEffectiveDisplayExtent();

    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();
    camera.SetRadius(6);

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
    input_handler.AddEvent(
        MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    AddMeshes(rendering_engine);
    engine.Loop(nullptr, Loopy);

    return EXIT_SUCCESS;
}
