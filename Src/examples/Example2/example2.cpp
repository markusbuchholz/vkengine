//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "example2.hpp"

/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "MasterEngine/Geometry/Parametrics.hpp"

using namespace std;
namespace fs = std::filesystem;

namespace
{
const std::string MODEL_PATH = "Assets/cube.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";

VulkanImage image;
vk::Buffer buffer;

struct Particle
{
    Eigen::Vector4f pos;
};
}  // namespace

// Master Engine implementation ----------------------------------------------------------

void SetDebugMesh(Gallery& asset_map, uint id, float size = 1.f)
{
    auto& mesh = asset_map.GetCpuGeometry<Mesh>(id);
    auto model_instance_data = Mesh::GetVertexData(mesh);

    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.04 * size, 0.2 * size, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<LargeVertex> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
            arrow_vertices.push_back({v, {0, 0}, normals[i++]});
        Mesh arrow;
        arrow.Load(arrow_vertices, indices);
        uint id = asset_map.StoreGeometryData(arrow, "arrow");

        asset_map.SetGeometryInstanceData(id, model_instance_data);
    }

    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.08 * size, 15, 15);
        auto normals = GenerateNormals(vertices, indices);
        vector<LargeVertex> sphere_vertices;
        uint i = 0;
        for(auto& v: vertices)
            sphere_vertices.push_back({v, {0, 0}, normals[i++]});
        Mesh sphere;
        sphere.Load(sphere_vertices, indices);
        uint id = asset_map.StoreGeometryData(sphere, "debug_sphere");
        asset_map.SetGeometryInstanceData(id, model_instance_data);
    }

    {
        // cylinder
        auto edges = asset_map.GetCpuGeometry<Mesh>("loaded_model").GetEdges();

        auto [vertices, indices] = GenerateCylinder(0.03 * size, 1.f, 20, 20);
        auto normals = GenerateNormals(vertices, indices);
        vector<LargeVertex> cylinder_vertices;
        uint i = 0;
        for(auto& v: vertices)
            cylinder_vertices.push_back({v, {0, 0}, normals[i++]});
        Mesh cylinder;
        cylinder.Load(cylinder_vertices, indices);
        uint id = asset_map.StoreGeometryData(cylinder, "cylinder");

        vector<Eigen::Vector3f> edge_instance_data;
        edge_instance_data.reserve(model_instance_data.size());
        for(auto& [p1, p2]: edges)
        {
            Eigen::Vector3f v1 =
                Eigen::Vector3d{p1(0, 0), p1(0, 1), p1(0, 2)}.cast<float>();
            Eigen::Vector3f v2 =
                Eigen::Vector3d{p2(0, 0), p2(0, 1), p2(0, 2)}.cast<float>();
            edge_instance_data.push_back(v1);
            edge_instance_data.push_back(v2);
            edge_instance_data.push_back(Eigen::Vector3f(1, 1, 1));
        }
        asset_map.SetGeometryInstanceData(id, edge_instance_data);
    }
}

void AddDebugEdges(
    Gallery& asset_map,
    vector<pair<Eigen::Vector3f, Eigen::Vector3f>> edges,
    float size = 1)
{
    {
        // cylinder
        auto [vertices, indices] = GenerateCylinder(0.03 * size, 1.f, 20, 20);
        auto normals = GenerateNormals(vertices, indices);
        vector<LargeVertex> cylinder_vertices;
        uint i = 0;
        for(auto& v: vertices)
            cylinder_vertices.push_back({v, {0, 0}, normals[i++]});
        Mesh cylinder;
        cylinder.Load(cylinder_vertices, indices);
        uint id = asset_map.StoreGeometryData(cylinder, "cylinder");

        vector<Eigen::Vector3f> edge_instance_data;
        for(auto& [p1, p2]: edges)
        {
            Eigen::Vector3f v1 =
                Eigen::Vector3d{p1(0, 0), p1(0, 1), p1(0, 2)}.cast<float>();
            Eigen::Vector3f v2 =
                Eigen::Vector3d{p2(0, 0), p2(0, 1), p2(0, 2)}.cast<float>();
            edge_instance_data.push_back(v1);
            edge_instance_data.push_back(v2);
            edge_instance_data.push_back(Eigen::Vector3f(1, 0, 0));
        }
        asset_map.SetGeometryInstanceData(id, edge_instance_data);
    }
}

void AddGuizmos(Gallery& asset_map, Guizmo3DPool& guizmo_pool)
{
    guizmo_pool.AddWidget({0, 0, 0}, {1, 0, 0, 0}, 1, Guizmo3DPool::GuizmoType::ROTATION);
    Eigen::RowMatrix3f hc1 = Generate3DHemiCircle(1, {0, 0, 1}, {1, 0, 0});
    Eigen::RowMatrix3f hc2 = Generate3DHemiCircle(1, {0, 1, 0}, {0, 0, 1});
    Eigen::RowMatrix3f hc3 = Generate3DHemiCircle(1, {1, 0, 0}, {0, 1, 0});
    Eigen::RowMatrix3f v(hc1.rows() + hc2.rows() + hc3.rows(), 3);
    v << hc1, hc2, hc3;
    uint id =
        asset_map.StoreGeometryData(v, "rotation_guizmo", GetFloatVectorsFromRowMatrix3f);

    auto& positions = guizmo_pool.GetPositions();
    auto& rotations = guizmo_pool.GetRotations();
    auto& scales = guizmo_pool.GetScales();
    assert(positions.size() == rotations.size() && positions.size() == scales.size());

    vector<float> rotation_widgets_instance_data;
    rotation_widgets_instance_data.reserve(positions.size() * (3 + 4 + 1));
    for(uint i = 0; i < positions.size(); i++)
    {
        rotation_widgets_instance_data.push_back(positions[i].x());
        rotation_widgets_instance_data.push_back(positions[i].y());
        rotation_widgets_instance_data.push_back(positions[i].z());

        rotation_widgets_instance_data.push_back(rotations[i].w());
        rotation_widgets_instance_data.push_back(rotations[i].x());
        rotation_widgets_instance_data.push_back(rotations[i].y());
        rotation_widgets_instance_data.push_back(rotations[i].z());

        rotation_widgets_instance_data.push_back(scales[i]);
    }
    asset_map.SetGeometryInstanceData(
        id, rotation_widgets_instance_data, positions.size());
}

void AddMeshes(VulkanEngine& rendering_engine)
{
    Gallery& asset_map = rendering_engine.GetGallery();

    CpuImage image(TEXTURE_PATH);
    asset_map.StoreImageData(image, "statue");

    // Model
    Mesh mesh;
    mesh.Load(MODEL_PATH);
    uint id = asset_map.StoreGeometryData(mesh, "loaded_model");
    auto& mesh2 = asset_map.GetCpuGeometry<Mesh>("loaded_model");
    Mesh::InitGaussianSigmas(mesh2);
    MatrixXd covariance = MatrixXd::Identity(3, 3);
    covariance *= 0.1;
    Mesh::SetCovarianceOfVertex(mesh2, 0, covariance);
    covariance = MatrixXd::Identity(3, 3);
    covariance *= 0.1;
    Mesh::SetCovarianceOfVertex(mesh2, 2, covariance);
    covariance = MatrixXd::Identity(3, 3);
    covariance *= 10.1;
    Mesh::SetCovarianceOfVertex(mesh2, 5, covariance);

    // Sphere
    auto [vertices, indices] = GenerateSphere(0.08, 50, 50);
    auto normals = GenerateNormals(vertices, indices);
    vector<LargeVertex> sphere_vertices;
    uint i = 0;
    for(auto& v: vertices)
        sphere_vertices.push_back({v, {0, 0}, normals[i++]});
    Mesh sphere;
    sphere.Load(sphere_vertices, indices);
    asset_map.StoreGeometryData(sphere, "sphere");

    SetDebugMesh(asset_map, id);
}

uint state = 0;
float d_scale = 1.f;

void KeyTemp(void* ptr, KeyActionState action)
{
    if(action == KeyActionState::PRESS)
    {
        auto engine = reinterpret_cast<MasterEngine*>(ptr);
        auto& asset_map = engine->GetRenderingEngine().GetGallery();
        auto& mesh = asset_map.GetCpuGeometry<Mesh>("loaded_model");
        uint time;
        Mesh::GaussianSubdivideMesh(mesh);
        asset_map.UpdateGeometryData("loaded_model");

        d_scale *= 0.5;
    }
}

bool toggle = false;
void KeyETemp(void* ptr, KeyActionState action)
{
    if(action == KeyActionState::PRESS) { toggle = !toggle; }
}

void KeyState(void* ptr, KeyActionState action)
{
    if(action == KeyActionState::PRESS)
    {
        auto engine = reinterpret_cast<MasterEngine*>(ptr);
        state = (state + 1) % 2;
    }
}

void Loopy(MasterEngine* engine, void* d)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();
    Eigen::Vector3f camera_pos = Eigen::Vector3f(0, 2, 0);
    Eigen::Vector3f camera_center = Eigen::Vector3f(0, 0, 0);
    uint width, height;
    rendering_engine.GetWindow()->GetDimensions(&width, &height);
    CellShadingInfo ubo = {};
    ubo.model = Rotate(0.0f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    ubo.view = camera.GetViewMatrix();
    ubo.proj = camera.GetProjectionMatrix();

    ubo.camera_position = camera.GetPosition();
    ubo.view_dir = Append(camera.GetLookDirection(), 0.0f);
    ubo.far_plane = 5;
    ubo.near_plane = 0.1;

    MVPOnlyUbo mvp = {};
    mvp.model = ubo.model;
    mvp.proj = ubo.proj;
    mvp.view = ubo.view;

    CameraInfo camera_info = {};
    camera_info.camera_position = camera.GetPosition();

    Gallery& asset_map = rendering_engine.GetGallery();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("sphere");
    gpu_buffer.instance_buffer = buffer;
    gpu_buffer.instance_count = 100;

    auto& target = EffectFramework::AtomicEffect(
        effect_framework,
        gpu_buffer,
        "shaders/compute/phong_shader",
        {},
        mvp,
        0,
        camera_info,
        1);

    EffectFramework::GpuCompute2(
        effect_framework, "shaders/compute/particles", buffer, 100 * sizeof(Particle));

    EffectFramework::PresentRender(asset_map, effect_framework, *target.GetColorImage(0));
}

int example()
{
    MasterEngine engine;

    auto extent =
        engine.GetRenderingEngine().GetEffectFramework().GetEffectiveDisplayExtent();
    image = VulkanImage(extent.width, extent.height, 4);
    vector<Particle> particles(100);
    uint i = 0;
    for(auto& particle: particles)
    {
        uint q = i / 10;
        uint r = i % 10;
        particle.pos = Eigen::Vector4f(q / 10.f, r / 10.f, 0, 0);
        i++;
    }
    buffer = engine.GetRenderingEngine().GetMemory()->CreateFilledBuffer(
        particles,
        vk::BufferUsageFlagBits::eStorageBuffer | vk::BufferUsageFlagBits::eVertexBuffer);

    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
    input_handler.AddEvent(
        MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(GLFW_KEY_SPACE, KeyTemp, &engine);
    input_handler.AddEvent(GLFW_KEY_E, KeyETemp, &engine);
    input_handler.AddEvent(GLFW_KEY_ENTER, KeyState, &engine);

    AddMeshes(rendering_engine);
    engine.Loop(nullptr, Loopy);

    return EXIT_SUCCESS;
}
