#include "MRS.hpp"

#include <iostream>

using namespace std;
using namespace glm;

template<typename T>
T Cross(const T& p1, const T& p2)
{
	return p1.cross(p2);
}

template<typename T>
float Dot(const T& p1, const T& p2)
{
	return p1.dot(p2);
}

template<typename T>
T Normalize(const T& p1)
{
	return p1.normalized();
}

template<typename T>
float Length(const T& p1)
{
	return p1.norm();
}

template<typename T, typename M>
Eigen::Quaternionf Rotate(const M& m, float angle, const T& axis)
{
	return Eigen::Quaternionf(Eigen::AngleAxisf(angle, axis));
}

void Elliptical_P_Decomposition(vector<Eigen::Vector3f> fine, vector<float> w,
	vector<Eigen::Vector3f> *coarse, vector<Eigen::Vector4f> *details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float))
{
	int m=fine.size();
	for(int j=w.size()-1; j>=0; j--)
	{
		if((j&1)==0)
			for(int i=0; i<=m-2; i+=2)
			{
				Eigen::Vector3f mid = interp(fine[(i-1+m)%m], fine[(i+1)%m], 0.5f);
				fine[i] = interp(fine[i],mid,w[j]/(w[j]-1.f));
			}

		else
			for(int i=1; i<=/*m-2*/m-1; i+=2)
			{
				Eigen::Vector3f mid = interp(fine[(i-1+m)%m], fine[(i+1)%m], 0.5f);
				fine[i] = interp(fine[i],mid,w[j]/(w[j]-1.f));
			}
	}
	for(int i=0; i<=m-2;i+=2)
	{
		Eigen::Vector3f mid = interp(fine[i], fine[(i+2)%m], 0.5);
		(*coarse).push_back(fine[i]);
		Eigen::Vector4f dets;
		dets <<
			Cross(mid, fine[(i + 1) % m]),
			acos(Dot(fine[(i + 1) % m], mid));
		if(Length(Eigen::Vector3f(dets.x(), dets.y(), dets.z())) <= 0.1f)
			dets=Eigen::Vector4f(1,1,1,0);
		(*details).push_back(dets);
	}
}

void Elliptical_P_Reconstruction(vector<Eigen::Vector3f> *fine, vector<float> w,
	vector<Eigen::Vector3f> coarse, vector<Eigen::Vector4f> &details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float))
{
	int n = coarse.size();

	int offset = details.size() - n;

	for(int i=0; i<=n-1; i++)
	{
		mat4 temp = mat4(1);
		(*fine)[2*i]=coarse[i];
		Eigen::Vector4f det_vec = details[i+offset];

		(*fine)[2*i+1] = Eigen::Vector3f(Rotate(temp, det_vec[3], Eigen::Vector3f(det_vec[0], det_vec[1], det_vec[2]))
			* interp(coarse[i], coarse[(i+1)%n],0.5));
	}
	for(int i=0; i<=n-1; i++)
		details.pop_back();
	int l = w.size();
	int f = (*fine).size();
	for(int j=0; j<=l-1; j++)
	{
		if((j & 1) ==0)
		{
			for(int i=0; i<= 2*n-2; i+=2)
			{
				Eigen::Vector3f mid = interp((*fine)[(i-1+f)%f], (*fine)[(i+1)%f],0.5);
				(*fine)[i]=interp((*fine)[i], mid, w[j]);
			}
		}
		else
		{
			for(int i=1; i<=f-1; i+=2)
			{
				Eigen::Vector3f mid = interp((*fine)[(i-1+f)%f], (*fine)[(i+1)%f],0.5);
				(*fine)[i]= interp((*fine)[i], mid, w[j]);
			}
		}

	}
}

void Elliptical_D_Decomposition(const vector<Eigen::Vector3f>& o_fine, const vector<float>& w,
	vector<Eigen::Vector3f> *coarse, vector<Eigen::Vector4f> *details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float))
{
	vector<Eigen::Vector3f> fine = o_fine;
	int m=fine.size();
	for(int j=w.size()-1; j>=0; j--)
	{
		if((j&1)==0)
			for(int i=1; i<=m-1; i+=2)
			{
				Eigen::Vector3f p = fine[i];
				fine[i] = interp(p, fine[(i+1)%m], w[j]/(2.d*w[j]-2.d));
				fine[(i+1)%m] = interp(fine[(i+1)%m],p,w[j]/(2.d*w[j]-2.d));
			}

		else
			for(int i=0; i<=m-2; i+=2)
			{
				Eigen::Vector3f p = fine[i];
				fine[i] = interp(p, fine[(i+1)%m], w[j]/2.d);
				fine[i] = interp(fine[(i+1)%m],p,w[j]/(2*w[j]-2.d));
			}
	}
	for(int i=0; i<=m-2;i+=2)
	{
		(*coarse).push_back(interp(fine[i],fine[(i+1)%m], 0.5));
		Eigen::Vector4f dets;
		dets <<
			Cross(fine[i],fine[(i+1)%m]),
			acos(Dot((fine[(i+1)%m]), fine[i])) / 0.5f;

		if(Length(Eigen::Vector3f(dets.x(), dets.y(), dets.z())) <= 0.1)
			dets=Eigen::Vector4f(1,1,1,0);
		(*details).push_back(dets);
	}
}

void Elliptical_D_Reconstruction(vector<Eigen::Vector3f> *fine, const vector<float>& w,
	const vector<Eigen::Vector3f>& coarse, vector<Eigen::Vector4f> &details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float))
{
	int n = coarse.size();
	int offset = details.size() - n;
	for(int i = 0; i <= n-1; i++)
	{
		mat4 temp = mat4(1);
		Eigen::Vector4f det_vec = details[i+offset];

		(*fine)[2*i]= Eigen::Vector3f(Rotate(temp, -det_vec[3], Eigen::Vector3f(det_vec[0], det_vec[1], det_vec[2]))
		* coarse[i]);

		(*fine)[2*i+1]= Eigen::Vector3f(Rotate(temp, det_vec[3], Eigen::Vector3f(det_vec[0], det_vec[1], det_vec[2]))
			* coarse[i]);
	}
	for(int i=0; i<=n-1; i++)
		details.pop_back();
	int l = w.size();
	int f = (*fine).size();
	for(int j=0; j<=l-1; j++)
	{
		if((j & 1) ==0)
		{
			for(int i=1; i<= 2*n-1; i+=2)
			{
				Eigen::Vector3f p = (*fine)[i];
				(*fine)[i] = interp(p, (*fine)[(i+1)%f],w[j]/2.d);
				(*fine)[(i+1)%f] = interp((*fine)[(i+1)%f], p, w[j]/2.d);
			}
		}
		else
		{
			for(int i=0; i<=2*n-2; i+=2)
			{
				Eigen::Vector3f p = (*fine)[i];
				(*fine)[i] = interp(p, (*fine)[(i+1)%f],w[j]/2.d);
				(*fine)[(i+1)%f]= interp((*fine)[i+1], p, w[j]/2.d);
			}
		}
	}
}

vector<Eigen::Vector3f> Subdivision(
	const std::vector<Eigen::Vector3f>& points,
	Eigen::Vector3f(*interp)(Eigen::Vector3f, Eigen::Vector3f, float))
{
	vector<Eigen::Vector3f> new_shape;
	for(uint i=0; i<points.size() ;i++)
	{
		new_shape.push_back(points[i]);
		new_shape.push_back(interp(points[i], points[(i+1)%points.size()], 0.5));
	}

	int n=new_shape.size();

	for(uint i = 0; i < n - 1; i += 2)
	{
		const Eigen::Vector3f intermediary =
			interp(new_shape[(i - 1 + n) % n], new_shape[(i + 1) % n], 0.5);

		new_shape[i] = interp(new_shape[i], intermediary, 0.5);
	}

	return new_shape;
}
