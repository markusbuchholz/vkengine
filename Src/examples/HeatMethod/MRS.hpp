#pragma once

#include <vector>

#include <glm/glm.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/transform.hpp>

#include "Eigen/Dense"

void Elliptical_P_Decomposition(std::vector<Eigen::Vector3f> fine, std::vector<float> w,
	std::vector<Eigen::Vector3f> *coarse, std::vector<Eigen::Vector4f> *details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float));

void Elliptical_P_Reconstruction(std::vector<Eigen::Vector3f> *fine, std::vector<float> w,
	std::vector<Eigen::Vector3f> coarse, std::vector<Eigen::Vector4f> &details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float));

void Elliptical_D_Decomposition(
	const std::vector<Eigen::Vector3f>& fine,
	const std::vector<float>& w,
	std::vector<Eigen::Vector3f> *coarse,
	std::vector<Eigen::Vector4f> *details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float));

void Elliptical_D_Reconstruction(
	std::vector<Eigen::Vector3f> *fine,
	const std::vector<float>& w,
	const std::vector<Eigen::Vector3f>& coarse,
	std::vector<Eigen::Vector4f> &details,
	Eigen::Vector3f(*interp)(Eigen::Vector3f,Eigen::Vector3f,float));

std::vector<Eigen::Vector3f> Subdivision(
	const std::vector<Eigen::Vector3f>& points,
	Eigen::Vector3f(*interp)(Eigen::Vector3f, Eigen::Vector3f, float));