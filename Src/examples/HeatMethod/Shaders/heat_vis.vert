/**
* Input Description:
* @depth_test: true
* @topology: triangle list
*
*/

#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(location = 0) in vec3 inPosition;
layout(location = 1) in vec3 color;

layout(location = 0) out vec3 out_color;

layout(binding = 0) uniform MVPOnlyUbo {
    mat4 model;
    mat4 view;
    mat4 proj;
};

void main()
{
    out_color = color;
    gl_Position = proj * view * vec4(inPosition, 1.0);
}