//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file GradientLerp.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-10-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <glm/glm.hpp>
#include <igl/heat_geodesics.h>
#include <igl/exact_geodesic.h>
#include <cmath>
#include <cfloat>

#include "Geometry/MeshGradient.hpp"

template<typename V, typename Vec = decltype(std::declval<V>().position)>
uint GetContainingFace(const Vec& point, const HMesh<V>& mesh)
{
    std::cout << mesh.Faces().size() << std::endl;
    std::cout << "----------------------------------" << std::endl;
    const auto& faces = mesh.Faces();
    float closest_distance = std::numeric_limits<float>::max();
    uint closest_face = 0;
    for(const auto& face: faces)
    {
        const auto vertices = face.VerticesD();
        const auto normal = face.Normal();
        float distance_to_face = (point - vertices[0]->Data().position).dot(normal);
        if(distance_to_face < closest_distance)
        {
            closest_distance = distance_to_face;
            closest_face = face.ID();
        }
        if(!IsCloseTo(distance_to_face, 0.00000001f))
        {
            closest_distance = distance_to_face;
            closest_face = face.ID();
        }
        if(!IsCloseTo(distance_to_face, 0.00000001f)) { continue; }

        auto [u, v, w] = Barycentric(
            point,
            vertices[0]->Data().position,
            vertices[1]->Data().position,
            vertices[2]->Data().position);
        assert(!std::isnan(u));
        assert(!std::isnan(v));
        assert(!std::isnan(w));

        if(IsInRange(u, 0.f, 1.f, 0.01f) && IsInRange(v, 0.f, 1.f, 0.01f) &&
           IsInRange(w, 0.f, 1.f, 0.01f))
        { return face.ID(); }
    }
    return closest_face;
    assert(false && "Error, no face contains the point");
    return -1;
}

template<typename T> int Sign(T val) { return (T(0) < val) - (val < T(0)); }
template<
    typename V,
    typename G,
    typename D,
    typename Vec = decltype(std::declval<V>().position)>
Vec PolyLerp(
    HMesh<V>& mesh,
    uint start_face_id,
    uint start_vert_id,
    uint end_vert_id,
    const G& gradient,
    const D& function,
    const float t)
{
    using MFace = typename HMesh<V>::MFace;
    using MEdge = typename HMesh<V>::MEdge;
    const MFace* current_face = &mesh.Faces()[start_face_id];

    const Vec& start = mesh.VertexData()[start_vert_id].position;

    Vec face_g = -Sign(t) * FaceGradient(start, current_face, gradient);

    Vec source = start;
    Vec prev_source = start;
    float current_distance = 0;
    const float start_distance = function[start_vert_id];
    const float target_distance = abs(t * start_distance);
    const MEdge* current_edge = &current_face->Edge();

    for(uint i = 0; current_distance < target_distance; i++)
    {
        auto vertices = current_face->VerticesD();
        auto vert_ids = current_face->VertexIds();

        const std::vector<const MEdge*> edges = {
            &(current_edge->Next()), &(current_edge->Prev()), current_edge};
        // All edges are valid possible intersections on the first iterations.
        // Afterwards we consider only the edges not present in the prior iteration.
        auto [collision, hit_flag, edge_index] = CoPlanarLinePolyLineIntersection(
            source,
            face_g,
            {edges[0]->Vert().Data().position,
             edges[1]->Vert().Data().position,
             edges[2]->Vert().Data().position},
            !i);

        if(!hit_flag)
        {
            Log::RecordLogError("Didn't hit the triangle. " + std::to_string(i));
            return source;
        }
        current_edge = &edges[edge_index]->Pair();

        current_face = &current_edge->Face();
        prev_source = source;
        source = collision;
        Vec new_g = -Sign(t) * FaceGradient(source, current_face, gradient);
        // If the gradient is 0, use the prior gradient
        face_g = new_g.norm() < 0.00000000001 ? face_g : new_g;

        current_distance =
            abs(start_distance - FunctionValue(source, current_face, function));
    }

    return source;
}

template<typename VData, typename Vec = decltype(std::declval<VData>().position)>
Vec GradientLerp(HMesh<VData>& mesh, const Vec& p1, const Vec& p2, const float t)
{
    uint f1_id = GetContainingFace(p1, mesh);
    uint f2_id = GetContainingFace(p2, mesh);

    if(f1_id == f2_id) return (1.f - t) * p1 + t * p2;

    HMesh<VData>::SplitFace(f1_id, mesh);
    mesh.VertexDataD().back().position = p1;
    uint p1_id = mesh.VertexData().size() - 1;

    HMesh<VData>::SplitFace(f2_id, mesh);
    mesh.VertexDataD().back().position = p2;
    uint p2_id = mesh.VertexData().size() - 1;

    // -----------------------------------------------------------------------------------
    Eigen::MatrixXf V;
    Eigen::MatrixXi F;
    Eigen::VectorXf D;

    std::vector<Eigen::Vector3f> vertices;
    for(const auto& v: mesh.VertexData())
        vertices.push_back(v.position);

    std::vector<uint> indices;
    for(const auto& f: mesh.Faces())
    {
        auto verts = f.VertexIds();
        for(auto& v: verts)
            indices.push_back(v);
    }
    V = Eigen::MatrixXf(vertices.size(), 3);
    F = Eigen::MatrixXi(indices.size() / 3, 3);

    uint count = 0;
    for(auto& v: vertices)
        V.row(count++) = v;
    count = 0;
    for(uint i = 0; i < indices.size(); i += 3)
    {
        F.row(count++) =
            Eigen::Vector3i{int(indices[i]), int(indices[i + 1]), int(indices[i + 2])}
                .transpose();
    }

    igl::HeatGeodesicsData<float> data;
    if(!igl::heat_geodesics_precompute(V, F, data))
    { std::cout << "ffffffffffffffffffff" << std::endl; }
    Eigen::VectorXi g(1);
    g[0] = p1_id;

    igl::heat_geodesics_solve(data, g, D);
    // -----------------------------------------------------------------------------------

    auto gradient = CalculateSimplifiedVertexGradient(mesh, D);
    gradient[g[0]] = {0, 0, 0};

    auto g_line = FollowGradient(
        mesh, p2, f2_id, Eigen::Vector3f(-gradient[p2_id]), (1.f - t) * D[p2_id]);

    return g_line;
}

template<typename VData, typename Vec = decltype(std::declval<VData>().position)>
float MeasureExactGeodesicDistance(HMesh<VData>& mesh, const Vec& p1, const Vec& p2)
{
    if((p1 - p2).norm() < 0.0001) return 0;
    uint f1_id = GetContainingFace(p1, mesh);
    uint f2_id = GetContainingFace(p2, mesh);

    uint p1_id = mesh.Faces()[f1_id].VertexIds()[0];
    uint p2_id = mesh.Faces()[f2_id].VertexIds()[0];

    /*HMesh<VData>::SplitFace(f1_id, mesh);
    mesh.VertexDataD().back().position = p1;
    uint p1_id = mesh.VertexData().size() - 1;

    HMesh<VData>::SplitFace(f2_id, mesh);
    mesh.VertexDataD().back().position = p2;
    uint p2_id = mesh.VertexData().size() - 1;*/

    // -----------------------------------------------------------------------------------
    Eigen::MatrixXf V;
    Eigen::MatrixXi F;
    Eigen::VectorXf D;

    std::vector<Eigen::Vector3f> vertices;
    for(const auto& v: mesh.VertexData())
        vertices.push_back(v.position);

    std::vector<uint> indices;
    for(const auto& f: mesh.Faces())
    {
        auto verts = f.VertexIds();
        for(auto& v: verts)
            indices.push_back(v);
    }
    V = Eigen::MatrixXf(vertices.size(), 3);
    F = Eigen::MatrixXi(indices.size() / 3, 3);

    uint count = 0;
    for(auto& v: vertices)
        V.row(count++) = v;
    count = 0;
    for(uint i = 0; i < indices.size(); i += 3)
    {
        F.row(count++) =
            Eigen::Vector3i{int(indices[i]), int(indices[i + 1]), int(indices[i + 2])}
                .transpose();
    }

    Eigen::VectorXi vs(1);
    vs[0] = p1_id;
    Eigen::VectorXi fs(0);
    //fs[0] = f1_id;
    Eigen::VectorXi vt(1);
    vt.setLinSpaced(V.rows(), 0, V.rows() - 1);
    Eigen::VectorXi ft(0);
    //ft[0] = f2_id;

    igl::exact_geodesic(V, F, vs, fs, vt, ft, D);
    // -----------------------------------------------------------------------------------

    return D[p2_id];
}

template<typename VData, typename Vec = decltype(std::declval<VData>().position)>
float MeasureHeatGeodesicDistance(HMesh<VData>& mesh, const Vec& p1, const Vec& p2)
{
    if((p1 - p2).norm() < 0.0001) return 0;
    uint f1_id = GetContainingFace(p1, mesh);
    uint f2_id = GetContainingFace(p2, mesh);

    //uint p1_id = mesh.Faces()[f1_id].VertexIds()[0];
    //uint p2_id = mesh.Faces()[f2_id].VertexIds()[0];

    HMesh<VData>::SplitFace(f1_id, mesh);
    mesh.VertexDataD().back().position = p1;
    uint p1_id = mesh.VertexData().size() - 1;

    HMesh<VData>::SplitFace(f2_id, mesh);
    mesh.VertexDataD().back().position = p2;
    uint p2_id = mesh.VertexData().size() - 1;

    // -----------------------------------------------------------------------------------
    Eigen::MatrixXf V;
    Eigen::MatrixXi F;
    Eigen::VectorXf D;

    std::vector<Eigen::Vector3f> vertices;
    for(const auto& v: mesh.VertexData())
        vertices.push_back(v.position);

    std::vector<uint> indices;
    for(const auto& f: mesh.Faces())
    {
        auto verts = f.VertexIds();
        for(auto& v: verts)
            indices.push_back(v);
    }
    V = Eigen::MatrixXf(vertices.size(), 3);
    F = Eigen::MatrixXi(indices.size() / 3, 3);

    uint count = 0;
    for(auto& v: vertices)
        V.row(count++) = v;
    count = 0;
    for(uint i = 0; i < indices.size(); i += 3)
    {
        F.row(count++) =
            Eigen::Vector3i{int(indices[i]), int(indices[i + 1]), int(indices[i + 2])}
                .transpose();
    }

    Eigen::VectorXi vs(1);
    vs[0] = p1_id;
    Eigen::VectorXi fs(0);
    //fs[0] = f1_id;
    Eigen::VectorXi vt(1);
    vt.setLinSpaced(V.rows(), 0, V.rows() - 1);
    Eigen::VectorXi ft(0);
    //ft[0] = f2_id;

    igl::HeatGeodesicsData<float> data;
    if(!igl::heat_geodesics_precompute(V, F, data))
    { std::cout << "ffffffffffffffffffff" << std::endl; }
    Eigen::VectorXi g(1);
    g[0] = p1_id;

    igl::heat_geodesics_solve(data, g, D);
    // -----------------------------------------------------------------------------------

    return D[p2_id];
}
