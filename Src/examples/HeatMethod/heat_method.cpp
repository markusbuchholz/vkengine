//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <igl/heat_geodesics.h>
#include <iostream>
#include <map>
#include <cmath>
#include <cfloat>
#include <random>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "GradientLerp.hpp"
#include "Helpers/Serializers.hpp"
#include "MRS.hpp"
#include "MasterEngine/Geometry/CpuImage.hpp"
#include "MasterEngine/Geometry/GeometryUtils.hpp"
#include "MasterEngine/Geometry/HMesh.hpp"
#include "MasterEngine/Geometry/HMeshUtils.hpp"
#include "MasterEngine/Geometry/MeshGradient.hpp"
#include "MasterEngine/Geometry/Parametrics.hpp"
#include "MasterEngine/MasterEngine.hpp"

using namespace std;
namespace fs = std::filesystem;

vector<Eigen::Vector3f> gradient;
vector<Eigen::Vector3f> manifold_curve;
vector<float> weights = {0.5};
vector<Eigen::Vector4f> details;
std::set<uint> global_faces;
//0.99664718689
float a = 1;
float b = 0.99664718689;
float c = 1;

std::vector<Eigen::Vector3f> CurveToInstanceData(const vector<Eigen::Vector3f>& curve)
{
    vector<Eigen::Vector3f> data;
    data.reserve(curve.size() * 3);
    for(const auto& p: curve)
    {
        data.push_back(p);
        data.push_back({0, 1, 0});
        data.push_back({1, 0, 0});
    }
    return data;
}

namespace
{
const std::string MODEL_PATH = "Assets/bunny/low_poly_bunny.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";
}  // namespace

Eigen::MatrixXf V;
Eigen::MatrixXi F;
Eigen::VectorXf D;

// Master Engine implementation ----------------------------------------------------------
std::pair<std::vector<float>, std::vector<uint>> GetGeodesicData(std::any& container)
{
    HMesh<VertexData>& mesh = std::any_cast<HMesh<VertexData>&>(container);

    std::vector<float> data;
    std::vector<uint> indices;
    const auto& vertices = mesh.VertexData();
    for(uint i = 0; i < D.rows(); i++)
    {
        data.push_back(vertices[i].position[0]);
        data.push_back(vertices[i].position[1]);
        data.push_back(vertices[i].position[2]);

        data.push_back(D[i]);
        data.push_back(D[i]);
        data.push_back(D[i]);
    }

    for(const auto& f: mesh.Faces())
    {
        auto v_ids = f.VertexIds();
        indices.push_back(v_ids[0]);
        indices.push_back(v_ids[1]);
        indices.push_back(v_ids[2]);
    }
    return {data, indices};
}

void Loopy(MasterEngine* engine, void* d)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();

    uint width, height;
    rendering_engine.GetWindow()->GetDimensions(&width, &height);
    MVPOnlyUbo mvp = {};
    mvp.model = Rotate(0.f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    Gallery& asset_map = rendering_engine.GetAssetMap();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("loaded_model");

    CellShadingInfo ubo = {};
    ubo.model = Rotate(0.f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    ubo.view = camera.GetViewMatrix();
    ubo.proj = camera.GetProjectionMatrix();

    ubo.camera_position = camera.GetPosition();
    ubo.view_dir = Append(camera.GetLookDirection(), 0.f);
    ubo.far_plane = 5;
    ubo.near_plane = 0.1;

    RenderTarget& target = rendering_engine.CellShadingRender(
        gpu_buffer, ubo, *asset_map.GetGpuImage("statue"));

    WireframeDebugInfo wire_info = {};
    wire_info.camera_position = camera.GetPosition();
    wire_info.treat_normal_as_point = false;
    GeometryInfo normal_buffer = asset_map.GetGpuGeometryBuffer("arrow");
    rendering_engine.AtomicEffect(
        normal_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);
    /*
    GeometryInfo n2 = asset_map.GetGpuGeometryBuffer("arrow2");
    rendering_engine.AtomicEffect(
        n2,
        "shaders/debugging/wireframe_debug", target, {},
        mvp, 0, wire_info, 1
    );*/
    GeometryInfo gradient_buffer = asset_map.GetGpuGeometryBuffer("sphere");
    rendering_engine.AtomicEffect(
        gradient_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);

    rendering_engine.PresentRender(*target.GetColorImage(0));
}

bool toggle = true;
uint id;
Eigen::Vector3f Slerp(Eigen::Vector3f p1, Eigen::Vector3f p2, float t)
{
    Eigen::Vector3f u1 = p1.normalized();
    Eigen::Vector3f u2 = p2.normalized();

    float omega = acos(u1.dot(u2));

    if(abs(omega) < 0.00000001 || isnan(omega)) return p1;

    float c1 = sin((1 - t) * omega) / sin(omega);
    float c2 = sin(t * omega) / sin(omega);

    assert(!(isnan(c1) || isnan(c2)));

    return (c1 * p1 + c2 * p2).normalized();
}

HMesh<VertexData>* global_mesh;
Eigen::Vector3f Glerp(Eigen::Vector3f p1, Eigen::Vector3f p2, float t)
{
    HMesh<VertexData> copy = *global_mesh;
    return GradientLerp(copy, p1, p2, t);
}

Eigen::Vector3f EllipseProject(const Eigen::Vector3f& p, float a, float b, float c)
{
    float vx = p[0], vy = p[1], vz = p[2];

    vx *= vx, vy *= vy, vz *= vz;

    a *= a, b *= b, c *= c;

    vx = vx / a, vy = vy / b, vz = vz / c;

    float t = 1.f / sqrt(vx + vy + vz);

    return t * p;
}

vector<Eigen::Vector3f> UniformPoints(uint size, float a, float b, float c)
{
    vector<Eigen::Vector3f> return_val;
    std::default_random_engine generator(glfwGetTime());
    std::normal_distribution<float> distributionx(0.0, a);
    std::normal_distribution<float> distributiony(0.0, b);
    std::normal_distribution<float> distributionz(0.0, c);

    for(uint i = 0; i < size; i++)
    {
        float x, y, z;
        x = distributionx(generator);
        y = distributiony(generator);
        z = distributionz(generator);

        return_val.push_back(Eigen::Vector3f(x, y, z));
    }

    for(uint i = 0; i < size; i++)
        return_val[i] = EllipseProject(return_val[i], a, b, c);

    return return_val;
}

Eigen::Vector3f Elerp(Eigen::Vector3f p1, Eigen::Vector3f p2, float t)
{
    auto e1 = p1.normalized();
    auto e2 = p2.normalized();
    auto result = Slerp(e1, e2, t);
    return EllipseProject(result, a, b, c);
}

void KeyToggle(void* ptr, int key, KeyActionState action)
{
    auto engine = reinterpret_cast<MasterEngine*>(ptr);
    auto camera = &engine->GetCamera();
    auto& asset_map = engine->GetRenderingEngine().GetGallery();

    if(action == KeyActionState::PRESS && key == GLFW_KEY_SPACE) { toggle = !toggle; }
    if(action == KeyActionState::PRESS && key == GLFW_KEY_KP_SUBTRACT)
    {
        vector<Eigen::Vector3f> temp;
        Elliptical_D_Decomposition(manifold_curve, weights, &temp, &details, Elerp);
        manifold_curve = temp;
        asset_map.SetGeometryInstanceData("sphere", CurveToInstanceData(manifold_curve));
    }

    if(action == KeyActionState::PRESS && key == GLFW_KEY_KP_ADD)
    {
        if(details.size())
        {
            vector<Eigen::Vector3f> temp =
                vector<Eigen::Vector3f>(manifold_curve.size() * 2);
            Elliptical_D_Reconstruction(&temp, weights, manifold_curve, details, Elerp);
            manifold_curve = temp;
            asset_map.SetGeometryInstanceData(
                "sphere", CurveToInstanceData(manifold_curve));
        }
    }

    if(action == KeyActionState::PRESS && key == GLFW_KEY_M)
    {
        auto copy = manifold_curve;
        manifold_curve.clear();
        for(float t = 0; t <= 1; t += 0.1)
            manifold_curve.push_back(Elerp(manifold_curve[0], manifold_curve[1], t));
        manifold_curve = Subdivision(copy, Elerp);
        asset_map.SetGeometryInstanceData("sphere", CurveToInstanceData(manifold_curve));
    }

    if(action == KeyActionState::PRESS && key == GLFW_KEY_Q)
    {
        cout << "curve" << endl;
        vector<Eigen::Vector3f> curve;
        Eigen::MatrixXf connections(manifold_curve.size(), manifold_curve.size());
        Eigen::MatrixXf truth(manifold_curve.size(), manifold_curve.size());
        for(int i = 0; i < manifold_curve.size(); i++)
        {
            for(uint j = 0; j < manifold_curve.size(); j++)
            {
                float distance = 0;
                Eigen::Vector3f prior = manifold_curve[j];
                for(float t = 0; t <= 1.f; t += 0.0001)
                {
                    auto interp = Elerp(manifold_curve[j], manifold_curve[i], t);
                    distance += (interp - prior).norm();
                    prior = interp;
                    curve.push_back(interp);
                }
                connections(i, j) = distance;
            }
        }
        Eigen::MatrixXf diffs(manifold_curve.size(), manifold_curve.size());
        asset_map.SetGeometryInstanceData("sphere", CurveToInstanceData(curve));

        cout << "Elerp, "
             << "Exact, "
             << "Heat, "
             << "Elerp diff, "
             << "Heat Diff, " << endl;
        for(int i = 1; i < manifold_curve.size(); i++)
        {
            for(uint j = 0; j < i; j++)
            {
                auto mesh =
                    asset_map.GetCpuGeometry<HMesh<VertexData>>("reference_loaded_model");
                float exact_distance = MeasureExactGeodesicDistance(
                    mesh, manifold_curve[i], manifold_curve[j]);
                float heat_distance = MeasureHeatGeodesicDistance(
                    mesh, manifold_curve[i], manifold_curve[j]);

                //float actual_geodesic =
                //    acos(manifold_curve[i].dot(manifold_curve[j]));
                cout << connections(i, j) << ", ";
                cout << exact_distance << ", ";
                cout << heat_distance << ",";
                cout << abs(connections(i, j) - exact_distance) / exact_distance << ",";
                cout << abs(heat_distance - exact_distance) / exact_distance << ", ";
                cout << endl;
            }
        }
    }

    if(action == KeyActionState::PRESS && key == GLFW_KEY_S)
    {
        auto& mesh =
            asset_map.GetCpuGeometry<HMesh<VertexData>>("reference_loaded_model");
        HMesh<VertexData>::LoopSubdivision(mesh);
        asset_map.UpdateGeometryData("reference_loaded_model");

        asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model") = mesh;
        asset_map.UpdateGeometryData("loaded_model");
    }

    if(action == KeyActionState::PRESS && key == GLFW_KEY_R)
    {
        auto& ref_mesh =
            asset_map.GetCpuGeometry<HMesh<VertexData>>("reference_loaded_model");
        auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
        set<uint> flooded;
        for(uint f: global_faces)
        {
            auto faces = FaceFlooding(f, 300, ref_mesh);
            for(auto face: faces)
                flooded.insert(face);
        }

        mesh = SubMesh(ref_mesh, std::vector<uint>(flooded.begin(), flooded.end()));
        global_mesh = &mesh;
        asset_map.UpdateGeometryData("loaded_model");
    }
}

void LeftDrag(void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset)
{
    if(toggle) { ArcballCamera::UpdateCameraAngles(ptr, position, offset); }
}

std::pair<std::vector<float>, std::vector<uint>> GetFloatsFromFloatVector(std::any& input)
{
    return std::any_cast<std::pair<vector<float>, vector<uint>>&>(input);
}

void UpdateGradient(Gallery& asset_map)
{
    auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
    const auto& faces = mesh.Faces();
    gradient = CalculateFacesGradient(mesh, D);
    gradient = CalculateFaceAverageVertexGradient(mesh, gradient);
    vector<float> model_instance_data;
    for(const auto& face: faces)
    {
        auto vertices = face.Vertices();
        for(uint i = 0; i < 3; i++)
        {
            ExtractFloats(vertices[i]->Data().position, model_instance_data);
            ExtractFloats(gradient[vertices[i]->ID()], model_instance_data);
            ExtractFloats(Eigen::Vector3f(0, 1, 0), model_instance_data);
        }
    }
    asset_map.SetGeometryInstanceData("arrow", model_instance_data);
}

void AddDebugInfo(Gallery& asset_map)
{
    auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
    const auto& faces = mesh.Faces();
    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.01, 0.05, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "arrow", GetFloatsFromFloatVector);

        UpdateGradient(asset_map);
    }
    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.01, 0.05, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "arrow2", GetFloatsFromFloatVector);
    }
    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.01, 5, 5);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "sphere", GetFloatsFromFloatVector);

        manifold_curve = UniformPoints(15, a, b, c);
        asset_map.SetGeometryInstanceData(id, CurveToInstanceData(manifold_curve));
    }
}

void LeftClick(void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset)
{
    if(!toggle)
    {
        auto engine = reinterpret_cast<MasterEngine*>(ptr);
        auto camera = &engine->GetCamera();
        auto& asset_map = engine->GetRenderingEngine().GetAssetMap();

        Eigen::Matrix4f mp = camera->GetProjectionMatrix();
        Eigen::Matrix4f mv = camera->GetViewMatrix();

        Eigen::Matrix4f view_proj = mp * mv;

        Eigen::Vector3f unprojected =
            Unproject({position.x(), position.y(), 0}, view_proj);

        Eigen::Vector3f camera_position = camera->GetPosition();
        Eigen::Vector3f dir = unprojected - camera_position;
        dir.normalize();

        auto& mesh =
            asset_map.GetCpuGeometry<HMesh<VertexData>>("reference_loaded_model");

        auto [face_id, distance] =
            HMesh<VertexData>::RayMeshIntersection(mesh, camera_position, dir);
        Eigen::Vector3f collision = camera_position + dir * distance;
        manifold_curve.push_back({collision.x(), collision.y(), collision.z()});

        global_faces.insert(face_id);

        asset_map.UpdateGeometryData("reference_loaded_model");
        asset_map.SetGeometryInstanceData("sphere", CurveToInstanceData(manifold_curve));

        if(face_id == HMesh<VertexData>::ABSENT) return;

        asset_map.UpdateGeometryData("reference_loaded_model");
    }
}

int main()
{
    MasterEngine engine;

    auto extent =
        engine.GetRenderingEngine().GetEffectFramework().GetEffectiveDisplayExtent();

    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();
    camera.SetRadius(6);

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
    input_handler.AddEvent(MouseInputState::LEFT_DRAG, LeftDrag, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);
    input_handler.AddEvent(MouseInputState::LEFT_DOWN, LeftClick, &engine);
    input_handler.AddEvent(KeyToggle, &engine);

    Gallery& asset_map = rendering_engine.GetAssetMap();

    // Model
    //HMesh<VertexData> mesh(MODEL_PATH);

    auto [vs, idx] = GenerateEllipsoid(a, b, c, 100, 100);
    auto normals = GenerateNormals(vs, idx);
    vector<VertexData> sphere_data;
    uint i = 0;
    for(auto& v: vs)
    {
        VertexData datum = {};
        datum.position = v;
        datum.uv = {0, 0};
        datum.normal = normals[i];
        sphere_data.push_back(datum);
        i++;
    }
    HMesh<VertexData> mesh(sphere_data, idx);

    std::vector<Eigen::Vector3f> vertices;
    for(const auto& v: mesh.VertexData())
        vertices.push_back(v.position);

    std::vector<uint> indices;
    for(const auto& f: mesh.Faces())
    {
        auto verts = f.VertexIds();
        for(auto& v: verts)
            indices.push_back(v);
    }
    V = Eigen::MatrixXf(vertices.size(), 3);
    F = Eigen::MatrixXi(indices.size() / 3, 3);

    uint count = 0;
    for(auto& v: vertices)
        V.row(count++) = v;
    count = 0;
    for(uint i = 0; i < indices.size(); i += 3)
    {
        F.row(count++) =
            Eigen::Vector3i{int(indices[i]), int(indices[i + 1]), int(indices[i + 2])}
                .transpose();
    }

    igl::HeatGeodesicsData<float> data;
    if(!igl::heat_geodesics_precompute(V, F, data))
        cout << "ffffffffffffffffffff" << endl;
    Eigen::VectorXi g(1);
    g[0] = 0;
    igl::heat_geodesics_solve(data, g, D);

    HMesh<VertexData> ref_mesh = mesh;
    uint id = asset_map.StoreGeometryData(mesh, "loaded_model");  //, GetGeodesicData);

    asset_map.StoreGeometryData(ref_mesh, "reference_loaded_model");
    CpuImage image(TEXTURE_PATH);
    asset_map.StoreImageData(image, "statue");
    AddDebugInfo(asset_map);

    global_mesh = &asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");

    engine.Loop(nullptr, Loopy);

    input_handler.AddEvent(KeyToggle, &engine);

    return EXIT_SUCCESS;
}
