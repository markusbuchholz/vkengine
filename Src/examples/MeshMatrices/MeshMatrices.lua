-- Setup project
project "MeshMatrices"
    -- Compilation variables
    kind "WindowedApp"
    targetdir ("../../../build/")
    language "C++"

    filter "configurations:Debug"
        defines { "DEBUG" }
        symbols "On"
    filter "configurations:DebugVerbose"
        defines { "DEBUG", "VERBOSITY_1" }
        symbols "On"
    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

    configuration { "linux", "gmake" }
        optimize "Debug"
        buildoptions {"-std=c++17"}
        linkoptions {"-lstdc++fs -O0 -ldl -export-dynamic"}

    files {"**.cpp", "**.hpp"}

    includedirs {PROJECT_PATH .. "/Src/Engine/"}
    links("CoreVulkanEngine")
