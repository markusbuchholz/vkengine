//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file main.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/** @cond */
#include <array>
#include <chrono>
#include <filesystem>
#include <fstream>
#include <igl/heat_geodesics.h>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "Helpers/Serializers.hpp"
#include "MasterEngine/Geometry/CpuImage.hpp"
#include "MasterEngine/Geometry/GeometryUtils.hpp"
#include "MasterEngine/Geometry/HMesh.hpp"
#include "MasterEngine/Geometry/MatrixMesh.hpp"
#include "MasterEngine/Geometry/MeshGradient.hpp"
#include "MasterEngine/Geometry/Parametrics.hpp"
#include "MasterEngine/MasterEngine.hpp"

using namespace std;
namespace fs = std::filesystem;

vector<Eigen::Vector3f> gradient;
vector<Eigen::Vector3f> manifold_curve;
vector<float> weights = {0.5};
vector<Eigen::Vector4f> details;

std::vector<Eigen::Vector3f> CurveToInstanceData(const vector<Eigen::Vector3f>& curve)
{
    vector<Eigen::Vector3f> data;
    data.reserve(curve.size() * 3);
    for(const auto& p: curve)
    {
        data.push_back(p);
        data.push_back({0, 1, 0});
        data.push_back({1, 0, 0});
    }
    return data;
}

namespace
{
const std::string MODEL_PATH = "Assets/corner.obj";
const std::string TEXTURE_PATH = "Assets/statue.png";
}  // namespace

Eigen::MatrixXf V;
Eigen::MatrixXi F;
Eigen::VectorXf D;

// Master Engine implementation ----------------------------------------------------------
std::pair<std::vector<float>, std::vector<uint>> GetGeodesicData(std::any& container)
{
    HMesh<VertexData>& mesh = std::any_cast<HMesh<VertexData>&>(container);

    std::vector<float> data;
    std::vector<uint> indices;
    const auto& vertices = mesh.VertexData();
    for(uint i = 0; i < D.rows(); i++)
    {
        data.push_back(vertices[i].position[0]);
        data.push_back(vertices[i].position[1]);
        data.push_back(vertices[i].position[2]);

        data.push_back(D[i]);
        data.push_back(D[i]);
        data.push_back(D[i]);
    }

    for(const auto& f: mesh.Faces())
    {
        auto v_ids = f.VertexIds();
        indices.push_back(v_ids[0]);
        indices.push_back(v_ids[1]);
        indices.push_back(v_ids[2]);
    }
    return {data, indices};
}

void Loopy(MasterEngine* engine, void* d)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();

    uint width, height;
    rendering_engine.GetWindow()->GetDimensions(&width, &height);
    MVPOnlyUbo mvp = {};
    mvp.model = Rotate(0.f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    Gallery& asset_map = rendering_engine.GetAssetMap();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("loaded_model");

    CellShadingInfo ubo = {};
    ubo.model = Rotate(0.f, Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    ubo.view = camera.GetViewMatrix();
    ubo.proj = camera.GetProjectionMatrix();

    ubo.camera_position = camera.GetPosition();
    ubo.view_dir = Append(camera.GetLookDirection(), 0.f);
    ubo.far_plane = 5;
    ubo.near_plane = 0.1;

    RenderTarget& target = rendering_engine.CellShadingRender(
        gpu_buffer, ubo, *asset_map.GetGpuImage("statue"));

    //RenderTarget& target = EffectFramework::AtomicEffect(
    //    effect_framework, gpu_buffer,
    //    "shaders/HeatMethod/heat_vis", {},
    //    mvp, 0
    //);

    WireframeDebugInfo wire_info = {};
    wire_info.camera_position = camera.GetPosition();
    wire_info.treat_normal_as_point = false;
    /*GeometryInfo normal_buffer = asset_map.GetGpuGeometryBuffer("arrow");
    EffectFramework::AtomicEffect(
        effect_framework, normal_buffer,
        "shaders/debugging/wireframe_debug", target, {},
        mvp, 0, wire_info, 1
    );*/
    /*
    GeometryInfo n2 = asset_map.GetGpuGeometryBuffer("arrow2");
    EffectFramework::AtomicEffect(
        effect_framework, n2,
        "shaders/debugging/wireframe_debug", target, {},
        mvp, 0, wire_info, 1
    );*/
    GeometryInfo gradient_buffer = asset_map.GetGpuGeometryBuffer("sphere");
    rendering_engine.AtomicEffect(
        gradient_buffer,
        "shaders/debugging/wireframe_debug",
        target,
        {},
        mvp,
        0,
        wire_info,
        1);

    rendering_engine.PresentRender(*target.GetColorImage(0));
}

bool toggle = true;
uint id;
void LeftDrag(void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset)
{
    if(toggle) { ArcballCamera::UpdateCameraAngles(ptr, position, offset); }
}

std::pair<std::vector<float>, std::vector<uint>> GetFloatsFromFloatVector(std::any& input)
{
    return std::any_cast<std::pair<vector<float>, vector<uint>>&>(input);
}

void UpdateGradient(Gallery& asset_map)
{
    auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
    const auto& faces = mesh.Faces();
    gradient = CalculateFacesGradient(mesh, D);
    gradient = CalculateFaceAverageVertexGradient(mesh, gradient);
    vector<float> model_instance_data;
    for(const auto& face: faces)
    {
        auto vertices = face.Vertices();
        for(uint i = 0; i < 3; i++)
        {
            ExtractFloats(vertices[i]->Data().position, model_instance_data);
            ExtractFloats(gradient[vertices[i]->ID()], model_instance_data);
            ExtractFloats(Eigen::Vector3f(0, 1, 0), model_instance_data);
        }
    }
    asset_map.SetGeometryInstanceData("arrow", model_instance_data);
}

void AddDebugInfo(Gallery& asset_map)
{
    auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
    const auto& faces = mesh.Faces();
    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.01, 0.05, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "arrow", GetFloatsFromFloatVector);

        //UpdateGradient(asset_map);
    }
    {
        // arrow
        auto [vertices, indices] = Generate3DArrow(0.01, 0.05, 10, 10);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "arrow2", GetFloatsFromFloatVector);
    }
    {
        // sphere
        auto [vertices, indices] = GenerateSphere(0.1, 15, 15);
        auto normals = GenerateNormals(vertices, indices);
        vector<float> arrow_vertices;
        uint i = 0;
        for(auto& v: vertices)
        {
            ExtractFloats(v, arrow_vertices);
            ExtractFloats(Eigen::Vector2f{0, 0}, arrow_vertices);
            ExtractFloats(normals[i++], arrow_vertices);
        }

        std::pair<std::vector<float>, std::vector<uint>> data = {arrow_vertices, indices};
        uint id = asset_map.StoreGeometryData(data, "sphere", GetFloatsFromFloatVector);

        asset_map.SetGeometryInstanceData(id, vector<float>{0, 0, 0, 0, 1, 0, 1, 0, 0});
    }
}

void LeftClick(void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset)
{
    if(!toggle)
    {
        auto engine = reinterpret_cast<MasterEngine*>(ptr);
        auto camera = &engine->GetCamera();
        auto& asset_map = engine->GetRenderingEngine().GetAssetMap();

        Eigen::Matrix4f mp = camera->GetProjectionMatrix();
        Eigen::Matrix4f mv = camera->GetViewMatrix();

        Eigen::Matrix4f view_proj = mp * mv;

        Eigen::Vector3f unprojected =
            Unproject({position.x(), position.y(), 0}, view_proj);

        Eigen::Vector3f camera_position = camera->GetPosition();
        Eigen::Vector3f dir = unprojected - camera_position;
        dir.normalize();

        auto& mesh = asset_map.GetCpuGeometry<HMesh<VertexData>>("loaded_model");
        auto [face_id, distance] =
            HMesh<VertexData>::RayMeshIntersection(mesh, camera_position, dir);
        Eigen::Vector3f collision = camera_position + dir * distance;
        manifold_curve.push_back({collision.x(), collision.y(), collision.z()});

        asset_map.UpdateGeometryData("loaded_model");
        asset_map.SetGeometryInstanceData("sphere", CurveToInstanceData(manifold_curve));

        if(face_id == HMesh<VertexData>::ABSENT) return;

        asset_map.UpdateGeometryData("loaded_model");
    }
}

int main()
{
    MasterEngine engine;

    auto extent =
        engine.GetRenderingEngine().GetEffectFramework().GetEffectiveDisplayExtent();

    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();
    camera.SetRadius(6);

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
    input_handler.AddEvent(MouseInputState::LEFT_DRAG, LeftDrag, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    Gallery& asset_map = rendering_engine.GetAssetMap();

    // Model
    HMesh<VertexData> mesh(MODEL_PATH);
    //HMesh<VertexData>::LoopSubdivision(mesh);
    //HMesh<VertexData>::LoopSubdivision(mesh);
    //HMesh<VertexData>::LoopSubdivision(mesh);
    auto [edge_m, face_m] = CastToMatrix(mesh);
    Eigen::SparseVector<bool> vs(mesh.Verts().size());
    vs.coeffRef(1) = true;
    vs.coeffRef(0) = true;
    vs.coeffRef(3) = true;
    Eigen::SparseVector<bool> edges(mesh.Edges().size() / 2);
    edges.coeffRef(1) = true;
    edges.coeffRef(0) = true;

    Eigen::SparseVector<bool> faces(mesh.Faces().size());

    auto [star_vertices, star_edges, star_faces] =
        Star(edge_m, face_m, {vs, edges, faces});
    auto [closure_vertices, closure_edges, closure_faces] =
        Closure(edge_m, face_m, {star_vertices, star_edges, star_faces});
    auto [link_vertices, link_edges, link_faces] =
        Link(edge_m, face_m, {vs, edges, faces});

    std::cout << IsComplex(edge_m, face_m, {vs, edges, faces}) << std::endl;
    std::cout << IsPureComplex(edge_m, face_m, {vs, edges, faces}) << std::endl;
    auto [boundary_vertices, boundary_edges, boundary_faces] =
        Boundary(edge_m, face_m, {vs, edges, faces});

    for(uint i = 0; i < star_vertices.size(); i++)
    {
        auto v = link_vertices.coeff(i);
        if(v == 0) continue;
        manifold_curve.push_back(mesh.VertexData()[i].position);
        manifold_curve.push_back({0, 1, 0});
        manifold_curve.push_back({0, 1, 0});
    }

    for(uint i = 0; i < star_edges.size(); i++)
    {
        auto e = link_edges.coeff(i);
        if(e == 0) continue;
        auto& end_point1 = mesh.Edges()[i * 2].Vert();
        auto& end_point2 = mesh.Edges()[i * 2].Next().Vert();

        auto v1 = end_point1.Data().position;
        auto v2 = end_point2.Data().position;
        for(float t = 0.f; t <= 1.f; t += 0.1001f)
        {
            manifold_curve.push_back(t * v1 + (1.f - t) * v2);
            manifold_curve.push_back({0, 1, 0});
            manifold_curve.push_back({0, 0, 1});
        }
    }

    for(uint i = 0; i < star_faces.size(); i++)
    {
        auto f = link_faces.coeff(i);
        if(f == 0) continue;
        auto center = mesh.Faces()[i].Centroid();

        manifold_curve.push_back(center);
        manifold_curve.push_back({0, 1, 0});
        manifold_curve.push_back({1, 0, 0});
    }

    /*auto[vs, idx] = GenerateSphere(1, 15, 15);
    auto normals = GenerateNormals(vs, idx);
    vector<VertexData> sphere_data;
    uint i=0;
    for(auto& v : vs)
    {
        VertexData datum = {};
        datum.position = v;
        datum.uv = {0,0};
        datum.normal = normals[i];
        sphere_data.push_back(datum);
        i++;
    }
    HMesh<VertexData> mesh(sphere_data, idx);*/

    std::vector<Eigen::Vector3f> vertices;
    for(const auto& v: mesh.VertexData())
        vertices.push_back(v.position);

    std::vector<uint> indices;
    for(const auto& f: mesh.Faces())
    {
        auto verts = f.VertexIds();
        for(auto& v: verts)
            indices.push_back(v);
    }
    V = Eigen::MatrixXf(vertices.size(), 3);
    F = Eigen::MatrixXi(indices.size() / 3, 3);

    uint count = 0;
    for(auto& v: vertices)
        V.row(count++) = v;
    count = 0;
    for(uint i = 0; i < indices.size(); i += 3)
    {
        F.row(count++) =
            Eigen::Vector3i{int(indices[i]), int(indices[i + 1]), int(indices[i + 2])}
                .transpose();
    }

    igl::HeatGeodesicsData<float> data;
    //if(!igl::heat_geodesics_precompute(V, F, data)) cout << "ffffffffffffffffffff" << endl;
    Eigen::VectorXi g(1);
    g[0] = 0;
    /*igl::heat_geodesics_solve(data, g, D);*/

    uint id = asset_map.StoreGeometryData(mesh, "loaded_model");  //, GetGeodesicData);
    CpuImage image(TEXTURE_PATH);
    asset_map.StoreImageData(image, "statue");
    AddDebugInfo(asset_map);

    asset_map.SetGeometryInstanceData("sphere", manifold_curve);

    engine.Loop(nullptr, Loopy);

    return EXIT_SUCCESS;
}
