group "Tests"
    project "test"
        kind "ConsoleApp"
        language "C++"
        targetdir "../../build/tests/"
        location "../../Generated/Tests/"

        objdir '!../../Generated/obj/Debug/'

        files {"./Master_test.cpp"}

        filter "configurations:Debug"
            defines { "DEBUG", "TESTING" }
            symbols "On"

        filter "configurations:Release"
            defines { "NDEBUG", "TESTING" }
            optimize "On"

        configuration { "linux", "gmake" }
            buildoptions {"-std=c++2a -O0"}
            linkoptions {"-lstdc++fs -ldl -pg -export-dynamic"}

        -- link to the main library
        libdirs {"build/"}
        links("CoreVulkanEngine")

        includedirs {PROJECT_PATH .. "/Src/Engine/"}
        IncludeLibraries(PROJECT_PATH)
        AddStaticLibraries(PROJECT_PATH)
