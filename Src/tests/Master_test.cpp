//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Master_test.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-04-24
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define ASSERT_DVEC_EQ(v1, v2, epsilon)   \
    ASSERT_NEAR(v1.x(), v2.x(), epsilon); \
    ASSERT_NEAR(v1.y(), v2.y(), epsilon); \
    ASSERT_NEAR(v1.z(), v2.z(), epsilon);

/** @cond */
#include <iostream>

#include "Eigen/Dense"
#include "gtest/gtest.h"
/** @endcond */

#include "Geometry/GeometryUtils.hpp"

using namespace std;

const double EPSILON = 0.003;

namespace
{
// Used for the purposes of testing
inline bool SameSide(
    const Eigen::Vector3d& p1,
    const Eigen::Vector3d& p2,
    const Eigen::Vector3d& a,
    const Eigen::Vector3d& b)
{
    auto cp1 = (b - a).cross(p1 - a);
    auto cp2 = (b - a).cross(p2 - a);
    if(cp1.dot(cp2) >= 0) return true;
    else
        return false;
}

bool PointInTriangleSide(
    const std::vector<Eigen::Vector3d>& triangle, const Eigen::Vector3d& p)
{
    if(SameSide(p, triangle[0], triangle[1], triangle[2]) &&
       SameSide(p, triangle[1], triangle[0], triangle[2]) &&
       SameSide(p, triangle[2], triangle[0], triangle[1]))
        return true;
    else
        return false;
}

bool PointInTriangleNaive(
    const std::vector<Eigen::Vector3d>& triangle, const Eigen::Vector3d& point)
{
    double angle = 0;
    std::array<Eigen::Vector3d, 3> dirs;
    for(uint i = 0; i < 3; i++)
        dirs[i] = (triangle[i] - point).normalized();

    for(uint i = 0; i < 3; i++)
        angle += acos(dirs[i].dot(dirs[(i + 1) % 3]));

    return abs(angle - 2 * M_PI) < 0.000001;
}
}  // namespace

TEST(MathUtilsTests, TrianglePointInclusionTest)
{
    Eigen::Vector3d v1 = {-1, 0, 0};
    Eigen::Vector3d v2 = {1, 0, 0};
    Eigen::Vector3d v3 = {0, 1, 0};
    {
        Eigen::Vector3d v = {0, 0, 0};
        ASSERT_TRUE(TestPointInTriangle({v1, v2, v3}, v));
    }
    {
        Eigen::Vector3d v = {0, 3, 0};
        ASSERT_FALSE(TestPointInTriangle({v1, v2, v3}, v));
    }
    {
        Eigen::Vector3d v = {0, 0, 1};
        ASSERT_FALSE(TestPointInTriangle({v1, v2, v3}, v));
    }
}

TEST(MathUtilsTests, TestRayTriangleIntersection)
{
    Eigen::Vector3d v1 = {-1, 0, 0};
    Eigen::Vector3d v2 = {1, 0, 0};
    Eigen::Vector3d v3 = {0, 1, 0};

    {
        Eigen::Vector3d p = {0, 0, -1};
        Eigen::Vector3d d = {0, 0, 1};
        float t = TriangleLineIntersection(p, d, {v1, v2, v3});
        ASSERT_TRUE(abs(t - 1.0) < 0.0001);
    }
}

TEST(MathUtilsTests, TestRayRayIntersection)
{
    Eigen::Vector3d v1 = {0, 1, 0};
    Eigen::Vector3d d1 = {0, -1, 0};
    Eigen::Vector3d v2 = {1, 0, 0};
    Eigen::Vector3d d2 = {-1, 0, 0};

    auto intersection = LineLineIntersection(v1, d1, v2, d2);
    ASSERT_EQ(intersection, 1);
}

struct Node
{
    typedef std::shared_ptr<Node> NodePtr;
    uint id;
    vector<std::shared_ptr<Node>> neighbours;
    Eigen::Vector3d position;
};

std::vector<Node::NodePtr> NodeGetNeighbours(const Node::NodePtr& v)
{
    return v->neighbours;
}

uint NodeGetId(const Node::NodePtr& v) { return v->id; }

double NodeGetDistance(const Node::NodePtr& v1, const Node::NodePtr& v2)
{
    return (v1->position - v2->position).norm();
}

TEST(MathUtilsTests, DjikstraTreeNodeTest)
{
    std::vector<Node::NodePtr> nodes;
    auto first = make_shared<Node>();
    first->id = 0;
    first->position = Eigen::Vector3d(0, 0, 0);
    nodes.push_back(first);
    uint count = 1;
    for(int i = 1; i < 10; i++)
    {
        for(int j = 0; j < i; j++)
        {
            Node::NodePtr node = make_shared<Node>();
            node->id = count++;
            node->neighbours.push_back(nodes[i - 1]);
            node->position = Eigen::Vector3d(i, j, 0);
            nodes[i - 1]->neighbours.push_back(node);
            nodes.push_back(node);
        }
    }

    auto [parent_list, distance_list] =
        Djikstra(nodes, NodeGetNeighbours, NodeGetId, NodeGetDistance, 0);

    ASSERT_EQ(parent_list[0], 0);
    ASSERT_EQ(parent_list[1], 0);
    ASSERT_EQ(parent_list[2], 1);
    ASSERT_EQ(parent_list[3], 1);
    ASSERT_EQ(parent_list[4], 2);
    ASSERT_EQ(parent_list[5], 2);
    ASSERT_EQ(parent_list[45], 8);
}

TEST(MathUtilsTests, DjikstraLatticeNodeTest)
{
    std::vector<Node::NodePtr> nodes;
    uint count = 0;
    const int radius = 10;
    for(int i = 0; i < radius; i++)
    {
        for(int j = 0; j < radius; j++)
        {
            Node::NodePtr node = make_shared<Node>();
            node->id = count++;
            if(i > 0)
            {
                node->neighbours.push_back(nodes[(i - 1) * radius + j]);
                nodes[(i - 1) * radius + j]->neighbours.push_back(node);
            }
            if(j > 0)
            {
                node->neighbours.push_back(nodes[i * radius + j - 1]);
                nodes[i * radius + j - 1]->neighbours.push_back(node);
            }
            node->position = Eigen::Vector3d(i, j, 0);
            nodes.push_back(node);
        }
    }

    auto [parent_list, distance_list] =
        Djikstra(nodes, NodeGetNeighbours, NodeGetId, NodeGetDistance, 0);

    ASSERT_EQ(parent_list[0], 0);
    ASSERT_EQ(parent_list[1], 0);
    ASSERT_EQ(parent_list[2], 1);
    ASSERT_EQ(parent_list[3], 2);
    ASSERT_EQ(parent_list[4], 3);
    ASSERT_EQ(parent_list[5], 4);
    ASSERT_EQ(parent_list[10], 0);
    ASSERT_EQ(parent_list[70], 60);

    ASSERT_NEAR(distance_list[0], 0, 0.0001);
    ASSERT_NEAR(distance_list[1], 1, 0.0001);
    ASSERT_NEAR(distance_list[9], 9, 0.0001);
    ASSERT_NEAR(distance_list[10], 1, 0.0001);
    ASSERT_NEAR(distance_list[20], 2, 0.0001);
    ASSERT_NEAR(distance_list[90], 9, 0.0001);
    ASSERT_NEAR(distance_list[99], 18, 0.0001);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
