-- Globals
separator = "===================================================================="

-- Setup project
project "CoreVulkanEngine"
    -- Compilation variables
    kind "StaticLib"
    targetdir ("../../build/")
    language "C++"

    files {"./**.hpp", "./**.cpp"}

    filter "configurations:Debug"
        defines { "DEBUG", "TESTING" }
        symbols "On"
    filter "configurations:DebugVerbose"
        defines { "DEBUG", "VERBOSITY_1", "TESTING"}
        symbols "On"
    filter "configurations:Release"
        defines { "NDEBUG" }
        optimize "On"

        includedirs {"./"}
