//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Serializers.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-10-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include "boost/pfr.hpp"
/** @endcond */

template <typename POD>
constexpr void ExtractFloats(const POD& pod, std::vector<float>& data)
{
    auto ptr = reinterpret_cast<const float*>(&pod);
    data.insert(data.end(), ptr, ptr + sizeof(POD) / sizeof(float)); // fixed parameters
}

