//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file EigenHelpers.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <any>
#include <numeric>
#include <string>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

namespace Eigen {

// Benchmarks showed this to be the fastest representation for iteration
typedef Eigen::Matrix<float, Eigen::Dynamic, 3, Eigen::RowMajor> RowMatrix3f;

} // Eigen

std::pair<std::vector<float>, std::vector<uint>>
    GetFloatVectorsFromRowMatrix3f(std::any& input);

std::pair<std::vector<float>, std::vector<uint>>
    GetFloatVectorsFromGenericMatrix(std::any& input);
// Modified from GLM
template <typename D>
Eigen::Matrix4f LookAt(
    const Eigen::Matrix<D, 3, 1>& eye,
    const Eigen::Matrix<D, 3, 1>& center,
    const Eigen::Matrix<D, 3, 1>& up)
{
    const Eigen::Matrix<D, 3, 1> f((center - eye).normalized());
    const Eigen::Matrix<D, 3, 1> s(f.cross(up).normalized());
    const Eigen::Matrix<D, 3, 1> u(s.cross(f));

    Eigen::Matrix<D, 4, 4> result = Eigen::Matrix<D, 4, 4>::Identity();
    result(0, 0) = s.x();
    result(0, 1) = s.y();
    result(0, 2) = s.z();
    result(1, 0) = u.x();
    result(1, 1) = u.y();
    result(1, 2) = u.z();
    result(2, 0) =-f.x();
    result(2, 1) =-f.y();
    result(2, 2) =-f.z();
    result(0, 3) =-s.dot(eye);
    result(1, 3) =-u.dot(eye);
    result(2, 3) = f.dot(eye);
    return result;
}

template <typename T>
Eigen::Matrix4f Perspective(
    T fov,
    T aspect,
    T z_near,
    T z_far)
{
    assert(abs(aspect - std::numeric_limits<T>::epsilon()) > static_cast<T>(0));

    const T tan_half_fovy = tan(fov / static_cast<T>(2));

    Eigen::Matrix<T, 4, 4> result = Eigen::Matrix<T, 4, 4>::Zero();
    result(0, 0) = static_cast<T>(1) / (aspect * tan_half_fovy);
    result(1, 1) = static_cast<T>(1) / (tan_half_fovy);
    result(2, 2) = - (z_far + z_near) / (z_far - z_near);
    result(3, 2) = - static_cast<T>(1);
    result(2, 3) = - (static_cast<T>(2) * z_far * z_near) / (z_far - z_near);
    return result;
}

template<typename D>
D Radians(D degrees)
{
    assert(std::numeric_limits<D>::is_iec559 && "'Radians' only accept floating-point input");
    return degrees * static_cast<D>(0.01745329251994329576923690768489);
}

template<typename D>
Eigen::Matrix<D, 4, 4> Rotate(D angle, const Eigen::Matrix<D, 3, 1>& axis)
{
    Eigen::Matrix<D, 3, 3> rotation(Eigen::AngleAxis<D>(angle, axis));

    Eigen::Matrix<D, 4, 4> result;
    result <<
        rotation.row(0), 0,
        rotation.row(1), 0,
        rotation.row(2), 0,
        0, 0, 0, 1;

    return result;
}

template<typename D>
Eigen::Quaternion<D> RotateQ(D angle, const Eigen::Matrix<D, 3, 1>& axis)
{
    return Eigen::Quaternion<D>(Eigen::AngleAxis<D>(angle, axis));
}

template<typename D, int N>
Eigen::Matrix<D, N + 1, 1> Append(const Eigen::Matrix<D, N, 1>& v, D s)
{
    auto vec = Eigen::Matrix<D, N+1, 1>();
    vec << v, s;
    return vec;
}

template<typename D, int N, int M>
Eigen::Matrix<D, N + M, 1> Append(
    const Eigen::Matrix<D, N, 1>& v,
    const Eigen::Matrix<D, M, 1>& u)
{
    auto vec = Eigen::Matrix<D, N + M, 1>();
    vec << v, u;
    return vec;
}

template<typename D, int N>
Eigen::Matrix<D, N-1, 1> XYZ(const Eigen::Matrix<D, N, 1>& v)
{
    static_assert(N >= 3);
    auto vec = Eigen::Matrix<D, N-1, 1>();
    vec << v[0], v[1], v[2];
    return vec;
}

template<typename D, int N>
Eigen::Matrix<D, N-1, 1> XY(const Eigen::Matrix<D, N, 1>& v)
{
    static_assert(N >= 2);
    auto vec = Eigen::Matrix<D, N-1, 1>();
    vec << v[0], v[1];
    return vec;
}

