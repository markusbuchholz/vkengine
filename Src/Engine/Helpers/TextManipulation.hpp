//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file TextManipulation.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-08-18
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <string>
#include <vector>
/** @endcond */

namespace TM {
inline void LefTrim(std::string& str)
{
    size_t first = str.find_first_not_of(" \n\r\t");
    if(std::string::npos == first)
    {
        return;
    }
    str = str.substr(first, str.size());
}

inline void RightTrim(std::string& str)
{
    size_t last = str.find_last_not_of(" \n\r\t");
    if(std::string::npos == last)
    {
        return;
    }
    str = str.substr(0, last + 1);
}

inline void Trim(std::string& str)
{
    LefTrim(str);
    RightTrim(str);
}

inline std::vector<std::string> Split(const std::string& str, char separator = ' ')
{
    std::vector<std::string> tokens;
    std::string accumulated = "";
    for(auto& c : str)
    {
        if(c != separator) accumulated += c;
        else
        {
            Trim(accumulated);
            tokens.push_back(accumulated);
            accumulated = "";
        }
    }
    Trim(accumulated);
    tokens.push_back(accumulated);

    return tokens;
}

} // TM

