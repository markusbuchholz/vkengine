//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file log.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2018-04-14
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

// IO libraries

/** @cond */
#include <algorithm>
#include <chrono>
#include <ctime>
#include <fstream>
#include <iostream>
#include <libgen.h>
#include <regex>
#include <sstream>
#include <string>
#include <sys/stat.h>
/** @endcond */

// Time handling libraries

namespace Log
{
// Style characters
extern const char* CHECK;
extern const char* CROSS;
// Width of the log
const uint LOG_WIDTH = 80;
/**
 * @brief Frame for log section titles
 *
 */
const std::string TITLE_BAR = std::string(LOG_WIDTH, '=');
/**
 * @brief Bar to separate text regions
 *
 */
const std::string SEPARATOR_BAR = std::string(LOG_WIDTH, '-');
/**
 * @brief Frame for log errors
 *
 */
const std::string ERROR_BAR = std::string(LOG_WIDTH, '!');
/**
 * @brief Set the log file string
 *
 */
void SetLogFile(const std::string& log_path);
/**
 * @brief Delete the log file
 *
 */
void WipeLog();
/**
 * @brief Append a message a the end of the log file
 *
 * @param message The message to record in the log
 */
void RecordLog(const std::string& message);
/**
 * @brief A function to print log messages with a specially aligned format
 *
 * The function records the first message plus some alignment plus the second
 * message. Space is added between the first and the second message such that
 * the second message always starts at the same character position.
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 */
void RecordLog(const std::string& message1, const std::string& message2, int alignment);
/**
 * @brief Similar to RecordLog(const std::string& message)
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 * @param fill the character that will be used to fill the aligned space
 */
void RecordLog(
    const std::string& message1, const std::string& message2, int alignment, char fill);
/**
 * @brief Similar to RecordLog(const std::string& message) except it
 * styles it with a title bar.
 *
 * @param message The message to style
 *
 */
void RecordLogHeader(const std::string& message);
/**
 * @brief Similar to RecordLog(std::string message) except it
 * styles it with a title bar.
 *
 * @param message1 The first part of the message
 * @param message2 The second part of the message
 * @param alignment The character position where the second part of the message
 * should be.
 */
void RecordLogHeader(
    const std::string& message1, const std::string& message2, int alignement);
/**
 * @brief Similar to RecordLog(std::string message) except it
 * styles it as an error message.
 *
 * @param message The message to record
 */
void RecordLogError(const std::string& message);
/**
 * @brief Record a message followed by the time and date at which this function
 * was called
 *
 * @param message
 */
void RecordLogTime(const std::string& message);
/**
 * @brief Wrap the text to so that no line exceeds the maximum
 *
 * @param text The text to modify
 * @param maximum The maximum line length
 * @return std::string The modified text
 */
std::string WrapText(const std::string& text, int maximum);
}  // namespace Log

