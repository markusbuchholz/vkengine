//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file MatrixMesh.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-10-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include "Eigen/Sparse"
/** @endcond */

#include "HMesh.hpp"

using SBV = Eigen::SparseVector<bool>;
using SBM = Eigen::SparseMatrix<bool>;

template <typename V>
std::pair<SBM, SBM>
CastToMatrix(const HMesh<V>& mesh)
{
    using Scalar = bool;
    auto& verts = mesh.Verts();
    auto& edges = mesh.Edges();
    auto& faces = mesh.Faces();

    uint vert_num = verts.size();
    uint edge_num = edges.size() / 2;
    uint face_num = faces.size();

    Eigen::SparseMatrix<Scalar> edge_matrix(edge_num, vert_num);
    Eigen::SparseMatrix<Scalar> face_matrix(face_num, edge_num);
    for(uint i=0; i < edge_num; i++)
    {
        auto& edge = edges[i * 2];
        auto& v1 = edge.Vert();
        auto& v2 = edge.Next().Vert();
        edge_matrix.coeffRef(i, v1.ID()) = true;
        edge_matrix.coeffRef(i, v2.ID()) = true;
    }

    for(uint i=0; i < face_num; i++)
    {
        auto& face = faces[i];
        auto edges = face.Edges();

        face_matrix.coeffRef(i, edges[0]->ID() / 2) = true;
        face_matrix.coeffRef(i, edges[1]->ID() / 2) = true;
        face_matrix.coeffRef(i, edges[2]->ID() / 2) = true;
    }

    return {edge_matrix, face_matrix};
}

std::array<SBV, 3> Star(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

std::array<SBV, 3> Closure(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

std::array<SBV, 3> Link(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

std::array<SBV, 3> Boundary(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

bool IsComplex(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

int IsPureComplex(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices);

