//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file HMeshUtils.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-10-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

#include "HMesh.hpp"

/** @cond */
#include <set>
/** @endcond */

template<typename V>
HMesh<V> SubMesh(const HMesh<V>& mesh, const std::vector<uint>& face_indices)
{
    std::vector<V> sub_mesh_data;
    std::vector<uint> sub_mesh_connectivity;
    const auto& faces = mesh.Faces();
    const auto& data = mesh.VertexData();

    std::map<uint, uint> vertex_map;
    uint count = 0;
    for(auto fi : face_indices)
    {
        auto vertices = faces[fi].VertexIds();
        for(uint i=0; i < 3; i++)
        {
            if(vertex_map.count(vertices[i]) == 0)
            {
                vertex_map[vertices[i]] = count++;
                sub_mesh_data.push_back(data[vertices[i]]);
            }
        }
    }

    for(auto fi : face_indices)
    {
        auto vertices = faces[fi].VertexIds();
        for(uint i=0; i < 3; i++)
        {
            sub_mesh_connectivity.push_back(vertex_map[vertices[i]]);
        }
    }

    return HMesh(sub_mesh_data, sub_mesh_connectivity);
}

template<typename V>
std::vector<uint> FaceFlooding(uint start_face_id, uint count, const HMesh<V>& mesh)
{
    std::vector<std::pair<uint, uint>> stack = {{start_face_id, count}};
    std::set<uint> flooded_faces = {start_face_id};
    while(!stack.empty())
    {
        auto[face_id, count] = stack.back();
        stack.pop_back();

        if(count-- <= 0) continue;

        auto edges = mesh.Faces()[face_id].Edges();
        for(auto edge : edges)
        {
            uint new_face_id = edge->Pair().Face().ID();
            if(new_face_id == HMesh<V>::ABSENT) continue;
            auto[_, is_new] = flooded_faces.insert(new_face_id);
            if(is_new)
            {
                stack.push_back({new_face_id, count});
            }
        }
    }

    return std::vector<uint>(flooded_faces.begin(), flooded_faces.end());
    return std::vector<uint>(flooded_faces.begin(), flooded_faces.end());
}
