//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file MeshGradient.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-09-01
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <vector>
/** @endcond */

#include "GeometryUtils.hpp"
#include "HMesh.hpp"

template <typename V>
V CalculateGradientBasis(const V& v1, const V& v2, const V& v3)
{
    using Scalar = decltype(v1.x());

    auto d1 = v2 - v1;
    auto d2 = v3 - v1;
    auto d3 = v3 - v2;

    auto normal = GLA::Cross(d1, d2); // twice the area of the triangle
    Scalar area = normal.norm() / static_cast<Scalar>(2);
    normal = GLA::Normalize(normal);

    return GLA::Cross(normal, d3) / area;
}

template <typename V, typename F>
auto CalculateSimplifiedVertexGradient(const HMesh<V>& mesh, const F& f)
    ->std::vector<decltype(std::declval<V>().position)>
{
    using Vec = decltype(std::declval<V>().position);
    using MFace = typename HMesh<V>::MFace;

    const std::vector<MFace>& faces = mesh.Faces();

    std::vector<Vec> gradient(mesh.Verts().size(), {0,0,0});

    for(const auto& face : faces)
    {
        auto verts = face.Vertices();
        const Vec v1 = (*verts[0]).Data().position;
        const Vec v2 = (*verts[1]).Data().position;
        const Vec v3 = (*verts[2]).Data().position;

        auto g1 = CalculateGradientBasis(v1, v2, v3);
        auto g2 = CalculateGradientBasis(v2, v3, v1);
        auto g3 = CalculateGradientBasis(v3, v1, v2);

        auto f1 = f[verts[0]->ID()];
        auto f2 = f[verts[1]->ID()];
        auto f3 = f[verts[2]->ID()];

        gradient[verts[0]->ID()] += (f2 - f1) * g2 + (f3 - f1) * g3;
        gradient[verts[1]->ID()] += (f1 - f2) * g1 + (f3 - f2) * g3;
        gradient[verts[2]->ID()] += (f2 - f3) * g2 + (f1 - f3) * g1;
    }

    for(auto& g : gradient)
    {
        g = GLA::Normalize(g);
    }

    return gradient;
}

template <typename V, typename G>
auto CalculateFaceAverageVertexGradient(const HMesh<V>& mesh, const G& face_gradient)
    ->std::vector<decltype(std::declval<V>().position)>
{
    using Vec = decltype(std::declval<V>().position);
    using MVert = typename HMesh<V>::MVert;

    const std::vector<MVert>& vertices = mesh.Verts();

    std::vector<Vec> gradient(mesh.Verts().size(), {0,0,0});

    for(const auto& vertex : vertices)
    {
        auto faces = vertex.Faces();
        Vec normal = {0, 0, 0};
        for(auto face : faces)
        {
            normal += face->UnormalizedNormal();
        }

        normal.normalize();

        for(auto face : faces)
        {
            gradient[vertex.ID()] =
                  face_gradient[face->ID()]
                - normal.dot(face_gradient[face->ID()]) * normal
                * face->UnormalizedNormal().norm();
        }

        gradient[vertex.ID()].normalize();
    }

    return gradient;
}

template <typename V, typename F>
auto CalculateFacesGradient(const HMesh<V>& mesh, const F& f)
    ->std::vector<decltype(std::declval<V>().position)>
{
    using Vec = decltype(std::declval<V>().position);
    using MFace = typename HMesh<V>::MFace;

    const std::vector<MFace>& faces = mesh.Faces();

    std::vector<Vec> gradient(mesh.Faces().size(), {0,0,0});

    for(const auto& face : faces)
    {
        auto verts = face.Vertices();
        const std::array<Vec, 3> vertices =
        {
            (*verts[0]).Data().position,
            (*verts[1]).Data().position,
            (*verts[2]).Data().position
        };

        float area = (vertices[1] - vertices[0]).cross(vertices[2] - vertices[0]).norm();
        Vec normal = face.Normal();

        for(uint i=0; i < 3; i++)
        {
            gradient[face.ID()] += f[verts[i]->ID()] * normal.cross(
                vertices[(i + 1) % 3] - vertices[(i + 2) % 3]);
        }

        gradient[face.ID()] *= 1.f / area;
    }

    for(auto& g : gradient)
    {
        g = GLA::Normalize(g);
    }

    return gradient;
}

template<typename V, typename F, typename G>
V FaceGradient(const V& start, const F& current_face, const G& gradient)
{
    auto vertices = current_face->VerticesD();
    auto[u, v, w] = Barycentric(
        start,
        vertices[0]->Data().position,
        vertices[1]->Data().position,
        vertices[2]->Data().position);

    const V g1 = gradient[vertices[0]->ID()];
    const V g2 = gradient[vertices[1]->ID()];
    const V g3 = gradient[vertices[2]->ID()];

    const V g = u * g1 + v * g2 + w * g3;
    const V normal = current_face->Normal();
    const V binormal = GLA::Normalize(GLA::Cross(g, normal));

    const V fg = g - g.dot(normal) * normal;

    return GLA::Normalize(GLA::Cross(normal, binormal)) * fg.norm();
}

template<typename V, typename F, typename G>
float FunctionValue(const V& start, const F& current_face, const G& function)
{
    auto vertices = current_face->VerticesD();
    auto[u, v, w] = Barycentric(
        start,
        vertices[0]->Data().position,
        vertices[1]->Data().position,
        vertices[2]->Data().position);

    const float g1 = function[vertices[0]->ID()];
    const float g2 = function[vertices[1]->ID()];
    const float g3 = function[vertices[2]->ID()];

    return u * g1 + v * g2 + w * g3;
}

template<
    typename V,
    typename G,
    typename D,
    typename Vec = decltype(std::declval<V>().position)
>
std::vector<Vec> ComputeGradientLine(
    HMesh<V>& mesh,
    const Vec& start,
    uint start_face_id,
    uint end_vert_id,
    const G& gradient,
    const D& function)
{
    using MFace = typename HMesh<V>::MFace;
    using MEdge = typename HMesh<V>::MEdge;
    const MFace* current_face = &mesh.Faces()[start_face_id];

    Vec face_g = -FaceGradient(start, current_face, gradient);

    std::vector<Vec> poly_line;
    poly_line.push_back(start);
    Vec source = start;
    float current_distance = FunctionValue(start, current_face, function);
    const MEdge* current_edge = &current_face->Edge();
    for(uint i=0; current_distance > 0.001; i++)
    {
        auto vertices = current_face->VerticesD();
        auto vert_ids = current_face->VertexIds();
        if(std::find(vert_ids.begin(), vert_ids.end(), end_vert_id) != vert_ids.end())
        {
            return poly_line;
        }

        const std::vector<const MEdge*> edges =
        {
            &(current_edge->Next()),
            &(current_edge->Prev()),
            current_edge
        };
        // All edges are valid possible intersections on the first iterations.
        // Afterwards we consider only the edges not present in the prior iteration.
        auto[collision, hit_flag, edge_index] = CoPlanarLinePolyLineIntersection(
            source,
            face_g,
            {edges[0]->Vert().Data().position,
             edges[1]->Vert().Data().position,
             edges[2]->Vert().Data().position},
            !i);

        if(!hit_flag)
        {
            Log::RecordLogError("Didn't hit the triangle. " + std::to_string(i));

            poly_line.push_back(collision);
            return poly_line;
        }
        current_edge = &edges[edge_index]->Pair();

        poly_line.push_back(collision);

        current_face = &current_edge->Face();
        source = collision;
        face_g = -FaceGradient(source, current_face, gradient);

        current_distance = FunctionValue(source, current_face, function);
    }
    return poly_line;
}

template<
    typename V,
    typename Vec = decltype(std::declval<V>().position)
>
Vec FollowGradient(
    HMesh<V>& mesh,
    const Vec& start,
    uint start_face_id,
    const Vec& grad,
    const float target_distance)
{
    Vec gradient = target_distance < 0? -grad : grad;
    using MFace = typename HMesh<V>::MFace;
    using MEdge = typename HMesh<V>::MEdge;
    const MFace* current_face = &mesh.Faces()[start_face_id];
    const MEdge* current_edge = &current_face->Edge();
    Vec source = start;
    Vec last_collision = start + gradient;

    float current_distance = 0;
    float final_distance = abs(target_distance);
    Vec dir;
    for(uint i=0; current_distance < final_distance && i < 1000; i++)
    {
        dir = last_collision - source;
        dir = dir.norm() < 0.0001? gradient.normalized() : dir.normalized();
        const std::vector<const MEdge*> edges =
        {
            &(current_edge->Next()),
            &(current_edge->Prev()),
            current_edge
        };
        // All edges are valid possible intersections on the first iterations.
        // Afterwards we consider only the edges not present in the prior iteration.
        auto[collision, hit_flag, edge_index] = CoPlanarLinePolyLineIntersection(
            source,
            dir,
            {edges[0]->Vert().Data().position,
             edges[1]->Vert().Data().position,
             edges[2]->Vert().Data().position},
            !i);

        if(!hit_flag)
        {
            Log::RecordLogError("Didn't hit the triangle (FG). " + std::to_string(i));
            return last_collision;
        }

        current_edge = &edges[edge_index]->Pair();
        current_face = &current_edge->Face();
        Vec normal = current_face->Normal();
        Vec edge_dir = current_edge->Dir();

        Vec v1 = current_edge->Vert().Data().position;
        Vec v2 = current_edge->Next().Vert().Data().position;
        Vec line_proj = ProjectPointOntoLine(source, v1, Vec(v2 - v1));

        source =
            line_proj - normal.cross(edge_dir.normalized()) * (line_proj - source).norm();

        current_distance = (collision - source).norm();
        last_collision = collision;
    }
    float correction = current_distance - final_distance;
    Vec face_point = current_edge->Pair().Vert().Data().position;
    Vec face_normal = current_edge->Pair().Face().Normal();
    Vec corrected = last_collision - dir * correction;
    Vec normal_component = (corrected - face_point).dot(face_normal) * face_normal;

    return corrected - normal_component;
}

