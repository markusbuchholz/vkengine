#pragma once

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file HMesh.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-08-03
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <any>
#include <array>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <tuple>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

#include "GeometryUtils.hpp"
#include "Helpers/MetaProgramming.hpp"
#include "Helpers/log.hpp"

HAS_FIELD(normal);
HAS_FIELD(position);
HAS_FIELD(uv);
HAS_FIELD(tex_coord);

struct VertexData
{
    Eigen::Vector3f position;
    Eigen::Vector2f uv;
    Eigen::Vector3f normal;
};

namespace GLA {
template<typename T>
auto Normalize(const T& t)
{ return t / std::sqrt(t[0] * t[0] + t[1] * t[1] + t[2] * t[2]); }

template<typename T, typename U>
Eigen::Vector3f Cross(const T& x, const U& y)
{
    return Eigen::Vector3f(
	    x[1] * y[2] - y[1] * x[2],
	    x[2] * y[0] - y[2] * x[0],
	    x[0] * y[1] - y[0] * x[1]); }
}
/**
 * @brief Half edge data structure.
 * Encapsulates a manifold mesh as a directed graph and provides useful operations for
 * geometric manipulation, such as edge splittis, flps, face splitting...
 *
 * For detailed information on the operations, see [the docs](Docs/HalfEdge.md)
 *
 * @tparam VertexData The underlying type that encapuslates vertex data. It MUST
 * have publically accessible members: position, uv, normal.
 */
template <typename V = VertexData>
class HMesh
{
public:
    // Denote a missing value, treat this as equivalent to NULL.
    static constexpr uint ABSENT = std::numeric_limits<uint>::max();
    struct MFace; struct MEdge;

    enum TagColor
    {
        BLACK,  // Default
        BLUE,   // blue tag
        YELLOW  // yellow tag
    };
    /**
     * @brief Class encapsulating a half edge vertex.
     *
     */
    struct MVert
    {
        friend class HMesh<V>;
    private:
        uint edge = ABSENT;
        TagColor color = BLACK;
        HMesh<V>* mesh = nullptr;

    public:
        MVert() = default;
        MVert(HMesh* mesh) : mesh(mesh) {}
        void Edge(uint id) { edge = id; }
        const MEdge& Edge() const { return mesh->edges[edge]; }
        MEdge& EdgeD() { return mesh->edges[edge]; }

        V& DataD() { return mesh->vertex_data[ID()]; }
        void Data(const V& v) { mesh->vertex_data[ID()] = v; }
        const V& Data() const { return mesh->vertex_data[ID()]; }

        const TagColor Color() const { return color; }
        void Color(TagColor c) { color = c; }

        std::vector<MEdge*> Neighbours() const
        {
            std::vector<MEdge*> edges;
            uint current_edge = edge;
            uint count = 0;
            do
            {
                edges.push_back(&mesh->edges[current_edge]);
                // If the next edge is null, terminate.
                // We assume the mesh was initialized such that the outgoing edge
                //from a vertex is guaranteed to be a boundary edge.
                if(mesh->edges[current_edge].Prev().Pair().IsBoundary()) break;
                assert(mesh->edges[current_edge].Prev().Pair().ID() != current_edge);
                current_edge = mesh->edges[current_edge].Prev().Pair().ID();
            } while(current_edge != edges[0]->ID() && count++ < 1000);
            return edges;
        }

        std::vector<MFace*> Faces() const
        {
            std::vector<MEdge*> edges = Neighbours();
            std::vector<MFace*> faces;
            for(auto& n : edges)
            {
                faces.push_back(&n->FaceD());
            }
            return faces;
        }

        uint ID() const { return this - &(mesh->verts[0]); }
    };
    /**
     * @brief Class encapuslating a half edge.
     *
     */
    struct MEdge
    {
        friend class HMesh<V>;
    private:
        uint vert = ABSENT;
        uint next = ABSENT ;
        uint prev = ABSENT ;
        uint pair = ABSENT ;
        uint face = ABSENT ;
        TagColor color = BLACK;

        HMesh<V>* mesh = nullptr;
    public:
        MEdge() = default;
        MEdge(HMesh* mesh) : mesh(mesh) {}

        const MVert& Vert() const { assert(vert != ABSENT); return mesh->verts[vert]; }
        const MEdge& Next() const { assert(next != ABSENT); return mesh->edges[next]; }
        const MEdge& Prev() const { assert(prev != ABSENT); return mesh->edges[prev]; }
        const MEdge& Pair() const { assert(pair != ABSENT); return mesh->edges[pair]; }
        const MFace& Face() const { assert(face != ABSENT); return mesh->faces[face]; }
        const TagColor Color() const { return color; }

        MVert& VertD() { assert(vert != ABSENT); return mesh->verts[vert]; }
        MEdge& NextD() { assert(next != ABSENT); return mesh->edges[next]; }
        MEdge& PrevD() { assert(prev != ABSENT); return mesh->edges[prev]; }
        MEdge& PairD() { assert(pair != ABSENT); return mesh->edges[pair]; }
        MFace& FaceD() { assert(face != ABSENT); return mesh->faces[face]; }

        void Vert(uint id) { assert(id < mesh->verts.size()); vert = id; }
        void Next(uint id) { assert(id < mesh->edges.size()); next = id; }
        void Prev(uint id) { assert(id < mesh->edges.size()); prev = id; }
        void Pair(uint id) { assert(id < mesh->edges.size()); pair = id; }
        void Face(uint id) { assert(id < mesh->faces.size()); face = id; }
        void Color(TagColor c) { color = c; }

        bool IsBoundary() const { return face == ABSENT; }

        uint ID() const { return this - &(mesh->edges[0]);}

        std::pair<const MVert*, const MVert*> const Vertices()
        {return {&Vert(), &(Next().Vert())};}

        auto Dir() const { return Next().Vert().Data().position - Vert().Data().position; }

        std::pair<MVert*, MVert*> VerticesD()
        { return {&VertD(), &(NextD().VertD())}; }
    };
    /**
     * @brief Class encapuslating a half edge face.
     *
     */
    struct MFace
    {
        friend class HMesh<V>;
    private:
        MFace() = default;
        MFace(HMesh* mesh) : mesh(mesh) {}
        uint edge = ABSENT;
        std::array<Eigen::Vector3f, 3> face_normals;
        std::array<Eigen::Vector2f, 3> face_uvs;
        TagColor color = BLACK;

        HMesh<V>* mesh = nullptr;

    public:
        void Edge(uint id) { edge = id;}

        const MEdge& Edge() const { return mesh->edges[edge]; }

        MEdge& EdgeD() { return mesh->edges[edge]; }

        uint ID() const { return this - &(mesh->faces[0]); }

        std::array<Eigen::Vector2f, 3> FaceUvs() const { return face_uvs; }
        Eigen::Vector3f Normal() const
        {
            return GLA::Normalize(UnormalizedNormal());
        }

        Eigen::Vector3f UnormalizedNormal() const
        {
            const auto ids = VertexIds();
            const Eigen::Vector3f e1 =
                (mesh->vertex_data[ids[1]].position - mesh->vertex_data[ids[0]].position)
                .block(0,0,3,1);
            const Eigen::Vector3f e2 =
                (mesh->vertex_data[ids[2]].position - mesh->vertex_data[ids[0]].position)
                .block(0,0,3,1);
            return GLA::Cross(e1, e2);
        }

        std::array<uint, 3> VertexIds() const
        {
            assert(edge != ABSENT);
            const auto& v_edge = mesh->edges[edge];

            return
            {
                v_edge.Vert().ID(),
                v_edge.Next().Vert().ID(),
                v_edge.Prev().Vert().ID()
            };
        }

        std::array<const MVert*, 3> Vertices() const
        {
            assert(edge != ABSENT);
            const auto& v_edge = mesh->edges[edge];

            return
            {
                &v_edge.Vert(),
                &v_edge.Next().Vert(),
                &v_edge.Prev().Vert()
            };
        }

        std::array<MVert*, 3> VerticesD() const
        {
            assert(edge != ABSENT);
            auto& v_edge = mesh->edges[edge];

            return
            {
                &v_edge.VertD(),
                &v_edge.NextD().VertD(),
                &v_edge.PrevD().VertD()
            };
        }

        std::array<const MEdge*, 3> Edges() const
        {
            assert(edge != ABSENT);
            MEdge& v_edge = mesh->edges[edge];

            return
            {
                &v_edge,
                &v_edge.NextD(),
                &v_edge.PrevD()
            };
        }

        std::array<MEdge*, 3> EdgesD()
        {
            assert(edge != ABSENT);
            MEdge& v_edge = mesh->edges[edge];

            return
            {
                &v_edge,
                &v_edge.NextD(),
                &v_edge.PrevD()
            };
        }

        auto Centroid() const
        {
            const auto& v_edge = mesh->edges[edge];
            const std::array<V, 3> vertices =
            {
                v_edge.Vert().Data(),
                v_edge.Next().Vert().Data(),
                v_edge.Prev().Vert().Data()
            };
            auto centroid = vertices[0].position;
            centroid += vertices[1].position;
            centroid += vertices[2].position;

            return centroid / 3.f;
        }
    };

private:
    uint id;

    std::vector<V> vertex_data;
    std::vector<MVert> verts;
    std::vector<MEdge> edges;
    std::vector<MFace> faces;

    void SetID(uint new_id) { id = new_id; }
    void UpdatePointers();

    void Init(
        const std::vector<Eigen::Vector3f>& vertices,
        const std::vector<Eigen::Vector2f>& uvs,
        const std::vector<Eigen::Vector3f>& normals,
        const std::vector<uint>& vertex_indices,
        const std::vector<uint>& uv_indices,
        const std::vector<uint>& normal_indices);

public:
    HMesh() {};
    HMesh(const std::string& file_path);
    HMesh(const std::vector<V>& data, const std::vector<uint>& connectivity);
    HMesh(const HMesh& other)
        : id(other.id)
        , vertex_data(other.vertex_data)
        , verts(other.verts)
        , edges(other.edges)
        , faces(other.faces) { UpdatePointers(); }

    HMesh(HMesh&& other) = delete; // No move constructor
    ~HMesh() = default;

    HMesh& operator=(const HMesh& other);
    HMesh& operator=(HMesh&& other);

    const std::vector<V>& VertexData() const { return vertex_data; };
    const std::vector<MVert>& Verts() const { return verts; };
    const std::vector<MEdge>& Edges() const { return edges; }
    const std::vector<MFace>& Faces() const { return faces; }

    std::vector<V>& VertexDataD() { return vertex_data; };
    std::vector<MVert>& VertsD() { return verts; };
    std::vector<MEdge>& EdgesD() { return edges; }
    std::vector<MFace>& FacesD() { return faces; }

    std::vector<std::pair<MVert*, MVert*>> EdgeVertices();
    /**
     * @brief Calculates the normals of each vertex and sets their value.
     *
     */
    void CalculateNormals();
    /**
     * @brief Split a face into 3 new faces. Default value of the new vertex is the
     * centroid of the face.
     *
     * @param face_id Id of the face to split
     * @param mesh Mesh containing the face.
     */
    static void SplitFace(uint face_id, HMesh<V>& mesh);
    /**
     * @brief Divide an edge into 2 new edges (Introduces new faces as well).
     *
     * @param edge_id ID of edge to split.
     * @param mesh Mesh containing the edge.
     */
    static void SplitEdge(uint edge_id, HMesh<V>& mesh);
    /**
     * @brief Same as @ref SplitEdge but specialized for boundary edges.
     *
     * @param edge_id ID of edge to split.
     * @param mesh Mesh containing the edge.
     */
    static void SplitBoundaryEdge(uint face_id, HMesh<V>& mesh);
    /**
     * @brief Split all edges in the mesh.
     *
     * @param mesh
     */
    static void SplitEdges(HMesh<V>& mesh);
    /**
     * @brief Flip an edge.
     *
     * @param edge_id ID of edge to flip.
     * @param mesh Mesh containing the edge.
     */
    static void FlipEdge(uint edge_id, HMesh<V>& mesh);
    /**
     * @brief Flip all edges in a range. Parameters must be within the total number
     * of edges currently stored in the mesh. And `start` must be less than `end`.
     *
     * @param start Beginning of the range (inclusive).
     * @param end End of the range (exclusive).
     * @param mesh Mesh containing the edges.
     */
    static void  FlipEdges(uint start, uint end, HMesh<V>& mesh);
    /**
     * @brief Split all edges in the mesh and then flip some of the newly introduced
     * edges. It creates a regular trianglular subdivision pattern but has no effect on
     * the shape. i.e. the new mesh has the exact same shape as the input.
     *
     * @param mesh Mesh to refine.
     * @param reset_colors Whether to reset the colors of the mesh.
     */
    static void RefineEdges(HMesh<V>& mesh, bool reset_colors=false);
    /**
     * @brief Execute Loop subdivision on a mesh.
     *
     * @param mesh Mesh to subdivide.
     * @param preserve_boundary Whether the boundary should move or not. If true, the
     * boundary will be subdivided but won't move.
     */
    static void LoopSubdivision(HMesh<V>& mesh, bool preserve_boundary = false);
    /**
     * @brief Sets `e2` as the next of `e1` and `e1` as the prev of `e2`
     *
     * @param e1 First edge.
     * @param e2 Second edge.
     */
    static void AttachEdges(MEdge& e1, MEdge& e2);
    /**
     * @brief Connect all the components of a face together.
     *
     * @param face Face where all the components are contained.
     * @param e1 First edge of the face.
     * @param e2 Second edge of the face.
     * @param e3 Third edge of the face.
     */
    static void ConnectFace(MFace& face, MEdge& e1, MEdge& e2, MEdge& e3);
    /**
     * @brief Pair 2 edges together.
     *
     * @param e1 First edge.
     * @param e2 Second edge.
     */
    static void Pair(MEdge& e1, MEdge& e2);
    /**
     * @brief Mark an edge with a color.
     *
     * @param e Edge to mark.
     * @param color Color to use as mark.
     */
    static void Mark(MEdge& e, TagColor color);
    /**
     * @brief Set the endpoints of an edge (affects both half edges in the edge).
     *
     * @param e1 One of the 2 half edges in the edge.
     * @param v1 Endpoint at `e1`.
     * @param v2 Endpoint at the pair of `e1`.
     */
    static void AttachVertices(MEdge& e1, MVert& v1, MVert& v2);
    /**
     * @brief Serialize the mesh into an array of floats and obtian the connectivity
     * info of it's vertices as an index array (used for rendering).
     *
     * @param container std::any containing a HMesh object.
     * @return std::pair<std::vector<float>, std::vector<uint>> Serialized data.
     */
    static std::pair<std::vector<float>, std::vector<uint>>
    GetGeometryData(std::any& container);

    static std::pair<uint, float> RayMeshIntersection(
        HMesh<V>& mesh,
        const Eigen::Vector3f& origin,
        const Eigen::Vector3f& ray);
};

// HE Operations--------------------------------------------------------------------------
template<typename V>
void HMesh<V>::FlipEdge(uint edge_id, HMesh<V>& mesh)
{
    HMesh<V>::MEdge& edge = mesh.edges[edge_id];
    // Gather the elements we need to modify the mesh
    HMesh<V>::MVert& v0 = edge.VertD();
    HMesh<V>::MVert& v1 = edge.PairD().VertD();
    HMesh<V>::MVert& v2 = edge.PrevD().VertD();
    HMesh<V>::MVert& v3 = edge.PairD().PrevD().VertD();

    HMesh<V>::MEdge& e0f1 = edge;
    HMesh<V>::MEdge& e1f1 = edge.NextD();
    HMesh<V>::MEdge& e2f1 = edge.PrevD();
    HMesh<V>::MEdge& e0f2 = edge.PairD();
    HMesh<V>::MEdge& e1f2 = edge.PairD().NextD();
    HMesh<V>::MEdge& e2f2 = edge.PairD().PrevD();

    HMesh<V>::MFace& f1 = edge.FaceD();
    HMesh<V>::MFace& f2 = edge.PairD().FaceD();

    // Re-assign
    ConnectFace(f1, e0f1, e2f1, e1f2);
    ConnectFace(f2, e0f2, e2f2, e1f1);

    AttachVertices(e0f1, v3, v2);
}

template<typename V>
void HMesh<V>::FlipEdges(uint start, uint end, HMesh<V>& mesh)
{
    assert(start <= end && "Wrong order of parameters");
    assert(end <= mesh.edges.size() && "Out of bounds error");

    uint edge_num = mesh.edges.size();
    for(uint i=0; i < edge_num; i++)
    {
        auto[v1, v2] = mesh.edges[i].VerticesD();
        // Never flip a boundary edge.
        if(mesh.edges[i].IsBoundary() || mesh.edges[i].Pair().IsBoundary()) continue;
        if(v1->Color() != v2->Color() && mesh.edges[i].color == BLUE)
        {
            mesh.edges[i].Color(BLACK);
            mesh.edges[i].PairD().Color(BLACK);
            FlipEdge(i, mesh);
        }
    }
    // Assert boundary properties
    for(auto& e : mesh.edges) if(e.IsBoundary()) e.VertD().Edge(e.ID());
}

template<typename V>
void HMesh<V>::RefineEdges(HMesh<V>& mesh, bool reset)
{
    uint v_num_start = mesh.verts.size();
    uint e_num_start = mesh.edges.size();

    HMesh<V>::SplitEdges(mesh);
    // Color the newly introduced vertices.
    for(uint i = v_num_start; i < mesh.verts.size(); i++) mesh.verts[i].Color(BLUE);
    HMesh<V>::FlipEdges(v_num_start, mesh.edges.size(), mesh);

    if(reset)
    {
        for(auto& v : mesh.verts) v.Color(BLACK);
        for(auto& e : mesh.edges) e.Color(BLACK);
    }
}

template<typename V>
void HMesh<V>::LoopSubdivision(HMesh<V>& mesh, bool preserve_edges)
{
    // Capture whatever is the type of the position member.
    using Vec = decltype(std::declval<V>().position);
    assert(mesh.edges.size() % 2 == 0);
    std::vector<Vec> new_positions(mesh.verts.size() + mesh.edges.size() / 2);
    const uint o_vert_num = mesh.vertex_data.size();
    // Calculate the new positions of the original vertices
    for(uint i=0; i < o_vert_num; i++)
    {
        auto& edge = mesh.verts[i].EdgeD();
        assert(edge.Vert().ID() == i);
        if(edge.IsBoundary())
        {
            Vec v = edge.Vert().Data().position;
            Vec a = edge.Next().Vert().Data().position;
            Vec b = edge.Prev().Vert().Data().position;
            if(preserve_edges) new_positions[i] = v;
            else new_positions[i] = (1.f / 8.f) * (a + b) + (3.f / 4.f) * v;
        }
        else
        {
            const std::vector<MEdge*> neighbours = mesh.verts[i].Neighbours();
            const uint n = neighbours.size();
            const float u = n == 3? 3.f/ 16.f : 3.f / (8.f * n);

            Vec n_pos = (1.f - n * u) * mesh.vertex_data[i].position;

            for(auto v : neighbours)
                n_pos += u * v->Next().Vert().Data().position;

            new_positions[i] = n_pos;
        }
    }
    // Calculate the new positions of the edge vertices
    uint count = 0;
    for(uint i=0; i < mesh.edges.size(); i+=2)
    {
        auto* edge = &mesh.edges[i];
        if(edge->Pair().IsBoundary()) edge = &edge->PairD();
        assert(o_vert_num + count < new_positions.size());
        edge->Color(BLUE);
        edge->PairD().Color(BLUE);

        // Check if we are at the boundary.
        if(edge->IsBoundary())
        {
            auto& v1 = edge->Vert().Data().position;
            auto& v2 = edge->Pair().Vert().Data().position;
            // Boundary conditions.
            if(preserve_edges) new_positions[o_vert_num + count++] = v1;
            else new_positions[o_vert_num + count++] = 0.5f * (v1 + v2);
        }
        else
        {
            const Vec& v1 = edge->Vert().Data().position;
            const Vec& v2 = edge->Next().Vert().Data().position;
            // Vertices opposite to the edge at each side
            const Vec& v3 = edge->Prev().Vert().Data().position;
            const Vec& v4 = edge->Pair().Prev().Vert().Data().position;

            assert(o_vert_num + count < new_positions.size());
            new_positions[o_vert_num + count++] =
                3.f / 8.f * (v1 + v2) + 1.f / 8.f * (v3 + v4);
        }
    }

    for(uint i=0; i < mesh.edges.size(); i++) mesh.edges[i].Color(BLACK);
    RefineEdges(mesh);

    assert(new_positions.size() == mesh.verts.size());
    for(uint i=0; i < new_positions.size(); i++)
        mesh.vertex_data[i].position = new_positions[i];
    for(uint i = o_vert_num; i < mesh.verts.size(); i++) mesh.verts[i].Color(BLACK);
    for(uint i = 0; i < mesh.edges.size(); i++) mesh.edges[i].Color(BLACK);
}

// As specified in the docs diagrams: Docs/HalfEdge.md
template<typename V>
void HMesh<V>::SplitEdge(uint edge_id, HMesh<V>& mesh)
{
    // Initialize 6 new half edges and get a reference to each.
    const uint ne = mesh.edges.size();
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});

    HMesh<V>::MEdge& n1f1 = mesh.edges[ne + 0];
    HMesh<V>::MEdge& n2f2 = mesh.edges[ne + 1];
    HMesh<V>::MEdge& n2f3 = mesh.edges[ne + 2];
    HMesh<V>::MEdge& n1f4 = mesh.edges[ne + 3];
    HMesh<V>::MEdge& n0f3 = mesh.edges[ne + 4];
    HMesh<V>::MEdge& n0f4 = mesh.edges[ne + 5];

    // Create the new vertex
    mesh.verts.push_back({&mesh});
    mesh.vertex_data.push_back({});
    HMesh<V>::MVert& m = mesh.verts.back();

    // Allocate space for 2 new faces
    // Create the new faces
    const uint nf = mesh.faces.size();
    mesh.faces.push_back({&mesh});
    mesh.faces.push_back({&mesh});

    HMesh<V>::MFace& f3 = mesh.faces[nf + 0];
    HMesh<V>::MFace& f4 = mesh.faces[nf + 1];

    // Gather the elements we need to modify the mesh
    HMesh<V>::MEdge& e0f1 = mesh.edges[edge_id];
    HMesh<V>::MEdge& e1f1 = e0f1.NextD();
    HMesh<V>::MEdge& e2f1 = e0f1.PrevD();
    HMesh<V>::MEdge& e0f2 = e0f1.PairD();
    HMesh<V>::MEdge& e1f2 = e0f1.PairD().NextD();
    HMesh<V>::MEdge& e2f2 = e0f1.PairD().PrevD();

    HMesh<V>::MVert& v0 = e0f1.VertD();
    HMesh<V>::MVert& v1 = e0f1.PairD().VertD();
    HMesh<V>::MVert& v2 = e0f1.PrevD().VertD();
    HMesh<V>::MVert& v3 = e0f1.PairD().PrevD().VertD();

    HMesh<V>::MFace& f1 = e0f1.FaceD();
    HMesh<V>::MFace& f2 = e0f1.PairD().FaceD();

    // Make the value of the mid point. First grab each individual element then set.
    // First vertex data.
    const auto p0 = v0.Data().position;
    const auto n0 = v0.Data().normal;
    const auto u0 = v0.Data().uv;
    // Second vertex data.
    const auto p1 = v1.Data().position;
    const auto n1 = v1.Data().normal;
    const auto u1 = v1.Data().uv;
    m.Data(V{
        0.5 * p0 + 0.5 * p1,
        0.5 * u0 + 0.5 * u1,
        GLA::Normalize((0.5 * n0 + 0.5 * n1))
    });

    ConnectFace(f1, e0f1, n1f1, e2f1);
    ConnectFace(f2, e0f2, e1f2, n2f2);
    ConnectFace(f3, n0f3, e1f1, n2f3);
    ConnectFace(f4, n0f4, n1f4, e2f2);

    Pair(n0f3, n0f4);
    Pair(n2f3, n1f1);
    Pair(n2f2, n1f4);

    AttachVertices(n2f3, v2, m);
    AttachVertices(n2f2, v3, m);
    AttachVertices(n0f4, v1, m);
    AttachVertices(e0f1, v0, m);

    Mark(n1f1, BLUE);
    Mark(n2f2, BLUE);
}

template<typename V>
void HMesh<V>::SplitEdges(HMesh<V>& mesh)
{
    // Subroutine appends values to the arrays so this prevents an infinite loop.
    uint edge_num = mesh.edges.size();
    for(uint i=0; i < edge_num; i+=2)
    {
        auto* edge = &mesh.edges[i];
        edge->Color(YELLOW);
        edge->PairD().Color(YELLOW);
        if(edge->IsBoundary()) edge = &(edge->PairD());
        if(edge->Pair().IsBoundary())
        {
            const uint id = edge->ID();
            assert(!edge->IsBoundary() || !edge->PairD().IsBoundary());
            SplitBoundaryEdge(id, mesh);
        }
        else SplitEdge(i, mesh);
    }
}

template<typename V>
void HMesh<V>::SplitBoundaryEdge(uint edge_id, HMesh<V>& mesh)
{
    // Create a list of new edges to represent the new 2 faces
    const uint n = mesh.edges.size();
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});

    HMesh<V>::MEdge& n1f1 = mesh.edges[n + 0];
    HMesh<V>::MEdge& n2f3 = mesh.edges[n + 1];
    HMesh<V>::MEdge& n0f3 = mesh.edges[n + 2];
    HMesh<V>::MEdge& n0f4 = mesh.edges[n + 3];

    // Create the new vertex
    mesh.verts.push_back({&mesh});
    mesh.vertex_data.push_back({});
    HMesh<V>::MVert& m = mesh.verts.back();
    // Create the new faces
    mesh.faces.push_back({&mesh});
    HMesh<V>::MFace& f3 = mesh.faces.back();

    HMesh<V>::MEdge& edge = mesh.edges[edge_id];

    // Gather the elements we need to modify the mesh
    HMesh<V>::MEdge& e0f1 = edge;
    HMesh<V>::MEdge& e0f2 = edge.PairD();
    HMesh<V>::MEdge& e1f1 = edge.NextD();
    HMesh<V>::MEdge& e2f1 = edge.PrevD();
    HMesh<V>::MEdge& e2f2 = edge.PairD().PrevD();
    HMesh<V>::MEdge& e1f2 = edge.PairD().NextD();

    HMesh<V>::MVert& v0 = edge.VertD();
    HMesh<V>::MVert& v1 = edge.NextD().VertD();
    HMesh<V>::MVert& v2 = edge.PrevD().VertD();

    HMesh<V>::MFace& f1 = edge.FaceD();

    // Make the value of the mid point. First grab each individual element then set.
    // First vertex data.
    const auto p0 = v0.Data().position;
    const auto n0 = v0.Data().normal;
    const auto u0 = v0.Data().uv;
    // Second vertex data.
    const auto p1 = v1.Data().position;
    const auto n1 = v1.Data().normal;
    const auto u1 = v1.Data().uv;
    m.Data(V{
        0.5 * p0 + 0.5 * p1,
        0.5 * u0 + 0.5 * u1,
        GLA::Normalize((0.5 * n0 + 0.5 * n1))
    });
    ConnectFace(f1, e0f1, n1f1, e2f1);
    ConnectFace(f3, n0f3, e1f1, n2f3);

    Pair(n2f3, n1f1);
    Pair(e0f1, e0f2);
    Pair(n0f3, n0f4);

    AttachEdges(e2f2, n0f4);
    AttachEdges(n0f4, e0f2);
    AttachEdges(e0f2, e1f2);

    AttachVertices(n1f1, m, v2);
    AttachVertices(e0f2, m, v0);
    AttachVertices(n0f3, m, v1);

    m.edge = e0f2.ID();

    Mark(n1f1, BLUE);
}

template<typename V>
void HMesh<V>::SplitFace(uint face_id, HMesh<V>& mesh)
{
    mesh.vertex_data.push_back({});
    mesh.verts.push_back({&mesh});
    HMesh<V>::MVert& c = mesh.verts.back();

    const uint n = mesh.edges.size();
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});
    mesh.edges.push_back({&mesh});

    HMesh<V>::MEdge& n00 = mesh.edges[n + 0];
    HMesh<V>::MEdge& n01 = mesh.edges[n + 1];
    HMesh<V>::MEdge& n10 = mesh.edges[n + 2];
    HMesh<V>::MEdge& n11 = mesh.edges[n + 3];
    HMesh<V>::MEdge& n20 = mesh.edges[n + 4];
    HMesh<V>::MEdge& n21 = mesh.edges[n + 5];

    const uint f = mesh.faces.size();
    mesh.faces.push_back({&mesh});
    mesh.faces.push_back({&mesh});

    auto& f0 = mesh.faces[face_id];
    auto& f1 = mesh.faces[f + 0];
    auto& f2 = mesh.faces[f + 1];

    auto& e0 = f0.EdgeD();
    auto& e1 = f0.EdgeD().NextD();
    auto& e2 = f0.EdgeD().PrevD();

    auto& v0 = e0.VertD();
    auto& v1 = e1.VertD();
    auto& v2 = e2.VertD();

    ConnectFace(f0, n00, e0, n10);
    ConnectFace(f1, n11, e1, n21);
    ConnectFace(f2, n20, e2, n01);

    Pair(n00, n01);
    Pair(n10, n11);
    Pair(n20, n21);

    AttachVertices(n00, c, v0);
    AttachVertices(n11, c, v1);
    AttachVertices(n20, c, v2);

    c.Data({
        (v0.Data().position + v1.Data().position + v2.Data().position) / 3.0,
        (v0.Data().uv + v1.Data().uv + v2.Data().uv) / 3.0,
        {0,0,1}
    });
}

// Helpers -------------------------------------------------------------------------------
template<typename V>
std::vector<std::pair<typename HMesh<V>::MVert*, typename HMesh<V>::MVert*>>
HMesh<V>::EdgeVertices()
{
    std::vector<std::pair<MVert*, MVert*>> vertex_pairs;
    for(auto& edge : edges)
    {
        if(edge.color == BLACK)
        {
            edge.color = YELLOW;
            edge.PairD().Color(YELLOW);
            vertex_pairs.push_back(edge.VerticesD());
        }
    }
    for(auto& edge : edges) { edge.color = BLACK; }

    return vertex_pairs;
}

std::tuple<
    std::vector<Eigen::Vector3f>,
    std::vector<Eigen::Vector2f>,
    std::vector<Eigen::Vector3f>,
    std::vector<uint>,
    std::vector<uint>,
    std::vector<uint>>
ParseObj(const std::string& path);

template <typename V>
void HMesh<V>::AttachVertices(MEdge& e1, MVert& v1, MVert& v2)
{
    e1.Vert(v1.ID());
    v1.Edge(e1.ID());

    e1.PairD().Vert(v2.ID());
    v2.Edge(e1.Pair().ID());
}

template <typename V>
void HMesh<V>::Pair(MEdge& e1, MEdge& e2)
{
    e1.Pair(e2.ID());
    e2.Pair(e1.ID());
}

template <typename V>
void HMesh<V>::AttachEdges(MEdge& e1, MEdge& e2)
{
    e1.Next(e2.ID());
    e2.Prev(e1.ID());
}

template <typename V>
void HMesh<V>::ConnectFace(
    MFace& face, MEdge& e1, MEdge& e2, MEdge& e3)
{
    e1.Face(face.ID());
    e2.Face(face.ID());
    e3.Face(face.ID());

    face.Edge(e1.ID());

    AttachEdges(e1, e2);
    AttachEdges(e2, e3);
    AttachEdges(e3, e1);
}

template <typename V>
void HMesh<V>::Mark(MEdge& e, TagColor color)
{
    e.Color(color);
    e.PairD().Color(color);
}

// Constructors --------------------------------------------------------------------------
template <typename V>
void HMesh<V>::Init(
    const std::vector<Eigen::Vector3f>& vertices,
    const std::vector<Eigen::Vector2f>& uvs,
    const std::vector<Eigen::Vector3f>& normals,
    const std::vector<uint>& vertex_indices,
    const std::vector<uint>& uv_indices,
    const std::vector<uint>& normal_indices)
{
    assert(vertices.size() > 0);
    assert(vertex_indices.size() > 0);
    assert(vertex_indices.size() % 3 == 0);
    this->vertex_data.resize(vertices.size());
    this->verts.resize(vertices.size());
    // Count he number of full edges, then allocate that many x2 half edges.
    std::set<std::pair<uint, uint>> unique_edges;
    for(uint i=0; i<vertex_indices.size(); i+=3)
    {
        for(uint j=0; j < 3; j++)
        {
            uint v1 = vertex_indices[i + j];
            uint v2 = vertex_indices[i + (j + 1) % 3];
            if(v2 < v1) std::swap(v1, v2);
            unique_edges.insert({v1, v2});
        }
    }
    this->edges.resize(unique_edges.size() * 2);
    // Initialize vertex data
    for(uint i = 0; i < vertices.size(); i++)
    {
        this->vertex_data[i].position[0] = vertices[i].x();
        this->vertex_data[i].position[1] = vertices[i].y();
        this->vertex_data[i].position[2] = vertices[i].z();
    }
    assert(uv_indices.empty() || uv_indices.size() == vertex_indices.size());

    std::map<std::tuple<uint, uint>, uint> edges_map;
    uint allocated_edges = 0;
    // Iterate over every face and store information on the maps.
    for(uint face_v0 = 0; face_v0 < vertex_indices.size(); face_v0 += 3)
    {
        // Allocate the face
        this->faces.push_back(this);
        MFace& face = this->faces.back();
        // For each edge, if its pair exists, store it at it's edge + 1.
        // If the pair does not exist, allocate 2 new edges and store the current edge
        // at the even offset of the allocation.
        const std::array<uint, 3> face_vi =
        {
            vertex_indices[face_v0],
            vertex_indices[face_v0 + 1],
            vertex_indices[face_v0 + 2]
        };
        // Every pair should be adjacent in the array. For the 3 new edges, check if their
        // pair was allocated already. If it was, store the new edge besides the pair.
        // If there is not pair, allocate 2 slots and store the new edge in the even slot.
        std::array<uint, 3> edges_i;
        const bool e1_absent = edges_map.count({face_vi[1], face_vi[0]}) == 0;
        edges_i[0] = !e1_absent?
              edges_map.at({face_vi[1], face_vi[0]}) + 1
            : allocated_edges;
        allocated_edges += 2 * e1_absent;
        const bool e2_absent = edges_map.count({face_vi[2], face_vi[1]}) == 0;
        edges_i[1] = !e2_absent?
              edges_map.at({face_vi[2], face_vi[1]}) + 1
            : allocated_edges;
        allocated_edges += 2 * e2_absent;
        const bool e3_absent = edges_map.count({face_vi[0], face_vi[2]}) == 0;
        edges_i[2] = !e3_absent?
              edges_map.at({face_vi[0], face_vi[2]}) + 1
            : allocated_edges;
        allocated_edges += 2 * e3_absent;

        for(uint j = 0; j < 3; j++)
        {
            const uint vertex_i = vertex_indices[face_v0 + j];
            // Conditionally initialize the missing fields in the vertex data
            if(normal_indices.size() == vertex_indices.size())
            {
                vertex_data[vertex_i].normal[0] = normals[normal_indices[face_v0 + j]][0];
                vertex_data[vertex_i].normal[1] = normals[normal_indices[face_v0 + j]][1];
                vertex_data[vertex_i].normal[2] = normals[normal_indices[face_v0 + j]][2];
            }

            if(uv_indices.size() == vertex_indices.size())
            {
                this->vertex_data[vertex_i].uv[0] = uvs[uv_indices[face_v0 + j]][0];
                this->vertex_data[vertex_i].uv[1] = uvs[uv_indices[face_v0 + j]][1];

                face.face_uvs[j][0] = uvs[uv_indices[face_v0 + j]][0];
                face.face_uvs[j][1] = uvs[uv_indices[face_v0 + j]][1];
            }

            const uint next_vertex_i = vertex_indices[face_v0 + ((j + 1) % 3)];
            // Map the current edge as the edge of the current vertex
            this->verts[vertex_i].edge = edges_i[j];
            // Initialize the current edge
            auto& c_edge = this->edges[edges_i[j]];
            c_edge.vert = vertex_i;
            c_edge.prev = edges_i[((j - 1) + 3) % 3];
            c_edge.next = edges_i[(j + 1) % 3];
            c_edge.face = this->faces.size() - 1;
            // Add this edge to the map
            edges_map[{vertex_i, next_vertex_i}] = edges_i[j];
            // We flip the order of the vertices and query if that edge has been stored
            // already. If it has it must be this edge's pair, trivially.
            bool pair_exists = edges_map.count({next_vertex_i, vertex_i});
            // If the pair exists then set the mapping between the edges
            if(pair_exists)
            {
                // Set the current edge's pair
                const uint pair_i = edges_map.at({next_vertex_i, vertex_i});
                c_edge.pair = pair_i;
                // Set the pair's pair
                auto& c_pair = this->edges[pair_i];
                c_pair.pair = edges_i[j];
            }
        }

        face.Edge(edges_i[0]);
    }

    // Index is the vertex, value is the edge
    std::vector<uint> boundaries(vertices.size(), ABSENT);
    const uint edge_num = this->edges.size();
    // Assert boundary properties.
    for(uint edge_i = 0; edge_i < edge_num; edge_i+=2)
    {
        auto& edge = this->edges[edge_i];
        assert(edge.vert != ABSENT);
        assert(edge.face != ABSENT);
        // If the edge has no pair at this stage then it is at the boundary
        if(edge.pair == ABSENT)
        {
            assert(this->edges[edge_i].face != ABSENT);
            assert(this->edges[edge_i + 1].vert == ABSENT);
            // Create a new edge and make the boundary edge be the outgoing edge of its
            // vertex.
            edge.pair = edge_i + 1;
            this->edges[edge_i + 1] = {this};
            this->verts[edge.vert].edge = edge.pair;

            // Grab the next edge.
            assert(edge.next != ABSENT);
            const auto& next = this->edges[edge.next];
            assert(next.vert != ABSENT);

            // Attach the current edge with its pair.
            auto& pair = this->edges[edge_i + 1];
            pair.pair = edge_i;
            pair.vert = next.vert;
            boundaries[pair.vert] = edge_i + 1;
            assert(edge.pair != edge_i);
        }
    }

    // Attach the boundaries
    for(uint edge_i = 0; edge_i < this->edges.size(); edge_i+=2)
    {
        auto& edge = this->edges[edge_i + 1];
        assert(edge.pair != edge_i + 1);
        if(edge.IsBoundary())
        {
            auto& pair = edges[edge.pair];
            edge.next = boundaries[pair.vert];
            this->edges[boundaries[pair.vert]].prev = edge_i + 1;
        }
    }

    for(auto& edge : edges)
    {
       if(edge.IsBoundary()) edge.VertD().Edge(edge.ID());
    }

    UpdatePointers();
    CalculateNormals();
}

template <typename V>
HMesh<V>::HMesh(const std::string& file_path)
{
    static_assert(!Has_normal<V>() || sizeof(std::declval<V>().normal) == 12);
    static_assert(Has_position<V>());
    auto [vertices, uvs, normals, vertex_indices, uv_indices, normal_indices] =
        ParseObj(file_path);

    Init(vertices, uvs, normals, vertex_indices, uv_indices, normal_indices);
}

template <typename V>
HMesh<V>::HMesh(const std::vector<V>& data, const std::vector<uint>& connectivity)
{
    std::vector<Eigen::Vector3f> vertices;
    std::vector<Eigen::Vector2f> uvs;
    std::vector<Eigen::Vector3f> normals;
    for(const auto& datum: data)
    {
        vertices.push_back({datum.position[0], datum.position[1], datum.position[2]});
        uvs.push_back({datum.uv[0], datum.uv[1]});
        normals.push_back({datum.normal[0], datum.normal[1], datum.normal[2]});
    }
    Init(vertices, uvs, normals, connectivity, connectivity, connectivity);
}

template <typename V>
HMesh<V>& HMesh<V>::operator=(const HMesh<V>& other)
{
    id = other.id;
    vertex_data = other.vertex_data;
    verts = other.verts;
    edges = other.edges;
    faces = other.faces;
    UpdatePointers();
    return *this;
}

template <typename V>
HMesh<V>& HMesh<V>::operator=(HMesh<V>&& other)
{
    id = other.id;
    vertex_data = other.vertex_data;
    verts = other.verts;
    edges = other.edges;
    faces = other.faces;
    UpdatePointers();
    return *this;
}

// Utility -------------------------------------------------------------------------------
template <typename V>
void HMesh<V>::CalculateNormals()
{
    for(auto& v : vertex_data) v.normal = {0, 0, 0};

    for(auto& f : faces)
    {
        const auto v_ids = f.VertexIds();

        const auto e1 =
            GLA::Normalize(vertex_data[v_ids[0]].position - vertex_data[v_ids[1]].position);
        const auto e2 =
            GLA::Normalize(vertex_data[v_ids[1]].position - vertex_data[v_ids[2]].position);
        const auto e3 =
            GLA::Normalize(vertex_data[v_ids[2]].position - vertex_data[v_ids[0]].position);
        const auto no = GLA::Normalize(GLA::Cross(-e2, e1));

        vertex_data[v_ids[0]].normal += no * acos(e1.dot(-e2));
        vertex_data[v_ids[1]].normal += no * acos(e2.dot(-e3));
        vertex_data[v_ids[2]].normal += no * acos(e3.dot(-e1));
    }

    for(auto& v : vertex_data) v.normal = GLA::Normalize(v.normal);
}

template <typename V>
std::pair<std::vector<float>, std::vector<uint>>
HMesh<V>::GetGeometryData(std::any& container)
{
    HMesh<V>& mesh = std::any_cast<HMesh<V>&>(container);

    std::vector<float> ret;
    std::vector<uint> indices;
    uint count = 0;
    for(const auto& f : mesh.faces)
    {
        const auto ids = f.VertexIds();
        const auto normal = f.Normal();
        for(uint i=0; i<3; i++)
        {
            const V& data = mesh.vertex_data[ids[i]];

            ret.push_back(data.position[0]);
            ret.push_back(data.position[1]);
            ret.push_back(data.position[2]);

            ret.push_back(f.face_uvs[i][0]);
            ret.push_back(f.face_uvs[i][1]);

            ret.push_back(normal[0]);
            ret.push_back(normal[1]);
            ret.push_back(normal[2]);
        }

        indices.push_back(count++);
        indices.push_back(count++);
        indices.push_back(count++);
    }

    return {ret, indices};
}

template <typename V>
void HMesh<V>::UpdatePointers()
{
    for(auto& v : verts) v.mesh = this;
    for(auto& e : edges) e.mesh = this;
    for(auto& f : faces) f.mesh = this;
}

template <typename V>
std::pair<uint, float> HMesh<V>::RayMeshIntersection(
    HMesh<V>& mesh,
    const Eigen::Vector3f& origin,
    const Eigen::Vector3f& ray)
{
    uint selected_face = ABSENT;
    float t = std::numeric_limits<float>::max();
    for(auto& face : mesh.faces)
    {
        auto verts = face.Vertices();
        Eigen::Vector3f v1 = verts[0]->Data().position;
        Eigen::Vector3f v2 = verts[1]->Data().position;
        Eigen::Vector3f v3 = verts[2]->Data().position;

        float current_t = TriangleLineIntersection(
            origin.cast<double>(),
            ray.cast<double>(),
            {v1.cast<double>(), v2.cast<double>(), v3.cast<double>()});
        if(current_t >= 0 && current_t < t)
        {
            t = current_t;
            selected_face = face.ID();
        }
    }
    t = selected_face == ABSENT? -1 : t;
    return {selected_face, t};
}

template class HMesh<VertexData>;

