//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file HMesh.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-08-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "HMesh.hpp"

/** @cond */
#include <filesystem>
/** @endcond */

#include "Helpers/TextManipulation.hpp"

using namespace std;
namespace fs = std::filesystem;

std::tuple<
    vector<Eigen::Vector3f>, // Vertices
    vector<Eigen::Vector2f>, // Texture coordinates
    vector<Eigen::Vector3f>, // Normals
    vector<uint>,            // Vertex indices
    vector<uint>,            // UV indices
    vector<uint>>            // Normal indices
ParseObj(const string& path)
{
    vector<Eigen::Vector3f> vertices;
    vector<Eigen::Vector2f> uvs;
    vector<Eigen::Vector3f> normals;

    if(!fs::exists(path)) Log::RecordLogError("Path to file does not exist");
    std::ifstream file_stream(path);

    // First parse the file to get all the vertex, coords and normals
    string line;
    while (getline(file_stream, line))
    {
        istringstream string_stream(line);
        string first_token;
        string_stream >> first_token;

        if(first_token == "v")
        {
            string c1, c2, c3;
            string_stream >> c1;
            string_stream >> c2;
            string_stream >> c3;

            vertices.push_back(Eigen::Vector3f(stof(c1), stof(c2), stof(c3)));
        }
        else if(first_token == "vt")
        {
            string c1, c2;
            string_stream >> c1;
            string_stream >> c2;

            uvs.push_back(Eigen::Vector2f(stof(c1), stof(c2)));
        }
        else if(first_token == "vn")
        {
            string c1, c2, c3;
            string_stream >> c1;
            string_stream >> c2;
            string_stream >> c3;

            normals.push_back(Eigen::Vector3f(stof(c1), stof(c2), stof(c3)));
        }
    }
    // On the next pass focus only on the faces and associate the face data together by
    // index
    file_stream.clear();
    file_stream.seekg(0, ios::beg);
    vector<uint> vertex_indices;
    vector<uint> uv_indices;
    vector<uint> normal_indices;
    while(getline(file_stream, line))
    {
        istringstream string_stream(line);
        string first_token;
        string_stream >> first_token;

        if(first_token == "f")
        {
            // We assume a triangular mesh
            string c1, c2, c3;
            string_stream >> c1;
            string_stream >> c2;
            string_stream >> c3;

            const array<string, 3> string_info = {c1, c2, c3};
            for(auto& vertex_info : string_info)
            {
                auto indices = TM::Split(vertex_info, '/');
                assert(indices.size() >= 1);
                assert(indices[0] != ""); // At least the vertex information should exist;
                vertex_indices.push_back(stoi(indices[0]) - 1);
                if(indices.size() >= 2 && indices[1] != "")
                    uv_indices.push_back(stoi(indices[1]) - 1);
                if(indices.size() >= 3 && indices[2] != "")
                    normal_indices.push_back(stoi(indices[2]) - 1);
            }
        }
    }
    return {vertices, uvs, normals, vertex_indices, uv_indices, normal_indices};
}

