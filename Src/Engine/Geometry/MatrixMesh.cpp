//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file MatrixMesh.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-10-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "MatrixMesh.hpp"

using namespace std;

// Template instantiation
template std::pair<SBM, SBM>
CastToMatrix(const HMesh<VertexData>& mesh);

std::array<SBV, 3> Star(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const auto face_vertex_adjacency = face_matrix * edge_matrix;

    const auto& vertices = simplices[0];
    const auto& edges = simplices[1];
    const auto& faces = simplices[2];

    auto star_edges = edge_matrix * vertices + edges;
    auto star_faces = face_vertex_adjacency * vertices + face_matrix * edges;
    return {vertices, star_edges, star_faces};
}

std::array<SBV, 3> Closure(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const auto face_vertex_adjacency = face_matrix * edge_matrix;
    const auto& vertices = simplices[0];
    const auto& edges = simplices[1];
    const auto& faces = simplices[2];

    SBV closure1_edges = edges + face_matrix.transpose() * faces;
    SBV closure1_vertices = vertices + edge_matrix.transpose() * edges;

    return {closure1_vertices, closure1_edges, faces};
}

std::array<SBV, 3> Link(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const auto& vertices = simplices[0];
    const auto& edges = simplices[1];
    const auto& faces = simplices[2];

    auto[star1_vertices, star1_edges, star1_faces] =
        Star(edge_matrix, face_matrix, {vertices, edges, faces});
    auto[closure1_vertices, closure1_edges, closure1_faces] =
        Closure(edge_matrix, face_matrix, {star1_vertices, star1_edges, star1_faces});

    auto[closure2_vertices, closure2_edges, closure2_faces] =
        Closure(edge_matrix, face_matrix, {vertices, edges, faces});
    auto[star2_vertices, star2_edges, star2_faces] =
        Star(edge_matrix, face_matrix, {closure2_vertices, closure2_edges, closure2_faces});

    return
    {
        closure1_vertices - star2_vertices,
        closure1_edges - star2_edges,
        closure1_faces - star2_faces,
    };
}

std::array<SBV, 3> Boundary(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const auto& verts = simplices[0].cast<int>();
    const auto& edges = simplices[1].cast<int>();
    const auto& faces = simplices[2].cast<int>();

    auto vertex_face_counts = (edges.transpose() * edge_matrix.cast<int>());
    auto vertex_sigma = (vertex_face_counts).unaryExpr( [](int v){ return int(v == 1); } );

    auto edge_face_counts = (faces.transpose() * face_matrix.cast<int>());
    auto edge_sigma = (edge_face_counts).unaryExpr( [](int v){ return int(v == 1); } );

    return Closure(
        edge_matrix,
        face_matrix,
        {vertex_sigma.transpose(), edge_sigma.transpose(), SBV(faces.size())});
}

bool IsComplex(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const auto& vertices = simplices[0];
    const auto& edges = simplices[1];
    const auto& faces = simplices[2];

    auto[closure_vertices, closure_edges, closure_faces] =
        Closure(edge_matrix, face_matrix, {vertices, edges, faces});

    return
        closure_vertices.isApprox(vertices) &&
        closure_edges.isApprox(edges) &&
        closure_faces.isApprox(faces);
}

int IsPureComplex(
    const SBM& edge_matrix,
    const SBM& face_matrix,
    const std::array<SBV, 3>& simplices)
{
    const SBV& verts = simplices[0];
    const SBV& edges = simplices[1];
    const SBV& faces = simplices[2];

    if(!IsComplex(edge_matrix, face_matrix, simplices)) return -1;

    SBV point_faces = edges.transpose() * edge_matrix;
    const bool has_point_facets = SBV((verts - point_faces).pruned()).nonZeros();

    SBV line_faces = faces.transpose() * face_matrix;
    const bool has_line_facets = SBV((edges - line_faces).pruned()).nonZeros();
    const bool has_triangle_facets = faces.nonZeros();

    if(int(has_point_facets) + int(has_line_facets) + int(has_triangle_facets) != 1)
        return -1;

    return 3 * has_triangle_facets + 2 * has_line_facets + has_point_facets;
}

