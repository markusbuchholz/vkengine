//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Graph.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-08-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <assert.h>
#include <vector>
/** @endcond */

#include "Helpers/MetaProgramming.hpp"

template <typename T>
std::vector<T>& Concat(T& t1, const T& t2)
{ t1.insert(t1.end(), t2.start(), t2.end()); return t1; }

template <typename NODE>
class Graph
{
private:
    std::vector<NODE> nodes;
    std::vector<std::vector<uint>> neighbours;

public:
    void AddNode(const NODE& n) { nodes.push_back(n); neighbours.resize(nodes.size()); }

    void Node(uint i, const NODE& n) { assert(i < nodes.size()); nodes[i] = n; }

    const NODE& Node(uint i) { assert(i < nodes.size()); return nodes[i]; }

    NODE& NodeUnsafe(uint i) { assert(i < nodes.size()); return nodes[i]; }

    void Neighbours(const std::vector<uint>& ns, uint i)
    { assert(i < nodes.size()); Concat(neighbours[i], ns); }

    std::vector<uint> Neighbours(uint i)
    { assert(i < nodes.size()); return neighbours[i]; }
};

