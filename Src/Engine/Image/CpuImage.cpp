//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file CpuImage.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define STB_IMAGE_IMPLEMENTATION
#include "CpuImage.hpp"

/** @cond */
#include <string.h>

#include "stb_image.h"
/** @endcond */

#include "Helpers/log.hpp"

using namespace std;

// Static memembers ----------------------------------------------------------------------
tuple<vector<byte>, uint, uint, uint> CpuImage::GetImageData(any& container)
{
    CpuImage& image = any_cast<CpuImage&>(container);

    return {image.GetBuffer(), image.GetWidth(), image.GetHeight(), image.GetChannelNum()};
}

// Instance functions --------------------------------------------------------------------
CpuImage::CpuImage(void* data, int width, int height, int channel_num)
    : width(width)
    , height(height)
    , channel_num(channel_num)
{
    LoadBuffer(data, width, height, STBI_rgb_alpha);
}

CpuImage::CpuImage(const std::string& path)
{
    stbi_uc* img_data =
        stbi_load(path.c_str(), &width, &height, &channel_num, STBI_rgb_alpha);
    if(img_data == nullptr)
        Log::RecordLogError(
            "Failed to load texture image\n" + path + "\nPath is likely incorrect");

    LoadBuffer((void*)img_data, width, height, STBI_rgb_alpha);
    // TODO(low): stb_image seems to load 3 channels out of pngs
    if(channel_num == 3) channel_num = 4;

    stbi_image_free(img_data);
}

void CpuImage::LoadBuffer(const void* bytes, int width, int height, int channel_num)
{
    size_t data_size = width * height * channel_num;
    buffer.resize(data_size);
    memcpy(buffer.data(), bytes, data_size);
}

