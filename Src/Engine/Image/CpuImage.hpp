//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file CpuImage.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <any>
#include <string>
#include <vector>
/** @endcond */

class CpuImage
{
private:
    int width = 0;
    int height = 0;
    int channel_num = 0;
    std::vector<std::byte> buffer;
    uint id;

    void LoadBuffer(const void* buffer, int width, int height, int channel_num);

public:
    static std::tuple<std::vector<std::byte>, uint, uint, uint>
        GetImageData(std::any& container);

    CpuImage() = default;
    CpuImage(void* data, int width, int height, int channel_num);
    CpuImage(const std::string& filepath);

    uint GetWidth() { return width; }
    uint GetHeight() { return height; }
    uint GetChannelNum() { return channel_num; }
    const std::vector<std::byte>& GetBuffer() { return buffer; }
};

