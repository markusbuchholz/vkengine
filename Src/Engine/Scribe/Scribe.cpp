//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** *
 * @file Scribe.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-08-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "Scribe.hpp"

/** @cond */
#include <codecvt>
#include <iostream>
#include <locale>
#include <string>

#include "stb_image_write.h"
/** @endcond */

using namespace std;

// Function declarations
// -----------------------------------------------------------------
namespace {
uint GetCharIndex(char c);

void CreateSingleLineGeometry(const string &text, ScGeomMetaInfo &info,
                              ScGeomtry &geometry, FT_Face &face);

void WriteGlyphsToAtlas(vector<ScGlyphTexture> &glyphs,
                        vector<char> &img_buffer, uint side_l);
} // namespace

// Function definitions ------------------------------------------------------------------
Scribe::Scribe(const string &font_path)
    : ft_library(make_unique<FT_Library>()), ft_face(make_unique<FT_Face>()) {
  // Initialize freetype
  auto error = FT_Init_FreeType(ft_library.get());
  if (error)
    Log::RecordLogError("FreeType failed to initialize");

  error = FT_New_Face(*ft_library, font_path.c_str(), 0, ft_face.get());
  if (error == FT_Err_Unknown_File_Format)
    Log::RecordLogError("Unkown font file format");
  else if (error)
    Log::RecordLogError("Couldn't open font file: " + font_path);

  error = FT_Set_Char_Size(*ft_face, 0, 16 * 64, 300, 300);
  if (error)
    Log::RecordLogError("Couldn't set character size");

  CreateFontAtlas(font_path);
}

ScGeomtry Scribe::CreateTextGeometry(const string &o_text) {
  auto text = o_text;
  // Add a final newline character for proper formatting
  if (text.back() != '\n')
    text += "\n";
  // Set the metadata information of the font
  ScGeomMetaInfo g_info = {
      .v_offset = -1,
      .h_offset = -1,
      .char_len = character_size,
      .c_num = 0,
      .max_len = font_max_length,
  };
  g_info.v_delta =
      g_info.char_len * float((*ft_face)->height) / float(g_info.max_len);
  g_info.v_delta += g_info.char_len * vertical_spacing;
  // Create the geometry of each character and append it to the geometry buffers
  ScGeomtry geometry;
  for (auto &c : text) {
    auto advance = glyph_map[FT_Get_Char_Index(*ft_face, c)].advance;
    float n_advance = float(advance) / float(g_info.max_len);
    // If the line is too long, or if a new line character is found, got to new
    // line
    if (g_info.h_offset + g_info.char_len * n_advance >= 1.f || c == '\n') {
      g_info.h_offset = -1; // Reset left margin
      g_info.v_offset +=
          g_info.v_delta * aspect_ratio; // Update vertical offset to new line
    }
    // Append quad geometry for current line
    CreateCharacterGeometry(c, g_info, geometry);
  }
  return move(geometry);
}

// Private Methods
// -----------------------------------------------------------------------
void Scribe::CreateCharacterGeometry(const char c, ScGeomMetaInfo &info,
                                     ScGeomtry &geometry) {
    float char_l = info.char_len;
    float v_anchor = info.v_offset;

    auto &vertices = geometry.first;
    auto &indices = geometry.second;

    auto glyph_index = FT_Get_Char_Index(*ft_face.get(), c);
    FT_Load_Glyph(*ft_face.get(), glyph_index, FT_LOAD_NO_HINTING);
    auto metrics_ft = (*ft_face)->glyph->metrics;
    // Calculate correction, the vertical offset from the baseline of this char
    auto metrics = glyph_map[glyph_index];
    // TODO(low): figure out why this number fixes the font rendering
    float correction =
        float(metrics.height - metrics.bearing_y) / float(info.max_len);
    correction *= char_l;

    // Calculate offset from cursor to the current character
    float bearingX = char_l * float(metrics.bearing_x) / float(info.max_len);

    float &h_offset = info.h_offset;
    // Insert the vertex positions and uvs into the returned geometry
    float h_coords[] = {h_offset + bearingX, h_offset + bearingX + char_l};
    float v_coords[] = {v_anchor + correction * aspect_ratio,
                        v_anchor + (correction + char_l) * aspect_ratio};

    auto glyph_data = glyph_map[glyph_index];
    float tex_h_coords[] = {glyph_data.lt_uv.x(), glyph_data.rb_uv.x()};
    float tex_v_coords[] = {glyph_data.lt_uv.y(), glyph_data.rb_uv.y()};
    for (int x = 0; x < 2; x++) {
        for (int y = 0; y < 2; y++) {
        vertices.insert(end(vertices), {h_coords[x], v_coords[y], 0});
        vertices.insert(end(vertices), {tex_h_coords[x], tex_v_coords[y]});
        }
    }

    // Setup the indices of the current quad
    // There's 4 vertices in a quad, so we offset each index by (quad_size *
    // num_quads)
    uint delta = 4 * info.c_num++;
    indices.insert(end(indices), {0 + delta, 3 + delta, 1 + delta, 0 + delta,
                                    3 + delta, 2 + delta});
    h_offset +=
        char_l * metrics.advance / info.max_len + char_l * horizontal_spacing;
}
// TODO(low): if the file exists load it from disk instead of doing the
// processing
void Scribe::CreateFontAtlas(const string &font_path) {
  // Obtain bitmaps for all the desired characters
  vector<ScGlyphTexture> glyph_textures;
  ExtractGlyphs(glyph_textures);

  // Find the largest dimensions vertically and horizontally for a character BB
  uint m_width = 0;
  uint m_height = 0;
  for (auto &glyph : glyph_textures) {
    m_width = max(glyph.width, m_width);
    m_height = max(glyph.height, m_height);
  }
  font_max_length = 0;
  for (auto &glyph : glyph_map) {
    font_max_length = max(font_max_length, glyph.second.width);
    font_max_length = max(font_max_length, glyph.second.height);
  }
  uint side_l = max(m_width, m_height); // Pick the largest dimension
  side_l += side_l % 2;                 // Align to the closest multiple of 2

  // Create the texture atlas bitmap
  WriteGlyphsToAtlas(glyph_textures, atlas_buffer, side_l);

  // Store atlas to disk
  uint height = side_l * glyph_map.size();
  stbi_write_png("char.png", side_l, height, 1, atlas_buffer.data(), side_l);

  atlas_width = side_l;
  atlas_height = height;
}

// Create the font atlas of a face
void Scribe::ExtractGlyphs(vector<ScGlyphTexture> &glyph_textures) {
  // For each ascii character render a bitmap
  for (uint i = ' '; i <= '~'; i++) {
    ScGlyphTexture texture;
    // Create the current glyph code
    auto glyph_index = FT_Get_Char_Index(*ft_face, (char16_t)i);
    auto error = FT_Load_Glyph(*ft_face, glyph_index, FT_LOAD_NO_HINTING);
    if (error)
      Log::RecordLogError("Couldn't load glyph: " + (char16_t)i);
    // Create the bitmap
    error = FT_Render_Glyph((*ft_face)->glyph, ft_render_mode_normal);
    if (error)
      Log::RecordLogError("Couldn't render glyph: " + (char16_t)i);

    auto bitmap = (*ft_face)->glyph->bitmap;
    texture.width = bitmap.width;
    texture.height = bitmap.rows;
    // Copy bitmap into the glyph structure and save it on the array
    auto size = texture.width * texture.height;
    texture.buffer.resize(texture.width * texture.height);
    memcpy(texture.buffer.data(), bitmap.buffer, size);

    auto metrics = (*ft_face)->glyph->metrics;
    ScGlyphData glyph_data = {
        .width = metrics.width,
        .height = metrics.height,
        .bearing_x = metrics.horiBearingX,
        .bearing_y = metrics.horiBearingY,
        .advance = metrics.horiAdvance,
    };
    glyph_data.lt_uv = {0, float(i - ' ') / float('~' - ' ' + 1)};
    glyph_data.rb_uv = {1, float(i + 1 - ' ') / float('~' - ' ' + 1)};

    glyph_map[glyph_index] = glyph_data;
    glyph_textures.push_back(texture);
  }
}

// Internal functions
// --------------------------------------------------------------------
namespace {
void WriteGlyphsToAtlas(vector<ScGlyphTexture> &glyphs,
                        vector<char> &img_buffer, uint side_l) {
  int i = 0;
  for (auto &glyph : glyphs) {
    // Calculate the offsets needed to place the character on a quad
    // Add a 2 pixel offset to prevent sampling artifacts
    uint delta_h = side_l - glyph.height - 2;
    uint delta_w = 0;
    for (int y = 0; y < side_l; y++) {
      for (int x = 0; x < side_l; x++) {
        // If the current coordinates are in the portion of the quad where the
        // character should be written to, copy the bitmap data
        bool in_range = (delta_h <= y) && (y < glyph.height + delta_h);
        in_range = in_range && (delta_w <= x) && (x < glyph.width + delta_w);
        char value =
            in_range ? glyph.buffer[(y - delta_h) * glyph.width + (x - delta_w)]
                     : 0;

        img_buffer.push_back(value);
      }
    }
  }
}

uint GetCharIndex(char c) { return c - ' '; }

} // namespace

