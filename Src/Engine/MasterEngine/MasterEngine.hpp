//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file MasterEngine.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-04-21
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

#include "Guizmo3DPool.hpp"
#include "IO/InputHandler.hpp"
#include "Rendering/VulkanEngine.hpp"
#include "Image/CpuImage.hpp"
#include "Geometry/GeometryUtils.hpp"
#include "Geometry/Graph.hpp"
#include "Geometry/HMesh.hpp"
#include "Geometry/HMeshUtils.hpp"
#include "Geometry/MatrixMesh.hpp"
#include "Geometry/MeshGradient.hpp"
#include "Geometry/Parametrics.hpp"

class MasterEngine
{
  private:
    VulkanEngine rendering_engine;
    InputHandler input_handler;
    ArcballCamera camera;
    Guizmo3DPool guizmo_pool;

  public:
    MasterEngine();

    VulkanEngine& GetRenderingEngine() { return rendering_engine; }
    InputHandler& GetInputHandler() { return input_handler; }
    ArcballCamera& GetCamera() { return camera; }
    Guizmo3DPool& GetGuizmoPool() { return guizmo_pool; }

    void Loop(void* data, void (*FrameRender)(MasterEngine* engine, void*));
    void LoopOnce(void* data, void (*FrameRender)(MasterEngine* engine, void*));
};

void ElementaryLoop(MasterEngine* engine, void* ptr);
void ElementaryInit(
    MasterEngine& engine, const std::string& obj_path, const std::string& img_path);