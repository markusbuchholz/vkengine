//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Guizmo3D.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include "Eigen/Dense"
/** @endcond */

class Guizmo3D
{
    Eigen::Vector3f position;
    float scale;
};

