//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file MasterEngine.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-04-21
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "MasterEngine.hpp"

/** @cond */
#include <chrono>
#include <numeric>
/** @endcond */

using namespace std;
using namespace Eigen;

// Class ---------------------------------------------------------------------------------

MasterEngine::MasterEngine()
{
    uint width, height;
    rendering_engine.GetWindow()->GetDimensions(&width, &height);
    camera = ArcballCamera(width, height);

    input_handler = InputHandler(rendering_engine.GetWindow()->GetGLFWWindow());
    input_handler.RegisterToGLFW();
}

void MasterEngine::Loop(void* data, void (*FrameRender)(MasterEngine* engine, void*))
{
    while(!glfwWindowShouldClose(rendering_engine.GetWindow()->GetGLFWWindow()))
    {
        glfwPollEvents();
        FrameRender(this, data);

        uint width, height;
        rendering_engine.GetWindow()->GetDimensions(&width, &height);
        camera.UpdateScreenDimensions(width, height);
    }
}

void MasterEngine::LoopOnce(void* data, void (*FrameRender)(MasterEngine* engine, void*))
{
    glfwPollEvents();
    FrameRender(this, data);
}

void ElementaryLoop(MasterEngine* engine, void* ptr)
{
    engine->GetInputHandler().CallEvents();
    auto& rendering_engine = engine->GetRenderingEngine();

    auto& camera = engine->GetCamera();

    MVPOnlyUbo mvp = {};
    mvp.model = Rotate(Radians(180.0f), Eigen::Vector3f(0.0f, 0.0f, 1.0f));
    mvp.view = camera.GetViewMatrix();
    mvp.proj = camera.GetProjectionMatrix();

    mvp.model = mvp.model * 0.2f;

    CameraInfo camera_info = {};
    camera_info.camera_position = camera.GetPosition();
    ;

    Gallery& asset_map = rendering_engine.GetGallery();
    auto& effect_framework = rendering_engine.GetEffectFramework();
    GeometryInfo gpu_buffer = asset_map.GetGpuGeometryBuffer("loaded_model");

    auto& target = rendering_engine.AtomicEffect(
        gpu_buffer, "shaders/debugging/phong_shader", {}, mvp, 0, camera_info, 1);

    rendering_engine.PresentRender(*target.GetColorImage(0));
}

void ElementaryInit(
    MasterEngine& engine, const std::string& obj_path, const std::string& img_path)
{
    auto& input_handler = engine.GetInputHandler();
    auto& rendering_engine = engine.GetRenderingEngine();
    auto& camera = engine.GetCamera();
    camera.SetRadius(6);

    input_handler.AddEvent(
        MouseInputState::LEFT_DRAG, ArcballCamera::UpdateCameraAngles, &camera);
    input_handler.AddEvent(
        MouseInputState::RIGHT_DRAG, ArcballCamera::UpdateCameraPosition, &camera);
    input_handler.AddEvent(
        ScrollInputState::SCROLL, ArcballCamera::UpdateCameraZoom, &camera);

    Gallery& asset_map = rendering_engine.GetGallery();

    CpuImage image(img_path);
    asset_map.StoreImageData(image, "statue");

    // Model
    HMesh<VertexData> mesh(obj_path);
    uint id = asset_map.StoreGeometryData(mesh, "loaded_model");
}