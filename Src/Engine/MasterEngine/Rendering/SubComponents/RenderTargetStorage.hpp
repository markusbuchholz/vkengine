//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file RenderTargetStorage.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-17
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <map>
#include <memory>
#include <vector>
/** @endcond */

class RenderTarget;
class VulkanMemory;

/**
 * @brief Container to cache render target objects. It's main purpose is to avoid the
 * costly recreation of vulkan state.
 *
*/
class RenderTargetStorage
{
  private:
    std::vector<std::unique_ptr<RenderTarget>> targets;
    // Key is number of outputs
    std::map<uint, std::vector<RenderTarget*>> target_output_map;
    // Map to keep track of which targets are in use
    std::map<RenderTarget*, bool> use_map;

    VulkanMemory* memory;

  public:
    RenderTargetStorage()
        : memory(nullptr)
    {
    }
    RenderTargetStorage(VulkanMemory* memory);

    RenderTarget& GetDisplayTarget() { return *targets[0]; }

    RenderTarget& GetTargetByOutputNum(uint output_num);

    void ResetFrame();
};

