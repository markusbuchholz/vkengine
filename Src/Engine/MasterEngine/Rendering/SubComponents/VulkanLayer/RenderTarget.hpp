//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** *
 * @file RenderTarget.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-06
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

#include "HardwareInterface.hpp"
#include "Image.hpp"
#include "Utils.hpp"

class OffScreenTarget;

/**
 * @brief Abstract class that encapsulates a rendering target.
 *
 * There are two rendering targets, display and off-screen. The main purpose of this class
 * is to try to provide a single way to handle rendering, as in a conceptual level,
 * rendering to the screen should be no different than rendering to an off-screen
 * framebuffer.
 *
 */
class RenderTarget
{
protected:
    std::vector<vk::UniqueRenderPass> clear_render_passes;
    std::vector<vk::UniqueRenderPass> no_clear_render_passes;
    vk::Extent2D extent;

    HardwareInterface* h_interface;
    VulkanMemory* memory;

    RenderTarget(){};

public:
    virtual ~RenderTarget(){};

    RenderTarget(const RenderTarget& other) = delete;             // copy constructor
    RenderTarget(RenderTarget&& other)      = delete;             // move constructor
    RenderTarget& operator=(RenderTarget&& other) = delete;       // move assignment
    RenderTarget& operator=(const RenderTarget& other) = delete;  // copy assignment

    /**
     * @brief Get the Extent of the current target. (The pixel dimensions of the attched
     * framebuffers).
     *
     * @return vk::Extent2D Dimensions of the render target (w, h).
     */
    vk::Extent2D GetExtent() { return extent; }

    /**
     * @brief Get the Active Render Pass.
     *
     * In the abstract case we assume a single render pass.
     *
     * @return vk::RenderPass The render pass.
     */
    virtual vk::RenderPass GetActiveRenderPass() { return *clear_render_passes.back(); }
    /**
     * @brief Get how many framebuffers are contained in this target.
     *
     * @return uint Number of framebuffers.
     */
    virtual uint GetFbNum()                      = 0;
    /**
     * @brief Get the current active swapchain image.
     *
     * This is here for api uniformity, might be better remove it. Use only if you know
     * the underlying type is a DisplayTarget.
     *
     * @return uint32_t Active swapchain image index.
     */
    virtual uint32_t GetActiveImageIndex()       = 0;
    /**
     * @brief Begin a renderpass.
     *
     *  Start recording commands to be executed for the current frame.
     *
     * @param target_num Subset of the image attachments to render to [0, target_num).
     * Must be >0.
     * @param should_clear Whether the attached images will be cleared by the passs or not.
     * @param external_depth If not null, this image will be used as the depth attachment
     * for depth testing. It won't be cleared. Must be a valid depth image.
     * @return vk::Result eSuccess if no problem occurs, the error code of the first error
     * otherwise.
     */
    virtual vk::Result StartPass(uint target_num = 1, bool should_clear = true,
        VulkanImage* external_depth = nullptr) = 0;
    /**
     * @brief Terminate the current pass.
     *
     *  Submit all recorded commands to the graphics queue, then wait on queue termination.
     *
     * @return vk::Result eSuccess or the error code of the first error found.
     */
    virtual vk::Result EndPass() = 0;
    /**
     * @brief Get the first Color Image contained in the render target.
     *
     * @return VulkanImage* First color image.
     */
    virtual VulkanImage* GetColorImage() { return nullptr; };
    /**
     * @brief Get the ith Color Image contained in the render target.
     *
     * @return VulkanImage* ith color image.
     */
    virtual VulkanImage* GetColorImage(uint i) { return nullptr; };
    /**
     * @brief Get the Depth Image contained in the render target.
     *
     * @return VulkanImage* Depth image.
     */
    virtual VulkanImage* GetDepthImage() = 0;
    /**
     * @brief Get the total number of color images in the RenderTarget.
     *
     * @return uint Total numver of color images.
     */
    virtual uint GetColorAttachmentCount() { return 1; };
    /**
     * @brief Get the Attachment Descriptions of all the current attachments.
     *
     * Descriptions tell vulkan how the image is to be treated, i.e. sampling, color
     * format, whether to keep the image after rendering...
     *
     * @return std::vector<vk::AttachmentDescription> List of descriptors, last
     * descriptor is always the depth attachment.
     */
    virtual std::vector<vk::AttachmentDescription> GetAttachmentDescriptions() = 0;
    /**
     * @brief Replace the depth attachment permanently.
     *
     * @param image The new depth image (must be a valid depth image).
     */
    virtual void SetDepthImage(VulkanImage& image) = 0;
};

class OffScreenTarget : public RenderTarget
{
  private:
    vk::UniqueFramebuffer framebuffer;
    std::vector<VulkanImage> color_images;
    VulkanImage depth_image;
    vk::RenderPass active_render_pass;

    // TODO(low): make this be detected rather than hard coded
    const vk::Format color_format = vk::Format::eB8G8R8A8Unorm;
    const vk::Format depth_format = vk::Format::eD32Sfloat;

public:
    vk::UniqueCommandBuffer cmd;

    OffScreenTarget(){};
    /**
     * @brief Construct a new Off Screen Target object.
     *
     * @param memory Pointer to initialized and valid VulkanMemory wrapper.
     * @param extent Dimensions of the attachments contained in the target.
     * @param output_num Total number of color attachments.
     * @param hi Pointer to valid hardware interface.
     */
    OffScreenTarget(
        VulkanMemory* memory,
        vk::Extent2D extent,
        uint output_num       = 1,
        HardwareInterface* hi = HardwareInterface::h_interface);

    virtual ~OffScreenTarget()                    = default;
    OffScreenTarget(const OffScreenTarget& other) = delete;        // copy constructor
    OffScreenTarget(OffScreenTarget&& other)      = default;       // move constructor
    OffScreenTarget& operator=(OffScreenTarget&& other) = delete;  // move assignment
    OffScreenTarget& operator=(const OffScreenTarget& other) = delete;  // copy assignment

    uint GetFbNum() override { return 1; };
    uint32_t GetActiveImageIndex() override { return 0; }

    vk::Result StartPass(uint target_num = -1, bool should_clear = true,
        VulkanImage* external_depth = nullptr) override;
    vk::Result EndPass() override;

    vk::RenderPass GetActiveRenderPass() override { return active_render_pass; }
    /**
     * @brief Get the underlying Framebuffer.
     *
     * @return vk::Framebuffer The framebuffer containing all the attachments.
     */
    vk::Framebuffer GetFramebuffer() { return *framebuffer; }
    VulkanImage* GetColorImage() override { return &color_images[0]; }
    VulkanImage* GetColorImage(uint i) override { return &color_images[i]; }
    VulkanImage* GetDepthImage() override { return &depth_image; }
    /**
     * @brief Get the Color Images of this target.
     *
     * @return std::vector<VulkanImage>& Color Images in the target.
     */
    std::vector<VulkanImage>& GetColorImages() { return color_images; }
    uint GetColorAttachmentCount() override { return color_images.size(); }
    /**
     * @brief Get the Attachment Descriptions of a subset of the attachments.
     *
     * @param output_num Subset of attachments [0,output_num). Must be output_num >0
     * @param clear
     * @return std::vector<vk::AttachmentDescription> The descriptions of the
     * subset ofattchments.
     */
    std::vector<vk::AttachmentDescription> GetAttachmentDescriptions(uint output_num,
        bool clear = true);

    virtual std::vector<vk::AttachmentDescription> GetAttachmentDescriptions() override;

    void SetDepthImage(VulkanImage& image) override;
};

class DisplayTarget : public RenderTarget
{
protected:
    vk::UniqueSwapchainKHR swap_chain;
    // TODO(low): move this to  RenderTarget
    vk::Format selected_format = vk::Format::eUndefined;
    vk::PresentModeKHR present_mode;

    VulkanImage depth_image;
    std::vector<vk::UniqueImageView> image_views;
    std::vector<vk::UniqueFramebuffer> framebuffers;

    std::vector<vk::UniqueSemaphore> img_available_sems;
    std::vector<vk::UniqueSemaphore> render_finished_sems;
    std::vector<vk::UniqueFence> in_flight_fences;

    size_t current_frame        = 0;
    uint32_t active_image_index = 0;
    bool use_vsync              = false;

    vk::Format FindSupportedFormat(
        const std::vector<vk::Format>& candidates,
        vk::ImageTiling tiling,
        vk::FormatFeatureFlags features);

public:
    DisplayTarget(){};
    ~DisplayTarget()                          = default;
    DisplayTarget(const DisplayTarget& other) = delete;             // copy constructor
    DisplayTarget(DisplayTarget&& other)      = default;            // move constructor
    DisplayTarget& operator=(DisplayTarget&& other) = default;      // move assignment
    DisplayTarget& operator=(const DisplayTarget& other) = delete;  // copy assignment

    /**
     * @brief Construct a new Display Target object.
     *
     * @param memory Pointer to initialized and valid VulkanMemory wrapper.
     * @param use_vsync Whether the display will use vsync or not.
     * @param hi Pointer to initialized and valid hardware interface.
     */
    DisplayTarget(
        VulkanMemory* memory,
        bool use_vsync        = true,
        HardwareInterface* hi = HardwareInterface::h_interface);
    uint GetFbNum() override { return framebuffers.size(); };
    /**
     * @brief Get the Swap Chain of the underlying surface.
     *
     * @return vk::SwapchainKHR Swapchain of the surface.
     */
    vk::SwapchainKHR GetSwapChain() { return *swap_chain; }
    /**
     * @brief Get the current Presentation Mode.
     *
     * @return vk::PresentModeKHR Current presentation mode.
     */
    vk::PresentModeKHR GetPresentMode() { return present_mode; }
    VulkanImage* GetDepthImage() override { return &depth_image; }
    /**
     * @brief Get the Surface Capabilities of the underlying surface.
     *
     * @return vk::SurfaceCapabilitiesKHR Surface Capabilities of the underlying surface.
     */
    vk::SurfaceCapabilitiesKHR GetSurfaceCapabilities();
    /**
     * @brief Get the Depth Format.
     *
     * @return vk::Format Depth Format.
     */
    vk::Format GetDepthFormat();
    /**
     * @brief Get the Current Format.
     *
     * @return vk::Format Current Format.
     */
    vk::Format GetSelectedFormat();
    uint32_t GetActiveImageIndex() override { return active_image_index; }
    /**
     * @brief Get the Image Views of the swapchain images.
     *
     * @return std::vector<vk::ImageView> Image Views of the swapchain images.
     */
    std::vector<vk::ImageView> GetImageViews() { return CastVkArray(image_views); }
    /**
     * @brief Get the Framebuffers of each image in the swapchain.
     *
     * @return std::vector<vk::Framebuffer> List of swapchain framebuffers.
     */
    std::vector<vk::Framebuffer> GetFramebuffers() { return CastVkArray(framebuffers); };
    /**
     * @brief Get the Image Available Semaphores of the swapchain attachments.
     *
     * @return std::vector<vk::Semaphore> List of semaphores of the available images.
     */
    std::vector<vk::Semaphore> GetImgAvailableSems()
    {
        return CastVkArray(img_available_sems);
    }
    /**
     * @brief Get the Render Finished Semaphores.
     *
     * @return std::vector<vk::Semaphore> List of semaphores of rendered swapchain images.
     */
    std::vector<vk::Semaphore> GetRenderFinishedSems()
    {
        return CastVkArray(render_finished_sems);
    }
    /**
     * @brief Get the In Flight Fences.
     *
     * @return std::vector<vk::Fence> Flight Fences.
     */
    std::vector<vk::Fence> GetInFlightFences() { return CastVkArray(in_flight_fences); }

    virtual std::vector<vk::AttachmentDescription> GetAttachmentDescriptions() override;
    //TODO(low): implement this
    void SetDepthImage(VulkanImage& image) override {}
    // TODO(low):Preserving prior contents of attachemnts (no clearing) has not yet been
    // implemented for DisplayTarget. Same for using an external depth.
    vk::Result StartPass(uint target_num = 1, bool should_clear = true,
        VulkanImage* external_depth = nullptr) override;
    /**
     * @brief Check if Vsync is being used.
     *
     * @return true Vsync is in use.
     * @return false Vsync is not in use.
     */
    bool UseVsync() { return use_vsync; }
    /**
     * @brief Replace the framebuffers of the swapchain.
     *
     * @param fbuffers
     */
    void SetFrameBuffers(std::vector<vk::UniqueFramebuffer>& fbuffers)
    {
        framebuffers = move(fbuffers);
    }
    /**
     * @brief Update the swapchain.
     *
     * For example if the screen size changes, or if it gets minimized.
     *
     */
    void UpdateSwapchain();
    vk::Result EndPass() override;
};

