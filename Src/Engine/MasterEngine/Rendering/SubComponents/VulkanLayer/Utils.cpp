//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Utils.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-05-07
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define STRINGIFY_LIMITS(struct, field) #field ": " + to_string(struct.field) + "\n"
#define STRINGIFY_FEATURES(struct, field) #field ": " + IsSupported(struct.field) + "\n"

#include "Utils.hpp"

using namespace std;

string to_string(VkPhysicalDeviceLimits limits)
{
    return
        STRINGIFY_LIMITS(limits, maxImageDimension1D)
        STRINGIFY_LIMITS(limits, maxImageDimension2D)
        STRINGIFY_LIMITS(limits, maxImageDimension3D)
        STRINGIFY_LIMITS(limits, maxImageDimensionCube)
        STRINGIFY_LIMITS(limits, maxImageArrayLayers)
        STRINGIFY_LIMITS(limits, maxTexelBufferElements)
        STRINGIFY_LIMITS(limits, maxUniformBufferRange)
        STRINGIFY_LIMITS(limits, maxStorageBufferRange)
        STRINGIFY_LIMITS(limits, maxPushConstantsSize)
        STRINGIFY_LIMITS(limits, maxMemoryAllocationCount)
        STRINGIFY_LIMITS(limits, maxSamplerAllocationCount)
        STRINGIFY_LIMITS(limits, bufferImageGranularity)
        STRINGIFY_LIMITS(limits, sparseAddressSpaceSize)
        STRINGIFY_LIMITS(limits, maxBoundDescriptorSets)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorSamplers)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorUniformBuffers)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorStorageBuffers)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorSampledImages)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorStorageImages)
        STRINGIFY_LIMITS(limits, maxPerStageDescriptorInputAttachments)
        STRINGIFY_LIMITS(limits, maxPerStageResources)
        STRINGIFY_LIMITS(limits, maxDescriptorSetSamplers)
        STRINGIFY_LIMITS(limits, maxDescriptorSetUniformBuffers)
        STRINGIFY_LIMITS(limits, maxDescriptorSetUniformBuffersDynamic)
        STRINGIFY_LIMITS(limits, maxDescriptorSetStorageBuffers)
        STRINGIFY_LIMITS(limits, maxDescriptorSetStorageBuffersDynamic)
        STRINGIFY_LIMITS(limits, maxDescriptorSetSampledImages)
        STRINGIFY_LIMITS(limits, maxDescriptorSetStorageImages)
        STRINGIFY_LIMITS(limits, maxDescriptorSetInputAttachments)
        STRINGIFY_LIMITS(limits, maxVertexInputAttributes)
        STRINGIFY_LIMITS(limits, maxVertexInputBindings)
        STRINGIFY_LIMITS(limits, maxVertexInputAttributeOffset)
        STRINGIFY_LIMITS(limits, maxVertexInputBindingStride)
        STRINGIFY_LIMITS(limits, maxVertexOutputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationGenerationLevel)
        STRINGIFY_LIMITS(limits, maxTessellationPatchSize)
        STRINGIFY_LIMITS(limits, maxTessellationControlPerVertexInputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationControlPerVertexOutputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationControlPerPatchOutputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationControlTotalOutputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationEvaluationInputComponents)
        STRINGIFY_LIMITS(limits, maxTessellationEvaluationOutputComponents)
        STRINGIFY_LIMITS(limits, maxGeometryShaderInvocations)
        STRINGIFY_LIMITS(limits, maxGeometryInputComponents)
        STRINGIFY_LIMITS(limits, maxGeometryOutputComponents)
        STRINGIFY_LIMITS(limits, maxGeometryOutputVertices)
        STRINGIFY_LIMITS(limits, maxGeometryTotalOutputComponents)
        STRINGIFY_LIMITS(limits, maxFragmentInputComponents)
        STRINGIFY_LIMITS(limits, maxFragmentOutputAttachments)
        STRINGIFY_LIMITS(limits, maxFragmentDualSrcAttachments)
        STRINGIFY_LIMITS(limits, maxFragmentCombinedOutputResources)
        STRINGIFY_LIMITS(limits, maxComputeSharedMemorySize)
        STRINGIFY_LIMITS(limits, maxComputeWorkGroupCount[3])
        STRINGIFY_LIMITS(limits, maxComputeWorkGroupInvocations)
        STRINGIFY_LIMITS(limits, maxComputeWorkGroupSize[3])
        STRINGIFY_LIMITS(limits, subPixelPrecisionBits)
        STRINGIFY_LIMITS(limits, subTexelPrecisionBits)
        STRINGIFY_LIMITS(limits, mipmapPrecisionBits)
        STRINGIFY_LIMITS(limits, maxDrawIndexedIndexValue)
        STRINGIFY_LIMITS(limits, maxDrawIndirectCount)
        STRINGIFY_LIMITS(limits, maxSamplerLodBias)
        STRINGIFY_LIMITS(limits, maxSamplerAnisotropy)
        STRINGIFY_LIMITS(limits, maxViewports)
        STRINGIFY_LIMITS(limits, maxViewportDimensions[2])
        STRINGIFY_LIMITS(limits, viewportBoundsRange[2])
        STRINGIFY_LIMITS(limits, viewportSubPixelBits)
        STRINGIFY_LIMITS(limits, minMemoryMapAlignment)
        STRINGIFY_LIMITS(limits, minTexelBufferOffsetAlignment)
        STRINGIFY_LIMITS(limits, minUniformBufferOffsetAlignment)
        STRINGIFY_LIMITS(limits, minStorageBufferOffsetAlignment)
        STRINGIFY_LIMITS(limits, minTexelOffset)
        STRINGIFY_LIMITS(limits, maxTexelOffset)
        STRINGIFY_LIMITS(limits, minTexelGatherOffset)
        STRINGIFY_LIMITS(limits, maxTexelGatherOffset)
        STRINGIFY_LIMITS(limits, minInterpolationOffset)
        STRINGIFY_LIMITS(limits, maxInterpolationOffset)
        STRINGIFY_LIMITS(limits, subPixelInterpolationOffsetBits)
        STRINGIFY_LIMITS(limits, maxFramebufferWidth)
        STRINGIFY_LIMITS(limits, maxFramebufferHeight)
        STRINGIFY_LIMITS(limits, maxFramebufferLayers)
        STRINGIFY_LIMITS(limits, framebufferColorSampleCounts)
        STRINGIFY_LIMITS(limits, framebufferDepthSampleCounts)
        STRINGIFY_LIMITS(limits, framebufferStencilSampleCounts)
        STRINGIFY_LIMITS(limits, framebufferNoAttachmentsSampleCounts)
        STRINGIFY_LIMITS(limits, maxColorAttachments)
        STRINGIFY_LIMITS(limits, sampledImageColorSampleCounts)
        STRINGIFY_LIMITS(limits, sampledImageIntegerSampleCounts)
        STRINGIFY_LIMITS(limits, sampledImageDepthSampleCounts)
        STRINGIFY_LIMITS(limits, sampledImageStencilSampleCounts)
        STRINGIFY_LIMITS(limits, storageImageSampleCounts)
        STRINGIFY_LIMITS(limits, maxSampleMaskWords)
        STRINGIFY_LIMITS(limits, timestampComputeAndGraphics)
        STRINGIFY_LIMITS(limits, timestampPeriod)
        STRINGIFY_LIMITS(limits, maxClipDistances)
        STRINGIFY_LIMITS(limits, maxCullDistances)
        STRINGIFY_LIMITS(limits, maxCombinedClipAndCullDistances)
        STRINGIFY_LIMITS(limits, discreteQueuePriorities)
        STRINGIFY_LIMITS(limits, pointSizeRange[2])
        STRINGIFY_LIMITS(limits, lineWidthRange[2])
        STRINGIFY_LIMITS(limits, pointSizeGranularity)
        STRINGIFY_LIMITS(limits, lineWidthGranularity)
        STRINGIFY_LIMITS(limits, strictLines)
        STRINGIFY_LIMITS(limits, standardSampleLocations)
        STRINGIFY_LIMITS(limits, optimalBufferCopyOffsetAlignment)
        STRINGIFY_LIMITS(limits, optimalBufferCopyRowPitchAlignment)
        STRINGIFY_LIMITS(limits, nonCoherentAtomSize);
};

inline string IsSupported(bool s) { return s? "Supported" : "Not Supported"; }
string to_string(VkPhysicalDeviceFeatures features)
{
    return
        STRINGIFY_FEATURES(features, robustBufferAccess)
        STRINGIFY_FEATURES(features, fullDrawIndexUint32)
        STRINGIFY_FEATURES(features, imageCubeArray)
        STRINGIFY_FEATURES(features, independentBlend)
        STRINGIFY_FEATURES(features, geometryShader)
        STRINGIFY_FEATURES(features, tessellationShader)
        STRINGIFY_FEATURES(features, sampleRateShading)
        STRINGIFY_FEATURES(features, dualSrcBlend)
        STRINGIFY_FEATURES(features, logicOp)
        STRINGIFY_FEATURES(features, multiDrawIndirect)
        STRINGIFY_FEATURES(features, drawIndirectFirstInstance)
        STRINGIFY_FEATURES(features, depthClamp)
        STRINGIFY_FEATURES(features, depthBiasClamp)
        STRINGIFY_FEATURES(features, fillModeNonSolid)
        STRINGIFY_FEATURES(features, depthBounds)
        STRINGIFY_FEATURES(features, wideLines)
        STRINGIFY_FEATURES(features, largePoints)
        STRINGIFY_FEATURES(features, alphaToOne)
        STRINGIFY_FEATURES(features, multiViewport)
        STRINGIFY_FEATURES(features, samplerAnisotropy)
        STRINGIFY_FEATURES(features, textureCompressionETC2)
        STRINGIFY_FEATURES(features, textureCompressionASTC_LDR)
        STRINGIFY_FEATURES(features, textureCompressionBC)
        STRINGIFY_FEATURES(features, occlusionQueryPrecise)
        STRINGIFY_FEATURES(features, pipelineStatisticsQuery)
        STRINGIFY_FEATURES(features, vertexPipelineStoresAndAtomics)
        STRINGIFY_FEATURES(features, fragmentStoresAndAtomics)
        STRINGIFY_FEATURES(features, shaderTessellationAndGeometryPointSize)
        STRINGIFY_FEATURES(features, shaderImageGatherExtended)
        STRINGIFY_FEATURES(features, shaderStorageImageExtendedFormats)
        STRINGIFY_FEATURES(features, shaderStorageImageMultisample)
        STRINGIFY_FEATURES(features, shaderStorageImageReadWithoutFormat)
        STRINGIFY_FEATURES(features, shaderStorageImageWriteWithoutFormat)
        STRINGIFY_FEATURES(features, shaderUniformBufferArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderSampledImageArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderStorageBufferArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderStorageImageArrayDynamicIndexing)
        STRINGIFY_FEATURES(features, shaderClipDistance)
        STRINGIFY_FEATURES(features, shaderCullDistance)
        STRINGIFY_FEATURES(features, shaderFloat64)
        STRINGIFY_FEATURES(features, shaderInt64)
        STRINGIFY_FEATURES(features, shaderInt16)
        STRINGIFY_FEATURES(features, shaderResourceResidency)
        STRINGIFY_FEATURES(features, shaderResourceMinLod)
        STRINGIFY_FEATURES(features, sparseBinding)
        STRINGIFY_FEATURES(features, sparseResidencyBuffer)
        STRINGIFY_FEATURES(features, sparseResidencyImage2D)
        STRINGIFY_FEATURES(features, sparseResidencyImage3D)
        STRINGIFY_FEATURES(features, sparseResidency2Samples)
        STRINGIFY_FEATURES(features, sparseResidency4Samples)
        STRINGIFY_FEATURES(features, sparseResidency8Samples)
        STRINGIFY_FEATURES(features, sparseResidency16Samples)
        STRINGIFY_FEATURES(features, sparseResidencyAliased)
        STRINGIFY_FEATURES(features, variableMultisampleRate)
        STRINGIFY_FEATURES(features, inheritedQueries);
}

string to_string(vk::SurfaceCapabilitiesKHR &details)
{
    return
        "minImageCount: " + to_string(details.minImageCount) + "\n"
        "maxImageCount: " + to_string(details.maxImageCount) + "\n"
        "currentExtent: " + to_string(details.currentExtent.width) + ", "
                          + to_string(details.currentExtent.height) + "\n"
        "minImageExtent: " + to_string(details.minImageExtent.width) + ", "
                           + to_string(details.minImageExtent.height) + "\n"
        "maxImageExtent: " + to_string(details.maxImageExtent.width) + ", "
                           + to_string(details.maxImageExtent.height) + "\n"
        "maxImageLayers: " + to_string(details.maxImageArrayLayers) + "\n"
        "supportedTransforms: " + to_string(details.supportedTransforms) + "\n"
        "currentTransform: " + to_string(details.currentTransform) + "\n"
        "supportedCompositeAlpha: " + to_string(details.supportedCompositeAlpha) + "\n"
        "supportedUsageFlags: " + to_string(details.supportedUsageFlags) + "\n";
}

