//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file VulkanDebugging.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-06
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define UNW_LOCAL_ONLY
#define ENUM_TO_STRING_CASE(ENUM) case ENUM: return #ENUM;
#include "VulkanDebugging.hpp"

/** @cond */
#include <cxxabi.h> // for __cxa_demangle
#include <dlfcn.h>     // for dladdr
#include <execinfo.h>  // for backtrace
#include <libunwind.h>
/** @endcond */

#include "Helpers/log.hpp"

using namespace std;
using namespace Log;

// Function Declarations -----------------------------------------------------------------
string to_string(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity);

// Function definitions ------------------------------------------------------------------
VKAPI_ATTR VkBool32 VKAPI_CALL DebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageType,
    const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
    void* pUserData)
{
    if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
    {
        RecordLogError("Vulkan error");

        // Message is important enough to show
        RecordLog("Message ID name: " + string(pCallbackData->pMessageIdName));
        RecordLog("Message: " + string(pCallbackData->pMessage));
        RecordLog("Severity: " + to_string(messageSeverity));
        exit(EXIT_FAILURE);
    }

    return VK_FALSE;
}

// Helper Functions ----------------------------------------------------------------------
string to_string(VkDebugUtilsMessageSeverityFlagBitsEXT message_severity)
{
    switch(message_severity)
    {
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        ENUM_TO_STRING_CASE(VK_DEBUG_UTILS_MESSAGE_SEVERITY_FLAG_BITS_MAX_ENUM_EXT)
        default:
            return "Unrecognized enum: " + to_string((long)message_severity);
    }
}

