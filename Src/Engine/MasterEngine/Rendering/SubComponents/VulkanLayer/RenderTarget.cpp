//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file RenderTarget.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-06
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "RenderTarget.hpp"

/** @cond */
#include <set>
/** @endcond */

#include "VulkanDebugging.hpp"

using namespace std;
using namespace Log;

// Function declarations -----------------------------------------------------------------
namespace
{
const int MAX_FRAMES_IN_FLIGHT = 3;

vk::UniqueRenderPass CreateRenderPass(
    HardwareInterface& hi,
    const std::vector<vk::AttachmentDescription>& attachment_descriptions);

vector<vk::UniqueFramebuffer> CreateDisplayTargetFramebuffers(
    HardwareInterface& h_interface, DisplayTarget& display);

vk::UniqueSwapchainKHR CreateSwapChain(
    HardwareInterface& hi,
    DisplayTarget& DisplayTarget,
    vk::Extent2D* extent,
    vk::PresentModeKHR* present_mode,
    uint min_image_num = 3);

vk::UniqueFramebuffer CreateFramebuffer(
    HardwareInterface& hi,
    OffScreenTarget& offscreen_target,
    uint image_num = 1,
    VulkanImage* external_depth = nullptr);

vk::UniqueImageView CreateImageView(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect = vk::ImageAspectFlagBits::eColor);

std::vector<vk::UniqueImageView> CreateImageViews(HardwareInterface&, DisplayTarget&);

vk::PresentModeKHR SelectPresentationMode(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface, bool use_vsync = false);

vk::SurfaceFormatKHR SelectSurfaceFormat(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface);
}  // namespace

// OffScreenTarget -----------------------------------------------------------------------
OffScreenTarget::OffScreenTarget(
    VulkanMemory* memory, vk::Extent2D extent, uint output_num, HardwareInterface* hi)
{
    this->extent = extent;
    this->memory = memory;
    this->h_interface = hi;

    auto device = hi->GetDevice();

    color_images.resize(output_num);
    for(uint i = 0; i < output_num; i++)
        color_images[i] =
            move(VulkanImage(color_format, {extent.width, extent.height, 1}));

    depth_image = VulkanImage(
        vk::Format::eD32Sfloat,
        vk::Extent3D(extent, 4),
        vk::ImageUsageFlagBits::eDepthStencilAttachment);
    // Create a set of renderpasses that DO clear the attachments
    for(uint i = 0; i < color_images.size(); i++)
    {
        clear_render_passes.push_back(
            CreateRenderPass(*h_interface, this->GetAttachmentDescriptions(i + 1, true)));

        _SetName(
            device,
            *clear_render_passes[i],
            "OS_clear_render_pass with output num: " + to_string(i + 1));
    }
    // Create a set of renderpasses that DON'T clear the attachments
    for(uint i = 0; i < color_images.size(); i++)
    {
        no_clear_render_passes.push_back(CreateRenderPass(
            *h_interface, this->GetAttachmentDescriptions(i + 1, false)));

        _SetName(
            device,
            *no_clear_render_passes[i],
            "OS_no_clear_render_pass with output num: " + to_string(i + 1));
    }
    if(!clear_render_passes.empty()) active_render_pass = *clear_render_passes.back();
}

void OffScreenTarget::SetDepthImage(VulkanImage& image)
{
    VulkanImage::CopyImage(image, depth_image);
}

vk::Result OffScreenTarget::StartPass(
    uint target_num, bool should_clear, VulkanImage* external_depth)
{
    // Choose which set of renderpasses to use based on input
    auto& render_passes = should_clear ? clear_render_passes : no_clear_render_passes;
    target_num = std::min((uint)color_images.size(), target_num);
    active_render_pass = *render_passes[target_num - 1];

    framebuffer = CreateFramebuffer(*h_interface, *this, target_num, external_depth);

    h_interface->StartCmdUpdate();
    auto cmd = h_interface->GetCmdBuffer();
    // Set clear value for all the color images and the depth attachment
    std::vector<vk::ClearValue> clears(target_num + 1);
    uint count = 0;
    for(count; count < target_num; count++)
    {
        clears[count].color = vk::ClearColorValue(array<float, 4>({0, 0, 0, 0}));
    }
    clears[count].depthStencil = vk::ClearDepthStencilValue(1.0f, 0.0f);

    vk::RenderPassBeginInfo render_pass_info;
    render_pass_info.renderPass = active_render_pass;
    render_pass_info.framebuffer = *framebuffer;
    render_pass_info.renderArea = vk::Rect2D({0, 0}, extent);
    render_pass_info.clearValueCount = clears.size();
    render_pass_info.pClearValues = clears.data();

    cmd.beginRenderPass(&render_pass_info, vk::SubpassContents::eInline);

    return vk::Result::eSuccess;
}

vk::Result OffScreenTarget::EndPass()
{
    auto& cmd = h_interface->GetCmdBuffer();
    auto& selected_queue = h_interface->GetQueue();
    cmd.endRenderPass();
    h_interface->EndCmdUpdate();

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd;

    auto result = selected_queue.submit(1, &submit_info, nullptr);
    if(result != vk::Result::eSuccess)
    {
        Log::RecordLog("Failed to submit off screen render command");
        return result;
    }

    result = selected_queue.waitIdle();
    if(result != vk::Result::eSuccess)
    {
        Log::RecordLogError("endpass waiting failed.");
        return result;
    }
    return result;
}

vector<vk::AttachmentDescription> OffScreenTarget::GetAttachmentDescriptions(
    uint output_num, bool clear)
{
    // Output num must be less than the number of color attachments
    assert(output_num <= color_images.size());
    for(uint i = 0; i < output_num; i++)
    {
        VulkanImage::TransitionImageLayoutToShaderReadOnly(
            color_images[i], vk::PipelineStageFlagBits::eBottomOfPipe);
    }
    // Wether this attachment set should be cleared or not
    vk::AttachmentLoadOp load_op =
        clear ? vk::AttachmentLoadOp::eClear : vk::AttachmentLoadOp::eLoad;
    vector<vk::AttachmentDescription> descriptions;
    for(uint i = 0; i < output_num; i++)
    {
        vk::AttachmentDescription color_attachment = {};
        color_attachment.format = color_format;
        color_attachment.samples = vk::SampleCountFlagBits::e1;
        color_attachment.loadOp = load_op;
        color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
        color_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
        color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
        color_attachment.initialLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
        color_attachment.finalLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

        descriptions.push_back(color_attachment);
    }

    vk::AttachmentDescription depth_attachment = {};
    depth_attachment.format = depth_format;
    depth_attachment.samples = vk::SampleCountFlagBits::e1;
    depth_attachment.loadOp = load_op;
    depth_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    depth_attachment.stencilLoadOp = vk::AttachmentLoadOp::eLoad;
    depth_attachment.stencilStoreOp = vk::AttachmentStoreOp::eStore;
    depth_attachment.initialLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
    depth_attachment.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

    descriptions.push_back(depth_attachment);

    return descriptions;
}

vector<vk::AttachmentDescription> OffScreenTarget::GetAttachmentDescriptions()
{
    return GetAttachmentDescriptions(color_images.size(), true);
}

// DisplayTarget -------------------------------------------------------------------------
DisplayTarget::DisplayTarget(
    VulkanMemory* memory, bool use_vsync, HardwareInterface* interface)
{
    this->memory = memory;
    this->use_vsync = use_vsync;
    this->h_interface = interface;

    auto& hi = *h_interface;
    auto instance = hi.GetInstance();
    auto physical_device = hi.GetPhysicalDevice();
    auto device = hi.GetDevice();

    clear_render_passes.push_back(
        CreateRenderPass(*h_interface, this->GetAttachmentDescriptions()));
    _SetName(device, *clear_render_passes[0], "main_render_pass");
    img_available_sems = h_interface->CreateSemaphores(MAX_FRAMES_IN_FLIGHT);
    render_finished_sems = h_interface->CreateSemaphores(MAX_FRAMES_IN_FLIGHT);
    in_flight_fences = h_interface->CreateFences(MAX_FRAMES_IN_FLIGHT);

    uint i = 0;
    for(auto& semaphore: img_available_sems)
        _SetName(device, *semaphore, "Image available Semaphore: " + to_string(i++));
    i = 0;
    for(auto& semaphore: render_finished_sems)
        _SetName(device, *semaphore, "Render Finished Semaphore: " + to_string(i++));

    UpdateSwapchain();
}

void DisplayTarget::UpdateSwapchain()
{
    auto device = h_interface->GetDevice();
    swap_chain = CreateSwapChain(*h_interface, *this, &extent, &present_mode);

    _SetName(device, *swap_chain, "Main swapchain");

    image_views = CreateImageViews(*h_interface, *this);
    for(uint32_t i = 0; i < image_views.size(); i++)
        _SetName(device, *image_views[i], "Main swapchain image " + to_string(i));

    depth_image = VulkanImage(
        vk::Format::eD32Sfloat,
        vk::Extent3D(extent, 4),
        vk::ImageUsageFlagBits::eDepthStencilAttachment);

    framebuffers = CreateDisplayTargetFramebuffers(*h_interface, *this);
}
// TODO(low): Implement 'no clear' functionality and external depth
vk::Result DisplayTarget::StartPass(
    uint target_num, bool should_clear, VulkanImage* external_depth)
{
    auto device = h_interface->GetDevice();

    auto result = device.waitForFences(
        1,
        &*in_flight_fences[current_frame],
        VK_TRUE,
        std::numeric_limits<uint64_t>::max());
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("StartPass fence waiting failed.");

    result = device.acquireNextImageKHR(
        *swap_chain,
        std::numeric_limits<uint64_t>::max(),
        *img_available_sems[current_frame],
        nullptr,
        &active_image_index);

    if(result != vk::Result::eSuccess)
    {
        Log::RecordLog("Failed to acquire image");
        return result;
    }

    h_interface->StartCmdUpdate();
    auto cmd = h_interface->GetCmdBuffer();

    std::array<vk::ClearValue, 2> clears = {};
    clears[0].color = vk::ClearColorValue(array<float, 4>({0, 0, 0, 0}));
    clears[1].depthStencil = vk::ClearDepthStencilValue(1.0f, 0.0f);
    vk::RenderPassBeginInfo render_pass_info(
        *clear_render_passes[0],
        *(framebuffers[active_image_index]),
        {{0, 0}, extent},
        clears.size(),
        clears.data());

    cmd.beginRenderPass(&render_pass_info, vk::SubpassContents::eInline);
    return result;
}

vk::Result DisplayTarget::EndPass()
{
    auto device = h_interface->GetDevice();
    auto& cmd_buff = h_interface->GetCmdBuffer();
    auto& graphics_queue = h_interface->GetQueue();
    cmd_buff.endRenderPass();
    h_interface->EndCmdUpdate();

    vk::Semaphore wait_semaphores[] = {*img_available_sems[current_frame]};
    vk::PipelineStageFlags wait_stages[] = {
        vk::PipelineStageFlagBits::eColorAttachmentOutput};

    vk::Semaphore signal_semaphores[] = {*render_finished_sems[current_frame]};
    vk::SubmitInfo submit_info(
        1, wait_semaphores, wait_stages, 1, &cmd_buff, 1, signal_semaphores);

    device.resetFences(1, &*in_flight_fences[current_frame]);

    auto result =
        graphics_queue.submit(1, &submit_info, *in_flight_fences[current_frame]);
    if(result != vk::Result::eSuccess)
    {
        Log::RecordLog("Failed to submit draw command buffer!");
        return result;
    }

    vk::SwapchainKHR swap_chains[] = {*swap_chain};
    vk::PresentInfoKHR present_info = {};
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = signal_semaphores;
    present_info.swapchainCount = 1;
    present_info.pSwapchains = swap_chains;
    present_info.pImageIndices = &active_image_index;

    result = graphics_queue.presentKHR(&present_info);

    current_frame = (current_frame + 1) % MAX_FRAMES_IN_FLIGHT;

    result = graphics_queue.waitIdle();
    if(result != vk::Result::eSuccess)
    {
        Log::RecordLogError("End Pass waiting failed.");
        return result;
    }
    result = device.waitIdle();
    if(result != vk::Result::eSuccess)
    {
        Log::RecordLogError("End Pass waiting failed.");
        return result;
    }

    return result;
}

vk::SurfaceCapabilitiesKHR DisplayTarget::GetSurfaceCapabilities()
{
    auto surface = h_interface->GetSurface();
    auto physical_device = h_interface->GetPhysicalDevice();
    auto [result, capabilities] = physical_device.getSurfaceCapabilitiesKHR(surface);
    return capabilities;
}

vk::Format DisplayTarget::GetSelectedFormat()
{
    auto surface = h_interface->GetSurface();
    if(selected_format == vk::Format::eUndefined)
    {
        auto physical_device = h_interface->GetPhysicalDevice();
        selected_format = SelectSurfaceFormat(physical_device, surface).format;
    }
    return selected_format;
}

vk::Format DisplayTarget::FindSupportedFormat(
    const std::vector<vk::Format>& candidates,
    vk::ImageTiling tiling,
    vk::FormatFeatureFlags features)
{
    auto physical_device = h_interface->GetPhysicalDevice();
    for(auto& format: candidates)
    {
        vk::FormatProperties properties;
        physical_device.getFormatProperties(format, &properties);

        if(tiling == vk::ImageTiling::eLinear &&
           (properties.linearTilingFeatures & features) == features)
        { return format; }
        else if(
            tiling == vk::ImageTiling::eOptimal &&
            (properties.optimalTilingFeatures & features) == features)
        {
            return format;
        }
    }

    Log::RecordLogError("Failed to find supported format");
    return vk::Format::eUndefined;
}

vk::Format DisplayTarget::GetDepthFormat()
{
    return FindSupportedFormat(
        {vk::Format::eD32Sfloat,
         vk::Format::eD32SfloatS8Uint,
         vk::Format::eD24UnormS8Uint},
        vk::ImageTiling::eOptimal,
        vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

vector<vk::AttachmentDescription> DisplayTarget::GetAttachmentDescriptions()
{
    // Attachments (format of the shading output)?
    vk::AttachmentDescription color_attachment = {};
    color_attachment.format = GetSelectedFormat();
    color_attachment.samples = vk::SampleCountFlagBits::e1;
    color_attachment.loadOp = vk::AttachmentLoadOp::eClear;
    color_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    color_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    color_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    color_attachment.finalLayout = vk::ImageLayout::ePresentSrcKHR;

    vk::AttachmentDescription depth_attachment = {};
    depth_attachment.format = GetDepthFormat();
    depth_attachment.samples = vk::SampleCountFlagBits::e1;
    depth_attachment.loadOp = vk::AttachmentLoadOp::eClear;
    depth_attachment.storeOp = vk::AttachmentStoreOp::eStore;
    depth_attachment.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
    depth_attachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
    depth_attachment.finalLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

    return {color_attachment, depth_attachment};
}

// Internal functions --------------------------------------------------------------------
namespace
{
// Create a render pass from the information of a render target
vk::UniqueRenderPass CreateRenderPass(
    HardwareInterface& hi,
    const std::vector<vk::AttachmentDescription>& attachment_descriptions)
{
    auto instance = hi.GetInstance();
    auto phys_device = hi.GetPhysicalDevice();
    auto device = hi.GetDevice();

    // Create a reference for each image target the renderpass will render to
    std::vector<vk::AttachmentReference> attachment_references(
        attachment_descriptions.size());
    for(uint i = 0; i < attachment_descriptions.size() - 1; i++)
    {
        vk::AttachmentReference color_attachment_ref(
            i, vk::ImageLayout::eColorAttachmentOptimal);
        attachment_references[i] = color_attachment_ref;
    }
    // Last attachment is depth
    vk::AttachmentReference depth_attachment_ref(
        attachment_descriptions.size() - 1,
        vk::ImageLayout::eDepthStencilAttachmentOptimal);
    attachment_references[attachment_references.size() - 1] = depth_attachment_ref;

    // TODO(low): Implement subasses
    const uint subpass_num = 1;
    vector<vk::SubpassDescription> subpasses(subpass_num);
    for(auto& sub_pass: subpasses)
    {
        // No input attachment (3rd and 4th parameters)
        sub_pass = vk::SubpassDescription();
        sub_pass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
        sub_pass.inputAttachmentCount = 0;
        sub_pass.pInputAttachments = nullptr;
        sub_pass.colorAttachmentCount = attachment_references.size() - 1;
        sub_pass.pColorAttachments = attachment_references.data();
        sub_pass.pResolveAttachments = nullptr;
        sub_pass.pDepthStencilAttachment = &attachment_references.back();
    }
    vector<vk::SubpassDependency> dependencies(subpass_num);
    uint current_dependency = 0;
    for(auto& dependency: dependencies)
    {
        // Set the dependency of this subpass
        uint32_t prev = current_dependency == 0 ? (uint32_t)VK_SUBPASS_EXTERNAL
                                                : current_dependency - 1;
        uint32_t next = current_dependency;
        current_dependency++;

        // Execution and memory dependencies between subpasses
        dependency = vk::SubpassDependency();
        dependency.srcSubpass = prev;
        dependency.dstSubpass = next;
        dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput |
                                  vk::PipelineStageFlagBits::eEarlyFragmentTests |
                                  vk::PipelineStageFlagBits::eLateFragmentTests;
        dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput |
                                  vk::PipelineStageFlagBits::eEarlyFragmentTests |
                                  vk::PipelineStageFlagBits::eLateFragmentTests;
        dependency.srcAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite |
                                   vk::AccessFlagBits::eDepthStencilAttachmentRead;
        dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentRead |
                                   vk::AccessFlagBits::eColorAttachmentWrite |
                                   vk::AccessFlagBits::eDepthStencilAttachmentRead |
                                   vk::AccessFlagBits::eDepthStencilAttachmentWrite;
    }
    vk::RenderPassCreateInfo render_pass_info(
        {},
        attachment_descriptions.size(),
        attachment_descriptions.data(),
        subpasses.size(),
        subpasses.data(),
        dependencies.size(),
        dependencies.data());

    auto [result, render_pass] = device.createRenderPassUnique(render_pass_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create render pass!");

    return move(render_pass);
}

vk::UniqueFramebuffer CreateFramebuffer(
    HardwareInterface& hi,
    OffScreenTarget& offscreen_target,
    uint image_num,
    VulkanImage* external_depth)
{
    auto device = hi.GetDevice();
    auto extent = offscreen_target.GetExtent();
    // Get the color attachments for the given target
    auto& color_attachments = offscreen_target.GetColorImages();
    // Clamp values just in case
    image_num = min(image_num, (uint)color_attachments.size());
    // Create an attachment vector to hold all attachments, including depth
    std::vector<vk::ImageView> attachments(image_num + 1);
    uint count = 0;
    for(count = 0; count < image_num; count++)
        attachments[count] = color_attachments[count].GetImageView();
    attachments[count] = external_depth == nullptr
                             ? offscreen_target.GetDepthImage()->GetImageView()
                             : external_depth->GetImageView();

    vk::FramebufferCreateInfo fb_info;
    fb_info.renderPass = offscreen_target.GetActiveRenderPass();
    fb_info.attachmentCount = attachments.size();
    fb_info.pAttachments = attachments.data();
    fb_info.width = extent.width;
    fb_info.height = extent.height;
    fb_info.layers = 1;

    auto [result, fb] = device.createFramebufferUnique(fb_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create off screen framebuffer");

    return move(fb);
}

vector<vk::UniqueFramebuffer> CreateDisplayTargetFramebuffers(
    HardwareInterface& h_interface, DisplayTarget& display)
{
    auto device = h_interface.GetDevice();
    auto image_views = display.GetImageViews();
    auto render_pass = display.GetActiveRenderPass();
    auto depth_image_view = display.GetDepthImage()->GetImageView();
    auto extent = display.GetExtent();

    vector<vk::UniqueFramebuffer> framebuffers(image_views.size());
    for(size_t i = 0; i < image_views.size(); i++)
    {
        std::array<vk::ImageView, 2> attachments = {image_views[i], depth_image_view};

        vk::FramebufferCreateInfo framebuffer_info(
            {},
            render_pass,
            attachments.size(),
            attachments.data(),
            extent.width,
            extent.height,
            1);

        auto [result, frame_buffer] = device.createFramebufferUnique(framebuffer_info);
        if(result != vk::Result::eSuccess) Log::RecordLog("Failed to create framebuffer");
        _SetName(device, *frame_buffer, "image_fb_" + to_string(i));
        framebuffers[i] = move(frame_buffer);
    }
    return move(framebuffers);
}

vk::UniqueSwapchainKHR CreateSwapChain(
    HardwareInterface& hi,
    DisplayTarget& DisplayTarget,
    vk::Extent2D* extent,
    vk::PresentModeKHR* present_mode,
    uint min_image_num)
{
    auto instance = hi.GetInstance();
    auto phys_device = hi.GetPhysicalDevice();
    auto device = hi.GetDevice();
    auto surface = hi.GetSurface();
    // Choose color format (bits per channel, color space...)
    auto selected_format = SelectSurfaceFormat(phys_device, surface);
    // Chose swapchain presentation mode (immediate, first in first out...)
    *present_mode =
        SelectPresentationMode(phys_device, surface, DisplayTarget.UseVsync());

    auto capabilities = DisplayTarget.GetSurfaceCapabilities();
    *extent = capabilities.currentExtent;

    vk::SwapchainCreateInfoKHR create_info;
    create_info.surface = surface;
    create_info.minImageCount = min_image_num;
    create_info.imageFormat = selected_format.format;
    create_info.imageColorSpace = selected_format.colorSpace;
    create_info.imageExtent = *extent;
    create_info.imageArrayLayers = 1;
    create_info.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
    create_info.imageSharingMode = vk::SharingMode::eExclusive;
    create_info.queueFamilyIndexCount = 0;
    create_info.pQueueFamilyIndices = nullptr;
    create_info.preTransform = capabilities.currentTransform;
    create_info.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
    create_info.presentMode = *present_mode;
    create_info.clipped = VK_TRUE;

    auto [result, swap_chain] = device.createSwapchainKHRUnique(create_info);
    return move(swap_chain);
}

vk::UniqueImageView CreateImageView(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect)
{
    //https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkImageViewCreateInfo.html
    vk::ImageViewCreateInfo create_info(
        {},
        image,
        vk::ImageViewType::e2D,
        format,
        vk::ComponentMapping(),
        vk::ImageSubresourceRange(image_aspect, 0, 1, 0, 1));
    auto [result, image_view] = device.createImageViewUnique(create_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create image view!");

    return move(image_view);
}

vector<vk::UniqueImageView> CreateImageViews(
    HardwareInterface& hi, DisplayTarget& DisplayTarget)
{
    auto physical_device = hi.GetPhysicalDevice();
    auto device = hi.GetDevice();
    auto surface = hi.GetSurface();
    auto swap_chain = DisplayTarget.GetSwapChain();
    // Get the presentation images in the swapchain
    auto sc_images = device.getSwapchainImagesKHR(swap_chain).value;
    auto selected_format = DisplayTarget.GetSelectedFormat();

    vector<vk::UniqueImageView> image_views(sc_images.size());
    for(size_t i = 0; i < sc_images.size(); i++)
        image_views[i] = CreateImageView(device, sc_images[i], selected_format);

    return image_views;
}

vk::SurfaceFormatKHR SelectSurfaceFormat(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface)
{
    // Get supported formats
    auto [result, formats] = phys_device.getSurfaceFormatsKHR(surface);
    if(result != vk::Result::eSuccess || formats.size() == 0)
        Log::RecordLogError("Failed to query surface formats");

    // Attempt to find the most suitable format. If it isn't found, return the first one
    for(auto& surf_format: formats)
    {
        // If the format supports 4 channels and non linbear sRGB, return this format
        if(surf_format.format == vk::Format::eB8G8R8A8Unorm &&
           surf_format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
            return surf_format;
    }
    return formats[0];
}

vk::PresentModeKHR SelectPresentationMode(
    vk::PhysicalDevice& phys_device, vk::SurfaceKHR& surface, bool use_vsync)
{
    auto [result, present_modes] = phys_device.getSurfacePresentModesKHR(surface);
    if(result != vk::Result::eSuccess || present_modes.size() == 0)
        Log::RecordLogError("Failed to query surface presetnation modes");
    // Attempt to return the best presentation orders (best to worse) if available.
    // Else, return the first one found.
    set<vk::PresentModeKHR> p_modes(present_modes.begin(), present_modes.end());
    if(!use_vsync && p_modes.find(vk::PresentModeKHR::eImmediate) != p_modes.end())
        return vk::PresentModeKHR::eImmediate;
    if(p_modes.find(vk::PresentModeKHR::eFifo) != p_modes.end())
        return vk::PresentModeKHR::eFifo;
    else if(p_modes.find(vk::PresentModeKHR::eFifoRelaxed) != p_modes.end())
        return vk::PresentModeKHR::eFifoRelaxed;
    else if(p_modes.find(vk::PresentModeKHR::eMailbox) != p_modes.end())
        return vk::PresentModeKHR::eMailbox;

    return present_modes[0];
}
}  // namespace
