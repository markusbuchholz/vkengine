//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/** *
 * @file HardwareInterface.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "HardwareInterface.hpp"

/** @cond */
#include <algorithm>
#include <set>
#include <vector>
/** @endcond */

#include "VkExtensionsStubs.hpp"
#include "VulkanDebugging.hpp"

using namespace std;
using namespace Log;

// Globals
// -------------------------------------------------------------------------------
const std::vector<const char*> VALIDATION_LAYERS = {
    "VK_LAYER_KHRONOS_validation",
};

const std::vector<const char*> DEVICE_EXTENSIONS = {
    VK_KHR_SWAPCHAIN_EXTENSION_NAME,
};
// TODO(undecided): singletons are bad, refactor this away (or maybe not)
HardwareInterface* HardwareInterface::h_interface;

// Function declarations
// -----------------------------------------------------------------
namespace
{
// Create a vulkan instance
static vk::UniqueInstance CreateInstance();
// Find available validation layers
static bool CheckAvailableLayers();
// Create a surface
static vk::UniqueSurfaceKHR CreateSurface(vk::Instance& instance, GLFWwindow* window);
// Create debug messenger
static vk::UniqueDebugUtilsMessengerEXT CreateDebugMessenger(vk::Instance& instance);
// Create a logical device
static vk::UniqueDevice CreateLogicalDevice(
    vk::Instance& instance,
    vk::SurfaceKHR& surface,
    vk::PhysicalDevice& phys_device,
    const vector<const char*>& device_extensions);

static vk::UniqueCommandPool CreateCommandPool(HardwareInterface& hi);

// Find and request mandatory extensions
static bool RequestExtensions(vector<const char*>& ret_vec);

static set<vk::PhysicalDevice> FilterDevices(
    const vector<vk::PhysicalDevice>& devices,
    vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions);

static bool VerifyExtensionsSupport(
    const vk::PhysicalDevice& device, const std::vector<const char*>& device_extensions);

static vk::PhysicalDevice PickPhysicalDevice(
    vk::Instance& instance,
    vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions);

int32_t _FindGraphicsQueueFamily(
    const vk::PhysicalDevice& device, const vk::SurfaceKHR& surface);
}  // namespace

// Class functions -----------------------------------------------------------------------
HardwareInterface::HardwareInterface(Window& window)
    : instance(CreateInstance())
{
    surface = CreateSurface(*instance, window.GetGLFWWindow());
    physical_device = PickPhysicalDevice(*instance, *surface, DEVICE_EXTENSIONS);
    device = CreateLogicalDevice(*instance, *surface, physical_device, DEVICE_EXTENSIONS);

    _SetName(*device, physical_device, physical_device.getProperties().deviceName);
    _SetName(
        *device,
        *device,
        "Logical device: " + std::string(physical_device.getProperties().deviceName));
    _SetName(*device, *surface, "Main surface");

    debug_messenger = CreateDebugMessenger(*instance);

    queue_family = _FindGraphicsQueueFamily(physical_device, *surface);
    selected_queue = device->getQueue(queue_family, 0);
    _SetName(*device, selected_queue, "Main queue");

    cmd_pool = CreateCommandPool(*this);
}

void HardwareInterface::StartCmdUpdate()
{
    vk::CommandBufferAllocateInfo alloc_info(
        *cmd_pool, vk::CommandBufferLevel::ePrimary, 1);

    auto [result, buffers] = device->allocateCommandBuffersUnique(alloc_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create command buffers");

    cmd_buffer = move(buffers[0]);

    vk::CommandBufferBeginInfo begin_info(
        vk::CommandBufferUsageFlagBits::eSimultaneousUse, nullptr);

    auto result_b = cmd_buffer->begin(&begin_info);
    if(result_b != vk::Result::eSuccess)
        Log::RecordLogError("Failed to begin recording command buffer!");

    _SetName(*device, *cmd_buffer, "graphics_cmd_buffer");
}

void HardwareInterface::DrawIndexed(
    vk::Buffer vertex_buffer,
    vk::Buffer index_buffer,
    uint index_num,
    uint instance_count,
    vk::Buffer instance_buffer)
{
    vector<vk::DeviceSize> offsets = {0};

    if(instance_count > 0)
        cmd_buffer->bindVertexBuffers(1, 1, &instance_buffer, offsets.data());

    instance_count = std::max(uint(1), instance_count);
    cmd_buffer->bindVertexBuffers(0, 1, &vertex_buffer, offsets.data());
    cmd_buffer->bindIndexBuffer(index_buffer, 0, vk::IndexType::eUint32);
    cmd_buffer->drawIndexed(index_num, instance_count, 0, 0, 0);
}

void HardwareInterface::EndCmdUpdate()
{
    auto result = cmd_buffer->end();
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to record command buffer!");
}

// TODO(low): try to create a single function that combines this and the next
vk::CommandBuffer HardwareInterface::BeginSingleTimeCommands()
{
    vk::CommandBufferAllocateInfo alloc_info(
        *cmd_pool, vk::CommandBufferLevel::ePrimary, 1);

    auto [result, command_buffers] = device->allocateCommandBuffers(alloc_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Single time command failed to allocate buffers.");

    vk::CommandBufferBeginInfo begin_info(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);
    result = command_buffers[0].begin(&begin_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Single time command failed to begin.");

    return command_buffers[0];
}

void HardwareInterface::EndSingleTimeCommands(vk::CommandBuffer& command_buffer)
{
    auto result = command_buffer.end();
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("End single time command failed.");

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;

    result = selected_queue.submit(1, &submit_info, nullptr);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Single time command failed to submit.");
    result = selected_queue.waitIdle();
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Single time command failed to wait.");

    device->freeCommandBuffers(*cmd_pool, 1, &command_buffer);
}

vector<vk::UniqueSemaphore> HardwareInterface::CreateSemaphores(uint32_t semaphore_num)
{
    std::vector<vk::UniqueSemaphore> semaphores(semaphore_num);
    vk::SemaphoreCreateInfo semaphore_info = {};
    for(size_t i = 0; i < semaphore_num; i++)
    {
        auto [result, semaphore] = device->createSemaphoreUnique(semaphore_info);
        if(result != vk::Result::eSuccess)
            Log::RecordLogError("Could not create semaphore");
        semaphores[i] = std::move(semaphore);
        _SetName(*device, *semaphores[i], "Semaphore: " + to_string(i));
    }

    return semaphores;
}

vector<vk::UniqueFence> HardwareInterface::CreateFences(uint32_t fence_num)
{
    std::vector<vk::UniqueFence> fences(fence_num);
    vk::FenceCreateInfo fence_info(vk::FenceCreateFlagBits::eSignaled);
    for(size_t i = 0; i < fence_num; i++)
    {
        auto [result, fence] = device->createFenceUnique(fence_info);
        if(result != vk::Result::eSuccess)
            Log::RecordLog("Failed to create synchronization objects for a frame!");
        fences[i] = std::move(fence);
    }

    return fences;
}

// Internal functions --------------------------------------------------------------------
namespace
{
// Create a vulkan instance
static vk::UniqueInstance CreateInstance()
{
    // Setup general information about the current application
    vk::ApplicationInfo program_info(
        "Vulkan Engine",
        VK_MAKE_VERSION(1, 0, 0),
        "No Engine",
        VK_MAKE_VERSION(1, 0, 0),
        VK_API_VERSION_1_2);

    if(!CheckAvailableLayers())
        RecordLogError("Validation layers requested, but not available!");

    vector<const char*> required_extensions;
    if(!RequestExtensions(required_extensions))
        RecordLogError("Warning: Missing a required extension");
    // Create Vulkan instance to communicate with the loader
    vk::InstanceCreateInfo createInfo(
        {},
        &program_info,
        static_cast<uint32_t>(VALIDATION_LAYERS.size()),
        VALIDATION_LAYERS.data(),
        static_cast<uint32_t>(required_extensions.size()),
        required_extensions.data());
    auto [result, instance] = vk::createInstanceUnique(createInfo);

    if(result != vk::Result::eSuccess) RecordLogError("Error: Failed to create instance");

    // Load Extension functions
    LoadExtensionStubs(*instance);
    return move(instance);
}
// TODO(low): move this into a window manager
static vk::UniqueSurfaceKHR CreateSurface(vk::Instance& instance, GLFWwindow* window)
{
    VkSurfaceKHR surface;
    if(glfwCreateWindowSurface(instance, window, nullptr, &surface) != VK_SUCCESS)
        RecordLogError("Failed to create window surface!");

    return vk::UniqueSurfaceKHR(surface, instance);
}

static vk::UniqueDebugUtilsMessengerEXT CreateDebugMessenger(vk::Instance& instance)
{
    {  // Anonymous namespace
        typedef vk::DebugUtilsMessageSeverityFlagBitsEXT vkd;
        typedef vk::DebugUtilsMessageTypeFlagBitsEXT vkm;

        auto debug_flags = vkd::eVerbose | vkd::eWarning | vkd::eError | vkd::eInfo;
        auto message_flags = vkm::eGeneral | vkm::eValidation | vkm::ePerformance;
        vk::DebugUtilsMessengerCreateInfoEXT createDebugInfo(
            {}, debug_flags, message_flags, DebugCallback, nullptr);

        VkDebugUtilsMessengerEXT messenger;
        auto info = static_cast<VkDebugUtilsMessengerCreateInfoEXT>(createDebugInfo);
        auto result =
            vkCreateDebugUtilsMessengerEXT(instance, &info, nullptr, &messenger);

        if(result != VK_SUCCESS) RecordLogError("Failed to create debug messenger");

        return vk::UniqueDebugUtilsMessengerEXT(messenger, instance);
        ;
    }  // Close anonymous namespace
}

static vk::PhysicalDevice PickPhysicalDevice(
    vk::Instance& instance,
    vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions)
{
    // Get physical devices in the system
    auto [result, devs] = instance.enumeratePhysicalDevices();
    if(result != vk::Result::eSuccess)
        RecordLogError("Failed to find GPU with Vulkan support");

    // Filter out devices that don't support the surface or extensions
    vk::SurfaceKHR t_surface = surface;
    std::set<vk::PhysicalDevice> devices =
        FilterDevices(devs, t_surface, device_extensions);
    if(devices.empty()) RecordLogError("No suitable devices found");

    // Find the best device under the qualitative heuristic "Discrete GPU >
    // anything else"
    vk::PhysicalDevice chosen_device;
    chosen_device = *std::max_element(
        devices.begin(), devices.end(), [](vk::PhysicalDevice d1, vk::PhysicalDevice d2) {
            // Permute enumerator values such that discrete GPUs
            // become the maximum
            // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkPhysicalDeviceType.html
            int p1 = ((int)d1.getProperties().deviceType + 2) % 5;
            int p2 = ((int)d2.getProperties().deviceType + 2) % 5;

            return p1 < p2;
        });

    return chosen_device;
}

static vk::UniqueDevice CreateLogicalDevice(
    vk::Instance& instance,
    vk::SurfaceKHR& surface,
    vk::PhysicalDevice& phys_device,
    const vector<const char*>& device_extensions)
{
    int32_t graphics_q_fam = _FindGraphicsQueueFamily(phys_device, surface);
    float priority = 1.0;
    std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos = {
        vk::DeviceQueueCreateInfo(
            vk::DeviceQueueCreateFlags(), graphics_q_fam, 1, &priority),
    };
    // Get the features of the selected device
    vk::PhysicalDeviceFeatures device_features;
    phys_device.getFeatures(&device_features);
    // The 4th and 5th parameters are deprecated, they should always be 0 and
    // nullptr
    // https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkDeviceCreateInfo.html
    vk::DeviceCreateInfo create_info(
        vk::DeviceCreateFlags(),
        queueCreateInfos.size(),
        queueCreateInfos.data(),
        0,
        nullptr,
        device_extensions.size(),
        device_extensions.data(),
        &device_features);

    auto [result, device] = phys_device.createDeviceUnique(create_info);
    if(result != vk::Result::eSuccess) RecordLogError("Failed to create logical device");

    return move(device);
}

static vk::UniqueCommandPool CreateCommandPool(HardwareInterface& hi)
{
    auto device = hi.GetDevice();
    auto queue_family = hi.GetQueueFamily();

    vk::CommandPoolCreateInfo command_pool_info({}, queue_family);
    auto [result, cmd_pool] = device.createCommandPoolUnique(command_pool_info);
    if(result != vk::Result::eSuccess) Log::RecordLog("Failed to create command pool!");

    return move(cmd_pool);
}

// Find available validation layers
static bool CheckAvailableLayers()
{
    // Query validation layers currently installed
    auto [result, availableLayers] = vk::enumerateInstanceLayerProperties();
    if(result != vk::Result::eSuccess)
        RecordLogError("Error: Failed to query validation layers");
    ;

    // For every required layer, see if that layer is in the supported layers
    for(const char* layer_name: VALIDATION_LAYERS)
    {
        // Check if the current required layer is supported
        bool layerFound = false;
        vk::LayerProperties layer_info;
        for(const auto& layer_properties: availableLayers)
        {
            if(strcmp(layer_name, layer_properties.layerName) == 0)
            {
                layerFound = true;
                layer_info = layer_properties;
                break;
            }
        }
        // If a required layer wasn't found, return
        if(!layerFound)
        {
            RecordLogError("Required validation layer not found: " + string(layer_name));
            return false;
        }
    }
    return true;
}

// Find and request mandatory extensions
static bool RequestExtensions(vector<const char*>& ret_vec)
{
    // Get glfw required extensions
    uint32_t glfwExtensionCount = 0;
    const char** glfwExtensions;
    glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);
    vector<const char*> required_extensions(
        glfwExtensions, glfwExtensions + glfwExtensionCount);
    required_extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    // Find all available Vulkan extensions
    auto [result, extensions] = vk::enumerateInstanceExtensionProperties();

    if(result != vk::Result::eSuccess)
        RecordLogError("Error: Failed to request available extensions");

    bool all_extensions_found = true;
    // For each required extension, see if that extension is in the supported
    // extensions
    for(const auto& rextension: required_extensions)
    {
        bool found = false;
        vk::ExtensionProperties extension_info;
        for(const auto& extension: extensions)
        {
            if(strcmp(extension.extensionName, rextension) == 0)
            {
                found = true;
                auto index = &extension - &extensions[0];
                extension_info = extension;
                // If the current extension was found, remove it from the list
                extensions.erase(extensions.begin() + index);
                break;
            }
        }

        all_extensions_found &= found;
    }
    // Put all required extensions into
    ret_vec = required_extensions;
    return all_extensions_found;
}

// Find a suitable graphics queue in the selected device
int32_t _FindGraphicsQueueFamily(
    const vk::PhysicalDevice& physical_device, const vk::SurfaceKHR& surface)
{
    vector<vk::QueueFamilyProperties> queue_families =
        physical_device.getQueueFamilyProperties();
    int32_t q_family_index = -1;
    // Iterate through each queue family in this device
    for(auto& q_family: queue_families)
    {
        // Get the flag bits for the kind of queue (e.g graphics, compute ...)
        uint32_t mask_bits = static_cast<uint32_t>(q_family.queueFlags);
        uint c_index = &q_family - &queue_families[0];
        // Check that this queue supports presentation to the surface
        auto [result, is_supported] =
            physical_device.getSurfaceSupportKHR(c_index, surface);
        // If both properties are true we have found an appropriate queue
        if((mask_bits & VK_QUEUE_GRAPHICS_BIT) && is_supported)
        {
            q_family_index = c_index;
            break;
        }
    }
    if(q_family_index < 0) Log::RecordLogError("Couldn't find appropriate Queue Family");

    return q_family_index;
}

// Return a set of available devices that support the requested surface and
// extensions
static set<vk::PhysicalDevice> FilterDevices(
    const vector<vk::PhysicalDevice>& devices,
    vk::SurfaceKHR& surface,
    const vector<const char*>& device_extensions)
{
    set<vk::PhysicalDevice> devs(devices.begin(), devices.end());
    for(auto& device: devs)
    {
        // Check there are graphic queue families
        int32_t graphics_q = _FindGraphicsQueueFamily(device, surface);
        if(graphics_q < 0) devs.erase(device);
        // Check device supports all extensions
        bool supports_extensions = VerifyExtensionsSupport(device, device_extensions);
        if(!supports_extensions) devs.erase(device);
        // Check device supports anisotropy
        vk::PhysicalDeviceFeatures device_features;
        device.getFeatures(&device_features);
        if(!device_features.samplerAnisotropy) devs.erase(device);
    }
    return devs;
}

// Verify that this device supports all required device extensions
static bool VerifyExtensionsSupport(
    const vk::PhysicalDevice& device, const std::vector<const char*>& device_extensions)
{
    auto [result, d_extensions] = device.enumerateDeviceExtensionProperties();
    set<string> r_extensions(device_extensions.begin(), device_extensions.end());
    // Eliminate from the set all extensions supported by the device
    for(auto& extension: d_extensions)
        r_extensions.erase(string(extension.extensionName));

    return r_extensions.empty();
}

}  // namespace
