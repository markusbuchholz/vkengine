//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file GeneralIncludes.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

#define GLFW_INCLUDE_VULKAN

/** @cond */
#include <GLFW/glfw3.h>
#include <algorithm>
#include <iostream>
#include <map>
#include <memory>
#include <set>
#include <string.h>
#include <vector>

#include "vulkan/vulkan.hpp"
/** @endcond */

#include "Helpers/log.hpp"

