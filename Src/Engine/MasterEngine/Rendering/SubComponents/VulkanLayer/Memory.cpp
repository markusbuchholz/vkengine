//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Memory.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-11
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#define VMA_IMPLEMENTATION
#include "Memory.hpp"

/** @cond */
#include <assert.h>
/** @endcond */

#include "HardwareInterface.hpp"
#include "Image.hpp"
#include "VulkanDebugging.hpp"

using namespace std;
// Function declarations -----------------------------------------------------------------
namespace
{
vk::UniqueImageView CreateImageView(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect);

uint32_t FindMemoryType(
    vk::PhysicalDevice& phys_device,
    uint32_t type_filter,
    vk::MemoryPropertyFlags properties);

vk::MemoryPropertyFlags SelectMemoryProperties(vk::BufferUsageFlags type);
}  // namespace

// Function definitions ------------------------------------------------------------------
VulkanMemory::VulkanMemory(HardwareInterface* hi)
    : h_interface(hi)
{
    VmaAllocatorCreateInfo allocator_info = {};
    allocator_info.physicalDevice = hi->GetPhysicalDevice();
    allocator_info.device = hi->GetDevice();
    allocator_info.instance = hi->GetInstance();
    vmaCreateAllocator(&allocator_info, &vma_allocator);
}

void VulkanMemory::Destroy()
{
    for(auto& buffer_allocation_pair: buffer_memory_allocations)
    {
        vmaDestroyBuffer(
            vma_allocator,
            (VkBuffer)buffer_allocation_pair.first,
            buffer_allocation_pair.second);
    }

    for(auto& image_allocation_pair: image_memory_allocations)
    {
        vmaDestroyImage(
            vma_allocator,
            (VkImage)image_allocation_pair.first,
            image_allocation_pair.second);
    }

    vmaDestroyAllocator(vma_allocator);
}

pair<vk::Image, vk::UniqueImageView> VulkanMemory::CreateImage(
    uint32_t width,
    uint32_t height,
    vk::Format format,
    vk::ImageTiling tiling,
    vk::ImageUsageFlags usage,
    vk::ImageLayout initial_layout)
{
    auto physical_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();

    // Set image creation information
    vk::ImageCreateInfo image_info = {};
    image_info.imageType = vk::ImageType::e2D;
    image_info.format = format;
    image_info.extent = vk::Extent3D(width, height, 1);
    image_info.mipLevels = 1;
    image_info.arrayLayers = 1;
    image_info.samples = vk::SampleCountFlagBits::e1;
    image_info.tiling = tiling;
    image_info.usage = usage;
    image_info.sharingMode = vk::SharingMode::eExclusive;
    image_info.initialLayout = initial_layout;

    VmaAllocationCreateInfo allocation_info = {};
    allocation_info.usage = VMA_MEMORY_USAGE_GPU_ONLY;
    VmaAllocation allocation;
    vk::Image image;
    VkResult result = vmaCreateImage(
        vma_allocator,
        (VkImageCreateInfo*)&image_info,
        &allocation_info,
        (VkImage*)&image,
        &allocation,
        nullptr);

    if(result != (uint)vk::Result::eSuccess)
        Log::RecordLogError(
            "Failed to create image.\nError code: " + vk::to_string((vk::Result)result));

    image_memory_allocations[(uint64_t)((VkImage)image)] = allocation;
    _SetName(device, image, "image");

    vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor;
    if(usage & vk::ImageUsageFlagBits::eDepthStencilAttachment)
        aspect = vk::ImageAspectFlagBits::eDepth;

    vk::UniqueImageView image_view = CreateImageView(device, image, format, aspect);

    return {image, move(image_view)};
}

void VulkanMemory::DestroyImage(vk::Image image)
{
    vmaDestroyImage(
        vma_allocator,
        (VkImage)image,
        image_memory_allocations[(uint64_t)((VkImage)image)]);

    image_memory_allocations.erase((uint64_t)((VkImage)image));
}

vk::Buffer VulkanMemory::CreateBuffer(
    size_t size, vk::BufferUsageFlags type, vk::MemoryPropertyFlags properties)
{
    auto physical_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::BufferCreateInfo buffer_info = {};
    buffer_info.size = size;
    buffer_info.usage = type;
    buffer_info.sharingMode = vk::SharingMode::eExclusive;

    VmaAllocationCreateInfo alloc_info = {};
    alloc_info.usage = VMA_MEMORY_USAGE_CPU_TO_GPU;
    VmaAllocation allocation;
    vk::Buffer buffer;
    VkResult result = vmaCreateBuffer(
        vma_allocator,
        (VkBufferCreateInfo*)&buffer_info,
        &alloc_info,
        (VkBuffer*)&buffer,
        &allocation,
        nullptr);

    if(result != (uint)vk::Result::eSuccess)
        Log::RecordLogError("Failed to create buffer:\n" + to_string((vk::Result)result));

    buffer_memory_allocations[(uint64_t)((VkBuffer)buffer)] = allocation;
    _SetName(device, buffer, "buffer");
    return buffer;
}

vk::Buffer VulkanMemory::CreateFilledBuffer(
    const void* object, uint32_t size, vk::BufferUsageFlags type)
{
    auto phys_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();

    using namespace vk;
    DeviceSize buffer_size = size;
    // Create buffer optimized for data copying
    auto staging_buffer = CreateBuffer(
        buffer_size,
        BufferUsageFlagBits::eTransferSrc,
        MemoryPropertyFlagBits::eHostVisible | MemoryPropertyFlagBits::eHostCoherent);
    auto staging_buffer_allocation = GetMemoryAllocation(staging_buffer);
    // Copy data from pointer onto vulkan buffer
    void* data;
    vk::Result result = device.mapMemory(
        staging_buffer_allocation.deviceMemory,
        staging_buffer_allocation.offset,
        buffer_size,
        MemoryMapFlags(0),
        &data);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Filled buffer memory creation failed.");
    memcpy(data, object, buffer_size);

    device.unmapMemory(staging_buffer_allocation.deviceMemory);
    // Create a buffer optimized for selected useage (e.g uniform, vertex...)
    auto mem_flags = SelectMemoryProperties(type);
    auto buffer =
        CreateBuffer(buffer_size, BufferUsageFlagBits::eTransferDst | type, mem_flags);
    // Copy data from staging buffer onto final buffer
    vk::CommandBuffer c_buffer = h_interface->BeginSingleTimeCommands();
    vk::BufferCopy copy_region(0, 0, buffer_size);
    c_buffer.copyBuffer(staging_buffer, buffer, 1, &copy_region);
    h_interface->EndSingleTimeCommands(c_buffer);

    _SetName(device, buffer, "filled_buffer");

    DeleteBuffer(staging_buffer);
    return buffer;
}

vector<vk::Buffer> VulkanMemory::CreateEmptyBuffers(
    uint32_t size, uint num, vk::BufferUsageFlagBits type)
{
    auto device = h_interface->GetDevice();
    vk::DeviceSize buffer_size = size;
    vector<vk::Buffer> buffers;

    auto mem_flags = SelectMemoryProperties(type);
    for(size_t i = 0; i < num; i++)
    {
        buffers.push_back(CreateBuffer(buffer_size, type, mem_flags));
        _SetName(device, buffers[i], "empty_buffer_" + to_string(i));
    }

    return buffers;
}
// TODO(undecided): this is probably unsafe since it can overwrite other buffers
void VulkanMemory::UpdateBuffer(
    vk::Buffer& buffer, const void* object_data, uint32_t size)
{
    auto device = h_interface->GetDevice();
    auto memory_allocation = GetMemoryAllocation(buffer);
    assert(size <= memory_allocation.size);
    void* data;
    vk::Result result = device.mapMemory(
        memory_allocation.deviceMemory,
        memory_allocation.offset,
        size,
        vk::MemoryMapFlags(0),
        &data);
    if(result != vk::Result::eSuccess) Log::RecordLogError("Updateing buffer failed.");

    memcpy(data, object_data, size);
    device.unmapMemory(memory_allocation.deviceMemory);
}

void VulkanMemory::DeleteBuffer(vk::Buffer& buffer)
{
    auto buffer_allocation = buffer_memory_allocations[(uint64_t)(VkBuffer)buffer];
    vmaDestroyBuffer(vma_allocator, buffer, buffer_allocation);
    buffer_memory_allocations.erase((uint64_t)(VkBuffer)buffer);
}

void VulkanMemory::CopyBufferToImage(
    vk::Buffer& buffer, vk::Image& image, uint32_t width, uint32_t height)
{
    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();

    vk::BufferImageCopy region(
        0,
        0,
        0,
        vk::ImageSubresourceLayers(vk::ImageAspectFlagBits::eColor, 0, 0, 1),
        {0, 0, 0},
        {width, height, 1});

    command_buffer.copyBufferToImage(
        buffer, image, vk::ImageLayout::eTransferDstOptimal, 1, &region);

    h_interface->EndSingleTimeCommands(command_buffer);
}

void VulkanMemory::CopyImageToImage(
    const vk::Image& image1,
    const vk::Image& image2,
    uint32_t width,
    uint32_t height,
    vk::ImageAspectFlags aspect1,
    vk::ImageAspectFlags aspect2)
{
    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();

    vk::ImageCopy image_copy = {};
    image_copy.srcSubresource.aspectMask = aspect1;
    image_copy.srcSubresource.layerCount = 1;
    image_copy.dstSubresource.aspectMask = aspect2;
    image_copy.dstSubresource.layerCount = 1;
    image_copy.extent.width = width;
    image_copy.extent.height = height;
    image_copy.extent.depth = 1;

    command_buffer.copyImage(
        image1,
        vk::ImageLayout::eTransferSrcOptimal,
        image2,
        vk::ImageLayout::eTransferDstOptimal,
        1,
        &image_copy);

    h_interface->EndSingleTimeCommands(command_buffer);
}

// Internal functions --------------------------------------------------------------------
namespace
{
uint32_t FindMemoryType(
    vk::PhysicalDevice& phys_device,
    uint32_t type_filter,
    vk::MemoryPropertyFlags properties)
{
    vk::PhysicalDeviceMemoryProperties mem_properties;
    phys_device.getMemoryProperties(&mem_properties);

    for(uint32_t i = 0; i < mem_properties.memoryTypeCount; i++)
    {
        if((type_filter & (1 << i)) &&
           (mem_properties.memoryTypes[i].propertyFlags & properties) == properties)
            return i;
    }

    Log::RecordLogError("Failed to find suitable memory type");
    return -1;
}

vk::UniqueImageView CreateImageView(
    vk::Device& device,
    vk::Image& image,
    vk::Format format,
    vk::ImageAspectFlags image_aspect)
{
    //https://www.khronos.org/registry/vulkan/specs/1.1-extensions/man/html/VkImageViewCreateInfo.html
    vk::ImageViewCreateInfo create_info(
        {},
        image,
        vk::ImageViewType::e2D,
        format,
        vk::ComponentMapping(),
        vk::ImageSubresourceRange(image_aspect, 0, 1, 0, 1));
    auto [result, image_view] = device.createImageViewUnique(create_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create image view!");

    return move(image_view);
}

vk::MemoryPropertyFlags SelectMemoryProperties(vk::BufferUsageFlags type)
{
    using namespace vk;
    return (type & BufferUsageFlagBits::eUniformBuffer) !=
                   BufferUsageFlagBits::eUniformBuffer
               ? MemoryPropertyFlagBits::eHostVisible |
                     MemoryPropertyFlagBits::eHostCoherent
               : MemoryPropertyFlagBits::eDeviceLocal;
}

}  // namespace
