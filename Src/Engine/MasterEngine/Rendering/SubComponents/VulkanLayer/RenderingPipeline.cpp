//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file RenderingPipeline.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-07
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "RenderingPipeline.hpp"

#include "RenderTarget.hpp"
#include "VulkanDebugging.hpp"

using namespace std;

// Function declarations -----------------------------------------------------------------
namespace
{
vk::UniquePipelineLayout CreatePipelineLayout(
    HardwareInterface& h_interface, RenderingComputePipeline& r_pipeline);

vk::UniqueDescriptorSetLayout CreateDescriptorSetLayout(
    HardwareInterface& h_interface, ShaderProgram& program);

vk::UniquePipeline CreateGraphicsPipeline(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& r_pipeline,
    ShaderProgram& program);

vk::UniquePipeline CreateComputePipeline(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& r_pipeline,
    ShaderProgram& program);

vk::UniqueDescriptorPool CreateDescriptorPool(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& pipeline);

vector<vk::UniqueDescriptorSet> CreateDescriptorSets(
    HardwareInterface& hi, RenderingComputePipeline& r_pipeline);

vk::UniqueSampler CreateTextureSampler(
    HardwareInterface& h_interface, VulkanImage& image);
}  // namespace

// Function definitions ------------------------------------------------------------------
RenderingComputePipeline::RenderingComputePipeline(
    VulkanMemory* memory,
    RenderTarget* r_target,
    ShaderProgram& program,
    HardwareInterface* hi)
    : h_interface(hi)
    , memory(memory)
    , render_target(r_target)
{
    this->program = move(program);
    InitializeUniformBindings();
    Update();
}

void RenderingComputePipeline::Update()
{
    descriptor_set_layout = CreateDescriptorSetLayout(*h_interface, program);

    descriptor_sets.clear();
    descriptor_pool = CreateDescriptorPool(*h_interface, *render_target, *this);

    pipeline_layout = CreatePipelineLayout(*h_interface, *this);
    if(program.IsCompute())
        pipeline = CreateComputePipeline(*h_interface, *render_target, *this, program);
    else
        pipeline = CreateGraphicsPipeline(*h_interface, *render_target, *this, program);
    _SetName(h_interface->GetDevice(), *pipeline, program.GetProgramName());
}

void RenderingComputePipeline::InitializeUniformBindings()
{
    auto uniform_binding_size_map = program.GetUniformBindingSizeMap();
    for(auto& binding_size_pair: uniform_binding_size_map)
    {
        auto binding = binding_size_pair.first;
        auto size = binding_size_pair.second;

        auto buffer = memory->CreateEmptyBuffers(
            size, 1, vk::BufferUsageFlagBits::eUniformBuffer)[0];
        uniform_buffers_map[binding] = {buffer, size};
    }

    auto& empty_image = VulkanImage::GetEmptyImage();
    for(auto& sampler_binding: program.GetSamplerBindingMap())
    {
        auto empty_sampler = CreateTextureSampler(*h_interface, empty_image);
        samplers_map[sampler_binding] = {empty_image.GetImageView(), move(empty_sampler)};
    }
}

void RenderingComputePipeline::PrepareComputePipeline(
    const std::vector<std::pair<VulkanBuffer, uint>>& buffers,
    const std::vector<std::pair<VulkanImage*, uint>>& textures,
    const std::vector<std::pair<VulkanImage*, uint>>& image_storages)
{
    auto& device = h_interface->GetDevice();
    auto& queue = h_interface->GetQueue();

    assert(image_storages.size() == image_storage_map.size());
    for(const auto& [image, binding]: image_storages)
        image_storage_map[binding] = image->GetImageView();

    for(const auto& [buffer, binding]: buffers)
    {
        storage_sizes_map[binding] = buffer.size;
        storage_buffers_map[binding] = buffer.buffer;
    }

    descriptor_sets.clear();
    descriptor_sets = CreateDescriptorSets(*h_interface, *this);

    h_interface->StartCmdUpdate();
    auto& cmd_buffer = h_interface->GetCmdBuffer();

    vk::FenceCreateInfo fence_create_info = {};
    fence_create_info.flags = {};
    auto [result, fence] = device.createFenceUnique(fence_create_info);

    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create compute fence");

    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to begin recording command buffer!");
    _SetName(device, cmd_buffer, "compute_cmd_buffer");

    cmd_buffer.bindPipeline(vk::PipelineBindPoint::eCompute, GetPipeline());
    cmd_buffer.bindDescriptorSets(
        vk::PipelineBindPoint::eCompute,
        GetPipelineLayout(),
        0,
        1,
        &GetDescriptorSets()[0],
        0,
        nullptr);
    cmd_buffer.dispatch(100, 1, 1);
    h_interface->EndCmdUpdate();

    vk::SubmitInfo submit_info = {};
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buffer;

    result = queue.submit(1, &submit_info, *fence);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Preparing compute Pipeline failed.");

    result =
        device.waitForFences(1, &*fence, VK_TRUE, std::numeric_limits<uint64_t>::max());

    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Compute waiting for fence failed.");
}

void RenderingComputePipeline::PrepareGraphicsPipeline(
    const std::vector<std::pair<VulkanImage*, uint>>& textures)
{
    for(const auto& [image, binding]: textures)
        samplers_map[binding] = {
            image->GetImageView(), CreateTextureSampler(*h_interface, *image)};

    descriptor_sets.clear();
    descriptor_sets = CreateDescriptorSets(*h_interface, *this);

    auto cmd = h_interface->GetCmdBuffer();
    auto extent = render_target->GetExtent();

    vk::Viewport viewport(0, 0, (float)extent.width, (float)extent.height, 0, 1);
    cmd.setViewport(0, 1, &viewport);
    vk::Rect2D scissor({0, 0}, extent);
    cmd.setScissor(0, 1, &scissor);

    cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, *pipeline);
    cmd.bindDescriptorSets(
        vk::PipelineBindPoint::eGraphics,
        *pipeline_layout,
        0,
        1,
        &*(descriptor_sets[0]),
        0,
        nullptr);
}

void RenderingComputePipeline::UpdateUniformBuffers(
    const vector<pair<uint, vector<byte>>>& uniform_data)
{
    auto& ubo_map = uniform_buffers_map;
    assert(uniform_data.size() == ubo_map.size());
    for(auto& [binding, data_buffer]: uniform_data)
    {
        if(ubo_map.count(binding) == 0)
        {
            Log::RecordLogError(
                "uniform binding " + std::to_string(binding) + " does not exist");
            exit(0);
            return;
        }
        assert(data_buffer.size() == ubo_map.at(binding).size);

        auto current_image_idx = render_target->GetActiveImageIndex();
        memory->UpdateBuffer(
            ubo_map[binding].buffer, data_buffer.data(), data_buffer.size());
    }
}

// Internal functions --------------------------------------------------------------------
namespace
{
vk::UniquePipelineLayout CreatePipelineLayout(
    HardwareInterface& h_interface, RenderingComputePipeline& r_pipeline)
{
    auto device = h_interface.GetDevice();
    auto d_set_layout = r_pipeline.GetDescriptorSetLayout();
    // Create the attachments for the pass, 1 for each subpass?
    vk::PipelineLayoutCreateInfo pipeline_layout_info({}, 1, &d_set_layout, 0, nullptr);
    auto [result, pipeline_layout] =
        device.createPipelineLayoutUnique(pipeline_layout_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLog("Failed to create pipeline layout!");

    _SetName(device, *pipeline_layout, "Pipeline layout");
    return move(pipeline_layout);
}

vk::UniqueDescriptorSetLayout CreateDescriptorSetLayout(
    HardwareInterface& h_interface, ShaderProgram& program)
{
    auto device = h_interface.GetDevice();

    vector<vk::DescriptorSetLayoutBinding> bindings = program.GetLayoutBindings();

    vk::DescriptorSetLayoutCreateInfo info(
        {}, (uint32_t)bindings.size(), bindings.data());
    auto [result, d_set_layout] = device.createDescriptorSetLayoutUnique(info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create descriptor set layout");

    _SetName(device, *d_set_layout, "descriptor_set_layout");
    return move(d_set_layout);
}

vk::UniqueDescriptorPool CreateDescriptorPool(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& pipeline)
{
    auto device = hi.GetDevice();
    auto size = render_target.GetFbNum();
    uint uniform_num = pipeline.GetUniformBuffersMap().size();
    uint sampler_num = pipeline.GetSamplerMap().size();
    uint image_num = pipeline.GetImageStorageMap().size();
    uint storage_num = pipeline.GetBufferStorageMap().size();

    // TODO(undecided): instead of making a dummy value for uniforms and samplers,
    // handle the case where no uniform/sampler is needed
    std::array<vk::DescriptorPoolSize, 4> pool_sizes = {};

    pool_sizes[0] = vk::DescriptorPoolSize(
        vk::DescriptorType::eUniformBuffer, size * std::max((uint)1, uniform_num));
    pool_sizes[1] = vk::DescriptorPoolSize(
        vk::DescriptorType::eCombinedImageSampler, size * std::max((uint)1, sampler_num));
    pool_sizes[2] = vk::DescriptorPoolSize(
        vk::DescriptorType::eStorageImage, size * std::max((uint)1, image_num));
    pool_sizes[3] = vk::DescriptorPoolSize(
        vk::DescriptorType::eStorageBuffer, size * std::max((uint)1, storage_num));

    auto c_flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
    vk::DescriptorPoolCreateInfo pool_info(
        c_flags, size, (uint32_t)pool_sizes.size(), pool_sizes.data());

    auto [result, descriptor_pool] = device.createDescriptorPoolUnique(pool_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create descriptor pool");

    _SetName(device, *descriptor_pool, "descriptor_pool");
    return move(descriptor_pool);
}

vector<vk::UniqueDescriptorSet> CreateDescriptorSets(
    HardwareInterface& hi, RenderingComputePipeline& r_pipeline)
{
    auto device = hi.GetDevice();
    auto descriptor_set_layout = r_pipeline.GetDescriptorSetLayout();
    auto descriptor_pool = r_pipeline.GetDescriptorPool();

    vk::DescriptorSetAllocateInfo alloc_info(descriptor_pool, 1, &descriptor_set_layout);
    auto [result, descriptor_sets] = device.allocateDescriptorSetsUnique(alloc_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to allocate descriptor sets");

    map<uint, VulkanBuffer>& uniform_map = r_pipeline.GetUniformBuffersMap();
    map<uint, pair<vk::ImageView, vk::UniqueSampler>>& sampler_map =
        r_pipeline.GetSamplerMap();
    map<uint, vk::ImageView>& image_storage_map = r_pipeline.GetImageStorageMap();
    map<uint, vk::Buffer>& buffer_storage_map = r_pipeline.GetBufferStorageMap();

    vector<vk::WriteDescriptorSet> descriptor_writes(
        sampler_map.size() + uniform_map.size() + image_storage_map.size() +
        buffer_storage_map.size());
    vector<vk::DescriptorBufferInfo> buffer_infos(uniform_map.size());
    uint index = 0;
    // Create uniform buffer descriptors
    for(auto& [binding, buffer]: uniform_map)
    {
        vk::DescriptorBufferInfo buffer_info;
        buffer_info.buffer = buffer.buffer;
        buffer_info.offset = 0;
        buffer_info.range = buffer.size;

        buffer_infos[index] = buffer_info;

        vk::WriteDescriptorSet descriptor_write = {};
        descriptor_write.dstSet = *descriptor_sets[0],
        descriptor_write.dstBinding = binding, descriptor_write.dstArrayElement = 0,
        descriptor_write.descriptorCount = 1,
        descriptor_write.descriptorType = vk::DescriptorType::eUniformBuffer,
        descriptor_write.pImageInfo = nullptr,
        descriptor_write.pBufferInfo = &buffer_infos[index];

        descriptor_writes[index] = descriptor_write;
        index++;
    }
    // Create sampler descriptors
    vector<vk::DescriptorImageInfo> image_infos(sampler_map.size());
    uint u = 0;
    for(auto& [binding, view_sampler_pair]: sampler_map)
    {
        auto sampler = r_pipeline.GetSampler(binding);
        auto sampler_view = r_pipeline.GetSamplerView(binding);

        vk::DescriptorImageInfo& image_info = image_infos[u];
        image_info.sampler = sampler;
        image_info.imageView = sampler_view;
        image_info.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;

        vk::WriteDescriptorSet descriptor_set_info = {};
        descriptor_set_info.dstSet = *descriptor_sets[0];
        descriptor_set_info.dstBinding = binding;
        descriptor_set_info.dstArrayElement = 0;
        descriptor_set_info.descriptorCount = 1;
        descriptor_set_info.descriptorType = vk::DescriptorType::eCombinedImageSampler;
        descriptor_set_info.pImageInfo = &image_info;
        descriptor_set_info.pBufferInfo = nullptr;
        descriptor_set_info.pTexelBufferView = nullptr;

        descriptor_writes[index] = descriptor_set_info;

        u++;
        index++;
    }

    // Create image storage descriptors
    vector<vk::DescriptorImageInfo> image_storage_infos(image_storage_map.size());
    u = 0;
    for(auto& [binding, image_view]: image_storage_map)
    {
        vk::DescriptorImageInfo& image_info = image_storage_infos[u];
        image_info.imageView = image_view;
        image_info.imageLayout = vk::ImageLayout::eGeneral;

        vk::WriteDescriptorSet descriptor_set_info = {};
        descriptor_set_info.dstSet = *descriptor_sets[0];
        descriptor_set_info.dstBinding = binding;
        descriptor_set_info.dstArrayElement = 0;
        descriptor_set_info.descriptorCount = 1;
        descriptor_set_info.descriptorType = vk::DescriptorType::eStorageImage;
        descriptor_set_info.pImageInfo = &image_info;
        descriptor_set_info.pBufferInfo = nullptr;
        descriptor_set_info.pTexelBufferView = nullptr;

        descriptor_writes[index] = descriptor_set_info;

        u++;
        index++;
    }

    // Create buffer storage descriptors
    vector<vk::DescriptorBufferInfo> buffer_storage_infos(buffer_storage_map.size());
    u = 0;
    for(auto& [binding, buffer]: buffer_storage_map)
    {
        vk::DescriptorBufferInfo& buffer_info = buffer_storage_infos[u];
        buffer_info.buffer = buffer;
        buffer_info.offset = 0;
        buffer_info.range = r_pipeline.GetBufferStorageSize(binding);

        vk::WriteDescriptorSet descriptor_set_info = {};
        descriptor_set_info.dstSet = *descriptor_sets[0];
        descriptor_set_info.dstBinding = binding;
        descriptor_set_info.dstArrayElement = 0;
        descriptor_set_info.descriptorCount = 1;
        descriptor_set_info.descriptorType = vk::DescriptorType::eStorageBuffer;
        descriptor_set_info.pImageInfo = nullptr;
        descriptor_set_info.pBufferInfo = &buffer_info;
        descriptor_set_info.pTexelBufferView = nullptr;

        descriptor_writes[index] = descriptor_set_info;

        u++;
        index++;
    }

    device.updateDescriptorSets(
        (uint32_t)descriptor_writes.size(), descriptor_writes.data(), 0, nullptr);

    return move(descriptor_sets);
}

vk::UniquePipeline CreateComputePipeline(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& r_pipeline,
    ShaderProgram& program)
{
    auto device = hi.GetDevice();
    auto pipeline_layout = r_pipeline.GetPipelineLayout();

    auto& shader_modules = program.GetShaderModules();
    assert(shader_modules.size() == 1);
    auto& [stage_flag, module] = *shader_modules.begin();

    vk::PipelineShaderStageCreateInfo shader_stage_info = {};
    shader_stage_info.stage = stage_flag;
    shader_stage_info.module = *module;
    shader_stage_info.pName = "main";

    vk::ComputePipelineCreateInfo create_info = {};
    create_info.stage = shader_stage_info;
    create_info.layout = pipeline_layout;

    auto [result, compute_pipeline] = device.createComputePipelineUnique({}, create_info);

    return move(compute_pipeline);
}

vk::UniquePipeline CreateGraphicsPipeline(
    HardwareInterface& hi,
    RenderTarget& render_target,
    RenderingComputePipeline& r_pipeline,
    ShaderProgram& program)
{
    auto device = hi.GetDevice();
    auto extent = render_target.GetExtent();
    auto render_pass = render_target.GetActiveRenderPass();
    uint color_attachment_num = render_target.GetColorAttachmentCount();
    auto pipeline_layout = r_pipeline.GetPipelineLayout();
    auto& shader_config = r_pipeline.GetShaderConfig();

    // Acquire shaders
    bool is_defined;
    bool is_valid = true;
    auto& shader_modules = program.GetShaderModules();

    vector<vk::PipelineShaderStageCreateInfo> shader_stages;
    for(auto& [stage_flag, module]: shader_modules)
    {
        vk::PipelineShaderStageCreateInfo shader_stage_info = {};
        shader_stage_info.stage = stage_flag;
        shader_stage_info.module = *module;
        shader_stage_info.pName = "main";

        shader_stages.push_back(shader_stage_info);
    }

    auto binding_descriptions = r_pipeline.GetProgram().GetVertexBindingDescriptions();
    auto attribute_descriptions = r_pipeline.GetProgram().GetAttributeDescriptions();

    // Pipeline vertex layout description
    vk::PipelineVertexInputStateCreateInfo vertex_input_info = {};
    vertex_input_info.vertexBindingDescriptionCount = binding_descriptions.size();
    vertex_input_info.pVertexBindingDescriptions = binding_descriptions.data();
    vertex_input_info.vertexAttributeDescriptionCount = attribute_descriptions.size();
    vertex_input_info.pVertexAttributeDescriptions = attribute_descriptions.data();

    // Select rasterization algorithm and indexed drawing
    vk::PipelineInputAssemblyStateCreateInfo input_assembly(
        {}, shader_config.topology, VK_FALSE);
    // Set viewport dimensions and properties
    vk::Viewport viewport(0, 0, (float)extent.width, (float)extent.height, 0, 1);
    vk::Rect2D scissor({0, 0}, extent);

    vk::PipelineViewportStateCreateInfo viewport_state({}, 1, &viewport, 1, &scissor);
    // Setup rasterization details
    vk::PipelineRasterizationStateCreateInfo rasterizer = {};
    rasterizer.depthClampEnable = VK_FALSE;
    rasterizer.rasterizerDiscardEnable = VK_FALSE;
    rasterizer.polygonMode = shader_config.polygon_mode;
    rasterizer.cullMode = vk::CullModeFlagBits::eNone;
    rasterizer.frontFace = vk::FrontFace::eCounterClockwise;
    rasterizer.depthBiasEnable = VK_FALSE;
    rasterizer.depthBiasConstantFactor = 0.0;
    rasterizer.depthBiasClamp = 0.0;
    rasterizer.depthBiasSlopeFactor = 0.0;
    rasterizer.lineWidth = shader_config.line_width;

    // Multisampling
    vk::PipelineMultisampleStateCreateInfo multisampling(
        {}, vk::SampleCountFlagBits::e1, VK_FALSE, 1.0, nullptr, VK_FALSE, VK_FALSE);
    // Color blending
    std::vector<vk::PipelineColorBlendAttachmentState> color_blends(color_attachment_num);
    uint i = 0;
    for(auto& color_blend: color_blends)
    {
        color_blend = vk::PipelineColorBlendAttachmentState();
        color_blend.blendEnable = VK_TRUE;
        color_blend.srcColorBlendFactor = vk::BlendFactor::eOne;
        color_blend.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
        color_blend.colorBlendOp = vk::BlendOp::eAdd;
        color_blend.srcAlphaBlendFactor = vk::BlendFactor::eOne;
        color_blend.dstAlphaBlendFactor = vk::BlendFactor::eZero;
        color_blend.alphaBlendOp = vk::BlendOp::eAdd;
        color_blend.colorWriteMask =
            vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG |
            vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    }
    vk::PipelineColorBlendStateCreateInfo color_blending(
        {},
        VK_FALSE,
        vk::LogicOp::eCopy,
        color_blends.size(),
        color_blends.data(),
        {{0, 0, 0, 0}});

    auto depth_test = shader_config.depth_test;
    vk::PipelineDepthStencilStateCreateInfo depth_stencil(
        {},
        depth_test,
        VK_TRUE,
        vk::CompareOp::eLess,
        VK_FALSE,
        VK_FALSE,
        {},
        {},
        0.0f,
        1.0f);

    std::array<vk::DynamicState, 2> dynamic_states = {
        vk::DynamicState::eViewport, vk::DynamicState::eScissor};
    vk::PipelineDynamicStateCreateInfo dynamic_info(
        {}, dynamic_states.size(), dynamic_states.data());
    // Create pipeline
    vk::GraphicsPipelineCreateInfo pipeline_info(
        {},
        shader_stages.size(),
        shader_stages.data(),
        &vertex_input_info,
        &input_assembly,
        nullptr,
        &viewport_state,
        &rasterizer,
        &multisampling,
        &depth_stencil,
        &color_blending,
        &dynamic_info,
        pipeline_layout,
        render_pass,
        0);

    auto [result, graphics_pipeline] =
        device.createGraphicsPipelineUnique({}, pipeline_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLog("Failed to create graphics pipeline!");
    return move(graphics_pipeline);
}

vk::UniqueSampler CreateTextureSampler(HardwareInterface& h_interface, VulkanImage& image)
{
    vk::SamplerAddressMode tiling_mode = image.GetTilingMode();
    auto device = h_interface.GetDevice();

    vk::SamplerCreateInfo sampler_info = {};
    sampler_info.magFilter = vk::Filter::eLinear;
    sampler_info.minFilter = vk::Filter::eLinear;
    sampler_info.mipmapMode = vk::SamplerMipmapMode::eNearest;
    sampler_info.addressModeU = tiling_mode;
    sampler_info.addressModeV = tiling_mode;
    sampler_info.addressModeW = tiling_mode;
    sampler_info.mipLodBias = 0;
    sampler_info.anisotropyEnable = VK_TRUE;
    sampler_info.maxAnisotropy = 16;
    sampler_info.compareEnable = VK_FALSE;
    sampler_info.compareOp = vk::CompareOp::eAlways;
    sampler_info.minLod = 0;
    sampler_info.maxLod = 0;
    sampler_info.borderColor = vk::BorderColor::eIntOpaqueBlack;
    sampler_info.unnormalizedCoordinates = VK_FALSE;

    auto [result, texture_sampler] = device.createSamplerUnique(sampler_info);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Failed to create texture sampler");

    _SetName(device, *texture_sampler, "texture_sampler");

    return move(texture_sampler);
}

}  // namespace
