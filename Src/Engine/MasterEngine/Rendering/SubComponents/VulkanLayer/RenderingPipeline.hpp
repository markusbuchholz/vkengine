//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file RenderingPipeline.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-07
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <unordered_map>
/** @endcond */

#include "ShaderProgram.hpp"
#include "Utils.hpp"

class RenderTarget;
class VulkanImage;
class VulkanMemory;

struct VulkanBuffer
{
    vk::Buffer buffer;
    size_t size;
};
/**
 * @brief Wrapper for Vulkan rendering pipeline objects. Used to setup all the information
 * needed to do a render pass or compute dispatch.
 *
 */
class RenderingComputePipeline
{
private:
    vk::UniqueDescriptorPool descriptor_pool;
    std::vector<vk::UniqueDescriptorSet> descriptor_sets;
    std::map<uint, VulkanBuffer> uniform_buffers_map;
    std::map<uint, std::pair<vk::ImageView, vk::UniqueSampler>> samplers_map;
    std::map<uint, vk::ImageView> image_storage_map;
    std::map<uint, vk::Buffer> storage_buffers_map;

    vk::UniquePipelineLayout pipeline_layout;
    vk::UniquePipeline pipeline;
    vk::UniqueDescriptorSetLayout descriptor_set_layout;
    vk::UniqueDescriptorSet descriptor_set;

    std::map<uint, size_t> uniform_sizes_map;
    std::map<uint, size_t> storage_sizes_map;

    ShaderProgram program;

    // TODO(medium): figure out a way to use smart pointers here
    HardwareInterface* h_interface;
    VulkanMemory* memory;

    RenderTarget* render_target;

    void InitializeUniformBindings();

  public:
    RenderingComputePipeline(){};
    RenderingComputePipeline(
        VulkanMemory* memory,
        RenderTarget* r_target,
        ShaderProgram& program,
        HardwareInterface* hi          = HardwareInterface::h_interface);

    void Update();

    typename std::pair<
        vk::VertexInputBindingDescription,
        std::vector<vk::VertexInputAttributeDescription>>
        InputDescriptorPair;
    /**
     * @brief Get the Shader Program .
     *
     * @return ShaderProgram& Reference to the shader program associated with this
     * pipeline.
     */
    ShaderProgram& GetProgram() { return program; }

    vk::PipelineLayout GetPipelineLayout() { return *pipeline_layout; }

    vk::Pipeline GetPipeline() { return *pipeline; }

    vk::DescriptorSetLayout& GetDescriptorSetLayout() { return *descriptor_set_layout; }

    vk::DescriptorPool& GetDescriptorPool() { return *descriptor_pool; }

    vk::ImageView GetSamplerView(uint binding)
    {
        return samplers_map[binding].first;
    }

    vk::Sampler GetSampler(uint binding)
    {
        return *samplers_map[binding].second;
    }

    std::map<uint, VulkanBuffer>& GetUniformBuffersMap() { return uniform_buffers_map; }

    std::map<uint, std::pair<vk::ImageView, vk::UniqueSampler>>& GetSamplerMap()
    {
        return samplers_map;
    }

    size_t GetUniformSize(uint binding) { return uniform_sizes_map.at(binding); }

    std::vector<vk::DescriptorSet> GetDescriptorSets()
    {
        return CastVkArray(descriptor_sets);
    }

    std::map<uint, vk::ImageView>& GetImageStorageMap() { return image_storage_map; }

    std::map<uint, vk::Buffer>& GetBufferStorageMap() { return storage_buffers_map; }

    size_t GetBufferStorageSize(uint binding) { return storage_sizes_map.at(binding); }

    const ShaderConfig& GetShaderConfig() { return program.GetShaderConfig(); }

    void PrepareGraphicsPipeline(
        const std::vector<std::pair<VulkanImage*, uint>>& textures);

    void UpdateUniformBuffers(
        const std::vector<std::pair<uint, std::vector<std::byte>>>& uniform_data);

    void CreateSampler(VulkanImage& image, uint binding);

    void PrepareComputePipeline(
        const std::vector<std::pair<VulkanBuffer, uint>>& buffers,
        const std::vector<std::pair<VulkanImage*, uint>>& textures,
        const std::vector<std::pair<VulkanImage*, uint>>& image_storages = {});
};

