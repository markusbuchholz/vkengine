//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file HardwareInterface.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-04
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

#define GLFW_INCLUDE_VULKAN

/** @cond */
#include "GLFW/glfw3.h"
#include "vulkan/vulkan.hpp"
/** @endcond */

#include "MasterEngine/Rendering/WindowSystem/Window.hpp"

/**
 * @brief Wrapper to facilitate communication with he graphics hardware through Vulkan.
 *
 * The instance, physical device, logical device... Are all encapsulated here, it mostly
 * reduces the amoutn of parameters that need to be passed around, but it also provides
 * some low level functionality to dispatch GPU work.
 *
 */
class HardwareInterface
{
  private:
    vk::UniqueInstance instance;
    vk::PhysicalDevice physical_device;
    vk::UniqueDevice device;
    vk::UniqueDebugUtilsMessengerEXT debug_messenger;
    vk::UniqueSurfaceKHR surface;
    vk::UniqueCommandPool cmd_pool;
    vk::UniqueCommandBuffer cmd_buffer;

    int32_t queue_family = -1;
    vk::Queue selected_queue;

  public:
    // TODO(medium): Try to make this into a shared_ptr (currently causes segfault)
    static HardwareInterface* h_interface;
    static void SetGlobalPointer(HardwareInterface* ptr) { h_interface = ptr; }

    HardwareInterface(){};
    HardwareInterface(Window& window);

    vk::Instance& GetInstance() { return *instance; }
    vk::PhysicalDevice& GetPhysicalDevice() { return physical_device; }
    vk::Device& GetDevice() { return *device; }
    vk::SurfaceKHR& GetSurface() { return *surface; }
    vk::CommandPool& GetCommandPool() { return *cmd_pool; }
    vk::Queue& GetQueue() { return selected_queue; }
    int32_t GetQueueFamily() { return queue_family; }
    vk::CommandBuffer& GetCmdBuffer() { return *cmd_buffer; }

    void StartCmdUpdate();
    void EndCmdUpdate();
    void DrawIndexed(
        vk::Buffer v_buffer,
        vk::Buffer i_buffer,
        uint index_num,
        uint instance_count = 1,
        vk::Buffer instance_buffer = vk::Buffer());

    vk::CommandBuffer BeginSingleTimeCommands();
    void EndSingleTimeCommands(vk::CommandBuffer& command_buffer);

    std::vector<vk::UniqueSemaphore> CreateSemaphores(uint32_t semaphore_num);
    std::vector<vk::UniqueFence> CreateFences(uint32_t fence_num);
};
