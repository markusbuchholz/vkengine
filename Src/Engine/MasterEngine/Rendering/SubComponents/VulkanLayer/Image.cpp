//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Image.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-14
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "Image.hpp"

/** @cond */
#include "stb_image.h"
/** @endcond */

#include "HardwareInterface.hpp"
#include "VulkanDebugging.hpp"

using namespace std;

// Globals -------------------------------------------------------------------------------
HardwareInterface* VulkanImage::h_interface;
VulkanMemory* VulkanImage::memory;
VulkanImage VulkanImage::empty_image;

// Static functions ----------------------------------------------------------------------
void VulkanImage::CopyImage(VulkanImage& image_src, VulkanImage& image_dst)
{
    assert(image_src.memory == image_dst.memory);
    auto& memory = *image_src.memory;

    VulkanImage::TransitionImageLayoutToTransferSrc(
        image_src, vk::PipelineStageFlagBits::eBottomOfPipe);
    VulkanImage::TransitionImageLayoutToTransferDst(
        image_dst, vk::PipelineStageFlagBits::eBottomOfPipe);

    memory.CopyImageToImage(
        image_src.GetImage(),
        image_dst.GetImage(),
        image_src.GetWidth(),
        image_src.GetHeight(),
        image_src.aspect,
        image_dst.aspect);

    VulkanImage::TransitionImageLayoutToDepthAttachment(
        image_src, vk::PipelineStageFlagBits::eTopOfPipe);
    VulkanImage::TransitionImageLayoutToDepthAttachment(
        image_dst, vk::PipelineStageFlagBits::eTopOfPipe);
}

// Function definitions ------------------------------------------------------------------
VulkanImage::VulkanImage(
    const vk::Format format, const vk::Extent3D& extent, vk::ImageUsageFlags usage)
{
    this->format = format;
    this->width = extent.width;
    this->height = extent.height;
    this->channel_num = 4;
    auto physical_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageUsageFlags attachment_useage =
        format == vk::Format::eD32Sfloat ? vk::ImageUsageFlagBits::eDepthStencilAttachment
                                         : vk::ImageUsageFlagBits::eColorAttachment;

    auto image_pair = memory->CreateImage(
        width,
        height,
        format,
        vk::ImageTiling::eOptimal,
        attachment_useage | usage | vk::ImageUsageFlagBits::eTransferDst |
            vk::ImageUsageFlagBits::eTransferSrc);

    layout = vk::ImageLayout::eUndefined;

    image = move(image_pair.first);
    image_view = move(image_pair.second);

    if(attachment_useage == vk::ImageUsageFlagBits::eDepthStencilAttachment)
    {
        TransitionImageLayoutToDepthAttachment(
            *this, vk::PipelineStageFlagBits::eTopOfPipe);
    }
}

VulkanImage::VulkanImage(uint width, uint height, uint channel_num)
{
    vector<byte> buffer(width * height * channel_num, byte(0));

    this->width = width;
    this->height = height;
    this->channel_num = channel_num;
    auto physical_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    auto size = width * height * channel_num;

    vk::DeviceSize image_size = size;
    vk::Buffer staging_buffer = memory->CreateBuffer(
        image_size,
        vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible |
            vk::MemoryPropertyFlagBits::eHostCoherent);
    _SetName(device, staging_buffer, "image_staging_buffer");

    VmaAllocationInfo staging_allocation = memory->GetMemoryAllocation(staging_buffer);

    void* data;
    vk::Result result = device.mapMemory(
        staging_allocation.deviceMemory,
        staging_allocation.offset,
        image_size,
        vk::MemoryMapFlags(0),
        &data);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Image memory creation failed.");

    memcpy(data, buffer.data(), buffer.size() * sizeof(buffer[0]));
    device.unmapMemory(staging_allocation.deviceMemory);

    // TODO(low): should this always be this? It didn't work with the other enumerators
    format = vk::Format::eR8G8B8A8Unorm;
    auto image_pair = memory->CreateImage(
        width,
        height,
        format,
        vk::ImageTiling::eOptimal,
        vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled |
            vk::ImageUsageFlagBits::eStorage);

    image = move(image_pair.first);
    image_view = move(image_pair.second);

    TransitionImageLayoutToTransferDst(*this, vk::PipelineStageFlagBits::eTransfer);

    memory->CopyBufferToImage(staging_buffer, image, (uint32_t)width, (uint32_t)height);

    TransitionImageLayoutToGeneral(*this, vk::PipelineStageFlagBits::eComputeShader);
}

VulkanImage::VulkanImage(
    const vector<byte>& buffer, uint width, uint height, uint channel_num)
{
    assert(buffer.size() >= width * height * channel_num);
    LoadBuffer(buffer.data(), width, height, channel_num);
};

void VulkanImage::LoadBuffer(const void* buffer, int width, int height, int channel_num)
{
    this->width = width;
    this->height = height;
    this->channel_num = channel_num;
    auto physical_device = h_interface->GetPhysicalDevice();
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    auto size = width * height * channel_num;

    vk::DeviceSize image_size = size;
    vk::Buffer staging_buffer = memory->CreateBuffer(
        image_size,
        vk::BufferUsageFlagBits::eTransferSrc,
        vk::MemoryPropertyFlagBits::eHostVisible |
            vk::MemoryPropertyFlagBits::eHostCoherent);
    _SetName(device, staging_buffer, "image_staging_buffer");

    VmaAllocationInfo staging_allocation = memory->GetMemoryAllocation(staging_buffer);

    void* data;
    vk::Result result = device.mapMemory(
        staging_allocation.deviceMemory,
        staging_allocation.offset,
        image_size,
        vk::MemoryMapFlags(0),
        &data);
    if(result != vk::Result::eSuccess)
        Log::RecordLogError("Image memory creation failed.");

    memcpy(data, buffer, image_size);
    device.unmapMemory(staging_allocation.deviceMemory);

    array<vk::Format, 4> formats = {
        vk::Format::eR8Unorm,
        vk::Format::eR8G8B8A8Unorm,
        vk::Format::eR8G8B8A8Unorm,
        vk::Format::eR8G8B8A8Unorm};
    format = formats[channel_num - 1];
    auto image_pair = memory->CreateImage(
        width,
        height,
        format,
        vk::ImageTiling::eOptimal,
        vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled |
            vk::ImageUsageFlagBits::eStorage);

    image = move(image_pair.first);
    image_view = move(image_pair.second);

    TransitionImageLayoutToTransferDst(*this, vk::PipelineStageFlagBits::eTransfer);

    memory->CopyBufferToImage(staging_buffer, image, (uint32_t)width, (uint32_t)height);

    TransitionImageLayoutToShaderReadOnly(
        *this, vk::PipelineStageFlagBits::eFragmentShader);
}

// Globals -------------------------------------------------------------------------------
void VulkanImage::TransitionImageLayoutToTransferSrc(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = image.layout;
    barrier.newLayout = vk::ImageLayout::eTransferSrcOptimal;
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferRead;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange = vk::ImageSubresourceRange(image.aspect, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits::eTransfer;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
}

void VulkanImage::TransitionImageLayoutToTransferDst(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = vk::ImageLayout::eUndefined;
    barrier.newLayout = vk::ImageLayout::eTransferDstOptimal;
    barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange = vk::ImageSubresourceRange(image.aspect, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits::eTransfer;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
}

void VulkanImage::TransitionImageLayoutToShaderReadOnly(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = vk::ImageLayout::eUndefined;
    barrier.newLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
    barrier.dstAccessMask = vk::AccessFlagBits::eShaderWrite;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange =
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits::eFragmentShader;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
}

void VulkanImage::TransitionImageLayoutToGeneral(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = vk::ImageLayout::eUndefined;
    barrier.newLayout = vk::ImageLayout::eGeneral;
    barrier.dstAccessMask =
        vk::AccessFlagBits::eShaderWrite | vk::AccessFlagBits::eShaderRead;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange =
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage = vk::PipelineStageFlagBits::eComputeShader;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
}

void VulkanImage::TransitionImageLayoutToColorAttachment(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = vk::ImageLayout::eUndefined;
    barrier.newLayout = vk::ImageLayout::eColorAttachmentOptimal;
    barrier.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange =
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eColor, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage =
        vk::PipelineStageFlagBits::eColorAttachmentOutput;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
}

void VulkanImage::TransitionImageLayoutToDepthAttachment(
    VulkanImage& image, vk::PipelineStageFlags source_stage)
{
    auto device = h_interface->GetDevice();
    auto pool = h_interface->GetCommandPool();
    auto queue = h_interface->GetQueue();

    vk::ImageMemoryBarrier barrier = {};
    // TODO(undecided): maybe this isn't always true
    barrier.oldLayout = vk::ImageLayout::eUndefined;
    barrier.newLayout = vk::ImageLayout::eDepthStencilAttachmentOptimal;
    barrier.dstAccessMask = vk::AccessFlagBits::eDepthStencilAttachmentWrite |
                            vk::AccessFlagBits::eDepthStencilAttachmentRead;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image.GetImage();
    barrier.subresourceRange =
        vk::ImageSubresourceRange(vk::ImageAspectFlagBits::eDepth, 0, 1, 0, 1);

    vk::PipelineStageFlags destination_stage =
        vk::PipelineStageFlagBits::eEarlyFragmentTests;

    vk::CommandBuffer command_buffer = h_interface->BeginSingleTimeCommands();
    command_buffer.pipelineBarrier(
        source_stage, destination_stage, {}, 0, nullptr, 0, nullptr, 1, &barrier);
    h_interface->EndSingleTimeCommands(command_buffer);

    image.layout = barrier.newLayout;
    image.aspect = vk::ImageAspectFlagBits::eDepth;
}
