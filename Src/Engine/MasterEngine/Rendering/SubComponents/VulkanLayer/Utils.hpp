//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Utils.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <map>
#include <string>

#include "Eigen/Dense"
#include "vulkan/vulkan.hpp"
/** @endcond */

/**
 * @brief Cast a vector of unique handles into a vector of the underlying type. e.g.
 * vk::UniqueBuffer to vk::Buffer.
 *
 * @tparam U Unique Handle type
 * @param unique_handles Array of handles to cast from.
 * @return auto Array of underlying handles.
 */
template<typename U> auto CastVkArray(std::vector<U>& unique_handles)
{
    std::vector<typename U::element_type> handles;
    for(auto& u_handle: unique_handles)
        handles.push_back(*u_handle);
    return handles;
}

inline std::string GetVKVersion(uint32_t mask)
{
    std::string version = std::to_string(VK_VERSION_MAJOR(mask));
    version += "." + std::to_string(VK_VERSION_MINOR(mask));
    version += "." + std::to_string(VK_VERSION_PATCH(mask));

    return version;
}

std::string to_string(VkPhysicalDeviceLimits limits);
std::string to_string(VkPhysicalDeviceFeatures features);
std::string to_string(vk::SurfaceCapabilitiesKHR& details);

inline std::string to_string(VkQueueFamilyProperties& q_family)
{
    return "Queue number: " + std::to_string(q_family.queueCount) +
           "\n"
           "Queue flags: " +
           to_string(vk::QueueFlags(q_family.queueFlags)) + "\n";
}

inline std::string to_string(VkExtensionProperties properties)
{
    return std::string(properties.extensionName) + ", extension version " +
           GetVKVersion(properties.specVersion);
}

