//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Memory.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-11
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <map>

#include "vk_mem_alloc.h"
#include "vulkan/vulkan.hpp"
/** @endcond */

class HardwareInterface;

class VulkanMemory
{
  private:
    std::map<std::uint64_t, VmaAllocation> buffer_memory_allocations;
    std::map<std::uint64_t, VmaAllocation> image_memory_allocations;

    HardwareInterface* h_interface;

    VmaAllocator vma_allocator;

  public:
    VulkanMemory(){};
    VulkanMemory(HardwareInterface* hi);
    void Destroy();

    std::pair<vk::Image, vk::UniqueImageView> CreateImage(
        uint32_t width,
        uint32_t height,
        vk::Format format,
        vk::ImageTiling tiling,
        vk::ImageUsageFlags usage,
        vk::ImageLayout initial_layout = vk::ImageLayout::eUndefined);

    void DestroyImage(vk::Image image);

    vk::Buffer CreateBuffer(
        size_t size, vk::BufferUsageFlags type, vk::MemoryPropertyFlags properties);

    vk::Buffer CreateFilledBuffer(
        const void* object, uint32_t size, vk::BufferUsageFlags type);

    template<typename T>
    vk::Buffer CreateFilledBuffer(const std::vector<T>& array, vk::BufferUsageFlags type)
    {
        return CreateFilledBuffer((void*)array.data(), sizeof(T) * array.size(), type);
    }

    std::vector<vk::Buffer> CreateEmptyBuffers(
        uint32_t size, uint num, vk::BufferUsageFlagBits type);

    VmaAllocationInfo GetMemoryAllocation(vk::Buffer buffer)
    {
        auto handle = (uint64_t)(VkBuffer)buffer;
        VmaAllocationInfo info;
        vmaGetAllocationInfo(vma_allocator, buffer_memory_allocations[handle], &info);
        return info;
    }

    VmaAllocationInfo GetMemoryAllocation(vk::Image image)
    {
        auto handle = (uint64_t)(VkImage)image;
        VmaAllocationInfo info;
        vmaGetAllocationInfo(vma_allocator, image_memory_allocations[handle], &info);
        return info;
    }

    void UpdateBuffer(vk::Buffer& buffer, const void* object_data, uint32_t size);

    void DeleteBuffer(vk::Buffer& buffer);

    void CopyBufferToImage(
        vk::Buffer& buffer, vk::Image& image, uint32_t width, uint32_t height);

    void CopyImageToImage(
        const vk::Image& image1,
        const vk::Image& image2,
        uint32_t width,
        uint32_t height,
        vk::ImageAspectFlags aspect1 = vk::ImageAspectFlagBits::eColor,
        vk::ImageAspectFlags aspect2 = vk::ImageAspectFlagBits::eColor);

    std::pair<vk::UniqueImage, vk::UniqueImageView> CreateDepthImage(
        vk::Extent2D extent, vk::Format format);
};
