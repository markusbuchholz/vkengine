//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file VkExtensionsStubs.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-03-17
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "VkExtensionsStubs.hpp"

// Extension function stubs --------------------------------------------------------------
VkResult (*VkCreateDebugUtilsMessenger)(
    VkInstance, const VkDebugUtilsMessengerCreateInfoEXT*,
    const VkAllocationCallbacks*, VkDebugUtilsMessengerEXT*);
VkResult vkCreateDebugUtilsMessengerEXT(VkInstance instance,
    const VkDebugUtilsMessengerCreateInfoEXT* info,
    const VkAllocationCallbacks* callbacks, VkDebugUtilsMessengerEXT* messenger)
{
    return VkCreateDebugUtilsMessenger(instance, info, callbacks, messenger);
}

void (*vkDestroyDebugUtilsMessenger)(
    VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger,
    const VkAllocationCallbacks* pAllocator);
void vkDestroyDebugUtilsMessengerEXT(VkInstance instance,
    VkDebugUtilsMessengerEXT debugMessenger, const VkAllocationCallbacks* pAllocator)
{
    vkDestroyDebugUtilsMessenger(instance, debugMessenger, pAllocator);
}

VkResult (*vkSetDebugUtilsObjectName)(VkDevice device,
    const VkDebugUtilsObjectNameInfoEXT* pNameInfo);
VkResult vkSetDebugUtilsObjectNameEXT(VkDevice device,
    const VkDebugUtilsObjectNameInfoEXT* pNameInfo)
{
    return vkSetDebugUtilsObjectName(device, pNameInfo);
}

// Loader --------------------------------------------------------------------------------
void LoadExtensionStubs(vk::Instance instance)
{
    VkCreateDebugUtilsMessenger = (PFN_vkCreateDebugUtilsMessengerEXT) instance.getProcAddr(
        "vkCreateDebugUtilsMessengerEXT");
    if (VkCreateDebugUtilsMessenger == nullptr)
        Log::RecordLogError("Failed to find extension function: "
            "vkCreateDebugUtilsMessengerEXT");

    vkDestroyDebugUtilsMessenger =
        (PFN_vkDestroyDebugUtilsMessengerEXT) vkGetInstanceProcAddr(
            instance, "vkDestroyDebugUtilsMessengerEXT");
    if (vkDestroyDebugUtilsMessenger == nullptr)
        Log::RecordLogError("Failed to find extension function: "
            "vkDestroyDebugUtilsMessengerEXT");

    vkSetDebugUtilsObjectName =
        (PFN_vkSetDebugUtilsObjectNameEXT) vkGetInstanceProcAddr(
            instance, "vkSetDebugUtilsObjectNameEXT");
    if (vkDestroyDebugUtilsMessenger == nullptr)
        Log::RecordLogError("Failed to find extension function: "
            "vkSetDebugUtilsObjectNameEXT");
}

