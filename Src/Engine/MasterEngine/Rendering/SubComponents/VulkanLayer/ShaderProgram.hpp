//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file ShaderProgram.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-09
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <map>

#include "shaderc/shaderc.hpp"
/** @endcond */

#include "HardwareInterface.hpp"
#include "Helpers/log.hpp"

struct ShaderConfig
{
    bool depth_test = true;
    float line_width = 3;
    vk::PrimitiveTopology topology = vk::PrimitiveTopology::eTriangleList;
    vk::PolygonMode polygon_mode = vk::PolygonMode::eFill;
};
/**
 * @brief Class encapsulaitng a shader program.
 *
 */
// TODO(low): Add the missing shaders
class ShaderProgram
{
  private:
    // TODO(medium): Consider adding a global cache of shader modules
    static uint program_count;
    // TODO(low): Instead of using a map, use the vulkan info structure directly
    std::unordered_map<vk::ShaderStageFlagBits, vk::UniqueShaderModule> shader_modules;
    std::map<uint, size_t> uniform_binding_size_map;
    // List of all bindings in the current program that use samplers
    std::vector<uint> sampler_binding_map;
    // List of all bindings in the current program that use image storage
    std::vector<uint> image_storage_binding_map;
    std::vector<uint> storage_buffer_binding_map;
    std::string program_name;
    std::vector<vk::DescriptorSetLayoutBinding> layout_bindings;
    std::vector<vk::VertexInputAttributeDescription> attribute_descriptions;
    std::vector<vk::VertexInputBindingDescription> vertex_binding_descriptions;

    ShaderConfig shader_config = {};

    // Number of output attachments this shader program writes to
    uint output_num = 0;
    bool is_compute = false;

    HardwareInterface* h_interface;

  public:
    ShaderProgram(){};
    /**
     * @brief Construct a new Shader Program from a list of paths to the shader files.
     *
     * @param shader_paths Paths to the shader sources.
     * @param name Name of the program for debugging.
     * @param hi Pointer to the hardware interface wrapper.
     */
    ShaderProgram(
        std::vector<std::string> shader_paths,
        const std::string& name = "shader_program_" + std::to_string(program_count),
        HardwareInterface* hi   = HardwareInterface::h_interface);
    /**
     * @brief Get the Shader Modules of this program.
     *
     * @return std::unordered_map<vk::ShaderStageFlagBits, vk::UniqueShaderModule>&
     * Map where the key is the shader type and the value is module.
     */
    std::unordered_map<vk::ShaderStageFlagBits, vk::UniqueShaderModule>&
        GetShaderModules()
    { return shader_modules; }
    /**
     * @brief Get the number of output tassociated shader program.
     *
     * @return uint Number of output parameters.
     */
    uint GetOutputNum() { return output_num; }
    /**
     * @brief Get the Program Name.
     *
     * @return std::string
     */
    std::string GetProgramName() { return program_name; }

    std::vector<vk::DescriptorSetLayoutBinding> GetLayoutBindings()
    {
        return layout_bindings;
    }

    std::vector<vk::VertexInputAttributeDescription> GetAttributeDescriptions()
    {
        return attribute_descriptions;
    }

    const std::vector<vk::VertexInputBindingDescription>& GetVertexBindingDescriptions()
    {
        return vertex_binding_descriptions;
    }

    const std::map<uint, size_t>& GetUniformBindingSizeMap()
    {
        return uniform_binding_size_map;
    }

    const std::vector<uint>& GetSamplerBindingMap() { return sampler_binding_map; }

    const std::vector<uint>& GetImageStorageBindingMap()
    { return image_storage_binding_map; }

    const std::vector<uint>& GetStorageBufferBindingMap()
    { return storage_buffer_binding_map; }

    const ShaderConfig& GetShaderConfig() { return shader_config; }

    bool IsCompute() { return is_compute; }
};
/**
 * @brief Convert between shaderc enumerators and Vulkan enumerators.
 *
 * @param shader_stage Shaderc enum.
 * @return vk::ShaderStageFlagBits Equivalent Vulkan enum.
 */
inline vk::ShaderStageFlagBits ShaderKindToEnum(shaderc_shader_kind shader_stage)
{
    // TODO(low): Add the missing shaders
    std::map<shaderc_shader_kind, vk::ShaderStageFlagBits> shader_map = {
        {shaderc_shader_kind::shaderc_glsl_vertex_shader,
         vk::ShaderStageFlagBits::eVertex},
        {shaderc_shader_kind::shaderc_glsl_geometry_shader,
         vk::ShaderStageFlagBits::eGeometry},
        {shaderc_shader_kind::shaderc_glsl_fragment_shader,
         vk::ShaderStageFlagBits::eFragment},
        {shaderc_shader_kind::shaderc_glsl_compute_shader,
         vk::ShaderStageFlagBits::eCompute}};

    return shader_map[shader_stage];
}

