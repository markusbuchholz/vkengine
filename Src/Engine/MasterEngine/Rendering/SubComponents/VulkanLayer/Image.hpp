//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Image.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-07-14
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

#include "Helpers/log.hpp"
#include "Memory.hpp"
/**
 * @brief Wrapper around vulkan Image handles.
 *
 * Constians the dimensions of the image, the image layout, format,
 * vk::ImageView, vk::Image...
 *
 */
class VulkanImage
{
  private:
    static HardwareInterface* h_interface;
    static VulkanMemory* memory;
    vk::UniqueImageView image_view;
    vk::Image image;

    int width;
    int height;
    int channel_num; //TODO(low): perhaps this field is redundant
    vk::Format format                  = vk::Format::eR8G8B8A8Unorm;
    vk::SamplerAddressMode tiling_mode = vk::SamplerAddressMode::eClampToBorder;
    vk::ImageLayout layout             = vk::ImageLayout::eUndefined;
    vk::ImageAspectFlags aspect        = vk::ImageAspectFlagBits::eColor;

    void LoadBuffer(const void* buffer, int width, int height, int channel_num);

  public:
    /**
     * @brief The unique empty image handle.
     *
     */
    static VulkanImage empty_image;
    /**
     * @brief For efficiency purposes there is a single empty image.
     *
     * @return VulkanImage& Access the unique empty image.
     */
    static VulkanImage& GetEmptyImage() { return empty_image; }
    /**
     * @brief Delete the empty image once we are done using the renderer.
     *
     */
    static void Clean() { empty_image = VulkanImage(); }

    static void CopyImage(VulkanImage& image_src, VulkanImage& image_dst);
    /**
     * @brief Construct a new Vulkan Image object in an uninitialized state.
     *
     * Using objects that were initialized this way will cause undefined behaviour.
     *
     */
    VulkanImage(){};

    VulkanImage(
        const vk::Format format,
        const vk::Extent3D& extent,
        vk::ImageUsageFlags usage = vk::ImageUsageFlagBits::eSampled);

    VulkanImage(uint width, uint height, uint channel_num);

    VulkanImage(
        const std::vector<std::byte>& buffer, uint width, uint height, uint channel_num);

    static void SetHardwareInterface(HardwareInterface* hi)
    {
        if(h_interface != nullptr)
            Log::RecordLogError("Pointer already set");
        h_interface = hi;
    }
    static void SetMemory(VulkanMemory* mem)
    {
        if(memory != nullptr)
            Log::RecordLogError("Pointer already set");
        memory = mem;

        empty_image = VulkanImage({(std::byte)255}, 1, 1, 1);
    }

    vk::Image GetImage() { return image; }
    vk::ImageView GetImageView() const { return *image_view; }
    vk::SamplerAddressMode GetTilingMode() { return tiling_mode; }
    vk::Format GetFormat() { return format; }
    uint GetWidth() { return width; }
    uint GetHeight() { return height; }
    uint GetChannelNum() { return channel_num; }
    vk::ImageLayout GetImageLayout() { return layout; }
    vk::ImageAspectFlags GetAspect() {return aspect; }

    void SetTilingMode(vk::SamplerAddressMode mode) { tiling_mode = mode; }
    void SetImageLayout(vk::ImageLayout new_layout) { layout = new_layout; }

    static void TransitionImageLayoutToTransferSrc(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    static void TransitionImageLayoutToTransferDst(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    static void TransitionImageLayoutToShaderReadOnly(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    static void TransitionImageLayoutToColorAttachment(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    static void TransitionImageLayoutToDepthAttachment(
        VulkanImage& image, vk::PipelineStageFlags source_stage);

    static void TransitionImageLayoutToGeneral(
        VulkanImage& image, vk::PipelineStageFlags source_stage);
};

