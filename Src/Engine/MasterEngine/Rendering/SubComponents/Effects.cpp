//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Effects.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-10-25
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "Effects.hpp"

/** @cond */
#include <filesystem>
#include <regex>
/** @endcond */

#include "Helpers/TextManipulation.hpp"

namespace fs = std::filesystem;
using namespace std;

// Local ---------------------------------------------------------------------------------
namespace
{
void CheckShaderGroups(const map<string, vector<string>>& file_groups)
{
    for(auto const& [group_name, group_files]: file_groups)
    {
        bool has_vert = false;
        bool has_frag = false;
        bool has_comp = false;
        bool has_non_comp = false;
        for(const auto& name: group_files)
        {
            auto tokens = TM::Split(name, '.');
            assert(tokens.size() > 1);
            has_vert = has_vert || tokens[1] == "vert";
            has_frag = has_frag || tokens[1] == "frag";
            has_comp = has_comp || tokens[1] == "comp";
            has_non_comp = has_non_comp || tokens[1] != "comp";
        }

        bool is_valid_graphics_shader = has_vert && has_frag && !has_comp;
        bool is_valid_comp_shader = has_comp && !has_non_comp;
        if(!(is_valid_comp_shader || is_valid_graphics_shader))
            Log::RecordLogError(
                "Invalid shader combination detected in file group:\n" + group_name);
    }
}
}  // namespace

// Static Methods ------------------------------------------------------------------------
void EffectFramework::Draw(
    RenderingComputePipeline& pipeline,
    const GeometryInfo& geom_info,
    const std::vector<std::pair<uint, std::vector<std::byte>>>& uniform_data,
    const std::vector<std::pair<VulkanImage*, uint>>& textures,
    HardwareInterface* h_interface)
{
    pipeline.UpdateUniformBuffers(uniform_data);

    // Setup the geometry then draw everything
    pipeline.PrepareGraphicsPipeline(textures);
    h_interface->DrawIndexed(
        geom_info.vertex_buffer,
        geom_info.index_buffer,
        geom_info.index_num,
        geom_info.instance_count,
        geom_info.instance_buffer);
}

RenderTarget& EffectFramework::PresentRender(
    Gallery& asset_map,
    EffectFramework& frame_work,
    VulkanImage& image,
    vk::Result& result)
{
    GuiTransform transform = {};
    transform.offset = Eigen::Vector4f(0, 0, 0, 0);

    GuiVisualProperties ubo = {};
    ubo.is_transparent = true;
    auto ubo_uniform = SerializeUniform(ubo, 2);
    auto transform_uniform = SerializeUniform(transform, 0);

    auto& display = frame_work.render_target_storage.GetDisplayTarget();
    result = display.StartPass();

    if(result == vk::Result::eErrorOutOfDateKHR) return display;

    Draw(
        frame_work.GetPipeline("shaders/shader_transparent_layer"),
        asset_map.GetGpuGeometryBuffer("UnitQuad"),
        {ubo_uniform, transform_uniform},
        {{&image, 1}},
        frame_work.h_interface);

    display.EndPass();

    frame_work.render_target_storage.ResetFrame();

    return display;
}

RenderTarget& EffectFramework::CellShadingRender(
    Gallery& asset_map,
    EffectFramework& frame_work,
    const GeometryInfo& model_drawable,
    CellShadingInfo& ubo,
    VulkanImage& image)
{
    auto& pipeline = frame_work.GetPipeline("shaders/cell_shading/cell_shading");
    uint cell_output_num = pipeline.GetProgram().GetOutputNum();
    auto& target = frame_work.render_target_storage.GetTargetByOutputNum(cell_output_num);
    VulkanImage* original_depth = target.GetDepthImage();

    auto uniform_data = SerializeUniform(ubo, 0);
    target.StartPass(cell_output_num, true);
    Draw(pipeline, model_drawable, {uniform_data}, {{&image, 1}}, frame_work.h_interface);
    target.EndPass();

    AtomicEffect(
        frame_work,
        model_drawable,
        "shaders/cell_shading/wire_frame",
        target,
        {},
        ubo,
        0);

    auto albedo = target.GetColorImage(0);
    auto normal = target.GetColorImage(1);
    auto mask = target.GetColorImage(2);
    auto depth = target.GetColorImage(3);
    auto dot_view = target.GetColorImage(4);

    auto quad = asset_map.GetGpuGeometryBuffer("UnitQuad");

    VulkanImage* dilated_mask =
        AtomicEffect(frame_work, quad, "shaders/cell_shading/dilation", {{mask, 0}})
            .GetColorImage();

    VulkanImage* blurred_normal1 =
        BlurRender(asset_map, frame_work, *normal, 2).GetColorImage();
    VulkanImage* blurred_normal2 =
        BlurRender(asset_map, frame_work, *normal, 1).GetColorImage();

    VulkanImage* diff = AtomicEffect(
                            frame_work,
                            quad,
                            "shaders/cell_shading/gaussian_difference",
                            {{blurred_normal1, 0}, {blurred_normal2, 1}})
                            .GetColorImage();

    VulkanImage* sobel =
        AtomicEffect(frame_work, quad, "shaders/cell_shading/sobel", {{depth, 0}})
            .GetColorImage();

    auto& composed_target = AtomicEffect(
        frame_work,
        quad,
        "shaders/cell_shading/mask_composition",
        {{albedo, 0}, {dilated_mask, 1}, {dot_view, 2}, {sobel, 3}, {diff, 4}});

    composed_target.SetDepthImage(*original_depth);
    auto& debug_pipeline = frame_work.GetPipeline("shaders/debugging/wireframe_debug");

    return composed_target;
}

RenderTarget& EffectFramework::BlurRender(
    Gallery& asset_map, EffectFramework& frame_work, VulkanImage& image, int radius)
{
    GaussianProperties properties = {};
    properties.radius = radius;
    properties.vertical = false;
    auto uniform_data = SerializeUniform(properties, 0);

    auto& pipeline = frame_work.GetPipeline("shaders/cell_shading/gaussian");
    uint output_num = pipeline.GetProgram().GetOutputNum();
    auto& target = frame_work.render_target_storage.GetTargetByOutputNum(output_num);

    target.StartPass();
    Draw(
        pipeline,
        asset_map.GetGpuGeometryBuffer("UnitQuad"),
        {uniform_data},
        {{&image, 1}},
        frame_work.h_interface);
    target.EndPass();

    auto h_blur = target.GetColorImage();

    auto& target2 = frame_work.render_target_storage.GetTargetByOutputNum(output_num);

    properties.vertical = true;
    uniform_data = SerializeUniform(properties, 0);
    target2.StartPass();
    Draw(
        pipeline,
        asset_map.GetGpuGeometryBuffer("UnitQuad"),
        {uniform_data},
        {{h_blur, 1}},
        frame_work.h_interface);
    target2.EndPass();

    return target2;
}

// Members -------------------------------------------------------------------------------

EffectFramework::EffectFramework(
    VulkanMemory* memory,
    vk::Extent2D extent,
    const std::string& shader_dir,
    HardwareInterface* hi)
    : h_interface(hi)
    , memory(memory)
{
    render_target_storage = std::move(RenderTargetStorage(memory));

    if(!fs::exists(shader_dir))
        Log::RecordLogError("Directory does not exist: " + shader_dir);

    std::regex dir_pattern(shader_dir);
    map<string, vector<string>> file_groups;
    uint vert_count = 0;
    for(auto& path: fs::recursive_directory_iterator(shader_dir))
    {
        // Skip directories in the enumeration
        if(fs::is_directory(path)) continue;
        string path_str = path.path().string();
        vector<string> path_extension = TM::Split(path_str, '.');
        vert_count += uint(path_extension[1] == "vert");
        vert_count += uint(path_extension[1] == "comp");

        file_groups[path_extension[0]].push_back(path_str);
    }
    pipelines.reserve(vert_count);

    CheckShaderGroups(file_groups);

    for(auto& [name, group]: file_groups)
    {
        ShaderProgram program(group, name);
        name_effect_map[name] = pipelines.size();
        auto target = &render_target_storage.GetTargetByOutputNum(program.GetOutputNum());
        pipelines.push_back(move(RenderingComputePipeline(memory, target, program)));
    }
}
