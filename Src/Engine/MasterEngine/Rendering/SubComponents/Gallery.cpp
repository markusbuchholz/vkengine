//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Gallery.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Gallery.hpp"

/** @cond */
#include <cassert>
/** @endcond */

using namespace std;

// Gallery functions --------------------------------------------------------------
void Gallery::UpdateGeometryData(const std::string& name)
{
    UpdateGeometryData(geometry_name_id_map.at(name));
}

void Gallery::UpdateGeometryData(uint id)
{
    GeometryMetaData& meta_data = cpu_geometry_data[id];
    // Call the stored function on the container to get the vertex information.
    auto[vertices, indices] = meta_data.get_data(meta_data.geometry_container);

    GeometryInfo& geometry_info = meta_data.geometry_buffer_info;
    // If this evaluates to true then the number of vertices didn't change
    // only the values.
    if(meta_data.geometry_buffer_info.index_num == indices.size())
    {
        memory->UpdateBuffer(geometry_info.vertex_buffer,
            vertices.data(), vertices.size() * sizeof(float));
        memory->UpdateBuffer(geometry_info.index_buffer,
            indices.data(), indices.size() * sizeof(uint));
    }
    else
    {
        // If the number of vertices in the buffer is different, we have to delete the
        // prior buffer before making a new one.
        memory->DeleteBuffer(geometry_info.vertex_buffer);
        memory->DeleteBuffer(geometry_info.index_buffer);
        geometry_info.vertex_buffer = memory->CreateFilledBuffer(
            vertices, vk::BufferUsageFlagBits::eVertexBuffer);
        geometry_info.index_buffer = memory->CreateFilledBuffer(
            indices, vk::BufferUsageFlagBits::eIndexBuffer);
        geometry_info.index_num = indices.size();
    }
}

