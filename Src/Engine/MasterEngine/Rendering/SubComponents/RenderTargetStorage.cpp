//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file RenderTargetStorage.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-02-17
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "VulkanLayer/RenderTarget.hpp"

/** @cond */
#include "RenderTargetStorage.hpp"
/** @endcond */

#include "VulkanLayer/RenderingPipeline.hpp"

using namespace std;

// Member functions ----------------------------------------------------------------------

RenderTargetStorage::RenderTargetStorage(VulkanMemory* memory)
    : memory(memory)
{
    targets.push_back(make_unique<DisplayTarget>(memory, true));
    target_output_map[0].push_back(targets.back().get());
}

RenderTarget& RenderTargetStorage::GetTargetByOutputNum(uint output_num)
{
    vector<RenderTarget*>& targets_by_output = target_output_map[output_num];
    for(auto target: targets_by_output)
    {
        if(!use_map[target])
        {
            use_map[target] = true;
            return *target;
        }
    }
    vk::Extent2D extent = targets[0]->GetExtent();
    targets.push_back(make_unique<OffScreenTarget>(memory, extent, output_num));
    RenderTarget* target = targets.back().get();
    targets_by_output.push_back(target);
    use_map[target] = true;

    return *targets.back();
}

void RenderTargetStorage::ResetFrame()
{
    for(auto& pair: use_map)
        pair.second = false;
}
