//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Gallery.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <any>
#include <tuple>
#include <vector>
/** @endcond */

#include "VulkanLayer/Image.hpp"

typedef std::pair<std::vector<float>, std::vector<uint>>
    (*GetDataFromGeometryContainer)(std::any&);
// Tuple contains width, height and channel num, in that order
typedef std::tuple<std::vector<std::byte>, uint, uint, uint>
    (*GetDataFromImageContainer)(std::any&);

struct GeometryInfo
{
    vk::Buffer vertex_buffer;
    vk::Buffer index_buffer;
    vk::Buffer instance_buffer; // Optional
    uint index_num;             // Number of indices in the index buffer
    uint instance_count = 0;    // Optional, 0 means not instanced
};

struct GeometryMetaData
{
    // Data structure containing the geometry information
    std::any geometry_container;
    // Function to get a raw float array from the data
    GetDataFromGeometryContainer get_data;
    // GPU (Vulkan) geometry information
    GeometryInfo geometry_buffer_info;
};

struct ImageMetaData
{
    // Data structure containing the image information
    std::any image_container;
    // Function to get a raw float array from the data
    GetDataFromImageContainer get_data;
    // GPU (Vulkan) geometry information
    VulkanImage image_buffer_info;
};

class Gallery
{
private:
    std::vector<GeometryMetaData> cpu_geometry_data;
    std::map<std::string, uint> geometry_name_id_map;

    std::vector<ImageMetaData> cpu_image_data;
    std::map<std::string, uint> image_name_id_map;

    VulkanMemory* memory;
public:
    Gallery() = default;
    Gallery(VulkanMemory* memory) : memory(memory) {}

    template <typename DATA_CONTAINER>
    uint StoreGeometryData(DATA_CONTAINER& data_container,
        const std::string& name="",
        GetDataFromGeometryContainer get_data=DATA_CONTAINER::GetGeometryData);

    void UpdateGeometryData(uint id);

    void UpdateGeometryData(const std::string& name);

    template <typename T>
    auto& GetCpuGeometry(uint id)
    {
        std::any& geometry_container = cpu_geometry_data[id].geometry_container;
        return std::any_cast<T&>(geometry_container);
    }

    template <typename T>
    auto& GetCpuGeometry(const std::string& name)
    {
        assert(geometry_name_id_map.count(name) > 0);
        return GetCpuGeometry<T>(geometry_name_id_map.at(name));
    }

    GeometryInfo GetGpuGeometryBuffer(uint id)
    { return cpu_geometry_data.at(id).geometry_buffer_info; }

    GeometryInfo GetGpuGeometryBuffer(const std::string& name)
    { return GetGpuGeometryBuffer(geometry_name_id_map.at(name)); }

    uint GetGeometryId(const std::string& name) { return geometry_name_id_map.at(name); }

    template<typename INSTANCE_INPUT>
    void SetGeometryInstanceData(const std::string& name,
        const std::vector<INSTANCE_INPUT> &data);
    template<typename INSTANCE_INPUT>
    void SetGeometryInstanceData(uint64_t id, const std::vector<INSTANCE_INPUT> &data);
    template<typename INSTANCE_INPUT>
    void SetGeometryInstanceData(uint64_t id,
        const std::vector<INSTANCE_INPUT> &data, uint instance_count);

    template <typename DATA_CONTAINER>
    uint StoreImageData(DATA_CONTAINER& data_container,
        const std::string& name="",
        GetDataFromImageContainer get_data=DATA_CONTAINER::GetImageData);

    VulkanImage* GetGpuImage(uint id) { return &cpu_image_data[id].image_buffer_info; }

    VulkanImage* GetGpuImage(const std::string& name)
    { return GetGpuImage(image_name_id_map[name]); }
};

// Template Implementations of Gallery --------------------------------------------
template <typename DATA_CONTAINER>
uint Gallery::StoreGeometryData(DATA_CONTAINER& data_container,
    const std::string& name,
    GetDataFromGeometryContainer get_data)
{
    std::any geometry_container = std::make_any<DATA_CONTAINER>(data_container);
    auto[vertices, indices] = get_data(geometry_container);

    GeometryInfo geometry_info = {};
    // If the object already exists, replace it
    if(geometry_name_id_map.count(name) != 0)
    {
        uint id = geometry_name_id_map[name];
        cpu_geometry_data[id].geometry_container = geometry_container;
        cpu_geometry_data[id].get_data = get_data;

        UpdateGeometryData(id);
        return id;
    }
    // Otherwise make an allocation.
    else
    {
        geometry_info.vertex_buffer =
            memory->CreateFilledBuffer(vertices, vk::BufferUsageFlagBits::eVertexBuffer);
        geometry_info.index_buffer =
            memory->CreateFilledBuffer(indices, vk::BufferUsageFlagBits::eIndexBuffer);
        geometry_info.index_num = indices.size();
    }

    uint id = cpu_geometry_data.size();
    geometry_name_id_map[name] = id;
    cpu_geometry_data.push_back({geometry_container, get_data, geometry_info});

    return id;
}

template<typename INSTANCE_INPUT>
void Gallery::SetGeometryInstanceData(const std::string& name,
    const std::vector<INSTANCE_INPUT> &data)
{
    SetGeometryInstanceData(geometry_name_id_map[name], data, data.size());
}

template<typename INSTANCE_INPUT>
void Gallery::SetGeometryInstanceData(uint64_t id,
    const std::vector<INSTANCE_INPUT> &data)
{
    SetGeometryInstanceData(id, data, data.size());
}

template<typename INSTANCE_INPUT>
void Gallery::SetGeometryInstanceData(uint64_t id,
    const std::vector<INSTANCE_INPUT> &data, uint instance_count)
{
    GeometryInfo& geom = cpu_geometry_data[id].geometry_buffer_info;
    geom.instance_buffer =
        memory->CreateFilledBuffer(data, vk::BufferUsageFlagBits::eVertexBuffer);
    geom.instance_count = instance_count;
}

template <typename DATA_CONTAINER>
uint Gallery::StoreImageData(DATA_CONTAINER& data_container,
    const std::string& name,
    GetDataFromImageContainer get_data)
{
    std::any image_container = std::make_any<DATA_CONTAINER>(data_container);
    auto[buffer, width, height, channel_num] = get_data(image_container);

    auto gpu_image = VulkanImage(buffer, width, height, channel_num);

    uint id = cpu_image_data.size();
    cpu_image_data.push_back({image_container, get_data, std::move(gpu_image)});

    image_name_id_map[name] = id;

    return id;
}

