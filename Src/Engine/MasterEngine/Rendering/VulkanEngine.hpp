//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file VulkanEngine.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <any>
#include <array>
#include <chrono>
#include <fstream>
#include <iostream>
#include <map>
#include <set>
#include <string.h>
#include <string>
#include <vector>
/** @endcond */

#include "MasterEngine/Camera/Camera.hpp"
#include "SubComponents/Effects.hpp"

// Vulkan Engine -------------------------------------------------------------------------
class VulkanEngine
{
  private:
    std::shared_ptr<Window> window;

    HardwareInterface h_interface;
    VulkanMemory memory;

    EffectFramework effect_framework;
    Gallery gallery;

  public:
    template<typename VEC>
    static std::pair<std::vector<float>, std::vector<uint>> GetDataFromVec(
        std::any& container);

    VulkanEngine();

    ~VulkanEngine();

    std::shared_ptr<Window>& GetWindow() { return window; }

    Gallery& GetGallery() { return gallery; }

    EffectFramework& GetEffectFramework() { return effect_framework; }

    VulkanMemory* GetMemory() { return &memory; }

    RenderTarget& PresentRender(VulkanImage& image)
    {
        vk::Result result;
        RenderTarget& target =
            EffectFramework::PresentRender(gallery, effect_framework, image, result);

        if(result == vk::Result::eErrorOutOfDateKHR)
        {
            window->UpdateDimensions();
            uint width, height;
            window->GetDimensions(&width, &height);
            effect_framework =
                std::move(EffectFramework(&memory, {width, height}, "shaders"));
        }

        return target;
    }

    RenderTarget& CellShadingRender(
        const GeometryInfo& model_drawable, CellShadingInfo& ubo, VulkanImage& image)
    {
        return EffectFramework::CellShadingRender(
            gallery, effect_framework, model_drawable, ubo, image);
    }

    RenderTarget& BlurRender(VulkanImage& image, int radius)
    {
        return EffectFramework::BlurRender(gallery, effect_framework, image, radius);
    }

    template<class... Types>
    RenderTarget& AtomicEffect(
        const GeometryInfo& model_drawable,
        const std::string& shader_path,
        const std::vector<std::pair<VulkanImage*, uint>> textures,
        const Types&... args)
    {
        return EffectFramework::AtomicEffect(
            effect_framework, model_drawable, shader_path, textures, args...);
    }

    template<class... Types>
    RenderTarget& AtomicEffect(
        const GeometryInfo& model_drawable,
        const std::string& shader_path,
        RenderTarget& target,
        const std::vector<std::pair<VulkanImage*, uint>> textures,
        const Types&... args)
    {
        return EffectFramework::AtomicEffect(
            effect_framework, model_drawable, shader_path, target, textures, args...);
    }
};

template<typename VEC>
std::pair<std::vector<float>, std::vector<uint>> VulkanEngine::GetDataFromVec(
    std::any& container)
{
    auto& pair =
        std::any_cast<std::pair<std::vector<VEC>, std::vector<uint>>&>(container);
    auto& vertices = pair.first;
    auto& indices = pair.second;
    std::vector<float> verts(vertices.size() * sizeof(VEC) / sizeof(float));
    memcpy(verts.data(), vertices.data(), vertices.size() * sizeof(VEC));

    return {verts, indices};
}
