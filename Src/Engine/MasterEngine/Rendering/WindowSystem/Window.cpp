//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Window.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-05-10
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "Window.hpp"

#include "GLFW_tools.hpp"

using namespace std;

Window::Window(uint width, uint height)
    : width(width)
    , height(height)
{
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    window = glfwCreateWindow(width, height, "Vulkan window", nullptr, nullptr);
    if(window == nullptr) Log::RecordLogError("Error, window not created");
}

Window::~Window() { glfwDestroyWindow(window); }

void Window::UpdateDimensions()
{
    int w, h;
    glfwGetFramebufferSize(window, &w, &h);
    width = w;
    height = h;
}
