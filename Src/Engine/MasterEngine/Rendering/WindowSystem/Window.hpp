//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Window.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-05-10
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

class GLFWwindow;

class Window
{
  public:
    Window(){};
    Window(unsigned int width, unsigned int height);
    ~Window();

    GLFWwindow* GetGLFWWindow() { return window; }
    void GetDimensions(unsigned int* w, unsigned int* h)
    {
        *w = width;
        *h = height;
    };

    void UpdateDimensions();

  protected:
    unsigned int width = 800;
    unsigned int height = 800;
    GLFWwindow* window;
};
