//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file GLFW_tools.hpp
 * @author Camilo Talero
 * @brief Header declarations for GLFW wrappers, error handling and similar help tools
 * @version 0.1
 * @date 2019-02-03
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

#define GLFW_INCLUDE_VULKAN

/** @cond */
#include <GLFW/glfw3.h>
/** @endcond */

#include "Helpers/log.hpp"

/**
 * @brief Initialize GLFW and record progress to the log
 *
 * @return int GLFW_TRUE upon success, otherwise GLFW_FALSE
*/
int SetupGLFW();
/**
 * @brief Free resources occupied by GLFW
 *
*/
void CleanGLFW();
/**
 * @brief Convert a GLFW error enumerator to a string
 *
 * @param error
 * @return std::string
*/
std::string GLFW_ErrorToString(int error);

