//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file GLFW_tools.cpp
 * @author Camilo Talero
 * @brief Implementations of GLFW helping tools (error handling, initialization, etc...)
 * @version 0.1
 * @date 2019-02-03
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

// clang-format off
#define ENUM_TO_STRING_CASE(ENUM) \
    case ENUM: return #ENUM;
#include "GLFW_tools.hpp"
// clang-format on

// Macro to simplify converting an enumerator to a string

using namespace std;

// Convert GLFW error codes to a string
string GLFW_ErrorToString(int error)
{
    switch(error)
    {
        ENUM_TO_STRING_CASE(GLFW_NOT_INITIALIZED);
        ENUM_TO_STRING_CASE(GLFW_NO_CURRENT_CONTEXT);
        ENUM_TO_STRING_CASE(GLFW_INVALID_ENUM);
        ENUM_TO_STRING_CASE(GLFW_INVALID_VALUE);
        ENUM_TO_STRING_CASE(GLFW_OUT_OF_MEMORY);
        ENUM_TO_STRING_CASE(GLFW_API_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_VERSION_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_PLATFORM_ERROR);
        ENUM_TO_STRING_CASE(GLFW_FORMAT_UNAVAILABLE);
        ENUM_TO_STRING_CASE(GLFW_NO_WINDOW_CONTEXT);
        default: return "Unrecognized GLFW error code";
    }
}

// Terminate GLFW (cleans memory)
void CleanGLFW()
{
    glfwTerminate();
    Log::RecordLog("\nGLFW terminated");
}

// Initialize the GLFW library and log problems if any
int SetupGLFW()
{
    // Set callback for error reporting
    glfwSetErrorCallback([](int error, const char* description) {
        string err_code = "Error code: " + GLFW_ErrorToString(error);
        string err_msg = "Error message: " + string(description);
        string msg = err_code + "\n" + err_msg;

        Log::RecordLog(Log::ERROR_BAR);
        Log::RecordLog("GLFW error occurred");
        Log::RecordLog(Log::ERROR_BAR);

        Log::RecordLog(msg);
        Log::RecordLog(Log::ERROR_BAR);
    });
    // If GLFW was initialized setup cleanup
    if(glfwInit() == GLFW_TRUE)
    {
        atexit(CleanGLFW);
        return GLFW_TRUE;
    }
    // Otherwise record failure
    else
    {
        Log::RecordLog("GLFW failed to initialize");
        return GLFW_FALSE;
    }
}
