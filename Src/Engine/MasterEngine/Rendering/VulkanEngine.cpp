//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file VulkanEngine.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-03-30
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "VulkanEngine.hpp"

#include "WindowSystem/GLFW_tools.hpp"

using namespace std;

VulkanEngine::VulkanEngine()
{
    SetupGLFW();
    window = make_shared<Window>(800, 800);

    h_interface = move(HardwareInterface(*window));
    HardwareInterface::SetGlobalPointer(&h_interface);
    memory = move(VulkanMemory(&h_interface));

    gallery = Gallery(&memory);

    VulkanImage::SetHardwareInterface(&h_interface);
    VulkanImage::SetMemory(&memory);

    // Add the screen quad for image presentation
    std::vector<Eigen::Vector3f> verts = {{-1, -1, 0}, {1, -1, 0}, {1, 1, 0}, {-1, 1, 0}};
    std::vector<unsigned int> indices = {2, 1, 0, 0, 3, 2};
    std::pair<std::vector<Eigen::Vector3f>, std::vector<uint>> pair = {verts, indices};
    gallery.StoreGeometryData(pair, "UnitQuad", GetDataFromVec<Eigen::Vector3f>);

    uint width, height;
    window->GetDimensions(&width, &height);
    effect_framework = move(EffectFramework(&memory, {width, height}, "shaders"));
}

VulkanEngine::~VulkanEngine()
{
    memory.Destroy();
    VulkanImage::Clean();
}
