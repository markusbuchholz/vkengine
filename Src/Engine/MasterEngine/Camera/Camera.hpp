//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Camera.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-01-08
 *
 * @copyright Copyright (c) 2020
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <math.h>

#include "Eigen/Dense"
/** @endcond */

// Camera --------------------------------------------------------------------------------
/**
 * @brief General representation of a camera state.
 *
 * Used to store and update the view and projection matrices. Also
 * defines useful static methods for camera manipualtion.
 *
 */
class Camera
{
  protected:
    Eigen::Vector3f up = Eigen::Vector3f(0, -1, 0);
    Eigen::Vector3f position = Eigen::Vector3f(0, 0, -5);
    Eigen::Vector3f look_at_point = Eigen::Vector3f(0, 0, 0);
    float screen_width = 512;
    float screen_height = 512;
    float z_near = 0.1;
    float z_far = 100.0;
    float fov = 45.f;

  public:
    Camera(){};
    Camera(float width, float height);

    virtual Eigen::Matrix4f GetViewMatrix();
    virtual Eigen::Matrix4f GetProjectionMatrix();

    Eigen::Vector3f GetPosition() { return position; }

    Eigen::Vector3f GetLookDirection() { return look_at_point - position; }
    void UpdateScreenDimensions(uint w, uint h)
    {
        screen_width = w;
        screen_height = h;
    }
};
/**
 * @brief Specialized camera object, defines a camera that always focuses
 *  a specific point.
 *
 * The up direction of the camera remains unchanged as it rotates and moves.
 * around the world.
 *
 */
class SphericalCamera : public Camera
{
  public:
    static void UpdateCameraAngles(
        void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset);
    static void UpdateCameraZoom(void* ptr, const Eigen::Vector2f& offset);
    static void UpdateCameraPosition(
        void* ptr, const Eigen::Vector2f&, const Eigen::Vector2f& offset);

  protected:
    float theta = 0;
    float phi = 0;
    float radius = 5;

    Eigen::Vector3f default_forward = Eigen::Vector3f(0, 0, -1);

  public:
    SphericalCamera() {}
    SphericalCamera(float width, float height)
        : Camera(width, height)
        , theta(0)
        , phi(0)
        , radius(5)
    {
    }

    float GetTheta() { return theta; }

    float GetPhi() { return phi; }

    float GetRadius() { return radius; }

    void SetTheta(float t) { theta = t; }

    void SetPhi(float t)
    {
        phi = t;
        phi = std::clamp(phi, -89.99f, 89.99f);
    }

    void SetRadius(float t) { radius = t; }
};
// https://www.khronos.org/opengl/wiki/Object_Mouse_Trackball
// Be careful since OGL's and Vulkan's coordinate systems have different handedness
/**
 * @brief Implementation of an arcball camera, which is a camera where the rotation
 * matrix at any time is infered from vector projections into a hemisphere.
 *
 * The theory can be found [here](https://www.khronos.org/opengl/wiki/Object_Mouse_Trackball).
 * Be careful since OGL's and Vulkan's coordinate systems have different handedness. In
 * our case we have to flip the z coordinate with respect to the linked reference.
 * (Above might no longer be true as the current implementation of the cameramatrix
 * might be correcting the issue by using a different handedness).
 *
 */
class ArcballCamera : public Camera
{
  public:
    static void UpdateCameraAngles(
        void* ptr, const Eigen::Vector2f& position, const Eigen::Vector2f& offset);
    static void UpdateCameraZoom(void* ptr, const Eigen::Vector2f& offset);
    static void UpdateCameraPosition(
        void* ptr, const Eigen::Vector2f&, const Eigen::Vector2f& offset);

  protected:
    float radius = 1;

    Eigen::Vector3f forward = Eigen::Vector3f(0, 0, -1);

    Eigen::Quaternionf rotation = Eigen::Quaternionf(1, 0, 0, 0);

  public:
    ArcballCamera() {}
    /**
     * @brief Construct a new Arcball Camera object
     *
     * @param width Width of the screen in number of pixels.
     * @param height Height of the screen in number of pixels.
     * @param radius Distance from the focal point to the camera. Default is 5.
     */
    ArcballCamera(float width, float height, float radius = 5)
        : Camera(width, height)
        , radius(radius)
    {
        position = Eigen::Vector3f(0, 0, -radius);
    }

    virtual Eigen::Matrix4f GetViewMatrix() override;

    void SetRotation(const Eigen::Quaternionf& rotation) { this->rotation = rotation; }

    void SetRadius(const float radius);
};
