//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Camera.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-01-08
 *
 * @copyright Copyright (c) 2020
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "Camera.hpp"

/** @cond */
#include <iostream>
/** @endcond */

#include "Helpers/EigenHelpers.hpp"

using namespace std;

namespace {
Eigen::Vector3f ScreenToArcSurface(Eigen::Vector2f pos)
{
    const float radius = 1.0f; // Controls the speed
    if(pos.x() * pos.x() + pos.y() * pos.y() >= (radius * radius) / 2.f - 0.00001)
    {
        // This is equal to (r^2 / 2) / (sqrt(x^2 + y^2)) since the magnitude of the
        // vector is sqrt(x^2 + y^2)
        return {pos.x(), pos.y(), -(radius * radius / 2.f) / (pos.norm())};
    }

    return {pos.x(), pos.y(), -sqrt(radius * radius - (pos.x() * pos.x() + pos.y() * pos.y()))};
}

} // namespace

// Camera --------------------------------------------------------------------------------
Camera::Camera(float width, float height)
    : screen_width(width)
    , screen_height(height)
{
}

Eigen::Matrix4f Camera::GetViewMatrix()
{
    return LookAt(position, look_at_point, up);
}

Eigen::Matrix4f Camera::GetProjectionMatrix()
{
    return Perspective(Radians(fov), screen_width / screen_height, z_near, z_far);
}

// Spherical Camera ----------------------------------------------------------------------
void SphericalCamera::UpdateCameraAngles(
    void* ptr,
    const Eigen::Vector2f& position,
    const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 150;
    camera->SetTheta(camera->GetTheta() - offset.x() * speed);
    camera->SetPhi(camera->GetPhi() - offset.y() * speed);

    Eigen::Vector3f up   = camera->up;
    const Eigen::Vector3f forward = camera->default_forward;
    Eigen::Vector3f side          = up.cross(forward).normalized();
    float phi            = camera->phi * M_PI / 180.f;
    float theta          = camera->theta * M_PI / 180.f;
    auto theta_rotation  = RotateQ(theta, up);
    auto phi_rotation    = RotateQ(phi, side);

    const Eigen::Vector3f rotated =
        theta_rotation * phi_rotation * Eigen::Vector3f(forward * camera->radius);
    camera->position = camera->look_at_point + rotated;
}

void SphericalCamera::UpdateCameraPosition(
    void* ptr,
    const Eigen::Vector2f& position,
    const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 3.5;

    auto c_pos = camera->position;
    Eigen::Vector3f up      = camera->up;
    Eigen::Vector3f forward = (camera->look_at_point - camera->position).normalized();
    Eigen::Vector3f side    = up.cross(forward).normalized();
    up    = forward.cross(side).normalized();

    Eigen::Vector3f offset_3d = (up * offset.y() + side * offset.x()) * speed;
    camera->position += offset_3d;
    camera->look_at_point += offset_3d;
}

void SphericalCamera::UpdateCameraZoom(void* ptr, const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<SphericalCamera*>(ptr);

    const float speed = 0.5;
    camera->radius -= speed * offset.y();
    camera->radius = std::max(0.0001f, camera->radius);

    const auto rotated =
          camera->look_at_point
        + (camera->position - camera->look_at_point).normalized() * camera->radius;
    camera->position = rotated;
}

// Arcball Camera ------------------------------------------------------------------------
void ArcballCamera::UpdateCameraAngles(
    void* ptr,
    const Eigen::Vector2f& position,
    const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    Eigen::Vector3f vb = ScreenToArcSurface({position.x(), -position.y()}).normalized();
    auto centered_pos = position - offset;
    Eigen::Vector3f va =
        ScreenToArcSurface({centered_pos.x(), -centered_pos.y()}).normalized();

    const float speed = 5000.f;
    float angle = acos(std::min(1.f, vb.dot(va))) * speed;
    angle = std::min(angle, Radians(60.f));
    Eigen::Vector3f axis = vb.cross(va);

    camera->rotation *= Eigen::Quaternionf(Eigen::AngleAxis(angle, axis)).normalized();

    const Eigen::Quaternion rot = camera->rotation;
    camera->position =
          camera->look_at_point
        + rot * (camera->forward * camera->radius - camera->look_at_point);
}

void ArcballCamera::UpdateCameraPosition(
    void* ptr,
    const Eigen::Vector2f& position,
    const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    Eigen::Vector3f up      = (camera->rotation * camera->up).normalized();
    Eigen::Vector3f side    =
        (camera->rotation * (camera->forward.cross(camera->up))).normalized();

    const float speed = 10.0;
    Eigen::Vector3f offset_3d = (up * -offset.y() + side * offset.x()) * speed;
    camera->position += offset_3d;
    camera->look_at_point += offset_3d;
}

void ArcballCamera::UpdateCameraZoom(void* ptr, const Eigen::Vector2f& offset)
{
    auto camera = reinterpret_cast<ArcballCamera*>(ptr);

    const float speed = 0.5;
    camera->radius -= speed * offset.y();
    camera->radius = std::max(0.0001f, camera->radius);

    camera->position =
          camera->look_at_point
        + (camera->position - camera->look_at_point).normalized() * camera->radius;
}

Eigen::Matrix4f ArcballCamera::GetViewMatrix()
{
    return LookAt(position, look_at_point, rotation * up);
}

void ArcballCamera::SetRadius(const float radius)
{
    this->radius = radius;
    const auto dir = (position - look_at_point).normalized();
    position = look_at_point + dir * radius;
}

