//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Guizmo3DPool.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#pragma once

/** @cond */
#include <vector>

#include "Eigen/Dense"
/** @endcond */

class Guizmo3DPool
{
public:
    enum class GuizmoType {
        ROTATION,
        TRANSLATION,
        SCALING
    };
private:
    std::vector<Eigen::Vector3f> positions;
    std::vector<Eigen::Quaternionf> rotations;
    std::vector<float> scales;
    std::vector<GuizmoType> types;
public:
    void AddWidget(
        const Eigen::Vector3f& position,
        const Eigen::Quaternionf& rotation,
        float scale,
        GuizmoType type);

    std::vector<Eigen::Vector3f>& GetPositions() { return positions; }
    std::vector<Eigen::Quaternionf>& GetRotations() {return rotations; }
    std::vector<float>& GetScales() { return scales; }
    std::vector<GuizmoType>& GetTypes() {return types; }
};

