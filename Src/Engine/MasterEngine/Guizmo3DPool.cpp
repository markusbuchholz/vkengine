//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file Guizmo3DPool.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "Guizmo3DPool.hpp"

using namespace std;

// Member functions ----------------------------------------------------------------------

void Guizmo3DPool::AddWidget(
    const Eigen::Vector3f& position,
    const Eigen::Quaternionf& rotation,
    float scale,
    GuizmoType type)
{
    positions.push_back(position);
    rotations.push_back(rotation);
    scales.push_back(scale);
    types.push_back(type);
}

