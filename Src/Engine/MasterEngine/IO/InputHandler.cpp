//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file InputHandler.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-09-29
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#include "InputHandler.hpp"

/** @cond */
#include <iostream>
/** @endcond */

using namespace std;

// TODO(ongoing): Add all the necessary missing states for mouse, gamepad, keyboard...
// Function declarations -----------------------------------------------------------------
namespace {
MouseInputState CalculateMouseMouseInputState(
    bool& left_pressed,
    bool& right_pressed,
    Eigen::Vector2f& position,
    Eigen::Vector2f& offset,
    MouseInputState current_state,
    GLFWwindow* window)
{
    // TODO(low): move these to callbacks to properly use glfw
    int current_left_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT);
    int current_right_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT);
    int scroll_state = glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_MIDDLE);

    int width, height;
    glfwGetWindowSize(window, &width, &height);

    double new_x, new_y;
    glfwGetCursorPos(window, &new_x, &new_y);
    new_x = 2.f * (new_x / width) - 1.f;
    new_y = 2.f * (new_y / height) - 1.f;

    Eigen::Vector2f new_pos(new_x, new_y);
    offset = new_pos - position;
    position = new_pos;

    if(current_left_state == GLFW_PRESS && !left_pressed)
    {
        left_pressed = true;
        return MouseInputState::LEFT_DOWN;
    }
    if(current_left_state == GLFW_PRESS && left_pressed)
    {
        left_pressed = true;
        return MouseInputState::LEFT_DRAG;
    }
    if(current_left_state == GLFW_RELEASE && left_pressed)
    {
        left_pressed = false;
        return MouseInputState::LEFT_UP;
    }

    if(current_right_state == GLFW_PRESS && !right_pressed)
    {
        right_pressed = true;
        return MouseInputState::RIGHT_DOWN;
    }
    if(current_right_state == GLFW_PRESS && right_pressed)
    {
        right_pressed = true;
        return MouseInputState::RIGHT_DRAG;
    }
    if(current_right_state == GLFW_RELEASE && right_pressed)
    {
        right_pressed = false;
        return MouseInputState::RIGHT_UP;
    }
    if(current_state == MouseInputState::MOVE && offset.norm() > 0.0001)
        return current_state;

    return MouseInputState::NO_ACTION;
}

KeyActionState CalculateKeyActionState(KeyActionState prior_action, int glfw_action)
{
    if(glfw_action == GLFW_PRESS)
    {
        return KeyActionState::PRESS;
    }
    else if(GLFW_REPEAT)
    {
        return KeyActionState::HELD;
    }
    else if(glfw_action == GLFW_RELEASE)
    {
        return KeyActionState::RELEASE;
    }

    return KeyActionState::RELEASE;
}

} // namespace

// Input Handler -------------------------------------------------------------------------
InputHandler::InputHandler(GLFWwindow* window) : window(window)
{
    glfwSetScrollCallback(window, ScrollCallback);
    glfwSetKeyCallback(window, KeyCallback);
    glfwSetCursorPosCallback(window, CursorPositionCallback);
}

void InputHandler::AddEvent(const MouseInputState state, MouseInputEvent input_event,
    void* data)
{
    mouse_event_registry[state].push_back(input_event);
    mouse_data_registry[state].push_back(data);
}

void InputHandler::AddEvent(const ScrollInputState state, ScrollEvent input_event,
    void* data)
{
    scroll_event_registry[state].push_back(input_event);
    scroll_data_registry[state].push_back(data);
}

void InputHandler::AddEvent(KeyEvent input_event, void* data)
{
    key_event_registry.push_back(input_event);
    key_data_registry.push_back(data);
}

void InputHandler::CallEvents()
{
    input_state = CalculateMouseMouseInputState(left_button_pressed, right_button_pressed,
        mouse_position, mouse_offset, input_state, window);

    for(int i=0; i<mouse_event_registry[input_state].size(); i++)
    {
        auto event = mouse_event_registry[input_state][i];
        auto data = mouse_data_registry[input_state][i];
        event(data, mouse_position, mouse_offset);
    }

    auto s_state = (ScrollInputState)scroll_state;
    for(int i=0; i<scroll_event_registry[s_state].size(); i++)
    {
        auto event = scroll_event_registry[s_state][i];
        auto data = scroll_data_registry[s_state][i];
        event(data, {scroll_offset.x(), scroll_offset.y()});
    }

    for(int i=0; i<key_event_registry.size(); i++)
    {
        auto event = key_event_registry[i];
        auto data = key_data_registry[i];
        event(data, action_key, key_action_state);
    }

    scroll_state = MouseInputState::NO_ACTION;
    action_key = GLFW_KEY_UNKNOWN;
    key_action_state = KeyActionState::RELEASE;
}

// GLFW callbacks ------------------------------------------------------------------------

void InputHandler::CursorPositionCallback(GLFWwindow* window, double xpos, double ypos)
{
    auto handler = reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));
    handler->input_state = MouseInputState::MOVE;
}

void InputHandler::ScrollCallback(GLFWwindow* window, double x, double y)
{
    auto handler = reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));
    handler->scroll_state = ScrollInputState::SCROLL;
    handler->scroll_offset = Eigen::Vector2f(x,y);
}

void InputHandler::KeyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    auto handler = reinterpret_cast<InputHandler*>(glfwGetWindowUserPointer(window));
    handler->action_key = key;
    handler->key_action_state = CalculateKeyActionState(handler->key_action_state, action);
}

