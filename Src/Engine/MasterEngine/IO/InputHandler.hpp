//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file InputHandler.hpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2019-09-29
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

#pragma once

/** @cond */
#include <GLFW/glfw3.h>
#include <map>
#include <memory>
#include <vector>

#include "Eigen/Dense"
/** @endcond */

enum MouseInputState
{
    NO_ACTION = 0b0,
    LEFT_DOWN = 0b1,
    LEFT_UP = 0b10,
    LEFT_DRAG = 0b100,
    RIGHT_DOWN = 0b1000,
    RIGHT_UP = 0b1000,
    RIGHT_DRAG = 0b10000,
    MOVE = 0b100000,
};

enum ScrollInputState
{
    SCROLL = 0b1,
};

enum KeyInputState
{
    NO_INPUT_KEY = 0,
    SPACE = 32,
};

enum KeyActionState
{
    PRESS,
    HELD,
    RELEASE,
};

class InputHandler
{
    typedef void (*MouseInputEvent)(
        void*, const Eigen::Vector2f& position, const Eigen::Vector2f& offset);
    typedef void (*ScrollEvent)(void*, const Eigen::Vector2f& offset);
    typedef void (*KeyEvent)(void*, int key, KeyActionState action);

  private:
    static void ScrollCallback(GLFWwindow* window, double x, double y);
    static void KeyCallback(
        GLFWwindow* window, int key, int scancode, int action, int mods);
    static void CursorPositionCallback(GLFWwindow* window, double xpos, double ypos);

    GLFWwindow* window;

    std::map<MouseInputState, std::vector<MouseInputEvent>> mouse_event_registry;
    std::map<MouseInputState, std::vector<void*>> mouse_data_registry;

    std::map<ScrollInputState, std::vector<ScrollEvent>> scroll_event_registry;
    std::map<ScrollInputState, std::vector<void*>> scroll_data_registry;

    // The integer should correspond with GLFW's defines
    std::vector<KeyEvent> key_event_registry;
    std::vector<void*> key_data_registry;

    // Input state
    Eigen::Vector2f mouse_position = Eigen::Vector2f(0, 0);
    Eigen::Vector2f mouse_offset = Eigen::Vector2f(0, 0);
    bool left_button_pressed = false;
    bool right_button_pressed = false;

    uint scroll_state = NO_ACTION;
    Eigen::Vector2f scroll_offset = Eigen::Vector2f(0, 0);

    int action_key = GLFW_KEY_UNKNOWN;
    KeyActionState key_action_state = RELEASE;

    MouseInputState input_state = NO_ACTION;

  public:
    InputHandler() {}
    InputHandler(GLFWwindow* window);
    // Call this immediately after initialization, little hack to prevent copying the data
    // from invalidating the pointer
    void RegisterToGLFW() { glfwSetWindowUserPointer(window, this); }

    void AddEvent(const MouseInputState state, MouseInputEvent event, void* data);
    void AddEvent(const ScrollInputState state, ScrollEvent event, void* data);
    void AddEvent(KeyEvent event, void* data);

    void CallEvents();
};
