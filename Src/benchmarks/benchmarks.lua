group "Benchmarks"
    project "benchmark"
        kind "ConsoleApp"
        language "C++"
        targetdir "../../build/benchmarks/"
        location "../../Generated/Benchmarks/"

        objdir '!../../Generated/obj/Debug/'

        files {"benchmarks.cpp"}

        filter "configurations:Debug"
            defines { "DEBUG" }
            symbols "On"

        filter "configurations:Release"
            defines { "NDEBUG" }
            optimize "On"

        configuration { "linux", "gmake" }
            optimize "Debug"
            buildoptions {"-std=c++2a"}
            linkoptions {"-lstdc++fs -ldl -pg -export-dynamic -lbenchmark"}

        IncludeLibraries("../../")
        addStaticLibraries("../../")
        -- link to the main library
        libdirs {"build/"}
        links("CoreVulkanEngine")

        includedirs {"../Engine/"}
