//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
/**                                                                                     *
 * @file benchmarks.cpp
 * @author Camilo Talero
 * @brief
 * @version 0.1
 * @date 2020-06-15
 *
 * @copyright Copyright (c) 2019
 *
 *                                                                                      */
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
#include "benchmark/benchmark.h"

/** @cond */
#include <memory>
#include <vector>

#include "../examples/Example1/example1.hpp"
/** @endcond */

#include "MasterEngine/Geometry/GeometryUtils.hpp"
#include "MasterEngine/MasterEngine.hpp"

using namespace Eigen;

class EngineFixture : public benchmark::Fixture
{
public:
    std::unique_ptr<MasterEngine> engine;

    void SetUp(const ::benchmark::State& state)
    {
    }

    void TearDown(const ::benchmark::State& state)
    {
    }
};

static void BM_EigenRowMajorIteration(benchmark::State& state)
{
    RowMatrix3f vectors(1'000'000, 3);
    for (auto _ : state)
      {
        for(uint i=0; i < 1'000'000; i++)
        {
            vectors.row(i) << 1,2,3;
        }
    }
}
// Register the function as a benchmark
BENCHMARK(BM_EigenRowMajorIteration);

static void BM_EigenColumnMajorIteration(benchmark::State& state)
{
    Matrix3Xf vectors(3, 1'000'000);
    for (auto _ : state)
      {
        for(uint i=0; i < 1'000'000; i++)
        {
            vectors.col(i) << 1,2,3;
        }
    }
}
// Register the function as a benchmark
BENCHMARK(BM_EigenColumnMajorIteration);

// Define another benchmark
static void BM_StlVectorIteration(benchmark::State& state)
{
    std::vector<Eigen::Vector3f> vectors(1'000'000);
    for (auto _ : state)
    {
        for(uint i=0; i < 1'000'000; i++)
        {
            vectors[i] = {1,2,3};
        }
    }
}
BENCHMARK(BM_StlVectorIteration);

static void BM_RotationIterationInterpolation(benchmark::State& state)
{
    uint divisions = 1000;
    Eigen::Vector3f axis = {1,0,0};
    Eigen::Vector3f start = {0,1,0};
    for (auto _ : state)
    {
        // 0th rotation quaternion
        Eigen::Quaternionf rotation_start = {1, 0, 0, 0};
        // 180 degree rotation around the axis
        float angle_sin = sin(M_PI / 2.f);
        Eigen::Quaternionf rotation_end =
            {cos(M_PI / 2.f), axis.x() * angle_sin, axis.y() * angle_sin, axis.z() * angle_sin};

        RowMatrix3f points(divisions, 3);
        for(uint i=0; i < divisions; i++)
        {
            float u = float(i) / float(divisions - 1);
            Eigen::Quaternionf rotation =
                Eigen::Quaternionf((1.f - u) * rotation_start.coeffs()
                + (u * rotation_end.coeffs())).normalized();

            points.row(i) = rotation * start;
        }
    }
}
BENCHMARK(BM_RotationIterationInterpolation);

static void BM_RotationIterationConstruction(benchmark::State& state)
{
    uint divisions = 1000;
    Eigen::Vector3f axis = {1,0,0};
    Eigen::Vector3f start = {0,1,0};
    for (auto _ : state)
    {

        RowMatrix3f points(divisions, 3);
        for(uint i=0; i < divisions; i++)
        {
            float u = float(i) * M_PI / float(divisions - 1);
            float angle_sin = sin(u / 2.f);
            Eigen::Quaternionf rotation =
                {(float)cos(u / 2.f),
                 axis.x() * angle_sin,
                 axis.y() * angle_sin,
                 axis.z() * angle_sin};

            points.row(i) = rotation * start;
        }
    }
}
BENCHMARK(BM_RotationIterationConstruction);

static void BM_HemiCircleCreation(benchmark::State& state)
{
    uint divisions = 1000;
    Eigen::Vector3f axis = {1,0,0};
    Eigen::Vector3f start = {0,1,0};
    for (auto _ : state)
    {
        Generate3DHemiCircle(1, axis, start, divisions);
    }
}
BENCHMARK(BM_HemiCircleCreation);

// Triangle tests ------------------------------------------------------------------------
namespace {
// Reference functions
inline bool SameSide(const Eigen::Vector3d& p1, const Eigen::Vector3d&p2,
    const Eigen::Vector3d&a, const Eigen::Vector3d& b)
{
    auto cp1 = (b - a).cross(p1 - a);
    auto cp2 = (b - a).cross(p2 - a);
    if (cp1.dot(cp2) >= 0) return true;
    else return false;
}

bool PointInTriangleSide(
    const std::vector<Eigen::Vector3d>& triangle,
    const Eigen::Vector3d& p)
{
    if (SameSide(p, triangle[0], triangle[1], triangle[2]) &&
        SameSide(p, triangle[1], triangle[0], triangle[2]) &&
        SameSide(p, triangle[2], triangle[0], triangle[1]))
        return true;
    else return false;
}

bool PointInTriangleNaive(
    const std::vector<Eigen::Vector3d>& triangle,
    const Eigen::Vector3d& point)
{
    double angle = 0;
    std::array<Eigen::Vector3d, 3> dirs;
    for(uint i=0; i < 3; i++)
        dirs[i] = (triangle[i] - point).normalized();

    for(uint i=0; i < 3; i++)
        angle += acos(dirs[i].dot(dirs[(i + 1) % 3]));

    return abs(angle - 2 * M_PI) < 0.000001;
}
} // namespace

static void BM_TriangleTestSide(benchmark::State& state)
{
    for (auto _ : state)
        PointInTriangleSide({{-1,0,0}, {1,0,0}, {0,1,0}}, {0,0,0});
}
BENCHMARK(BM_TriangleTestSide);

static void BM_TriangleTestNaive(benchmark::State& state)
{
    for (auto _ : state)
        PointInTriangleNaive({{-1,0,0}, {1,0,0}, {0,1,0}}, {0,0,0});
}
BENCHMARK(BM_TriangleTestNaive);

static void BM_TriangleTestImplemented(benchmark::State& state)
{
    for (auto _ : state)
        TestPointInTriangle({{-1,0,0}, {1,0,0}, {0,1,0}}, {0,0,0});
}
BENCHMARK(BM_TriangleTestImplemented);

BENCHMARK_MAIN();

