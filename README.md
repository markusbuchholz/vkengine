## Vulkan Engine Introduction

Vulkan Engine is a low level rendering engine designed with the purpose of making
the task of testing rendering techniques easier. It tries to provide a healthy balance of
abstraction and exposure, to make it easier to use existing functionality without much
work, but also being versatile enough to be easily extensible.

## Motivation

Why another rendering engine? Unreal and Unity already cover a large amount of the use
cases for game development. However, I find them unsuitable for graphics development.
Both tools are big, heavy and bury the graphics APIs under layers and layers of
abstractions. Both tools are also very old, Unreal now has 20 years and Unity has 15.
They are both oriented towards non-programmers, with the goal of helping game designers
achieve their goals as simply as possible.

However, this makes them very hard to modify from a programming perspective. Unreal alone
is 2 million lines of code, without adding application logic. Customizing, or worse,
replacing their rendering pipeline is something only the most seasoned developers have
a shot at doing. The tools simply have not been built with the idea of being easily
manipulated by the programmer.
> Note: I am talking about the graphics pipeline specifically. Unreal has excellent
> customisability for game logic.

A simple example to prove my point. Go and try to roll your own GPU driven cloth
simulation in either tool. You will probably find that rolling your own from scratch
is probably easier.

Vulkan Engine aims at alleviating the problem. It tries to be small and to expose the
Vulkan API as much as possible. It does not provide a visual material system, nor a
does it try to be GUI centric. Its goal is not to provide abstractions, but rather
an expressive system to make it as easy as possible to make those abstractions yourself.
However, I will also provide plugins to hook into the engine, to provide functionality
for common use cases.

Useful docs:
- [Vulkan information](Docs/VulkanInfo.md)
- [Half edge data structure](Docs/HalfEdge.md)
