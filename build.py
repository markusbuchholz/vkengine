# Author: Camilo Talero

import glob
import os
import subprocess
import re
import argparse
import shutil
import shlex
import sys

def create_parser():
    parser = argparse.ArgumentParser(description='Build project')

    parser.add_argument('--clean_logs', action='store_true',
                        help='If true, old debug logs will be deleted.')

    parser.add_argument('--run', action='store_true',
                        help="If true, executable will run after compilation.")

    parser.add_argument('--executable', default="VulkanEngine",
                        help="Defines the name of the executable to run.")

    parser.add_argument('--benchmark', action='store_true',
                        help="If true, benchmarks will run after compilation.")

    parser.add_argument('--test', action='store_true',
                        help="If true, tests will run after compilation.")

    parser.add_argument('--clean_all', action='store_true',
                        help="If true, all generated files will be deleted and the"
                        " directory will be reset to a pristine condition.")

    parser.add_argument('--print_command', default="no",
                        help="Specifies whether to print the assembled gcc command."
                        " Values are 'no', 'run', 'dry-run'. 'no' is default")

    parser.add_argument('--args', default="",
                        help="Arguments to pass to the executable.")

    parser.add_argument('--debug', action='store_true',
                        help="If present, gdb will be used to call the executable.")

    return parser.parse_args()


def FormatGccCommad(command):
    tokens = command.split()
    i = next(i for i, val in enumerate(tokens) if val == "g++")

    # Cut sections of the command not related to gcc compilation
    tokens = tokens[i:-5]
    for token in tokens:
        print(token)


def main():
    parser = create_parser()

    # Fully clean the workspace, put it in pristine condition
    if parser.clean_all:
        print("==== Cleaning Workspace ====\n")
        subprocess.run(["rm", "-rf", "Generated"])
        subprocess.run(["rm", "-rf", "build"])
        subprocess.run(["rm", "-rf", "Documentation"])
        subprocess.run(["rm", "-rf", "logs"])
        return

    # Just remove the debug logs
    if parser.clean_logs:
        subprocess.run(["rm", "-rf", "logs"])

    # TODO(low): make all of these options parameters
    print('\n==== Preprocessing ====\n')
    subprocess.check_output(["python3", "Scripts/shader_parser.py"])
    # Export module paths
    os.environ['PREMAKE_PATH'] = "'./Src/*'"

    # Define if and how printing the assembled gcc command will be handled
    if parser.print_command == "no":
        print_action = ""
    elif parser.print_command == "run":
        print_action = "-n"
    elif parser.print_command == "dry-run":
        print_action = "--dry-run"
    elif parser.print_command == "print-only":
        print_action = "--print-only"

    command = f"make -C  ./Generated {print_action}"
    # 'no' means actually building the executable
    if parser.print_command == "no":
        command = "make -C ./Generated CC='ccache g++' -j4  config=debug"
    process = subprocess.Popen(shlex.split(command),shell=False,stdout=subprocess.PIPE)

    command_output = ""
    while True:
        if  process.poll() is not None:
            break
        output = process.stdout.readline()
        if output:
            if parser.print_command == "no":
                print(output.strip().decode())
            command_output += output.decode()

    if process.returncode is None or process.returncode == 2:
        raise Exception("Compilation failed.")

    # No executable will be generated
    if parser.print_command != "no":
        FormatGccCommad(command_output)
        return

    if parser.run:
        # Run the executable
        print('[Running...]\n')
        #ASAN_OPTIONS=detect_leaks=1
        command_list = [f"./{parser.executable}", parser.args]
        if parser.debug:
            command_list = ["gdb"] + command_list
        subprocess.run(command_list, cwd="build/")

    if parser.benchmark:
        # Run the executable
        print('[Benchmarks...]\n')
        #ASAN_OPTIONS=detect_leaks=1
        subprocess.run("./benchmarks/benchmark", cwd="build/")

    if parser.test:
        # Run the executable
        print('[Tests...]\n')
        #ASAN_OPTIONS=detect_leaks=1
        subprocess.run("./tests/test", cwd="build/")


if __name__ == "__main__":
    main()