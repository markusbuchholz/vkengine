Vulkan Engine uses comments as a way to configure pipeline execution. A configuration
block comment looks like this:

```cpp
/**
 * Input Description:
 *
 * @depth_test: true
 * @topology: line list
 * @line_width: <float value>
 * @polygon_mode: fill
*/
```

The `Input Description:` header is necesary and it is case sensitive. Also, there can only be one such comment per shader group and it must be put on the vertex shader of that group.

The valid values for `@topology:` are:

|string value      |  equivalent vulkan hpp enumerator           |
|:---------------: | :-----------------------------------------: |
|"line list"       |  vk::PrimitiveTopology::eLineList           |
|"line strip"      |  vk::PrimitiveTopology::eLineStrip          |
|"path list"       |  vk::PrimitiveTopology::ePatchList          |
|"point list"      |  vk::PrimitiveTopology::ePointList          |
|"triangle fan"    |  vk::PrimitiveTopology::eTriangleFan        |
|"triangle list"   |  vk::PrimitiveTopology::eTriangleList       |
|"triangle strip"  |  vk::PrimitiveTopology::eTriangleStrip      |

The valid values for `@polygon_mode:` are:

|string value        |  equivalent vulkan hpp enumerator         |
|:-----------------: | :---------------------------------------: |
|"fill"              |  vk::PolygonMode::eFill                   |
|"line"              |  vk::PolygonMode::eLine                   |
|"point"             |  vk::PolygonMode::ePoint                  |
|"fill rectangle nv" |  vk::PolygonMode::eFillRectangleNV        |
