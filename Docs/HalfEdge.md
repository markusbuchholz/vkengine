# Half Edge data structure

The half edge data structure is a versatile structure that allows for dynamic bounded access to mesh elements such as faces, vertices and edges.

## Definition

The half edge is usually composed of 3 objects with their respective fields:

* Vertex:
    - Position
    - Normal
    - UV
    - Pointer to half edge going out from this vertex
    - (optional fields like an ID)

* Edge:
    - Pointer to vertex
    - Pointer to pair half edge
    - Pointer to face
    - Pointer to next half edge in the face
    - (Optional) Pointer to previous half edge in the face
    - (Optional fields like a weight or ID)

* Face:
    - Pointer to any half edge in the current face
    - (Optional fields like face normal)

# Construction

A possible way to construct the HE from a triangle list is to do the following:

* Create 3 maps
    - Vertex map where the `key` is identifier (a pointer or an index) and the `value` is a vertex object.
    - Edge map where the `key` is a vertex identifier pair (again, either pointer or index) and the `value` is a half edge object.
    - Face map where the `key` is a triple of vertex ids and the `value` is a face object.

* Initialize a list of vertex objects from the triangle list
* Iterate over every triple of vertices (a triangle) in the triangle list and for each face:
    - Create the 3 half edges of the current face and assert their pairs (if they exist in the
        map already, assign them, otherwise, add them to the map and then assign).
    - Join the in face half edges in the winding order (set next and prev pointers) and assign them their vertices.
    - Create a new face and assign it to each of the edges, and conversely assign one edge to it.
    - Copy the data into a linear container (e.g `std::vector`) because maintaining the maps isn't necessary and is inconvenient to keep the maps up to date.

## Common Operations

In the following diagrams, blue stands for modified elements. Red for new elements.

### Edge Splitting

Edge Splitting takes a pair of half edges and splites them into 4. Consider the following target edge for splitting (in blue):

![](edge_splitting/edge_split_1-1_small.png)

If we just add a new point and half edges in between \f$v_0\f$ and \f$v_1\f$ and do no further job we will end up with an invalid configuration
(as we assume only triangle faces are valid).

Thus we need to create 2 new faces and a new pair of half edges and set all pointers according to the following diagram:

![](edge_splitting/edge_split_2-1_small.png)

### Edge Flipping

Edge flipping consists in taking a pair of triangular faces that share a common edge and changing said edge to
connect the 2 vertices that were not originally connected.

In other words, given the target edge \f$v_0, v_1\f$ (in blue):

![](edge_flipping/edge_flip_1-1_small.png)

We will modify the connectivity information such that the edge now connects \f$v_2, v_3\f$ instead:

![](edge_flipping/edge_flip_2-1_small.png)

Remember than in the above examples all non black elements represent elements that have changed.
The vertices, half edges and faces with color must all have their pointers (e.g nex half edge pointer, or face half edge pointer) modified to match the diagrams.

### Face Splitting

![](face_splitting/face_split_1.svg)

![](face_splitting/face_split_2.svg)
