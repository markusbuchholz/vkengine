# Vulkan Concepts

## Initialization

Initialization of Vulkan is a verbose process. The program must manually query all available devices, select one among them based on any possible criteria, then
use that device to create all subsequent objects.

After this has been done, assuming the application is a graphics application, a surface must be created, however vulkan surfaces must be created externally (e.g by GLFW) and then handed to the program.

## Glossary
> For more info read the [official spec](https://vulkan.lunarg.com/doc/sdk/1.2.131.2/windows/chunked_spec/index.html)

* `Attachments`: A framebuffer has one or more image attachments, which represent a region within that framebuffer that data is rendered to. Some examples are one ore more color attachments or a depth attachment. They are created through the use of attachment descriptions, which describe the intended use, color format, sampling method...

* `DescriptorSets`: Descriptor sets are descriptions of the objects the shaders recieve.
Uniform buffers, samplers, storage images, ssbo's... All need to be configured with a respective
descriptor set. Before binding a pipeline to use, we bind descriptor sets in a command buffer
to the GPU. In this descriptor we put information like, which `vk::Image` will actually be
used by the sampler at binding 1. Or for example in compute shaders, will will specify which
actual `vk::Buffer` handle holds the memory that will be written to by the compute command
by binding it to the the binding point specified in the shader.

* `RenderPass`: A description of how resources are used over the lifetime of a single render pass (i.e the lifetime from clearing the attachments to rendering to them and terminaing the pass). It contains one or more subpasses which specify a single rendering (i.e call to draw) to the specified attachments.

* `SubPass`: A subpass represents a phase of rendering that reads and writes a subset of the attachments in a render pass. Rendering commands are recorded into a particular subpass of a render pass instance.